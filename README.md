##资产统计平台

    是用python编写的管理机房设备的平台,前端框架使用easyui,由于是后台管理建议浏览器使用firefox、chrome，不能很好的支持IE

##演示
地址：[http://demo.zc.17fengmi.com/](http://demo.zc.17fengmi.com/)
```javascript
用户名：admin
密码:   123456
```

	
##软件环境
    1) python2.7
	2) redis 2.6+
    3) uliweb 0.2.6
    4) sqlalchemy 0.9.3
    5) IPy 0.81
    6) plugs 0.1.5
    7) xlwt 0.7.5
	8) MySQL-python
	9) python的redis模块 redis 2.9.1
	10) simplejson 3.3.3

##安装
    * 安装软件环境需要软件
    * 导入doc下的sql文件
    * 访问 http://ip
    * 默认用户名和密码 admin 123456

##运行
    # uliweb runserver -p 80 -h 0.0.0.0

##截图
* 登录
![login](http://git.oschina.net/devop/honeybee/raw/master/doc/screenshots/login.png)

* 主界面
![main](http://git.oschina.net/devop/honeybee/raw/master/doc/screenshots/main.png)

* 机房
![idc](http://git.oschina.net/devop/honeybee/raw/master/doc/screenshots/idc.png)

##有问题反馈
在使用中有任何问题，欢迎反馈给我，可以用以下联系方式跟我交流

* 邮件(sftang#17fengmi.com, 把#换成@)
* QQ: 249545020

##授权
    BSD license

##捐助开发者
在兴趣的驱动下,写一个`免费`的东西，有欣喜，也还有汗水，希望你喜欢我的作品，同时也能支持一下。
当然，有钱捧个钱场（右上角的爱心标志，支持支付宝和PayPal捐助），没钱捧个人场，谢谢各位。

##关于作者

```javascript
  var 17fengmi = {
    nickName  : "炎舞皇",
    site : "http://17fengmi.com"
  }
```