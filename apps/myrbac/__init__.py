#-*- encoding: utf-8 -*-

from uliweb.orm import get_model
from uliweb import request,error

def superuser(user):
    """
    检查用户是否是超级管理员
    返回:
        超级管理员   True
        普通用户     False
    """
    return user and user.is_superuser
    
def has_role():
    """
    验证用户是否有权限
    参数：
        user  登录用户id
        url   访问的url地址  /test/index
    """
    #登录用户
    user = request.user
    #完整的url路径
    url = request.url
    #访问的url不带域名
    path = request.path
    #print "path===>",path
    #检测是否是超级管理员，超级管理员不检查权限
    if user.is_superuser:
        pass
    else:
        #记录角色中是否有这个用户，默认没有
        nouser = True
        
        Role = get_model('role')
        roles = Role.all()
        Permission = get_model('permission')
        #得到url
        try:
            #检查url是否在权限表中有设置
            permission = list(Permission.filter(Permission.c.url == path))[0]
        except:
            error("管理员没有设置访问权限")
        
        for role in roles:
            #检查用户是否在权限列表
            if role.users.has(user):
                #检查用户有访问的url权限
                if role.permissions.has(permission.id):
                    pass
                    nouser = False
                break
    
        if nouser:
            error("你没有权限访问")

    return