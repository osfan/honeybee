# -*- coding:utf-8 -*- #

CernterServerConfig = {}

CernterServerConfig['SERVER_INDEX']=0
CernterServerConfig['SERVER_PORT']=25000
CernterServerConfig['DEBUG'] =  0    #信息输出
CernterServerConfig['CMLOG'] = 'logs/server.log' #'log.lgo' #'E:\\siteroot\\pydev\\fcgi.log'

#MYSQL
CernterServerConfig['DB_SERVER'] = "localhost"
CernterServerConfig['USER'] = 'root'
CernterServerConfig['PASSWORD'] = '123456'
CernterServerConfig['DB'] = "rubberband"
CernterServerConfig['DB_DEBUG'] = False

#一些结构性设置
CernterServerConfig['suggestThreadPoolSize'] = 1000
CernterServerConfig['AdminIpList'] = ['127.0.0.1'] #管理员的ip列表
#for i in xrange(254):
    #CernterServerConfig['AdminIpList'].append( '172.17.0.' + str(i) )
    #CernterServerConfig['AdminIpList'].append( '172.17.1.' + str(i) )
CernterServerConfig['refuseIpList'] = ['127.0.0.2'] #拒绝的ip列表
CernterServerConfig['checkAfterConnect'] = 0.2 #连接后检查是否登陆的时间间隔,每ip此时间内仅允许一 个等待登陆连接