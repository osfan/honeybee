#coding=utf-8
from uliweb.orm import *
import datetime

    
class NetworkBusiness(Model):
    """
    运营商信息
    """ 

    networkbusiness_name = Field(str, verbose_name='运营商名称',max_length=20, required=True, unique=True, index=True)

    
class ComputerRoom(Model):
    """
    # required 指明字段值是否不能为None
    # unique 指示在数据库中，本字段是否可以为 NULL
    # verbose_name 用于显示字段的描述信息。一般是用在显示界面上
    
    机房信息
    """
    networkbusiness_id = Reference('networkbusiness', verbose_name='关联运营商ID')
    #areaname = Field(str, verbose_name='地区名字', max_length=50, required=True, index=True)
    cr_name = Field(str, verbose_name='机房名称', max_length=50, unique=True, required=True, index=True)
    cr_level = Field(int, verbose_name='机房等级')
    cr_phone = Field(str, verbose_name='机房电话', max_length=50)
    cr_tech_linkname = Field(str, verbose_name='机房技术联系人', max_length=30)
    cr_tech_phone = Field(str, verbose_name='技术联系电话', max_length=50)
    cr_chat_tools = Field(str, verbose_name='IM/QQ/MSN聊天联系号', max_length=100)
    cr_sales = Field(str, verbose_name='客户经理', max_length=30)
    cr_sales_phone = Field(str, verbose_name='客户经理电话', max_length=50)
    cr_sales_mobile = Field(str, verbose_name='客户经理手机', max_length=50)
    cr_sales_email = Field(str, verbose_name='客户经理邮件地址', max_length=50)
    province_id = Reference('regionprovince', verbose_name='关联省ID')
    city_id = Reference('regioncity', verbose_name='关联市ID')
    area_id = Reference('regionarea', verbose_name='关联区ID')
    cr_address = Field(str, verbose_name='机房地址', max_length=200)
    comment = Field(str, verbose_name='备注', max_length=500)

    
class Rackspace(Model):
    """
    机架信息
    """
    rackspace_name = Field(str, verbose_name='机柜编号', max_length=50, required=True, index=True)
    computerroom_id = Reference('computerroom', verbose_name='关联机房ID')
    comment = Field(str, verbose_name='备注', max_length=500)
    
class IPPool(Model):
    """
    IP地址池信息
    """
    computerroom_id = Reference('computerroom', verbose_name='关联机房ID')
    ippool_prefix = Field(str, verbose_name='IP网段', max_length=128, required=True, index=True)
    ippool_netmask = Field(str, verbose_name='IP掩码', max_length=128)
    ippool_gateway = Field(str, verbose_name='IP网关', max_length=128)
    ippool_len = Field(int, verbose_name='IP数量')
    #ippool_used = Field(int, verbose_name='IP使用数量')
    comment = Field(str, verbose_name='备注', max_length=500)

class IPAddress(Model):
    """
    IP地址分配
    """
    ip_address = Field(str, verbose_name='IP地址', max_length=128)
    ippool_id = Reference('ippool', collection_name='pool', verbose_name='关联IP地址池ID')
    server_id = Reference('server', collection_name='server_ip', verbose_name='关联服务器设备ID')
    switch_id = Reference('switch', collection_name='switch_ip', verbose_name='关联交换机设备ID')
    ip_status = Field(int, verbose_name='IP使用状态 0:未使用,1:使用', default = 0)