#coding=utf-8
from uliweb import expose,json

#可以省略直接引用
from uliweb import request, error, redirect
import datetime     
#from common.components import cmdpopen

@expose('/projectpost')
class PostData(object):

    #def __begin__(self):
        #"""
        #用户验证
        #"""
        #from uliweb import functions
        #return functions.require_login()
   
    
    def __init__(self):
        from uliweb.orm import get_model
        self.rsyncmodel = get_model('rsync')
        self.repomodel = get_model('repo')
        self.repotypemodel = get_model('repotype')
    
    def repo(self):
        """
        仓库配置
        """
        pageIndex= int(request.POST.get('page',0))
        pageSize = int(request.POST.get('rows',10))
        
        if pageIndex == 0:
            pageIndex = 1
            content = 0
        else:
            content = pageSize * (pageIndex - 1)
            
        data = {}
        data['total'] = self.repomodel.all().count()
        data['rows'] = []
        data_info = {'data':self.repomodel.all().order_by(self.repomodel.c.id).limit(pageSize).offset(content)}
        
        for i in data_info['data']:
            
            data['rows'].append({"id":i.id,
                             "repo_name":i.repo_name,
                             "repo_username":i.repo_username,
                             "repo_passwd":i.repo_passwd,
                             "repo_url":i.repo_url,
                             "repotype_name":i.repotype_id.repotype_name
                             })

        #返回json串
        return json(data)

    def repoinfo(self):
        """
        返回所有仓库名称信息，用于combobox
        """

        data = []
        data_info = {'data':self.repomodel.all().order_by(self.repomodel.c.id)}
        
                
        for i in data_info['data']:

            data.append({"id":i.id,
                        "repo_id":i.repo_name
                        })
            
        #返回json串
        return json(data)

    def repotypeinfo(self):
        """
        返回所有仓库类型名称信息，用于combobox
        """

        data = []
        data_info = {'data':self.repotypemodel.all().order_by(self.repotypemodel.c.id)}
        
                
        for i in data_info['data']:

            data.append({"id":i.id,
                        "repotype_id":i.repotype_name
                        })
            
        #返回json串
        return json(data)    
    
    def rsync(self):
        """
        rsync配置
        """
        pageIndex= int(request.POST.get('page',0))
        pageSize = int(request.POST.get('rows',10))
        
        if pageIndex == 0:
            pageIndex = 1
            content = 0
        else:
            content = pageSize * (pageIndex - 1)
            
        data = {}
        data['total'] = self.rsyncmodel.all().count()
        data['rows'] = []
        data_info = {'data':self.rsyncmodel.all().order_by(self.rsyncmodel.c.id).limit(pageSize).offset(content)}
        
        for i in data_info['data']:
            
            if i.awsloadbalancer_id == None:
                lb_name = ""
            else:
                lb_name = i.awsloadbalancer_id.lb_name
                
            data['rows'].append({"id":i.id,
                             "rsync_module":i.rsync_module,
                             "svn_webdir_server":i.svn_webdir_server,
                             "svn_webdir_client":i.svn_webdir_client,
                             "svn_msg":i.svn_msg,
                             "svn_id":i.svn_id.svn_name,
                             "svn_hook":i.svn_hook,
                             "awsloadbalancer_id":lb_name
                             })

        #返回json串
        return json(data)
    
    def rsyncinfo(self):
        """
        返回所有rsync信息，用于combobox
        """

        data = []
        data_info = {'data':self.rsyncmodel.all().order_by(self.rsyncmodel.c.id)}
        
                
        for i in data_info['data']:

            data.append({"id":i.id,
                        "rsync_module":i.rsync_module
                        })
            
        #返回json串
        return json(data)
    