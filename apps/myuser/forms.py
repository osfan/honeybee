#-*- encoding: utf-8 -*- 

from uliweb.form import *
from uliweb.i18n import ugettext as _
    
class LoginForm(Form):
    form_buttons = Submit(value='登录', _class="btn btn-primary")
    #form_title = '欢迎登录'
    
    username = UnicodeField(label='用户名', required=True)
    password = PasswordField(label='密码', required=True)
    rememberme = BooleanField(label='记住')
    next = HiddenField()
    
    def form_validate(self, all_data):
        from uliweb import settings
        from uliweb.orm import get_model
        
        User = get_model('user')
        user = User.get(User.c.username==all_data['username'])
        if not user:
            return {'username': '用户 "%s" 不存在!' % all_data['username']}
        if not user.check_password(all_data['password']):
            return {'password' : '密码错误'}
            
class ChangePasswordForm(Form):
    form_buttons = Submit(value='Save', _class="btn btn-primary")
    form_title = 'Change Password'
    
    oldpassword = PasswordField(label='Old Password', required=True)
    password = PasswordField(label='Password', required=True)
    password1 = PasswordField(label='Password again', required=True)
    action = HiddenField(default='changepassword')

    def form_validate(self, all_data):
        if all_data.password != all_data.password1:
            return {'password1' : 'Password is not right.'}

    def validate_oldpassword(self, data):
        from uliweb import request
        
        if not request.user.check_password(data):
            return 'Password is not right.'