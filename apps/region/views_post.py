#coding=utf-8
from uliweb import expose

#可以省略直接引用
from uliweb import request, error, redirect
import simplejson
from sqlalchemy.sql import and_, or_, not_

import sys
reload(sys)
sys.setdefaultencoding('utf8')

def __begin__():
    """
    用户验证
    """
    from uliweb import functions
    return functions.require_login()

@expose('/regionpost')
class PostData(object):

    def __init__(self):
        from uliweb.orm import get_model
        self.regionprovincemodel = get_model('regionprovince')
        self.regioncitymodel = get_model('regioncity')
        self.regionareamodel = get_model('regionarea')
        self.view_regionmodel = get_model('view_region')
        self.regionmodel = get_model('region')

    #def region(self):
        #"""
        #返回给jquery easyui的datagride

        #所有地区的数据
        #"""
        #pageIndex= int(request.POST.get('page',0))
        #pageSize = int(request.POST.get('rows',20))
        ##排序使用
        #order_by_type = request.POST.get('order','asc')
        #order_by_sort = request.POST.get('sort','id')
        #order_by = order_by_sort + ' ' + order_by_type        

        #filterRules = request.POST.get('filterRules')


        #if pageIndex == 0:
            #pageIndex = 1
            #content = 0
        #else:
            #content = pageSize * (pageIndex - 1)

        #data = {}
        
        #data['rows'] = []
        
        ##print "filterRules==>",filterRules,type(filterRules)
        ##[{"field":"province_name","op":"contains","value":"上海"},{"field":"city_name","op":"contains","value":"徐汇"}]

        #province_name = ""
        #city_name = ""
        #area_name = ""
        #if filterRules != None:
            #filterRules = simplejson.loads(filterRules)

            #for i in filterRules:
                #field = i["field"]
                #value = i["value"]
                #if value != '':
                    #if field == 'province_name':
                        #province_name = value
                    #if field == 'city_name':
                        #city_name = value                    
                    #if field == 'area_name':
                        #area_name = value                       

        #if province_name != "" or city_name != "" or area_name != "":
            #data['total'] = self.view_regionmodel.filter(and_("province_name like '%" + province_name + "%'", \
                                                              #"city_name like '%" + city_name + "%'", \
                                                              #"area_name like '%" + area_name + "%'")).count()
            #data_info = {'data':self.view_regionmodel.filter(and_("province_name like '%" + province_name + "%'", \
                                                              #"city_name like '%" + city_name + "%'", \
                                                              #"area_name like '%" + area_name + "%'")).limit(pageSize).offset(content)}
        #else:
            #data['total'] = self.view_regionmodel.all().count()
            #data_info = {'data':self.view_regionmodel.all().order_by(order_by).limit(pageSize).offset(content)}
            
        ##data_info = {'data':self.view_regionmodel.all().order_by(order_by).limit(pageSize).offset(content)}
        ##data_info = {'data':self.view_regionmodel.all().order_by(self.view_regionmodel.c.id)}

        #for i in data_info['data']:

            #data['rows'].append({"id":i.id,
                                 ##"province_code":i.province_code,
                                 #"province_name":i.province_name,
                                 ##"city_code":i.city_code,
                                 #"city_name":i.city_name,
                                 ##"area_code":i.area_code,
                                 #"area_name":i.area_name
                                 #})
        ##返回json串
        #return json(data)

    def region(self):
        """
        返回给jquery easyui的datagride

        所有区域分布信息
        """
        pageIndex= int(request.POST.get('page',0))
        pageSize = int(request.POST.get('rows',20))
        #排序使用
        order_by_type = request.POST.get('order','asc')
        order_by_sort = request.POST.get('sort','id')
        order_by = order_by_sort + ' ' + order_by_type 
        
        if pageIndex == 0:
            pageIndex = 1
            content = 0
        else:
            content = pageSize * (pageIndex - 1)

        data = {}
        data['total'] = self.regionmodel.all().count()
        data['rows'] = []


        data_info = {'data':self.regionmodel.all().order_by(order_by).limit(pageSize).offset(content)}

        for i in data_info['data']:

            data['rows'].append({"id":i.id,
                                 "region_name":i.region_name
                                 })
        #返回json串
        return json(data)
    
    def regionprovince(self):
        """
        返回给jquery easyui的datagride

        所有区域省份
        """
        pageIndex= int(request.POST.get('page',0))
        pageSize = int(request.POST.get('rows',20))
        #排序使用
        order_by_type = request.POST.get('order','asc')
        order_by_sort = request.POST.get('sort','id')
        order_by = order_by_sort + ' ' + order_by_type 
        
        if pageIndex == 0:
            pageIndex = 1
            content = 0
        else:
            content = pageSize * (pageIndex - 1)

        data = {}
        data['total'] = self.regionprovincemodel.all().count()
        data['rows'] = []


        data_info = {'data':self.regionprovincemodel.all().order_by(order_by).limit(pageSize).offset(content)}

        for i in data_info['data']:

            data['rows'].append({"id":i.id,
                                 "province_code":i.province_code,
                                 "province_name":i.province_name
                                 })
        #返回json串
        return json(data)

    def regioncity(self):
        """
        返回给jquery easyui的datagride

        所有区域市区
        """
        pageIndex= int(request.POST.get('page',0))
        pageSize = int(request.POST.get('rows',20))
        #排序使用
        order_by_type = request.POST.get('order','asc')
        order_by_sort = request.POST.get('sort','id')
        order_by = order_by_sort + ' ' + order_by_type 
        
        if pageIndex == 0:
            pageIndex = 1
            content = 0
        else:
            content = pageSize * (pageIndex - 1)

        data = {}
        data['total'] = self.regioncitymodel.all().count()
        data['rows'] = []


        data_info = {'data':self.regioncitymodel.all().order_by(order_by).limit(pageSize).offset(content)}

        for i in data_info['data']:

            data['rows'].append({"id":i.id,
                                 "city_code":i.city_code,
                                 "city_name":i.city_name,
                                 "province_id":i.province_id.province_name
                                 })
        #返回json串
        return json(data)

    def regionarea(self):
        """
        返回给jquery easyui的datagride

        所有区域地区
        """
        pageIndex= int(request.POST.get('page',0))
        pageSize = int(request.POST.get('rows',20))
        #排序使用
        order_by_type = request.POST.get('order','asc')
        order_by_sort = request.POST.get('sort','id')
        order_by = order_by_sort + ' ' + order_by_type 
        
        if pageIndex == 0:
            pageIndex = 1
            content = 0
        else:
            content = pageSize * (pageIndex - 1)

        data = {}
        data['total'] = self.regionareamodel.all().count()
        data['rows'] = []


        data_info = {'data':self.regionareamodel.all().order_by(order_by).limit(pageSize).offset(content)}

        for i in data_info['data']:

            data['rows'].append({"id":i.id,
                                 "area_code":i.area_code,
                                 "area_name":i.area_name,
                                 "city_id":i.city_id.city_name,
                                 "province_id":i.city_id.province_id.province_name
                                 })
        #返回json串
        return json(data)

####################################################################################################    


    def regionprovinceinfo(self):
        """
        获取省名称，提供给combobox
        """

        data = []
        data_info = {'data':self.regionprovincemodel.all().order_by(self.regionprovincemodel.c.id)}

        for i in data_info['data']:

            data.append({"id":i.id,
                         "province_name":i.province_name
                         })

        #返回json串
        return json(data)

    def regioncityinfo(self):
        """
        获取市名称，提供给combobox
        """

        _id = request.GET.get("id")
        data = []

        #data_info = {'data':functions.get_object(self.regioncitymodel, id,cache=True,use_local=True)}


        data_info = {'data':self.regioncitymodel.filter(self.regioncitymodel.c.province_id == _id)}

        #print "===>",data_info

        for i in data_info['data']:

            data.append({"id":i.id,
                         "city_name":i.city_name
                         })

        #返回json串
        return json(data)    

    def regionareainfo(self):
        """
        获取区名称，提供给combobox
        """

        _id = request.GET.get("id")
        
        data = []
        #data_info = {'data':self.regionareamodel.all().order_by(self.regionareamodel.c.id)}
        data_info = {'data':self.regionareamodel.filter(self.regionareamodel.c.city_id == _id)}

        for i in data_info['data']:

            data.append({"id":i.id,
                         "area_name":i.area_name
                         })

        #返回json串
        return json(data)     
