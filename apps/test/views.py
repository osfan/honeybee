#coding=utf-8
from uliweb import expose

#可以省略直接引用
from uliweb import request, error, redirect,response
from uliweb.orm import get_model,do_,Begin,Commit,Rollback
from sqlalchemy.sql import and_, or_, not_
from sqlalchemy.sql import select, func

#def __begin__():
    #from uliweb import functions
    #functions.require_login()
    #return functions.has_role()
    ##return functions.require_login()

@expose('/test')
class Test(object):

    def __init__(self):
        from uliweb.orm import get_model
        self.hardwarestatusmodel = get_model('hardwarestatus')
        self.hardwaremodel = get_model('hardware')
        self.hardwaretypemodel = get_model('hardwaretype')
        self.servermodel = get_model('server')
        self.switchmodel = get_model('switch')
        self.servercpumodel = get_model('servercpu')
        self.servermemmodel = get_model('servermem')
        self.serverdiskmodel = get_model('serverdisk')
        self.serverpowermodel = get_model('serverpower')
        self.serverethmodel = get_model('serverethernet')
        self.serverethipmodel = get_model('serverethernetip')
        self.ipaddrmodel = get_model('ipaddress')
        self.regionprovincemodel = get_model('regionprovince')
        self.regioncitymodel = get_model('regioncity')
        self.regionareamodel = get_model('regionarea')
        self.view_regionmodel = get_model('view_region')
        self.networkbusinessmodel = get_model('networkbusiness')
        self.computerroommodel = get_model('computerroom')
        self.rackspacemodel = get_model('rackspace')
        self.ippoolmodel = get_model('ippool')
        self.ipaddrmodel = get_model('ipaddress')
        self.menumodel = get_model('menu')
        self.menurolemodel = get_model('menurole')
        self.serversystemmodel = get_model('serversystem')

    def superuser(self,user):
        return user and user.is_superuser

    def notfound(self):
        return {}  


    def index(self): 
     
        data = {}
        ormdata_ethip = self.serverethipmodel.filter(self.serverethipmodel.c.server_id == 135).one()
        
        print "ormdata_ethip===>",ormdata_ethip
        if ormdata_ethip == None:
            data['111'] = '没有'
        else:
            data['111'] = ormdata_ethip.ethernet_ip
            print "ormdata_ethip===>",ormdata_ethip.ethernet_ip


        #返回json串
        return json(data)        
                

