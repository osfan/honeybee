#coding=utf-8
from uliweb import expose

#可以省略直接引用
from uliweb import request, error, redirect
from uliweb.orm import get_model,do_,Begin,Commit,Rollback
from sqlalchemy.sql import and_, or_, not_

def __begin__():
    """
    用户验证 权限验证
    """
    from uliweb import functions
    return functions.require_login()

@expose('/statpost')
class PostData(object):

    def __init__(self):
        from uliweb.orm import get_model
        self.hardwarestatusmodel = get_model('hardwarestatus')
        self.hardwaremodel = get_model('hardware')
        self.hardwaretypemodel = get_model('hardwaretype')
        self.servermodel = get_model('server')
        self.switchmodel = get_model('switch')
        self.servercpumodel = get_model('servercpu')
        self.servermemmodel = get_model('servermem')
        self.serverdiskmodel = get_model('serverdisk')
        self.serverpowermodel = get_model('serverpower')
        self.serverethmodel = get_model('serverethernet')
        self.serverethipmodel = get_model('serverethernetip')
        self.ipaddrmodel = get_model('ipaddress')
        self.regionprovincemodel = get_model('regionprovince')
        self.regioncitymodel = get_model('regioncity')
        self.regionareamodel = get_model('regionarea')
        self.view_regionmodel = get_model('view_region')
        self.networkbusinessmodel = get_model('networkbusiness')
        self.computerroommodel = get_model('computerroom')
        self.rackspacemodel = get_model('rackspace')
        self.ippoolmodel = get_model('ippool')
        self.ipaddrmodel = get_model('ipaddress')

    def sthardware(self):
        """
        设备统计
        """
        data = {}
        data['total'] = ""
        data['rows'] = []
        data['footer'] = []
        
        #用于datagrid动态字段
        data["columns"] = [{"field":"cr_name","title":"机房名称","width":100},
                           {"field":"city_name","title":"地区","width":100},
                           {"field":"networkbusiness_name","title":"运营商","width":100},
                           {"field":"rackspace_num","title":"机柜数量","width":100}
                           ]

        #状态信息
        hd_status = self.hardwarestatusmodel.all()

        for i in hd_status:
            data["columns"].append({"field":"s_hd_status" + str(i.id),
                                    "title":"服务器" + i.hardwarestatus_name,
                                    "width":100
                                    })
            
            data["columns"].append({"field":"sw_hd_status" + str(i.id),
                                    "title":"交换机" + i.hardwarestatus_name,
                                    "width":100
                                    }
                                   )
        #机房信息
        cr_data = self.computerroommodel.all() 

        for c in cr_data:
            
            rackspace_count = self.rackspacemodel.filter(self.rackspacemodel.c.computerroom_id ==c.id).count()

            #存放组合零时信息
            tmp = {}
            for i in hd_status:
                #functions.set_echo(True)
                server_count = self.servermodel.filter(self.servermodel.c.hardwarestatus_id == i.id
                                                  ).filter(self.rackspacemodel.c.computerroom_id == c.id
                                                           ).filter(self.servermodel.c.rackspace_id == self.rackspacemodel.c.id).order_by(
                                                               self.servermodel.c.hardwarestatus_id).count()
                
                
                switch_count = self.switchmodel.filter(self.switchmodel.c.computerroom_id==c.id
                                                       ).filter(self.switchmodel.c.hardwarestatus_id == i.id).count()

                tmp["s_hd_status" + str(i.id)] = server_count
                tmp["sw_hd_status" + str(i.id)] = switch_count
                
            tmp["cr_name"] = c.cr_name
            tmp["city_name"] = c.city_id.city_name
            tmp["networkbusiness_name"] = c.networkbusiness_id.networkbusiness_name
            tmp["rackspace_num"] = rackspace_count
            
            data['rows'].append(tmp)

        tmp = {}
        rackspace_count = self.rackspacemodel.all().count()
        for i in hd_status:
            server_count = self.servermodel.filter(self.servermodel.c.hardwarestatus_id == i.id).count()
            switch_count = self.switchmodel.filter(self.switchmodel.c.hardwarestatus_id == i.id).count()
            tmp["s_hd_status" + str(i.id)] = server_count
            tmp["sw_hd_status" + str(i.id)] = switch_count            

        tmp["cr_name"] = "统计"
        #tmp["city_name"] = c.city_id.city_name
        #tmp["networkbusiness_name"] = c.networkbusiness_id.networkbusiness_name
        tmp["rackspace_num"] = rackspace_count        

        data['footer'].append(tmp)
        data['footer'].append({"cr_name":"服务器总数",
                               "city_name":self.servermodel.all().count()
                               })
        data['footer'].append({"cr_name":"交换机总数",
                                "city_name":self.switchmodel.all().count()
                                })  
        
        return json(data)
