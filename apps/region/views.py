#coding=utf-8
from uliweb import expose

#可以省略直接引用
from uliweb import request, error, redirect
from include import ip

def __begin__():
    """
    用户验证 权限验证
    """
    from uliweb import functions
    functions.require_login()
    return functions.has_role()

@expose('/region')
class Region(object):

    #def __begin__(self):
        #"""
        #用户验证
        #"""
        #from uliweb import functions
        #return functions.require_login()

    def __init__(self):
        from uliweb.orm import get_model
        self.regionmodel = get_model('region')
        self.regionprovincemodel = get_model('regionprovince')
        self.regioncitymodel = get_model('regioncity')
        self.regionareamodel = get_model('regionarea')
        self.view_regionmodel = get_model('view_region')

    def region(self):
        """
        区域分布信息
        """
        return {}    

    def regionadd(self):
        """
        添加区域分布信息
        """
        return {}

    def regionedit(self):
        """
        编辑区域分布信息
        """
        _id = request.GET.get("id")
        return {}

    def regionsave(self):
        """
        保存区域分布信息
        """
        _id = request.POST.get('id')
        region_name = request.POST.get('region_name')

        if _id == None:
            ormdata = self.regionmodel(
                                       region_name=region_name
                                       )
        else:
            ormdata = functions.get_object(self.regionmodel, _id, cache=True, use_local=True)

            ormdata.region_name=region_name

        try:
            ormdata.save()

            return json({"success":200,"msg":"添加成功"})
        except:
            return json({"error":500,"msg":"区域分布已经存在"})

    def regiondel(self):
        """
        删除国区域分布信息
        """
        _id = request.POST.get('id')

        mid = []
        for i in _id.split(','):
            mid.append(int(i))

        #一次删除多个
        self.regionmodel.filter(self.regionmodel.c.id.in_(mid)).remove()

        d = {"status":"200","msg":"删除成功"}
        return json(d)
    
    def regionprovince(self):
        """
        区域省份信息
        """
        return {}    

    def regionprovinceadd(self):
        """
        添加区域省份信息
        """
        return {}

    def regionprovinceedit(self):
        """
        编辑区域省份信息
        """
        _id = request.GET.get("id")
        return {}

    def regionprovincesave(self):
        """
        保存区域省份信息
        """
        _id = request.POST.get('id')
        province_code = request.POST.get('province_code')
        province_name = request.POST.get('province_name')

        if _id == None:
            ormdata = self.regionprovincemodel(province_code=province_code,
                                               province_name = province_name
                                               )
        else:
            ormdata = functions.get_object(self.regionprovincemodel, _id, cache=True, use_local=True)

            ormdata.province_code=province_code
            ormdata.province_name = province_name

        try:
            ormdata.save()

            return json({"success":200,"msg":"添加成功"})
        except:
            return json({"error":500,"msg":"区域省份已经存在"})

    def regionprovincedel(self):
        """
        删除区域省份信息
        """
        _id = request.POST.get('id')

        mid = []
        for i in _id.split(','):
            mid.append(int(i))

        #一次删除多个
        self.regionprovincemodel.filter(self.regionprovincemodel.c.id.in_(mid)).remove()

        d = {"status":"200","msg":"删除成功"}
        return json(d)
    
    
    def regioncity(self):
        """
        区域市区信息
        """
        return {}    

    def regioncityadd(self):
        """
        添加区域市区信息
        """
        return {}

    def regioncityedit(self):
        """
        编辑区域市区信息
        """
        _id = request.GET.get("id")
        
        ormdata = functions.get_object(self.regioncitymodel, _id, cache = True, use_local = True)
        
        return {'province_id':ormdata.province_id}

    def regioncitysave(self):
        """
        保存区域市区信息
        """
        _id = request.POST.get('id')
        city_code = request.POST.get('city_code')
        city_name = request.POST.get('city_name')
        province_id = request.POST.get('province_id')

        if _id == None:
            ormdata = self.regioncitymodel(city_code=city_code,
                                            city_name = city_name,
                                            province_id = province_id
                                               )
        else:
            ormdata = functions.get_object(self.regioncitymodel, _id, cache=True, use_local=True)

            ormdata.city_code=city_code
            ormdata.city_name = city_name
            ormdata.province_id = province_id

        try:
            ormdata.save()

            return json({"success":200,"msg":"添加成功"})
        except:
            return json({"error":500,"msg":"区域市区已经存在"})

    def regioncitydel(self):
        """
        删除区域市区信息
        """
        _id = request.POST.get('id')

        mid = []
        for i in _id.split(','):
            mid.append(int(i))

        #一次删除多个
        self.regioncitymodel.filter(self.regioncitymodel.c.id.in_(mid)).remove()

        d = {"status":"200","msg":"删除成功"}
        return json(d)
    
    
    def regionarea(self):
        """
        区域地区信息
        """
        return {}    

    def regionareaadd(self):
        """
        添加区域地区信息
        """
        return {}

    def regionareaedit(self):
        """
        编辑区域地区信息
        """
        _id = request.GET.get("id")
        
        ormdata = functions.get_object(self.regionareamodel, _id, cache = True, use_local = True)
        
        return {'province_id':ormdata.city_id.province_id,
                'city_id':ormdata.city_id}

    def regionareasave(self):
        """
        保存区域市区信息
        """
        _id = request.POST.get('id')
        area_code = request.POST.get('area_code')
        area_name = request.POST.get('area_name')
        city_id = request.POST.get('city_id')

        if _id == None:
            ormdata = self.regionareamodel(area_code=area_code,
                                            area_name = area_name,
                                            city_id = city_id
                                               )
        else:
            ormdata = functions.get_object(self.regionareamodel, _id, cache=True, use_local=True)

            ormdata.area_code=area_code
            ormdata.area_name = area_name
            ormdata.city_id = city_id

        try:
            ormdata.save()

            return json({"success":200,"msg":"添加成功"})
        except:
            return json({"error":500,"msg":"区域地区已经存在"})

    def regionareadel(self):
        """
        删除区域地区信息
        """
        _id = request.POST.get('id')

        mid = []
        for i in _id.split(','):
            mid.append(int(i))

        #一次删除多个
        self.regionareamodel.filter(self.regionareamodel.c.id.in_(mid)).remove()

        d = {"status":"200","msg":"删除成功"}
        return json(d)