#coding=utf-8
from uliweb import expose,json

from uliweb import request, error, redirect
from uliweb import decorators

def __begin__():
    """
    用户验证
    """
    from uliweb import functions
    return functions.require_login()

@expose('/main')
class Main(object):
    
    def __init__(self):
        from uliweb.orm import get_model
        self.usermodel = get_model('user')
        self.menumodel = get_model('menu')
        self.menurolemodel = get_model('menurole')
    
    #使用构造函数
    @expose('/')
    #@decorators.require_login #验证用户是否登录
    def index(self):
        #userid = request.user

        return {}
        
    def top(self):
        #获取登录用户的id
        userid = request.user
        #获取登录用户的ip
        userip = request.remote_addr
        
        return {'username':list(self.usermodel.filter(self.usermodel.c.id == userid).limit(1).values(self.usermodel.c.username))[0][0],
                'userip':userip
                }
        

    def center(self):
        return {}

    def left(self):
        return {}

    def right(self):
        return {}
    
    def footer(self):
        """
        页尾
        """
        return {}
    
    def home(self):
        return {}
    
    def menutree_abort(self):
        """
        异步加载菜单 无权限(已不用)
        """   

        return error('禁止访问')
        menuid = request.POST.get("id")
        treedata = []
        if menuid == None:
            data = self.menumodel.filter(self.menumodel.c.pid == 0).limit(1)
            for i in data:
                treedata.append({"id":i.id,
                                         "text":i.menuname,
                                         "state":"closed",
                                         "checked":False,
                                         "iconCls":i.menuicon,
                                         "attributes":{"url":i.menuurl}
                                         })
        
        else:
            data = self.menumodel.filter(self.menumodel.c.pid == menuid).order_by(self.menumodel.c.menuseq)

            for i in data:
                #统计下面是否有子菜单
                count_data = self.menumodel.filter(self.menumodel.c.pid == i.id).count()

                if int(count_data) == 0:
                    state = "open"
                else:
                    state = "closed"
                treedata.append({"id":i.id,
                                "text":i.menuname,
                                "state":state,
                                "checked":False,
                                "iconCls":i.menuicon,
                                "attributes":{"url":i.menuurl}
                                })

        return json(treedata)

    def menutree(self):

        """
        权限,异步加载菜单
        """
        
        menuid = request.POST.get("id")
        treedata = []
        if menuid == None:
            data = self.menumodel.filter(self.menumodel.c.pid == 0).limit(1)
            for i in data:
                treedata.append({"id":i.id,
                                         "text":i.menuname,
                                         "state":"closed",
                                         "checked":False,
                                         "iconCls":i.menuicon,
                                         "attributes":{"url":i.menuurl}
                                         })
        
        else:
            if request.user and request.user.is_superuser:
                #超级管理员不做权限验证,为所有权限
                data = self.menumodel.filter(self.menumodel.c.pid == menuid).order_by(self.menumodel.c.menuseq)

                for i in data:
                    #统计下面是否有子菜单
                    count_data = self.menumodel.filter(self.menumodel.c.pid == i.id).count()
    
                    if int(count_data) == 0:
                        state = "open"
                    else:
                        state = "closed"
                    treedata.append({"id":i.id,
                                    "text":i.menuname,
                                    "state":state,
                                    "checked":False,
                                    "iconCls":i.menuicon,
                                    "attributes":{"url":i.menuurl}
                                    })
            else:
                menupath = []
                #获取登录用户
                login_id = request.user
                #获取登录用户权限
                try:
                    roledata = self.menurolemodel.all()
                    for i in roledata:
                        #如果权限中有当前用户存在
                        if i.users.has(login_id):
                            #得到当前用户的菜单id
                            #menu_id = i.menus.ids()
                            for j in i.menus:
                                #menupath.append(j.menupath)
                                menupath.extend(map(lambda x: x , j.menupath.split('-')))
                                if str(j.id) not in menupath:
                                    menupath.append(str(j.id))
                            break
                    
                except IndexError:
                    #用户未设置任何权限
                    treedata.append({"id":'',
                                     "text":'您没有任何权限',
                                     "state":'open',
                                     "checked":False,
                                     "iconCls":'icon-cancel',
                                     "attributes":{"url":''}
                                     })
                    return json(treedata)
                
                
                menupath = set(menupath)
                #print "menupath===>",menupath
                data = self.menumodel.filter(self.menumodel.c.pid == menuid).order_by(self.menumodel.c.menuseq)
    
                for i in data:
                    if str(i.id) in menupath:
                        #统计下面是否有子菜单
                        count_data = self.menumodel.filter(self.menumodel.c.pid == i.id).count()
        
                        if int(count_data) == 0:
                            state = "open"
                        else:
                            state = "closed"
                        treedata.append({"id":i.id,
                                        "text":i.menuname,
                                        "state":state,
                                        "checked":False,
                                        "iconCls":i.menuicon,
                                        "attributes":{"url":i.menuurl}
                                        })
        if len(treedata) == 0:
            treedata.append({"id":'',
                             "text":'您没有任何权限',
                             "state":'open',
                             "checked":False,
                             "iconCls":'icon-cancel',
                             "attributes":{"url":''}
                             })
        return json(treedata)
    