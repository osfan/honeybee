#coding=utf-8
from uliweb.orm import *
import datetime

class Department(Model):
    """
    部门信息
    """
    department_name = Field(str, verbose_name='部门名称', max_length=100, required=True, index=True, unique=True )
    comment = Field(str, verbose_name='备注', max_length=100)

class OpersGroup(Model):
    """
    运营组信息
    """
    opersgroup_name = Field(str, verbose_name='运营组名称', max_length=100, required=True, index=True, unique=True )
    department_id = Reference('department', verbose_name='关联部门信息ID')
    comment = Field(str, verbose_name='备注', max_length=100)
    
class OpersGroupProject(Model):
    """
    运营组项目信息
    """
    ogproject_name = Field(str, verbose_name='运营组项目名称', max_length=100, required=True, index=True, unique=True)
    opersgroup_id = Reference('opersgroup', verbose_name='关联运营组信息ID')
    ogproject_user = Reference('user', verbose_name='项目负责人 关联用户信息ID')
    ogproject_opuser = Reference('user', verbose_name='运维负责人 关联用户信息ID')
    comment = Field(str, verbose_name='备注', max_length=100)
    
class ApplicationType(Model):
    """
    应用主题表
    """ 

    applicationtype_name = Field(str, verbose_name='应用名称',max_length=50,required=True )
    opersgroupproject_id = Reference('opersgroupproject', verbose_name='关联运营组项目信息ID')
    applicationtype_dep_env = Field(str, verbose_name='部署要求',max_length=200)
    applicationtype_dep_doc = Field(TEXT, verbose_name='部署文档')
    comment = Field(str, verbose_name='备注', max_length=100)
    
    @classmethod
    def OnInit(cls):
        #建立联合索引
        Index('ix_name_projectid', cls.c.applicationtype_name, cls.c.opersgroupproject_id,unique=True)     
    


    
    
    
