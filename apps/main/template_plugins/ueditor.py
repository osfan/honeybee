#coding=utf-8


#def call(app, var, env, ajaxForm=False, hoverIntent=False, spin=False):
	#a = []
	#a.append('css/bootstrap.min.css')
	#a.append('css/jquery-ui-1.10.0.custom.css')
	
	#if spin:
		#a.append('jqutils/spin.min.js')
	#a.append('jqutils/jqrselect.js')
	#a.append('jqutils/jqutils.js')
	#if ajaxForm:
		#a.append('jqutils/jquery.form.js')
	#if hoverIntent:
		#a.append('jqutils/jquery.hoverIntent.minified.js')
	#return {'toplinks':a, 'depends':[('jquery', {'ui':True})]}
	
def call(app, var, env,version='1.3.6'):
	a = []

	ueditor = 'ueditor-' + version

	#加载ueditor
	a.append(ueditor + '/ueditor.config.js')
	a.append(ueditor + '/ueditor.all.min.js')
	a.append(ueditor + '/lang/zh-cn/zh-cn.js')
	a.append(ueditor + '/ueditor.parse.min.js')
	
	return {'toplinks':a}