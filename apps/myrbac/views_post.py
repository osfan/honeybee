#coding=utf-8
from uliweb import expose

#可以省略直接引用
from uliweb import request, error, redirect
from uliweb.form import *
from uliweb.orm import get_model

def __begin__():
    """
    用户验证 权限验证
    """
    from uliweb import functions
    functions.require_login()
    return functions.has_role()

@expose('/myrbacpost')
class PostData(object):

    def __init__(self):
        self.menurolemodel = get_model('menurole')
        self.rolemodel = get_model('role')
        self.permissionmodel = get_model('permission')
        self.resourcetypemodel = get_model('resourcetype')
    
    def __begin__(self):
        if not request.user.is_superuser:
            error('你不是超级用户不能进行这项操作！')    
    
    def menurole(self):
        """
        返回给jquery easyui的datagride

        """        
        pageIndex= int(request.POST.get('page',0))
        pageSize = int(request.POST.get('rows',10))
        
        if pageIndex == 0:
            pageIndex = 1
            content = 0
        else:
            content = pageSize * (pageIndex - 1)
            
        data = {}
        data['total'] = self.menurolemodel.all().count()
        data['rows'] = []
        data_info = {'data':self.menurolemodel.all().order_by(self.menurolemodel.c.id).limit(pageSize).offset(content)}
        
        for i in data_info['data']:
            
            menusname = []
            menusid = []
            usersid = []
            usersname = []
            for j in i.menus:
                menusname.append(j.menuname)
                menusid.append(str(j.id))
                
            for j in i.users:
                usersname.append(j.nickname)
                usersid.append(str(j.id))         
                
            data['rows'].append({"id":i.id,
                             "role_name":i.name,
                             "role_desc":i.description,
                             "menusid":",".join(menusid),
                             "menusname":",".join(menusname),
                             "usersid":",".join(usersid),
                             "usersname":",".join(usersname)                             
                             })

        #返回json串
        return json(data)
    
    def permission(self):
        """
        返回给jquery easyui的datagride

        """        
        data = []

        data_info = {'data':self.permissionmodel.all().order_by(self.permissionmodel.c.id)}
        
        for i in data_info['data']:
            
            data.append({"id":i.id,
                                 "name":i.name,
                                 "url":i.url,
                                 "pid":i.pid,
                                 "_parentId":i.pid,
                                 "resourcetype":i.resourcetype.resourcetype_name,
                                 "description":i.description
                                 })

        #返回json串
        return json(data)
    
    def role(self):
            """
            返回给jquery easyui的datagride
    
            """        
            pageIndex= int(request.POST.get('page',0))
            pageSize = int(request.POST.get('rows',10))
            
            if pageIndex == 0:
                pageIndex = 1
                content = 0
            else:
                content = pageSize * (pageIndex - 1)
                
            data = {}
            data['total'] = self.rolemodel.all().count()
            data['rows'] = []
            data_info = {'data':self.rolemodel.all().order_by(self.rolemodel.c.id).limit(pageSize).offset(content)}
            
            for i in data_info['data']:
                
                permissionsname = []
                permissionsid = []
                usersid = []
                usersname = []
                for j in i.permissions:
                    permissionsname.append(j.name)
                    permissionsid.append(str(j.id))
                    
                for j in i.users:
                    usersname.append(j.nickname)
                    usersid.append(str(j.id))         
                    
                data['rows'].append({"id":i.id,
                                 "name":i.name,
                                 "description":i.description,
                                 "permissionsid":",".join(permissionsid),
                                 "permissionsname":",".join(permissionsname),
                                 "usersid":",".join(usersid),
                                 "usersname":",".join(usersname)                             
                                 })
    
            #返回json串
            return json(data)    
    
######################################################################################################

    def resourcetypeinfo(self):
        """
        获取资源类型信息，提供给combobox
        """
        data = []

        data_info = {'data':self.resourcetypemodel.all()}

        for i in data_info['data']:

            data.append({"id":i.resourcetype_id,
                         "resourcetype_name":i.resourcetype_name
                         })

        #返回json串
        return json(data) 
    
    def permissioninfo(self):
        """
        用于授权的菜单数,树默认全部展开

        """

        treedata = []
        orm_data = self.permissionmodel.all()
        
        for i in orm_data:
    
            treedata.append({"id":i.id,
                        "text":i.name,
                        "pid":i.pid,
                        'parent_id':i.pid
                        })
        
        return json(treedata)