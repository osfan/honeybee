#coding=utf-8
from uliweb import expose,json

#可以省略直接引用
from uliweb import request, error, redirect

        
@expose('/project')
class Project(object):

    #def __begin__(self):
        #"""
        #用户验证
        #"""
        #from uliweb import functions
        #return functions.require_login()
   
    
    def __init__(self):
        from uliweb.orm import get_model
        self.rsyncmodel = get_model('rsync')
        self.repomodel = get_model('repo')
        self.repotypemodel = get_model('repotype')
        
    def _get_repo(self, id):
        repo = self.repomodel.get(int(id))
        if not repo:
            error('没找到这条记录')
        return repo  

    def _get_rsync(self, id):
        rsync = self.rsyncmodel.get(int(id))
        if not rsync:
            error('没找到这条记录')
        return rsync     
    
    def repo(self):
        """
        仓库配置
        """
        return {}
    
    def repoadd(self):
        """
        添加仓库配置
        """
        return {}
    
    def repoedit(self,id):
        """
        编辑仓库配置
        """
        return {}

    def reposave(self):
        """
        保存仓库配置
        """
        _id = request.POST.get('id')
        
        repo_name = request.POST.get('repo_name')
        repo_username = request.POST.get('repo_username')
        repo_passwd = request.POST.get('repo_passwd')
        repo_url = request.POST.get('repo_url')
        repotype_id = request.POST.get('repotype_id')

        if _id == None:
            orm_data = self.repomodel(repo_name=repo_name,
                                        repo_username=repo_username,
                                        repo_passwd=repo_passwd,
                                        repo_url=repo_url,
                                        repotype_id=repotype_id
                                        )
        else:

            orm_data = self._get_repo(_id)
        
            orm_data.repo_name=repo_name
            orm_data.repo_username=repo_username
            orm_data.repo_passwd=repo_passwd
            orm_data.repo_url=repo_url
            orm_data.repotype_id=repotype_id

        try:
            orm_data.save()
            
            return json({"success":200,"msg":"添加成功"})
        except:
            return json({"error":500,"msg":"监控已经存在"})
    
    def repodel(self):
        """
        删除repo配置
        """
        
        _id = request.POST.get('id')
        
        list_id = []
        for i in _id.split(','):
            list_id.append(int(i))
            
        #一次删除多个
        self.repomodel.filter(self.repomodel.c.id.in_(list_id)).remove()
        
        d = {"status":"200","msg":"删除功能"}
        return json(d)    
    
    def rsync(self):
        """
        rsync同步配置
        """
        return {}
    
    def rsyncadd(self):
        """
        添加rsync配置
        """
        return {}
    
    def rsyncedit(self,id):
        """
        编辑rsync配置
        """
        
        
        orm_data = list(self.rsyncmodel.filter(self.rsyncmodel.c.id == id).limit(1))[0]
        
        if orm_data.awsloadbalancer_id == None:
            lb_name = 0
        else:
            lb_name = orm_data.awsloadbalancer_id.id

        return {"repo_id":orm_data.repo_id.id,
                "awsloadbalancer_id":lb_name
                }

    def rsyncsave(self):
        """
        保存rsync配置
        """
        _id = request.POST.get('id')
        
        rsync_module = request.POST.get('rsync_module')
        repo_webdir_server = request.POST.get('repo_webdir_server')
        repo_webdir_client = request.POST.get('repo_webdir_client')
        repo_id = request.POST.get('repo_id')
        isdistribute = request.POST.get('isdistribute')
        awsloadbalancer_id = request.POST.get('awsloadbalancer_id')

        #多选框没有被选中
        if isdistribute != 'on' or awsloadbalancer_id == '0':
            awsloadbalancer_id = None
            repo_hook = request.url_root + 'projectaction/repoupdatehook?repo=' + rsync_module
        else:
            repo_hook = request.url_root + 'projectaction/repoupdatehook?repo=' + rsync_module \
                     + '&id=' + awsloadbalancer_id
        
        if _id == None:
            orm_data = self.rsyncmodel(rsync_module=rsync_module,
                                        repo_webdir_server=repo_webdir_server,
                                        repo_webdir_client=repo_webdir_client,
                                        repo_id=repo_id,
                                        repo_hook = repo_hook,
                                        awsloadbalancer_id = awsloadbalancer_id
                                        )
        else:

            orm_data = self._get_rsync(_id)
        
            orm_data.rsync_module=rsync_module
            orm_data.repo_webdir_server=repo_webdir_server
            orm_data.repo_webdir_client=repo_webdir_client
            orm_data.repo_id=repo_id
            orm_data.repo_hook=repo_hook
            orm_data.awsloadbalancer_id=awsloadbalancer_id

        try:
            orm_data.save()
            
            return json({"success":200,"msg":"添加成功"})
        except:
            return json({"error":500,"msg":"监控已经存在"})
    
    def rsyncdel(self):
        """
        删除rsync配置
        """
        
        _id = request.POST.get('id')
        
        list_id = []
        for i in _id.split(','):
            list_id.append(int(i))
            
        #一次删除多个
        self.rsyncmodel.filter(self.rsyncmodel.c.id.in_(list_id)).remove()
        
        d = {"status":"200","msg":"删除功能"}
        return json(d) 