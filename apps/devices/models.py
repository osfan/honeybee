#coding=utf-8
from uliweb.orm import *
import datetime

    
class HardwareStatus(Model):
    """
    硬件状态信息表
    """ 

    hardwarestatus_name = Field(str, verbose_name='状态名称',max_length=50, required=True, unique=True, index=True)
    hardwarestatus_type = Field(int, verbose_name='状态类型 0:关机 1：开机')
    comment = Field(str, verbose_name='备注', max_length=500)

class HardwareType(Model):
    """
    硬件类型信息表
    """ 

    hardwaretype_name = Field(str, verbose_name='硬件类型名称',max_length=50, required=True, unique=True, index=True)
    hardware_isport = Field(int, verbose_name='是否有端口统计 0:没有 1：有')
    comment = Field(str, verbose_name='备注', max_length=500)

class HardwareWarranty(Model):
    """
    硬件保修状态信息表
    """ 

    hardwarewarranty_name = Field(str, verbose_name='保修状态名称',max_length=50, required=True, unique=True, index=True)
    comment = Field(str, verbose_name='备注', max_length=100)
    
class HardwareManufacturer(Model):
    """
    设备品牌
    """
    hardwaremanufacturer_name = Field(str, verbose_name='设备品牌',max_length=50, required=True, index=True, unique=True)
    
class Hardware(Model):
    """
    硬件表
    """ 

    #hardware_manufacturer = Field(str, verbose_name='设备品牌',max_length=50, required=True, index=True)
    hardwaremanufacturer_id = Reference('hardwaremanufacturer', verbose_name='关联设备品牌ID')
    hardware_name = Field(str, verbose_name='设备型号', max_length=50,index=True, required=True, unique=True)
    hardwaretype_id = Reference('hardwaretype', verbose_name='关联硬件类型ID')
    hardware_port_num = Field(int, verbose_name='如果是交换机，则必须填写端口总数量')
    comment = Field(str, verbose_name='备注', max_length=500)

class Switch(Model):
    """
    交换机表
    """ 

    switch_aliasname = Field(str, verbose_name='交换机别名',max_length=50, required=True, unique=True, index=True)
    hardware_id = Reference('hardware', verbose_name='关联品牌硬件信息表ID')
    computerroom_id = Reference('computerroom', verbose_name='关联机房ID')
    #补机柜关联
    rackspace_id = Reference('rackspace', verbose_name='关联机柜ID')
    switch_asset_tag = Field(str, verbose_name='资产编号',max_length=100, required=True, unique=True)
    switch_sn = Field(str, verbose_name='序列号',max_length=100, required=True, unique=True)
    hardwarestatus_id = Reference('hardwarestatus', verbose_name='关联状态信息表ID')
    comment = Field(str, verbose_name='备注', max_length=500)
    
class SwitchPort(Model):
    """
    交换机端口表
    """ 

    switch_port_num = Field(int, verbose_name='交换机端口号')
    switch_id = Reference('switch',collection_name='switch_port',verbose_name='关联交换机ID')
    server_id = Reference('server',collection_name='server_port',verbose_name='关联服务器ID')
    switch_port_status = Field(int, verbose_name='端口使用状态 0:未使用 1:使用')

class SwitchIP(Model):
    """
    交换机IP表
    """ 

    switch_ip_vlan = Field(int, verbose_name='交换机vlan IP')
    switch_id = Reference('switch',collection_name='s_ip',verbose_name='关联交换机ID')
    switch_ip_addr = Field(str, verbose_name='ip地址', max_length=128)

    
class Server(Model):
    
    """
    服务器信息
    """
    #server_oid = Field(str, verbose_name='oid号,唯一标识号', max_length=50, required=True, index=True, unique=True)
    #computerroom_id = Reference('computerroom', verbose_name='关联机房ID')
    rackspace_id = Reference('rackspace', verbose_name='关联机柜ID')    
    hardware_id = Reference('hardware', verbose_name='关联品牌硬件信息表ID')
    server_sn = Field(str, verbose_name='序列号', max_length=100)
    #server_cpu = Field(str, verbose_name='cpu信息', max_length=200)
    #server_mem = Field(str, verbose_name='内存信息', max_length=200)
    #server_disk = Field(str, verbose_name='硬盘信息', max_length=200)
    server_height = Field(int, verbose_name='设备U数')
    #server_hostname = Field(str, verbose_name='主机名',max_length=200)
    server_asset_tag = Field(str, verbose_name='资产编号',max_length=200,required=True, index=True, unique=True)
    server_cdrom = Field(int, verbose_name='是否包含CDROM 0:没有 1：有')
    server_raidtype = Field(str, verbose_name='raid卡型号',max_length=200)
    #server_ethernet = Field(str, verbose_name='网卡信息',max_length=200)
    server_buydate = Field(datetime.datetime, verbose_name='购买日期')
    #server_power = Field(str, verbose_name='电源信息',max_length=200)
    #server_warranty = Field(int, verbose_name='保修状态 0:过保 1：在保')
    
    hardwarewarranty_id = Reference('hardwarewarranty',collection_name='hd_warranty', verbose_name='关联保修状态信息表ID')
    server_warranty_datetime = Field(datetime.datetime, verbose_name='保修到期日')
    server_price = Field(str, verbose_name='采购价格',max_length=200)
    server_purchasing_director = Field(str, verbose_name='采购负责人',max_length=200)
    server_buy_datetime = Field(datetime.datetime, verbose_name='采购日期')
    hardwarestatus_id = Reference('hardwarestatus', verbose_name='关联状态信息表ID')
    comment = Field(str, verbose_name='备注', max_length=500)
    
    is_virtual = Field(int, verbose_name='是否虚拟机 0:虚拟机 1：物理机')
    parasitifer = SelfReference(collection_name='myserver_id', verbose_name='宿主机ID,关联自己')
    
class ServerCpu(Model):
    """
    服务器CPU信息
    """

    server_id = Reference('server',collection_name='cpu', verbose_name='关联服务器信息表ID',index=True)
    servercpu_type = Field(str, verbose_name='CPU型号',max_length=100)
    servercpu_hz = Field(int, verbose_name='CPU频率')
    servercpu_num = Field(int, verbose_name='CPU数量')

class ServerMem(Model):
    """
    服务器内存信息
    """

    server_id = Reference('server', collection_name='mem',verbose_name='关联服务器信息表ID',index=True)
    servermem_type = Field(str, verbose_name='内存型号',max_length=100)
    servermem_hz = Field(int, verbose_name='内存频率')
    servermem_size = Field(int, verbose_name='内存单条容量')
    servermem_num = Field(int, verbose_name='内存数量')    

class ServerDisk(Model):
    """
    服务器硬盘信息
    """

    server_id = Reference('server', collection_name='disk',verbose_name='关联服务器信息表ID',index=True)
    serverdisk_type = Field(str, verbose_name='硬盘型号',max_length=100)
    #serverdisk_speed = Field(DecimalProperty, verbose_name='硬盘转速',precision=5,scale=2)
    serverdisk_speed = Field(int, verbose_name='硬盘转速')
    serverdisk_size = Field(int, verbose_name='硬盘单盘容量')
    serverdisk_num = Field(int, verbose_name='硬盘数量')      

class ServerPower(Model):
    """
    服务器电源信息
    """

    server_id = Reference('server', collection_name='power', verbose_name='关联服务器信息表ID',index=True)
    serverpower_type = Field(str, verbose_name='电源型号',max_length=100)
    serverpower_kw = Field(int, verbose_name='电源功率')
    serverpower_num = Field(int, verbose_name='电源数量')     
    
class ServerEthernet(Model):
    """
    服务器网卡信息
    """

    server_id = Reference('server', collection_name='eth', verbose_name='关联服务器信息表ID',index=True)
    servereth_type = Field(str, verbose_name='网卡型号',max_length=100)
    servereth_num = Field(int, verbose_name='网卡数量')    
    
class ServerEthernetIP(Model):
    """
    设备接口信息
    """

    server_id = Reference('server',collection_name='eth_ip', verbose_name='关联设备ID',index=True)
    ethernet_type = Field(int, verbose_name='网卡类型 0:虚拟 1：物理')
    ethernet_physical_interface = Field(int, verbose_name='网卡物理接口')
    ethernet_dev_name = Field(str, verbose_name='设备名', max_length=20)
    ethernet_mac = Field(str, verbose_name='MAC地址', max_length=128)
    ethernet_ip = Field(str, verbose_name='ip地址', max_length=128)
    #ippool_id = Reference('ippool', collection_name='server_pool', verbose_name='关联IP地址表ID')
    ethernet_mip = Field(str, verbose_name='映射IP', max_length=128)
    switch_id = Reference('switch',verbose_name='关联交换机ID')
    switchport_id_port = Field(int,verbose_name='连接交换机端口号')
    
class OperatingSystem(Model):
    """
    操作系统字典
    """
    os_name = Field(str, verbose_name='系统名称', max_length=50)
    os_version = Field(str, verbose_name='系统版本', max_length=50)
    os_arch = Field(str, verbose_name='系统架构', max_length=10)
    @classmethod
    def OnInit(cls):
        #建立联合索引
        Index('ix_name_ver_arch', cls.c.os_name, cls.c.os_version,cls.c.os_arch, unique=True)    

class ServerSystem(Model):
    """
    服务器系统属性
    """

    server_id = Reference('server',collection_name='system', verbose_name='关联设备ID',index=True)
    os_id = Reference('operatingsystem',collection_name='os', verbose_name='关联操作系统ID')
    system_hostname = Field(str, verbose_name='主机名',max_length=200)
    system_raid = Field(str, verbose_name='RAID配置信息', max_length=50)
    system_partition = Field(TEXT, verbose_name='系统分区')
    department_id = Reference('department', verbose_name='关联部门ID')
    opersgroup_id = Reference('opersgroup', verbose_name='运营组信息ID')
    opersgroupproject_id = Reference('opersgroupproject', verbose_name='运营组项目信息ID')
    applicationtype_id = Reference('applicationtype', verbose_name='关联应用主题ID')