#!/usr/bin/env python
# -*- coding: UTF-8 -*-


from IPy import IP

def ip_check(ip):
	"""
	检查IP和掩码填写是否合法
	"""
	try:
		data = IP(ip)
		return True
	except:
		return False

def ip_checksame_net(ipaddr,ip_prefix):
	"""
	检查IP是否属于这个IP网段
	"""
	return IP(ipaddr) in IP(ip_prefix)

def ip_list(ip):
	"""
	返回所有IP
	"""
	data = IP(ip)
	
	iplist = []
	for i in data:
		iplist.append(str(i))
		
	return iplist

def ip_prefix(ip):
	"""
	返回IP掩码整形 如 24 32
	"""
	return int(IP(ip).prefixlen())

def ip_netmask(ip):
	"""
	返回子网掩码 如255.255.255.0
	"""	
	return str(IP(ip).netmask())

def ip_len(ip):
	"""
	返回ip个数
	"""
	return int(IP(ip).len())

def ip_ip_prefix(ip):
	"""
	返回IP+整形子网掩码
	"""
	return str(IP(ip).strNormal(1))

def ip_ip_netmask(ip):
	"""
	返回IP+子网掩码
	"""
	return str(IP(ip).strNormal(2))