/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : honeybee

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2014-04-04 13:01:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for applicationtype
-- ----------------------------
DROP TABLE IF EXISTS `applicationtype`;
CREATE TABLE `applicationtype` (
  `applicationtype_name` varchar(50) DEFAULT NULL,
  `opersgroupproject_id` int(11) DEFAULT NULL,
  `applicationtype_dep_env` varchar(200) DEFAULT NULL,
  `applicationtype_dep_doc` text,
  `comment` varchar(100) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_applicationtype_applicationtype_name` (`applicationtype_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of applicationtype
-- ----------------------------

-- ----------------------------
-- Table structure for computerroom
-- ----------------------------
DROP TABLE IF EXISTS `computerroom`;
CREATE TABLE `computerroom` (
  `networkbusiness_id` int(11) DEFAULT NULL,
  `cr_name` varchar(50) DEFAULT NULL,
  `cr_level` int(11) DEFAULT NULL,
  `cr_phone` varchar(50) DEFAULT NULL,
  `cr_tech_linkname` varchar(30) DEFAULT NULL,
  `cr_tech_phone` varchar(50) DEFAULT NULL,
  `cr_chat_tools` varchar(100) DEFAULT NULL,
  `cr_sales` varchar(30) DEFAULT NULL,
  `cr_sales_phone` varchar(50) DEFAULT NULL,
  `cr_sales_mobile` varchar(50) DEFAULT NULL,
  `cr_sales_email` varchar(50) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `cr_address` varchar(200) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_computerroom_cr_name` (`cr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of computerroom
-- ----------------------------

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `department_name` varchar(100) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_department_department_name` (`department_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of department
-- ----------------------------

-- ----------------------------
-- Table structure for hardware
-- ----------------------------
DROP TABLE IF EXISTS `hardware`;
CREATE TABLE `hardware` (
  `hardware_name` varchar(50) DEFAULT NULL,
  `hardwaretype_id` int(11) DEFAULT NULL,
  `hardware_port_num` int(11) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hardwaremanufacturer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hardware_name` (`hardware_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hardware
-- ----------------------------

-- ----------------------------
-- Table structure for hardwaremanufacturer
-- ----------------------------
DROP TABLE IF EXISTS `hardwaremanufacturer`;
CREATE TABLE `hardwaremanufacturer` (
  `hardwaremanufacturer_name` varchar(50) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_hardwaremanufacturer_hardwaremanufacturer_name` (`hardwaremanufacturer_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hardwaremanufacturer
-- ----------------------------

-- ----------------------------
-- Table structure for hardwarestatus
-- ----------------------------
DROP TABLE IF EXISTS `hardwarestatus`;
CREATE TABLE `hardwarestatus` (
  `hardwarestatus_name` varchar(50) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hardwarestatus_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_hardwarestatus_hardwarestatus_name` (`hardwarestatus_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hardwarestatus
-- ----------------------------

-- ----------------------------
-- Table structure for hardwaretype
-- ----------------------------
DROP TABLE IF EXISTS `hardwaretype`;
CREATE TABLE `hardwaretype` (
  `hardwaretype_name` varchar(50) DEFAULT NULL,
  `hardware_isport` int(11) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_hardwaretype_hardwaretype_name` (`hardwaretype_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hardwaretype
-- ----------------------------

-- ----------------------------
-- Table structure for hardwarewarranty
-- ----------------------------
DROP TABLE IF EXISTS `hardwarewarranty`;
CREATE TABLE `hardwarewarranty` (
  `hardwarewarranty_name` varchar(50) DEFAULT NULL,
  `comment` varchar(100) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_hardwarewarranty_hardwarewarranty_name` (`hardwarewarranty_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hardwarewarranty
-- ----------------------------

-- ----------------------------
-- Table structure for ipaddress
-- ----------------------------
DROP TABLE IF EXISTS `ipaddress`;
CREATE TABLE `ipaddress` (
  `ip_address` varchar(128) DEFAULT NULL,
  `ippool_id` int(11) DEFAULT NULL,
  `switch_id` int(11) DEFAULT NULL,
  `server_id` int(11) DEFAULT NULL,
  `ip_status` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ipaddress
-- ----------------------------

-- ----------------------------
-- Table structure for ippool
-- ----------------------------
DROP TABLE IF EXISTS `ippool`;
CREATE TABLE `ippool` (
  `computerroom_id` int(11) DEFAULT NULL,
  `ippool_prefix` varchar(128) DEFAULT NULL,
  `ippool_netmask` varchar(128) DEFAULT NULL,
  `ippool_gateway` varchar(128) DEFAULT NULL,
  `ippool_len` int(11) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `ix_ippool_ippool_prefix` (`ippool_prefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ippool
-- ----------------------------

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `menupath` varchar(100) DEFAULT NULL,
  `menuname` varchar(30) NOT NULL,
  `menuurl` varchar(150) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menuseq` int(11) DEFAULT NULL,
  `menuicon` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_menu_menuname` (`menuname`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('0', '平台管理', '', '0', '1', null, null);
INSERT INTO `menu` VALUES ('0-1', '平台维护', '', '1', '2', '1', '');
INSERT INTO `menu` VALUES ('0-1-2', '菜单管理', '/menu/menu', '2', '3', '1', '');
INSERT INTO `menu` VALUES ('0-1-26', '用户信息', '/user/user', '26', '4', '1', '');
INSERT INTO `menu` VALUES ('0-1', '测试', '', '1', '5', '100', '');
INSERT INTO `menu` VALUES ('0-1-5', '测试1', '/test/index', '5', '6', '1', '');
INSERT INTO `menu` VALUES ('0-1', '机房管理', '', '1', '7', '4', '');
INSERT INTO `menu` VALUES ('0-1-7', '地区信息', '/idc/region', '7', '8', '1', '');
INSERT INTO `menu` VALUES ('0-1-7', '运营商信息', '/idc/networkbusiness', '7', '9', '2', '');
INSERT INTO `menu` VALUES ('0-1-7', '机房信息', '/idc/computerroom', '7', '10', '3', 'icon-server-edit');
INSERT INTO `menu` VALUES ('0-1', '设备管理', '', '1', '11', '5', '');
INSERT INTO `menu` VALUES ('0-1-7', '机柜信息', '/idc/rackspace', '7', '12', '4', '');
INSERT INTO `menu` VALUES ('0-1-11', '服务器设备信息', '/devices/server', '11', '13', '7', '');
INSERT INTO `menu` VALUES ('0-1-11', '硬件状态信息', '/devices/hardwarestatus', '11', '14', '1', '');
INSERT INTO `menu` VALUES ('0-1-20', '应用主题', '/operation/applicationtype', '20', '15', '4', '');
INSERT INTO `menu` VALUES ('0-1-11', '网络设备信息', '/devices/switch', '11', '16', '6', '');
INSERT INTO `menu` VALUES ('0-1-11', '硬件类型', '/devices/hardwaretype', '11', '17', '3', '');
INSERT INTO `menu` VALUES ('0-1-11', '硬件信息', '/devices/hardware', '11', '18', '5', '');
INSERT INTO `menu` VALUES ('0-1-7', 'IP池信息', '/idc/ippool', '7', '19', '5', '');
INSERT INTO `menu` VALUES ('0-1', '业务管理', '', '1', '20', '6', '');
INSERT INTO `menu` VALUES ('0-1-20', '部门信息', '/operation/department', '20', '21', '1', '');
INSERT INTO `menu` VALUES ('0-1-20', '运营组项目信息', '/operation/opersgroupproject', '20', '22', '3', '');
INSERT INTO `menu` VALUES ('0-1-11', '硬件保修状态', '/devices/hardwarewarranty', '11', '23', '2', '');
INSERT INTO `menu` VALUES ('0-1-20', '运营组信息', '/operation/opersgroup', '20', '24', '2', '');
INSERT INTO `menu` VALUES ('0-1', '用户管理', '', '1', '26', '2', '');
INSERT INTO `menu` VALUES ('0-1', '统计报表', '', '1', '27', '10', '');
INSERT INTO `menu` VALUES ('0-1-27', '设备统计', '/stat/sthardware', '27', '28', '1', '');
INSERT INTO `menu` VALUES ('0-1-2', '菜单角色', '/myrbac/menurole', '2', '33', '2', '');
INSERT INTO `menu` VALUES ('0-1-2', '操作权限', '/myrbac/permission', '2', '34', '3', '');
INSERT INTO `menu` VALUES ('0-1-2', '操作角色', '/myrbac/role', '2', '35', '4', '');
INSERT INTO `menu` VALUES ('0-1-11', '设备机柜图', '/devices/devicesmain', '11', '36', '10', '');
INSERT INTO `menu` VALUES ('0-1-11', '操作系统信息', '/devices/operatingsystem', '11', '37', '1', '');
INSERT INTO `menu` VALUES ('0-1-11', '硬件品牌信息', '/devices/hardwaremanufacturer', '11', '38', '4', '');

-- ----------------------------
-- Table structure for menurole
-- ----------------------------
DROP TABLE IF EXISTS `menurole`;
CREATE TABLE `menurole` (
  `name` varchar(80) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `reserve` tinyint(1) DEFAULT NULL,
  `relative_usergroup` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menurole
-- ----------------------------

-- ----------------------------
-- Table structure for menurole_menu_menus
-- ----------------------------
DROP TABLE IF EXISTS `menurole_menu_menus`;
CREATE TABLE `menurole_menu_menus` (
  `menurole_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  UNIQUE KEY `menurole_menu_menus_mindx` (`menurole_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menurole_menu_menus
-- ----------------------------

-- ----------------------------
-- Table structure for menurole_usergroup_usergroups
-- ----------------------------
DROP TABLE IF EXISTS `menurole_usergroup_usergroups`;
CREATE TABLE `menurole_usergroup_usergroups` (
  `menurole_id` int(11) DEFAULT NULL,
  `usergroup_id` int(11) DEFAULT NULL,
  UNIQUE KEY `menurole_usergroup_usergroups_mindx` (`menurole_id`,`usergroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menurole_usergroup_usergroups
-- ----------------------------

-- ----------------------------
-- Table structure for menurole_user_users
-- ----------------------------
DROP TABLE IF EXISTS `menurole_user_users`;
CREATE TABLE `menurole_user_users` (
  `menurole_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  UNIQUE KEY `menurole_user_users_mindx` (`menurole_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menurole_user_users
-- ----------------------------

-- ----------------------------
-- Table structure for networkbusiness
-- ----------------------------
DROP TABLE IF EXISTS `networkbusiness`;
CREATE TABLE `networkbusiness` (
  `networkbusiness_name` varchar(20) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_networkbusiness_networkbusiness_name` (`networkbusiness_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of networkbusiness
-- ----------------------------

-- ----------------------------
-- Table structure for operatingsystem
-- ----------------------------
DROP TABLE IF EXISTS `operatingsystem`;
CREATE TABLE `operatingsystem` (
  `os_name` varchar(50) DEFAULT NULL,
  `os_version` varchar(50) DEFAULT NULL,
  `os_arch` varchar(10) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_name_ver_arch` (`os_name`,`os_version`,`os_arch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of operatingsystem
-- ----------------------------

-- ----------------------------
-- Table structure for opersgroup
-- ----------------------------
DROP TABLE IF EXISTS `opersgroup`;
CREATE TABLE `opersgroup` (
  `opersgroup_name` varchar(100) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `comment` varchar(100) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_opersgroup_opersgroup_name` (`opersgroup_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of opersgroup
-- ----------------------------

-- ----------------------------
-- Table structure for opersgroupproject
-- ----------------------------
DROP TABLE IF EXISTS `opersgroupproject`;
CREATE TABLE `opersgroupproject` (
  `ogproject_name` varchar(100) DEFAULT NULL,
  `opersgroup_id` int(11) DEFAULT NULL,
  `comment` varchar(100) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ogproject_user` int(11) DEFAULT NULL,
  `ogproject_opuser` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_opersgroupproject_ogproject_name` (`ogproject_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of opersgroupproject
-- ----------------------------

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `name` varchar(80) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `props` blob,
  `url` varchar(255) DEFAULT NULL,
  `pid` varchar(10) DEFAULT NULL,
  `resourcetype` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES ('机房管理', '', 0x800255002E, '', '', '1', '1');
INSERT INTO `permission` VALUES ('机房信息', '', 0x800255002E, '/idc/computerroom', '1', '1', '2');
INSERT INTO `permission` VALUES ('添加机房信息', '', 0x800255002E, '/idc/computerroomadd', '1', '2', '3');
INSERT INTO `permission` VALUES ('修改机房信息', null, 0x800255002E, '/idc/computerroomedit', '1', '2', '4');
INSERT INTO `permission` VALUES ('删除机房信息', '', 0x800255002E, '/idc/computerroomdel', '1', '2', '5');
INSERT INTO `permission` VALUES ('机柜管理', '', 0x800255002E, '', '', '1', '8');
INSERT INTO `permission` VALUES ('机柜信息', '', 0x800255002E, '/idc/rackspace', '8', '1', '9');
INSERT INTO `permission` VALUES ('添加机柜信息', '', 0x800255002E, '/idc/rackspaceadd', '8', '2', '10');
INSERT INTO `permission` VALUES ('修改机柜信息', '', 0x800255002E, '/idc/rackspaceedit', '8', '2', '11');
INSERT INTO `permission` VALUES ('删除机柜信息', '', 0x800255002E, '/idc/rackspacedel', '8', '2', '12');
INSERT INTO `permission` VALUES ('保存修改机房信息', '', 0x800255002E, '/idc/computerroomsave', '1', '2', '14');
INSERT INTO `permission` VALUES ('硬件信息管理', '', 0x800255002E, '', '', '1', '15');
INSERT INTO `permission` VALUES ('硬件信息', '', 0x800255002E, '/devices/hardware', '15', '1', '16');
INSERT INTO `permission` VALUES ('添加硬件信息', '', 0x800255002E, '/devices/hardwareadd', '15', '2', '17');
INSERT INTO `permission` VALUES ('删除硬件信息', '', 0x800255002E, '/devices/hardwaredel', '15', '2', '18');
INSERT INTO `permission` VALUES ('修改硬件信息', '', 0x800255002E, '/devices/hardwareedit', '15', '2', '19');
INSERT INTO `permission` VALUES ('保存修改硬件信息', '', 0x800255002E, '/devices/hardwaresave', '15', '2', '20');
INSERT INTO `permission` VALUES ('保存修改机柜信息', '', 0x800255002E, '/idc/rackspacesave', '8', '2', '21');
INSERT INTO `permission` VALUES ('硬件状态管理', '', 0x800255002E, '', '', '1', '22');
INSERT INTO `permission` VALUES ('硬件状态信息', '', 0x800255002E, '/devices/hardwarestatus', '22', '1', '23');
INSERT INTO `permission` VALUES ('添加硬件状态信息', '', 0x800255002E, '/devices/hardwarestatusadd', '22', '2', '24');
INSERT INTO `permission` VALUES ('删除硬件状态信息', '', 0x800255002E, '/devices/hardwarestatusdel', '22', '2', '25');
INSERT INTO `permission` VALUES ('修改硬件状态信息', '', 0x800255002E, '/devices/hardwarestatusedit', '22', '2', '26');
INSERT INTO `permission` VALUES ('保存修改硬件状态信息', '', 0x800255002E, '/devices/hardwarestatussave', '22', '2', '27');
INSERT INTO `permission` VALUES ('硬件类型管理', '', 0x800255002E, '', '', '1', '28');
INSERT INTO `permission` VALUES ('硬件类型信息', '', 0x800255002E, '/devices/hardwaretype', '28', '1', '29');
INSERT INTO `permission` VALUES ('添加硬件类型信息', '', 0x800255002E, '/devices/hardwaretypeadd', '28', '2', '30');
INSERT INTO `permission` VALUES ('删除硬件类型信息', '', 0x800255002E, '/devices/hardwaretypedel', '28', '2', '31');
INSERT INTO `permission` VALUES ('修改硬件类型信息', '', 0x800255002E, '/devices/hardwaretypeedit', '28', '2', '32');
INSERT INTO `permission` VALUES ('保存修改硬件类型信息', '', 0x800255002E, '/devices/hardwaretypesave', '28', '2', '33');
INSERT INTO `permission` VALUES ('硬件保修状态管理', '', 0x800255002E, '', '', '1', '34');
INSERT INTO `permission` VALUES ('硬件保修状态信息', '', 0x800255002E, '/devices/hardwarewarranty', '34', '1', '35');
INSERT INTO `permission` VALUES ('添加硬件保修状态信息', '', 0x800255002E, '/devices/hardwarewarrantyadd', '34', '2', '36');
INSERT INTO `permission` VALUES ('删除硬件保修状态信息', '', 0x800255002E, '/devices/hardwarewarrantydel', '34', '2', '37');
INSERT INTO `permission` VALUES ('修改硬件保修状态信息', '', 0x800255002E, '/devices/hardwarewarrantyedit', '34', '2', '38');
INSERT INTO `permission` VALUES ('保存修改硬件保修状态信息', '', 0x800255002E, '/devices/hardwarewarrantysave', '34', '2', '39');
INSERT INTO `permission` VALUES ('服务器设备信息管理', '', 0x800255002E, '', '', '1', '40');
INSERT INTO `permission` VALUES ('服务器设备信息', '', 0x800255002E, '/devices/server', '40', '1', '41');
INSERT INTO `permission` VALUES ('添加服务器设备信息', '', 0x800255002E, '/devices/serveradd', '40', '2', '42');
INSERT INTO `permission` VALUES ('删除服务器设备信息', '', 0x800255002E, '/devices/serverdel', '40', '2', '43');
INSERT INTO `permission` VALUES ('修改服务器设备信息', '', 0x800255002E, '/devices/serveredit', '40', '2', '44');
INSERT INTO `permission` VALUES ('保存修改服务器信息', '', 0x800255002E, '/devices/serversave', '40', '2', '45');
INSERT INTO `permission` VALUES ('查看服务器设备信息明细', '', 0x800255002E, '/devices/serverdetail', '40', '2', '46');
INSERT INTO `permission` VALUES ('网络设备管理', '', 0x800255002E, '', '', '1', '47');
INSERT INTO `permission` VALUES ('网络设备信息', '', 0x800255002E, '/devices/switch', '47', '1', '48');
INSERT INTO `permission` VALUES ('添加网络设备信息', '', 0x800255002E, '/devices/switchadd', '47', '2', '49');
INSERT INTO `permission` VALUES ('删除网络设备信息', '', 0x800255002E, '/devices/switchdel', '47', '2', '50');
INSERT INTO `permission` VALUES ('保存修改网络设备信息', '', 0x800255002E, '/devices/switchsave', '47', '2', '51');
INSERT INTO `permission` VALUES ('修改网络设备信息', '', 0x800255002E, '/devices/switchedit', '47', '2', '52');
INSERT INTO `permission` VALUES ('搜索服务器设备', '', 0x800255002E, '/devicessearch/server', '40', '2', '53');
INSERT INTO `permission` VALUES ('IP池管理', '', 0x800255002E, '', '', '1', '54');
INSERT INTO `permission` VALUES ('IP池信息', '', 0x800255002E, '/idc/ippool', '54', '1', '55');
INSERT INTO `permission` VALUES ('添加IP池信息', '', 0x800255002E, '/idc/ippooladd', '54', '2', '56');
INSERT INTO `permission` VALUES ('删除IP池信息', '', 0x800255002E, '/idc/ippooldel', '54', '2', '57');
INSERT INTO `permission` VALUES ('修改IP池信息', '', 0x800255002E, '/idc/ippooledit', '54', '2', '58');
INSERT INTO `permission` VALUES ('保存修改IP池信息', '', 0x800255002E, '/idc/ippoolsave', '54', '2', '59');
INSERT INTO `permission` VALUES ('查看IP池信息明细', '', 0x800255002E, '/idc/ippooldetail', '54', '2', '60');
INSERT INTO `permission` VALUES ('运营商管理', '', 0x800255002E, '', '', '1', '61');
INSERT INTO `permission` VALUES ('运营商信息', '', 0x800255002E, '/idc/networkbusiness', '61', '1', '62');
INSERT INTO `permission` VALUES ('添加运营商信息', '', 0x800255002E, '/idc/networkbusinessadd', '61', '2', '63');
INSERT INTO `permission` VALUES ('删除运营商信息', '', 0x800255002E, '/idc/networkbusinessdel', '61', '2', '64');
INSERT INTO `permission` VALUES ('修改运营商信息', '', 0x800255002E, '/idc/networkbusinessedit', '61', '2', '65');
INSERT INTO `permission` VALUES ('保存修改运营商信息', '', 0x800255002E, '/idc/networkbusinesssave', '61', '2', '66');
INSERT INTO `permission` VALUES ('地区管理', '', 0x800255002E, '', '', '1', '67');
INSERT INTO `permission` VALUES ('地区信息', '', 0x800255002E, '/idc/region', '67', '1', '68');
INSERT INTO `permission` VALUES ('添加地区信息', '', 0x800255002E, '/idc/regionadd', '67', '2', '69');
INSERT INTO `permission` VALUES ('删除地区信息', '', 0x800255002E, '/idc/regiondel', '67', '2', '70');
INSERT INTO `permission` VALUES ('修改地区信息', '', 0x800255002E, '/idc/regionedit', '67', '2', '71');
INSERT INTO `permission` VALUES ('保存修改地区信息', '', 0x800255002E, '/idc/regionsave', '67', '2', '72');
INSERT INTO `permission` VALUES ('应用主题管理', '', 0x800255002E, '', '', '1', '73');
INSERT INTO `permission` VALUES ('应用主题信息', '', 0x800255002E, '/operation/applicationtype', '73', '1', '74');
INSERT INTO `permission` VALUES ('添加应用主题信息', '', 0x800255002E, '/operation/applicationtypeadd', '73', '2', '75');
INSERT INTO `permission` VALUES ('删除应用主题信息', '', 0x800255002E, '/operation/applicationtypedel', '73', '2', '76');
INSERT INTO `permission` VALUES ('修改应用主题信息', '', 0x800255002E, '/operation/applicationtypeedit', '73', '2', '77');
INSERT INTO `permission` VALUES ('保存修改应用主题信息', '', 0x800255002E, '/operation/applicationtypesave', '73', '2', '78');
INSERT INTO `permission` VALUES ('部门信息管理', '', 0x800255002E, '', '', '1', '79');
INSERT INTO `permission` VALUES ('部门信息', '', 0x800255002E, '/operation/department', '79', '1', '80');
INSERT INTO `permission` VALUES ('添加部门信息', '', 0x800255002E, '/operation/departmentadd', '79', '2', '81');
INSERT INTO `permission` VALUES ('删除部门信息', '', 0x800255002E, '/operation/departmentdel', '79', '2', '82');
INSERT INTO `permission` VALUES ('修改部门信息', '', 0x800255002E, '/operation/departmentedit', '79', '2', '83');
INSERT INTO `permission` VALUES ('保存修改部门信息', '', 0x800255002E, '/operation/departmentsave', '79', '2', '84');
INSERT INTO `permission` VALUES ('运营组信息管理', '', 0x800255002E, '', '', '1', '85');
INSERT INTO `permission` VALUES ('运营组信息', '', 0x800255002E, '/operation/opersgroup', '85', '1', '86');
INSERT INTO `permission` VALUES ('添加运营组信息', '', 0x800255002E, '/operation/opersgroupadd', '85', '2', '87');
INSERT INTO `permission` VALUES ('删除运营组信息', '', 0x800255002E, '/operation/opersgroupdel', '85', '2', '88');
INSERT INTO `permission` VALUES ('修改运营组信息', '', 0x800255002E, '/operation/opersgroupedit', '85', '2', '89');
INSERT INTO `permission` VALUES ('保存修改运营组信息', '', 0x800255002E, '/operation/opersgroupsave', '85', '2', '90');
INSERT INTO `permission` VALUES ('运营组项目管理', '', 0x800255002E, '', '', '1', '91');
INSERT INTO `permission` VALUES ('运营组项目信息', null, 0x800255002E, '/operation/opersgroupproject', '91', '1', '92');
INSERT INTO `permission` VALUES ('添加运营组项目信息', null, 0x800255002E, '/operation/opersgroupprojectadd', '91', '2', '93');
INSERT INTO `permission` VALUES ('删除运营组项目信息', null, 0x800255002E, '/operation/opersgroupprojectdel', '91', '2', '94');
INSERT INTO `permission` VALUES ('修改运营组项目信息', '', 0x800255002E, '/operation/opersgroupprojectedit', '91', '2', '95');
INSERT INTO `permission` VALUES ('保存修改运营组项目信息', '', 0x800255002E, '/operation/opersgroupprojectsave', '91', '2', '96');
INSERT INTO `permission` VALUES ('统计报表管理', '', 0x800255002E, '', '', '1', '97');
INSERT INTO `permission` VALUES ('设备统计', '', 0x800255002E, '/stat/sthardware', '97', '1', '98');
INSERT INTO `permission` VALUES ('操作系统管理', '', 0x800255002E, '', '', '1', '101');
INSERT INTO `permission` VALUES ('操作系统信息', '', 0x800255002E, '/devices/operatingsystem', '101', '1', '102');
INSERT INTO `permission` VALUES ('添加操作系统信息', '', 0x800255002E, '/devices/operatingsystemadd', '101', '2', '103');
INSERT INTO `permission` VALUES ('修改操作系统信息', '', 0x800255002E, '/devices/operatingsystemedit', '101', '2', '104');
INSERT INTO `permission` VALUES ('删除操作系统信息', '', 0x800255002E, '/devices/operatingsystemdel', '101', '2', '105');
INSERT INTO `permission` VALUES ('保存修改操作系统信息', '', 0x800255002E, '/devices/operatingsystemsave', '101', '2', '106');
INSERT INTO `permission` VALUES ('设备机柜图', '', 0x800255002E, '/devices/devicesmain', '40', '1', '107');
INSERT INTO `permission` VALUES ('服务器设备导出Excel', '导出excel，可以按机房导出', 0x800255002E, '/devices/exportExcel', '40', '2', '108');
INSERT INTO `permission` VALUES ('查看服务器设备摘要', '在服务器设备点击+号展示的内容', 0x800255002E, '/devices/server_getdetail', '40', '2', '109');
INSERT INTO `permission` VALUES ('硬件品牌管理', '', 0x800255002E, '', '', '1', '110');
INSERT INTO `permission` VALUES ('硬件品牌信息', '', 0x800255002E, '/devices/hardwaremanufacturer', '110', '1', '111');
INSERT INTO `permission` VALUES ('添加硬件品牌信息', '', 0x800255002E, '/devices/hardwaremanufactureradd', '110', '2', '112');
INSERT INTO `permission` VALUES ('修改硬件品牌信息', '', 0x800255002E, '/devices/hardwaremanufactureredit', '110', '2', '113');
INSERT INTO `permission` VALUES ('删除硬件品牌信息', '', 0x800255002E, '/devices/hardwaremanufacturerdel', '110', '2', '114');
INSERT INTO `permission` VALUES ('保存修改硬件品牌信息', '', 0x800255002E, '/devices/hardwaremanufacturersave', '110', '2', '115');

-- ----------------------------
-- Table structure for rackspace
-- ----------------------------
DROP TABLE IF EXISTS `rackspace`;
CREATE TABLE `rackspace` (
  `rackspace_name` varchar(50) DEFAULT NULL,
  `computerroom_id` int(11) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `ix_rackspace_rackspace_name` (`rackspace_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rackspace
-- ----------------------------

-- ----------------------------
-- Table structure for region
-- ----------------------------
DROP TABLE IF EXISTS `region`;
CREATE TABLE `region` (
  `region_code` int(11) DEFAULT NULL,
  `region_name` varchar(50) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_region_region_code` (`region_code`),
  KEY `ix_region_region_name` (`region_name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of region
-- ----------------------------
INSERT INTO `region` VALUES ('1000', '华南', '1');
INSERT INTO `region` VALUES ('2000', '华北', '2');
INSERT INTO `region` VALUES ('3000', '华东', '3');
INSERT INTO `region` VALUES ('4000', '华中', '4');
INSERT INTO `region` VALUES ('5000', '西南', '5');
INSERT INTO `region` VALUES ('6000', '西北', '6');

-- ----------------------------
-- Table structure for regionarea
-- ----------------------------
DROP TABLE IF EXISTS `regionarea`;
CREATE TABLE `regionarea` (
  `area_code` int(11) DEFAULT NULL,
  `area_name` varchar(50) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_regionarea_area_code` (`area_code`),
  KEY `ix_regionarea_area_name` (`area_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2954 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of regionarea
-- ----------------------------
INSERT INTO `regionarea` VALUES ('110101', '东城区', '1', '1');
INSERT INTO `regionarea` VALUES ('110102', '西城区', '1', '2');
INSERT INTO `regionarea` VALUES ('110105', '朝阳区', '1', '3');
INSERT INTO `regionarea` VALUES ('110106', '丰台区', '1', '4');
INSERT INTO `regionarea` VALUES ('110107', '石景山区', '1', '5');
INSERT INTO `regionarea` VALUES ('110108', '海淀区', '1', '6');
INSERT INTO `regionarea` VALUES ('110109', '门头沟区', '1', '7');
INSERT INTO `regionarea` VALUES ('110111', '房山区', '1', '8');
INSERT INTO `regionarea` VALUES ('110112', '通州区', '1', '9');
INSERT INTO `regionarea` VALUES ('110113', '顺义区', '1', '10');
INSERT INTO `regionarea` VALUES ('110114', '昌平区', '1', '11');
INSERT INTO `regionarea` VALUES ('110115', '大兴区', '1', '12');
INSERT INTO `regionarea` VALUES ('110116', '怀柔区', '1', '13');
INSERT INTO `regionarea` VALUES ('110117', '平谷区', '1', '14');
INSERT INTO `regionarea` VALUES ('110228', '密云县', '1', '15');
INSERT INTO `regionarea` VALUES ('110229', '延庆县', '1', '16');
INSERT INTO `regionarea` VALUES ('120101', '和平区', '2', '17');
INSERT INTO `regionarea` VALUES ('120102', '河东区', '2', '18');
INSERT INTO `regionarea` VALUES ('120103', '河西区', '2', '19');
INSERT INTO `regionarea` VALUES ('120104', '南开区', '2', '20');
INSERT INTO `regionarea` VALUES ('120105', '河北区', '2', '21');
INSERT INTO `regionarea` VALUES ('120106', '红桥区', '2', '22');
INSERT INTO `regionarea` VALUES ('120110', '东丽区', '2', '23');
INSERT INTO `regionarea` VALUES ('120111', '西青区', '2', '24');
INSERT INTO `regionarea` VALUES ('120112', '津南区', '2', '25');
INSERT INTO `regionarea` VALUES ('120113', '北辰区', '2', '26');
INSERT INTO `regionarea` VALUES ('120114', '武清区', '2', '27');
INSERT INTO `regionarea` VALUES ('120115', '宝坻区', '2', '28');
INSERT INTO `regionarea` VALUES ('120116', '滨海新区', '2', '29');
INSERT INTO `regionarea` VALUES ('120221', '宁河县', '2', '30');
INSERT INTO `regionarea` VALUES ('120223', '静海县', '2', '31');
INSERT INTO `regionarea` VALUES ('120225', '蓟县', '2', '32');
INSERT INTO `regionarea` VALUES ('130102', '长安区', '3', '33');
INSERT INTO `regionarea` VALUES ('130103', '桥东区', '3', '34');
INSERT INTO `regionarea` VALUES ('130104', '桥西区', '3', '35');
INSERT INTO `regionarea` VALUES ('130105', '新华区', '3', '36');
INSERT INTO `regionarea` VALUES ('130107', '井陉矿区', '3', '37');
INSERT INTO `regionarea` VALUES ('130108', '裕华区', '3', '38');
INSERT INTO `regionarea` VALUES ('130121', '井陉县', '3', '39');
INSERT INTO `regionarea` VALUES ('130123', '正定县', '3', '40');
INSERT INTO `regionarea` VALUES ('130124', '栾城县', '3', '41');
INSERT INTO `regionarea` VALUES ('130125', '行唐县', '3', '42');
INSERT INTO `regionarea` VALUES ('130126', '灵寿县', '3', '43');
INSERT INTO `regionarea` VALUES ('130127', '高邑县', '3', '44');
INSERT INTO `regionarea` VALUES ('130128', '深泽县', '3', '45');
INSERT INTO `regionarea` VALUES ('130129', '赞皇县', '3', '46');
INSERT INTO `regionarea` VALUES ('130130', '无极县', '3', '47');
INSERT INTO `regionarea` VALUES ('130131', '平山县', '3', '48');
INSERT INTO `regionarea` VALUES ('130132', '元氏县', '3', '49');
INSERT INTO `regionarea` VALUES ('130133', '赵县', '3', '50');
INSERT INTO `regionarea` VALUES ('130181', '辛集市', '3', '51');
INSERT INTO `regionarea` VALUES ('130182', '藁城市', '3', '52');
INSERT INTO `regionarea` VALUES ('130183', '晋州市', '3', '53');
INSERT INTO `regionarea` VALUES ('130184', '新乐市', '3', '54');
INSERT INTO `regionarea` VALUES ('130185', '鹿泉市', '3', '55');
INSERT INTO `regionarea` VALUES ('130202', '路南区', '4', '56');
INSERT INTO `regionarea` VALUES ('130203', '路北区', '4', '57');
INSERT INTO `regionarea` VALUES ('130204', '古冶区', '4', '58');
INSERT INTO `regionarea` VALUES ('130205', '开平区', '4', '59');
INSERT INTO `regionarea` VALUES ('130207', '丰南区', '4', '60');
INSERT INTO `regionarea` VALUES ('130208', '丰润区', '4', '61');
INSERT INTO `regionarea` VALUES ('130230', '曹妃甸区', '4', '62');
INSERT INTO `regionarea` VALUES ('130223', '滦县', '4', '63');
INSERT INTO `regionarea` VALUES ('130224', '滦南县', '4', '64');
INSERT INTO `regionarea` VALUES ('130225', '乐亭县', '4', '65');
INSERT INTO `regionarea` VALUES ('130227', '迁西县', '4', '66');
INSERT INTO `regionarea` VALUES ('130229', '玉田县', '4', '67');
INSERT INTO `regionarea` VALUES ('130281', '遵化市', '4', '68');
INSERT INTO `regionarea` VALUES ('130283', '迁安市', '4', '69');
INSERT INTO `regionarea` VALUES ('130302', '海港区', '5', '70');
INSERT INTO `regionarea` VALUES ('130303', '山海关区', '5', '71');
INSERT INTO `regionarea` VALUES ('130304', '北戴河区', '5', '72');
INSERT INTO `regionarea` VALUES ('130321', '青龙满族自治县', '5', '73');
INSERT INTO `regionarea` VALUES ('130322', '昌黎县', '5', '74');
INSERT INTO `regionarea` VALUES ('130323', '抚宁县', '5', '75');
INSERT INTO `regionarea` VALUES ('130324', '卢龙县', '5', '76');
INSERT INTO `regionarea` VALUES ('130402', '邯山区', '6', '77');
INSERT INTO `regionarea` VALUES ('130403', '丛台区', '6', '78');
INSERT INTO `regionarea` VALUES ('130404', '复兴区', '6', '79');
INSERT INTO `regionarea` VALUES ('130406', '峰峰矿区', '6', '80');
INSERT INTO `regionarea` VALUES ('130421', '邯郸县', '6', '81');
INSERT INTO `regionarea` VALUES ('130423', '临漳县', '6', '82');
INSERT INTO `regionarea` VALUES ('130424', '成安县', '6', '83');
INSERT INTO `regionarea` VALUES ('130425', '大名县', '6', '84');
INSERT INTO `regionarea` VALUES ('130426', '涉县', '6', '85');
INSERT INTO `regionarea` VALUES ('130427', '磁县', '6', '86');
INSERT INTO `regionarea` VALUES ('130428', '肥乡县', '6', '87');
INSERT INTO `regionarea` VALUES ('130429', '永年县', '6', '88');
INSERT INTO `regionarea` VALUES ('130430', '邱县', '6', '89');
INSERT INTO `regionarea` VALUES ('130431', '鸡泽县', '6', '90');
INSERT INTO `regionarea` VALUES ('130432', '广平县', '6', '91');
INSERT INTO `regionarea` VALUES ('130433', '馆陶县', '6', '92');
INSERT INTO `regionarea` VALUES ('130434', '魏县', '6', '93');
INSERT INTO `regionarea` VALUES ('130435', '曲周县', '6', '94');
INSERT INTO `regionarea` VALUES ('130481', '武安市', '6', '95');
INSERT INTO `regionarea` VALUES ('130502', '桥东区', '7', '96');
INSERT INTO `regionarea` VALUES ('130503', '桥西区', '7', '97');
INSERT INTO `regionarea` VALUES ('130521', '邢台县', '7', '98');
INSERT INTO `regionarea` VALUES ('130522', '临城县', '7', '99');
INSERT INTO `regionarea` VALUES ('130523', '内丘县', '7', '100');
INSERT INTO `regionarea` VALUES ('130524', '柏乡县', '7', '101');
INSERT INTO `regionarea` VALUES ('130525', '隆尧县', '7', '102');
INSERT INTO `regionarea` VALUES ('130526', '任县', '7', '103');
INSERT INTO `regionarea` VALUES ('130527', '南和县', '7', '104');
INSERT INTO `regionarea` VALUES ('130528', '宁晋县', '7', '105');
INSERT INTO `regionarea` VALUES ('130529', '巨鹿县', '7', '106');
INSERT INTO `regionarea` VALUES ('130530', '新河县', '7', '107');
INSERT INTO `regionarea` VALUES ('130531', '广宗县', '7', '108');
INSERT INTO `regionarea` VALUES ('130532', '平乡县', '7', '109');
INSERT INTO `regionarea` VALUES ('130533', '威县', '7', '110');
INSERT INTO `regionarea` VALUES ('130534', '清河县', '7', '111');
INSERT INTO `regionarea` VALUES ('130535', '临西县', '7', '112');
INSERT INTO `regionarea` VALUES ('130581', '南宫市', '7', '113');
INSERT INTO `regionarea` VALUES ('130582', '沙河市', '7', '114');
INSERT INTO `regionarea` VALUES ('130602', '新市区', '8', '115');
INSERT INTO `regionarea` VALUES ('130603', '北市区', '8', '116');
INSERT INTO `regionarea` VALUES ('130604', '南市区', '8', '117');
INSERT INTO `regionarea` VALUES ('130621', '满城县', '8', '118');
INSERT INTO `regionarea` VALUES ('130622', '清苑县', '8', '119');
INSERT INTO `regionarea` VALUES ('130623', '涞水县', '8', '120');
INSERT INTO `regionarea` VALUES ('130624', '阜平县', '8', '121');
INSERT INTO `regionarea` VALUES ('130625', '徐水县', '8', '122');
INSERT INTO `regionarea` VALUES ('130626', '定兴县', '8', '123');
INSERT INTO `regionarea` VALUES ('130627', '唐县', '8', '124');
INSERT INTO `regionarea` VALUES ('130628', '高阳县', '8', '125');
INSERT INTO `regionarea` VALUES ('130629', '容城县', '8', '126');
INSERT INTO `regionarea` VALUES ('130630', '涞源县', '8', '127');
INSERT INTO `regionarea` VALUES ('130631', '望都县', '8', '128');
INSERT INTO `regionarea` VALUES ('130632', '安新县', '8', '129');
INSERT INTO `regionarea` VALUES ('130633', '易县', '8', '130');
INSERT INTO `regionarea` VALUES ('130634', '曲阳县', '8', '131');
INSERT INTO `regionarea` VALUES ('130635', '蠡县', '8', '132');
INSERT INTO `regionarea` VALUES ('130636', '顺平县', '8', '133');
INSERT INTO `regionarea` VALUES ('130637', '博野县', '8', '134');
INSERT INTO `regionarea` VALUES ('130638', '雄县', '8', '135');
INSERT INTO `regionarea` VALUES ('130681', '涿州市', '8', '136');
INSERT INTO `regionarea` VALUES ('130682', '定州市', '8', '137');
INSERT INTO `regionarea` VALUES ('130683', '安国市', '8', '138');
INSERT INTO `regionarea` VALUES ('130684', '高碑店市', '8', '139');
INSERT INTO `regionarea` VALUES ('130702', '桥东区', '9', '140');
INSERT INTO `regionarea` VALUES ('130703', '桥西区', '9', '141');
INSERT INTO `regionarea` VALUES ('130705', '宣化区', '9', '142');
INSERT INTO `regionarea` VALUES ('130706', '下花园区', '9', '143');
INSERT INTO `regionarea` VALUES ('130721', '宣化县', '9', '144');
INSERT INTO `regionarea` VALUES ('130722', '张北县', '9', '145');
INSERT INTO `regionarea` VALUES ('130723', '康保县', '9', '146');
INSERT INTO `regionarea` VALUES ('130724', '沽源县', '9', '147');
INSERT INTO `regionarea` VALUES ('130725', '尚义县', '9', '148');
INSERT INTO `regionarea` VALUES ('130726', '蔚县', '9', '149');
INSERT INTO `regionarea` VALUES ('130727', '阳原县', '9', '150');
INSERT INTO `regionarea` VALUES ('130728', '怀安县', '9', '151');
INSERT INTO `regionarea` VALUES ('130729', '万全县', '9', '152');
INSERT INTO `regionarea` VALUES ('130730', '怀来县', '9', '153');
INSERT INTO `regionarea` VALUES ('130731', '涿鹿县', '9', '154');
INSERT INTO `regionarea` VALUES ('130732', '赤城县', '9', '155');
INSERT INTO `regionarea` VALUES ('130733', '崇礼县', '9', '156');
INSERT INTO `regionarea` VALUES ('130802', '双桥区', '10', '157');
INSERT INTO `regionarea` VALUES ('130803', '双滦区', '10', '158');
INSERT INTO `regionarea` VALUES ('130804', '鹰手营子矿区', '10', '159');
INSERT INTO `regionarea` VALUES ('130821', '承德县', '10', '160');
INSERT INTO `regionarea` VALUES ('130822', '兴隆县', '10', '161');
INSERT INTO `regionarea` VALUES ('130823', '平泉县', '10', '162');
INSERT INTO `regionarea` VALUES ('130824', '滦平县', '10', '163');
INSERT INTO `regionarea` VALUES ('130825', '隆化县', '10', '164');
INSERT INTO `regionarea` VALUES ('130826', '丰宁满族自治县', '10', '165');
INSERT INTO `regionarea` VALUES ('130827', '宽城满族自治县', '10', '166');
INSERT INTO `regionarea` VALUES ('130828', '围场满族蒙古族自治县', '10', '167');
INSERT INTO `regionarea` VALUES ('130902', '新华区', '11', '168');
INSERT INTO `regionarea` VALUES ('130903', '运河区', '11', '169');
INSERT INTO `regionarea` VALUES ('130921', '沧县', '11', '170');
INSERT INTO `regionarea` VALUES ('130922', '青县', '11', '171');
INSERT INTO `regionarea` VALUES ('130923', '东光县', '11', '172');
INSERT INTO `regionarea` VALUES ('130924', '海兴县', '11', '173');
INSERT INTO `regionarea` VALUES ('130925', '盐山县', '11', '174');
INSERT INTO `regionarea` VALUES ('130926', '肃宁县', '11', '175');
INSERT INTO `regionarea` VALUES ('130927', '南皮县', '11', '176');
INSERT INTO `regionarea` VALUES ('130928', '吴桥县', '11', '177');
INSERT INTO `regionarea` VALUES ('130929', '献县', '11', '178');
INSERT INTO `regionarea` VALUES ('130930', '孟村回族自治县', '11', '179');
INSERT INTO `regionarea` VALUES ('130981', '泊头市', '11', '180');
INSERT INTO `regionarea` VALUES ('130982', '任丘市', '11', '181');
INSERT INTO `regionarea` VALUES ('130983', '黄骅市', '11', '182');
INSERT INTO `regionarea` VALUES ('130984', '河间市', '11', '183');
INSERT INTO `regionarea` VALUES ('131002', '安次区', '12', '184');
INSERT INTO `regionarea` VALUES ('131003', '广阳区', '12', '185');
INSERT INTO `regionarea` VALUES ('131022', '固安县', '12', '186');
INSERT INTO `regionarea` VALUES ('131023', '永清县', '12', '187');
INSERT INTO `regionarea` VALUES ('131024', '香河县', '12', '188');
INSERT INTO `regionarea` VALUES ('131025', '大城县', '12', '189');
INSERT INTO `regionarea` VALUES ('131026', '文安县', '12', '190');
INSERT INTO `regionarea` VALUES ('131028', '大厂回族自治县', '12', '191');
INSERT INTO `regionarea` VALUES ('131081', '霸州市', '12', '192');
INSERT INTO `regionarea` VALUES ('131082', '三河市', '12', '193');
INSERT INTO `regionarea` VALUES ('131102', '桃城区', '13', '194');
INSERT INTO `regionarea` VALUES ('131121', '枣强县', '13', '195');
INSERT INTO `regionarea` VALUES ('131122', '武邑县', '13', '196');
INSERT INTO `regionarea` VALUES ('131123', '武强县', '13', '197');
INSERT INTO `regionarea` VALUES ('131124', '饶阳县', '13', '198');
INSERT INTO `regionarea` VALUES ('131125', '安平县', '13', '199');
INSERT INTO `regionarea` VALUES ('131126', '故城县', '13', '200');
INSERT INTO `regionarea` VALUES ('131127', '景县', '13', '201');
INSERT INTO `regionarea` VALUES ('131128', '阜城县', '13', '202');
INSERT INTO `regionarea` VALUES ('131181', '冀州市', '13', '203');
INSERT INTO `regionarea` VALUES ('131182', '深州市', '13', '204');
INSERT INTO `regionarea` VALUES ('140105', '小店区', '14', '205');
INSERT INTO `regionarea` VALUES ('140106', '迎泽区', '14', '206');
INSERT INTO `regionarea` VALUES ('140107', '杏花岭区', '14', '207');
INSERT INTO `regionarea` VALUES ('140108', '尖草坪区', '14', '208');
INSERT INTO `regionarea` VALUES ('140109', '万柏林区', '14', '209');
INSERT INTO `regionarea` VALUES ('140110', '晋源区', '14', '210');
INSERT INTO `regionarea` VALUES ('140121', '清徐县', '14', '211');
INSERT INTO `regionarea` VALUES ('140122', '阳曲县', '14', '212');
INSERT INTO `regionarea` VALUES ('140123', '娄烦县', '14', '213');
INSERT INTO `regionarea` VALUES ('140181', '古交市', '14', '214');
INSERT INTO `regionarea` VALUES ('140202', '城区', '15', '215');
INSERT INTO `regionarea` VALUES ('140203', '矿区', '15', '216');
INSERT INTO `regionarea` VALUES ('140211', '南郊区', '15', '217');
INSERT INTO `regionarea` VALUES ('140212', '新荣区', '15', '218');
INSERT INTO `regionarea` VALUES ('140221', '阳高县', '15', '219');
INSERT INTO `regionarea` VALUES ('140222', '天镇县', '15', '220');
INSERT INTO `regionarea` VALUES ('140223', '广灵县', '15', '221');
INSERT INTO `regionarea` VALUES ('140224', '灵丘县', '15', '222');
INSERT INTO `regionarea` VALUES ('140225', '浑源县', '15', '223');
INSERT INTO `regionarea` VALUES ('140226', '左云县', '15', '224');
INSERT INTO `regionarea` VALUES ('140227', '大同县', '15', '225');
INSERT INTO `regionarea` VALUES ('140302', '城区', '16', '226');
INSERT INTO `regionarea` VALUES ('140303', '矿区', '16', '227');
INSERT INTO `regionarea` VALUES ('140311', '郊区', '16', '228');
INSERT INTO `regionarea` VALUES ('140321', '平定县', '16', '229');
INSERT INTO `regionarea` VALUES ('140322', '盂县', '16', '230');
INSERT INTO `regionarea` VALUES ('140402', '城区', '17', '231');
INSERT INTO `regionarea` VALUES ('140411', '郊区', '17', '232');
INSERT INTO `regionarea` VALUES ('140421', '长治县', '17', '233');
INSERT INTO `regionarea` VALUES ('140423', '襄垣县', '17', '234');
INSERT INTO `regionarea` VALUES ('140424', '屯留县', '17', '235');
INSERT INTO `regionarea` VALUES ('140425', '平顺县', '17', '236');
INSERT INTO `regionarea` VALUES ('140426', '黎城县', '17', '237');
INSERT INTO `regionarea` VALUES ('140427', '壶关县', '17', '238');
INSERT INTO `regionarea` VALUES ('140428', '长子县', '17', '239');
INSERT INTO `regionarea` VALUES ('140429', '武乡县', '17', '240');
INSERT INTO `regionarea` VALUES ('140430', '沁县', '17', '241');
INSERT INTO `regionarea` VALUES ('140431', '沁源县', '17', '242');
INSERT INTO `regionarea` VALUES ('140481', '潞城市', '17', '243');
INSERT INTO `regionarea` VALUES ('140502', '城区', '18', '244');
INSERT INTO `regionarea` VALUES ('140521', '沁水县', '18', '245');
INSERT INTO `regionarea` VALUES ('140522', '阳城县', '18', '246');
INSERT INTO `regionarea` VALUES ('140524', '陵川县', '18', '247');
INSERT INTO `regionarea` VALUES ('140525', '泽州县', '18', '248');
INSERT INTO `regionarea` VALUES ('140581', '高平市', '18', '249');
INSERT INTO `regionarea` VALUES ('140602', '朔城区', '19', '250');
INSERT INTO `regionarea` VALUES ('140603', '平鲁区', '19', '251');
INSERT INTO `regionarea` VALUES ('140621', '山阴县', '19', '252');
INSERT INTO `regionarea` VALUES ('140622', '应县', '19', '253');
INSERT INTO `regionarea` VALUES ('140623', '右玉县', '19', '254');
INSERT INTO `regionarea` VALUES ('140624', '怀仁县', '19', '255');
INSERT INTO `regionarea` VALUES ('140702', '榆次区', '20', '256');
INSERT INTO `regionarea` VALUES ('140721', '榆社县', '20', '257');
INSERT INTO `regionarea` VALUES ('140722', '左权县', '20', '258');
INSERT INTO `regionarea` VALUES ('140723', '和顺县', '20', '259');
INSERT INTO `regionarea` VALUES ('140724', '昔阳县', '20', '260');
INSERT INTO `regionarea` VALUES ('140725', '寿阳县', '20', '261');
INSERT INTO `regionarea` VALUES ('140726', '太谷县', '20', '262');
INSERT INTO `regionarea` VALUES ('140727', '祁县', '20', '263');
INSERT INTO `regionarea` VALUES ('140728', '平遥县', '20', '264');
INSERT INTO `regionarea` VALUES ('140729', '灵石县', '20', '265');
INSERT INTO `regionarea` VALUES ('140781', '介休市', '20', '266');
INSERT INTO `regionarea` VALUES ('140802', '盐湖区', '21', '267');
INSERT INTO `regionarea` VALUES ('140821', '临猗县', '21', '268');
INSERT INTO `regionarea` VALUES ('140822', '万荣县', '21', '269');
INSERT INTO `regionarea` VALUES ('140823', '闻喜县', '21', '270');
INSERT INTO `regionarea` VALUES ('140824', '稷山县', '21', '271');
INSERT INTO `regionarea` VALUES ('140825', '新绛县', '21', '272');
INSERT INTO `regionarea` VALUES ('140826', '绛县', '21', '273');
INSERT INTO `regionarea` VALUES ('140827', '垣曲县', '21', '274');
INSERT INTO `regionarea` VALUES ('140828', '夏县', '21', '275');
INSERT INTO `regionarea` VALUES ('140829', '平陆县', '21', '276');
INSERT INTO `regionarea` VALUES ('140830', '芮城县', '21', '277');
INSERT INTO `regionarea` VALUES ('140881', '永济市', '21', '278');
INSERT INTO `regionarea` VALUES ('140882', '河津市', '21', '279');
INSERT INTO `regionarea` VALUES ('140902', '忻府区', '22', '280');
INSERT INTO `regionarea` VALUES ('140921', '定襄县', '22', '281');
INSERT INTO `regionarea` VALUES ('140922', '五台县', '22', '282');
INSERT INTO `regionarea` VALUES ('140923', '代县', '22', '283');
INSERT INTO `regionarea` VALUES ('140924', '繁峙县', '22', '284');
INSERT INTO `regionarea` VALUES ('140925', '宁武县', '22', '285');
INSERT INTO `regionarea` VALUES ('140926', '静乐县', '22', '286');
INSERT INTO `regionarea` VALUES ('140927', '神池县', '22', '287');
INSERT INTO `regionarea` VALUES ('140928', '五寨县', '22', '288');
INSERT INTO `regionarea` VALUES ('140929', '岢岚县', '22', '289');
INSERT INTO `regionarea` VALUES ('140930', '河曲县', '22', '290');
INSERT INTO `regionarea` VALUES ('140931', '保德县', '22', '291');
INSERT INTO `regionarea` VALUES ('140932', '偏关县', '22', '292');
INSERT INTO `regionarea` VALUES ('140981', '原平市', '22', '293');
INSERT INTO `regionarea` VALUES ('141002', '尧都区', '23', '294');
INSERT INTO `regionarea` VALUES ('141021', '曲沃县', '23', '295');
INSERT INTO `regionarea` VALUES ('141022', '翼城县', '23', '296');
INSERT INTO `regionarea` VALUES ('141023', '襄汾县', '23', '297');
INSERT INTO `regionarea` VALUES ('141024', '洪洞县', '23', '298');
INSERT INTO `regionarea` VALUES ('141025', '古县', '23', '299');
INSERT INTO `regionarea` VALUES ('141026', '安泽县', '23', '300');
INSERT INTO `regionarea` VALUES ('141027', '浮山县', '23', '301');
INSERT INTO `regionarea` VALUES ('141028', '吉县', '23', '302');
INSERT INTO `regionarea` VALUES ('141029', '乡宁县', '23', '303');
INSERT INTO `regionarea` VALUES ('141030', '大宁县', '23', '304');
INSERT INTO `regionarea` VALUES ('141031', '隰县', '23', '305');
INSERT INTO `regionarea` VALUES ('141032', '永和县', '23', '306');
INSERT INTO `regionarea` VALUES ('141033', '蒲县', '23', '307');
INSERT INTO `regionarea` VALUES ('141034', '汾西县', '23', '308');
INSERT INTO `regionarea` VALUES ('141081', '侯马市', '23', '309');
INSERT INTO `regionarea` VALUES ('141082', '霍州市', '23', '310');
INSERT INTO `regionarea` VALUES ('141102', '离石区', '24', '311');
INSERT INTO `regionarea` VALUES ('141121', '文水县', '24', '312');
INSERT INTO `regionarea` VALUES ('141122', '交城县', '24', '313');
INSERT INTO `regionarea` VALUES ('141123', '兴县', '24', '314');
INSERT INTO `regionarea` VALUES ('141124', '临县', '24', '315');
INSERT INTO `regionarea` VALUES ('141125', '柳林县', '24', '316');
INSERT INTO `regionarea` VALUES ('141126', '石楼县', '24', '317');
INSERT INTO `regionarea` VALUES ('141127', '岚县', '24', '318');
INSERT INTO `regionarea` VALUES ('141128', '方山县', '24', '319');
INSERT INTO `regionarea` VALUES ('141129', '中阳县', '24', '320');
INSERT INTO `regionarea` VALUES ('141130', '交口县', '24', '321');
INSERT INTO `regionarea` VALUES ('141181', '孝义市', '24', '322');
INSERT INTO `regionarea` VALUES ('141182', '汾阳市', '24', '323');
INSERT INTO `regionarea` VALUES ('150102', '新城区', '25', '324');
INSERT INTO `regionarea` VALUES ('150103', '回民区', '25', '325');
INSERT INTO `regionarea` VALUES ('150104', '玉泉区', '25', '326');
INSERT INTO `regionarea` VALUES ('150105', '赛罕区', '25', '327');
INSERT INTO `regionarea` VALUES ('150121', '土默特左旗', '25', '328');
INSERT INTO `regionarea` VALUES ('150122', '托克托县', '25', '329');
INSERT INTO `regionarea` VALUES ('150123', '和林格尔县', '25', '330');
INSERT INTO `regionarea` VALUES ('150124', '清水河县', '25', '331');
INSERT INTO `regionarea` VALUES ('150125', '武川县', '25', '332');
INSERT INTO `regionarea` VALUES ('150202', '东河区', '26', '333');
INSERT INTO `regionarea` VALUES ('150203', '昆都仑区', '26', '334');
INSERT INTO `regionarea` VALUES ('150204', '青山区', '26', '335');
INSERT INTO `regionarea` VALUES ('150205', '石拐区', '26', '336');
INSERT INTO `regionarea` VALUES ('150206', '白云鄂博矿区', '26', '337');
INSERT INTO `regionarea` VALUES ('150207', '九原区', '26', '338');
INSERT INTO `regionarea` VALUES ('150221', '土默特右旗', '26', '339');
INSERT INTO `regionarea` VALUES ('150222', '固阳县', '26', '340');
INSERT INTO `regionarea` VALUES ('150223', '达尔罕茂明安联合旗', '26', '341');
INSERT INTO `regionarea` VALUES ('150302', '海勃湾区', '27', '342');
INSERT INTO `regionarea` VALUES ('150303', '海南区', '27', '343');
INSERT INTO `regionarea` VALUES ('150304', '乌达区', '27', '344');
INSERT INTO `regionarea` VALUES ('150402', '红山区', '28', '345');
INSERT INTO `regionarea` VALUES ('150403', '元宝山区', '28', '346');
INSERT INTO `regionarea` VALUES ('150404', '松山区', '28', '347');
INSERT INTO `regionarea` VALUES ('150421', '阿鲁科尔沁旗', '28', '348');
INSERT INTO `regionarea` VALUES ('150422', '巴林左旗', '28', '349');
INSERT INTO `regionarea` VALUES ('150423', '巴林右旗', '28', '350');
INSERT INTO `regionarea` VALUES ('150424', '林西县', '28', '351');
INSERT INTO `regionarea` VALUES ('150425', '克什克腾旗', '28', '352');
INSERT INTO `regionarea` VALUES ('150426', '翁牛特旗', '28', '353');
INSERT INTO `regionarea` VALUES ('150428', '喀喇沁旗', '28', '354');
INSERT INTO `regionarea` VALUES ('150429', '宁城县', '28', '355');
INSERT INTO `regionarea` VALUES ('150430', '敖汉旗', '28', '356');
INSERT INTO `regionarea` VALUES ('150502', '科尔沁区', '29', '357');
INSERT INTO `regionarea` VALUES ('150521', '科尔沁左翼中旗', '29', '358');
INSERT INTO `regionarea` VALUES ('150522', '科尔沁左翼后旗', '29', '359');
INSERT INTO `regionarea` VALUES ('150523', '开鲁县', '29', '360');
INSERT INTO `regionarea` VALUES ('150524', '库伦旗', '29', '361');
INSERT INTO `regionarea` VALUES ('150525', '奈曼旗', '29', '362');
INSERT INTO `regionarea` VALUES ('150526', '扎鲁特旗', '29', '363');
INSERT INTO `regionarea` VALUES ('150581', '霍林郭勒市', '29', '364');
INSERT INTO `regionarea` VALUES ('150602', '东胜区', '30', '365');
INSERT INTO `regionarea` VALUES ('150621', '达拉特旗', '30', '366');
INSERT INTO `regionarea` VALUES ('150622', '准格尔旗', '30', '367');
INSERT INTO `regionarea` VALUES ('150623', '鄂托克前旗', '30', '368');
INSERT INTO `regionarea` VALUES ('150624', '鄂托克旗', '30', '369');
INSERT INTO `regionarea` VALUES ('150625', '杭锦旗', '30', '370');
INSERT INTO `regionarea` VALUES ('150626', '乌审旗', '30', '371');
INSERT INTO `regionarea` VALUES ('150627', '伊金霍洛旗', '30', '372');
INSERT INTO `regionarea` VALUES ('150702', '海拉尔区', '31', '373');
INSERT INTO `regionarea` VALUES ('150721', '阿荣旗', '31', '374');
INSERT INTO `regionarea` VALUES ('150722', '莫力达瓦达斡尔族自治旗', '31', '375');
INSERT INTO `regionarea` VALUES ('150723', '鄂伦春自治旗', '31', '376');
INSERT INTO `regionarea` VALUES ('150724', '鄂温克族自治旗', '31', '377');
INSERT INTO `regionarea` VALUES ('150725', '陈巴尔虎旗', '31', '378');
INSERT INTO `regionarea` VALUES ('150726', '新巴尔虎左旗', '31', '379');
INSERT INTO `regionarea` VALUES ('150727', '新巴尔虎右旗', '31', '380');
INSERT INTO `regionarea` VALUES ('150781', '满洲里市', '31', '381');
INSERT INTO `regionarea` VALUES ('150782', '牙克石市', '31', '382');
INSERT INTO `regionarea` VALUES ('150783', '扎兰屯市', '31', '383');
INSERT INTO `regionarea` VALUES ('150784', '额尔古纳市', '31', '384');
INSERT INTO `regionarea` VALUES ('150785', '根河市', '31', '385');
INSERT INTO `regionarea` VALUES ('150802', '临河区', '32', '386');
INSERT INTO `regionarea` VALUES ('150821', '五原县', '32', '387');
INSERT INTO `regionarea` VALUES ('150822', '磴口县', '32', '388');
INSERT INTO `regionarea` VALUES ('150823', '乌拉特前旗', '32', '389');
INSERT INTO `regionarea` VALUES ('150824', '乌拉特中旗', '32', '390');
INSERT INTO `regionarea` VALUES ('150825', '乌拉特后旗', '32', '391');
INSERT INTO `regionarea` VALUES ('150826', '杭锦后旗', '32', '392');
INSERT INTO `regionarea` VALUES ('150902', '集宁区', '33', '393');
INSERT INTO `regionarea` VALUES ('150921', '卓资县', '33', '394');
INSERT INTO `regionarea` VALUES ('150922', '化德县', '33', '395');
INSERT INTO `regionarea` VALUES ('150923', '商都县', '33', '396');
INSERT INTO `regionarea` VALUES ('150924', '兴和县', '33', '397');
INSERT INTO `regionarea` VALUES ('150925', '凉城县', '33', '398');
INSERT INTO `regionarea` VALUES ('150926', '察哈尔右翼前旗', '33', '399');
INSERT INTO `regionarea` VALUES ('150927', '察哈尔右翼中旗', '33', '400');
INSERT INTO `regionarea` VALUES ('150928', '察哈尔右翼后旗', '33', '401');
INSERT INTO `regionarea` VALUES ('150929', '四子王旗', '33', '402');
INSERT INTO `regionarea` VALUES ('150981', '丰镇市', '33', '403');
INSERT INTO `regionarea` VALUES ('152201', '乌兰浩特市', '34', '404');
INSERT INTO `regionarea` VALUES ('152202', '阿尔山市', '34', '405');
INSERT INTO `regionarea` VALUES ('152221', '科尔沁右翼前旗', '34', '406');
INSERT INTO `regionarea` VALUES ('152222', '科尔沁右翼中旗', '34', '407');
INSERT INTO `regionarea` VALUES ('152223', '扎赉特旗', '34', '408');
INSERT INTO `regionarea` VALUES ('152224', '突泉县', '34', '409');
INSERT INTO `regionarea` VALUES ('152501', '二连浩特市', '35', '410');
INSERT INTO `regionarea` VALUES ('152502', '锡林浩特市', '35', '411');
INSERT INTO `regionarea` VALUES ('152522', '阿巴嘎旗', '35', '412');
INSERT INTO `regionarea` VALUES ('152523', '苏尼特左旗', '35', '413');
INSERT INTO `regionarea` VALUES ('152524', '苏尼特右旗', '35', '414');
INSERT INTO `regionarea` VALUES ('152525', '东乌珠穆沁旗', '35', '415');
INSERT INTO `regionarea` VALUES ('152526', '西乌珠穆沁旗', '35', '416');
INSERT INTO `regionarea` VALUES ('152527', '太仆寺旗', '35', '417');
INSERT INTO `regionarea` VALUES ('152528', '镶黄旗', '35', '418');
INSERT INTO `regionarea` VALUES ('152529', '正镶白旗', '35', '419');
INSERT INTO `regionarea` VALUES ('152530', '正蓝旗', '35', '420');
INSERT INTO `regionarea` VALUES ('152531', '多伦县', '35', '421');
INSERT INTO `regionarea` VALUES ('152921', '阿拉善左旗', '36', '422');
INSERT INTO `regionarea` VALUES ('152922', '阿拉善右旗', '36', '423');
INSERT INTO `regionarea` VALUES ('152923', '额济纳旗', '36', '424');
INSERT INTO `regionarea` VALUES ('210102', '和平区', '37', '425');
INSERT INTO `regionarea` VALUES ('210103', '沈河区', '37', '426');
INSERT INTO `regionarea` VALUES ('210104', '大东区', '37', '427');
INSERT INTO `regionarea` VALUES ('210105', '皇姑区', '37', '428');
INSERT INTO `regionarea` VALUES ('210106', '铁西区', '37', '429');
INSERT INTO `regionarea` VALUES ('210111', '苏家屯区', '37', '430');
INSERT INTO `regionarea` VALUES ('210112', '东陵区', '37', '431');
INSERT INTO `regionarea` VALUES ('210113', '沈北新区', '37', '432');
INSERT INTO `regionarea` VALUES ('210114', '于洪区', '37', '433');
INSERT INTO `regionarea` VALUES ('210122', '辽中县', '37', '434');
INSERT INTO `regionarea` VALUES ('210123', '康平县', '37', '435');
INSERT INTO `regionarea` VALUES ('210124', '法库县', '37', '436');
INSERT INTO `regionarea` VALUES ('210181', '新民市', '37', '437');
INSERT INTO `regionarea` VALUES ('210202', '中山区', '38', '438');
INSERT INTO `regionarea` VALUES ('210203', '西岗区', '38', '439');
INSERT INTO `regionarea` VALUES ('210204', '沙河口区', '38', '440');
INSERT INTO `regionarea` VALUES ('210211', '甘井子区', '38', '441');
INSERT INTO `regionarea` VALUES ('210212', '旅顺口区', '38', '442');
INSERT INTO `regionarea` VALUES ('210213', '金州区', '38', '443');
INSERT INTO `regionarea` VALUES ('210224', '长海县', '38', '444');
INSERT INTO `regionarea` VALUES ('210281', '瓦房店市', '38', '445');
INSERT INTO `regionarea` VALUES ('210282', '普兰店市', '38', '446');
INSERT INTO `regionarea` VALUES ('210283', '庄河市', '38', '447');
INSERT INTO `regionarea` VALUES ('210302', '铁东区', '39', '448');
INSERT INTO `regionarea` VALUES ('210303', '铁西区', '39', '449');
INSERT INTO `regionarea` VALUES ('210304', '立山区', '39', '450');
INSERT INTO `regionarea` VALUES ('210311', '千山区', '39', '451');
INSERT INTO `regionarea` VALUES ('210321', '台安县', '39', '452');
INSERT INTO `regionarea` VALUES ('210323', '岫岩满族自治县', '39', '453');
INSERT INTO `regionarea` VALUES ('210381', '海城市', '39', '454');
INSERT INTO `regionarea` VALUES ('210402', '新抚区', '40', '455');
INSERT INTO `regionarea` VALUES ('210403', '东洲区', '40', '456');
INSERT INTO `regionarea` VALUES ('210404', '望花区', '40', '457');
INSERT INTO `regionarea` VALUES ('210411', '顺城区', '40', '458');
INSERT INTO `regionarea` VALUES ('210421', '抚顺县', '40', '459');
INSERT INTO `regionarea` VALUES ('210422', '新宾满族自治县', '40', '460');
INSERT INTO `regionarea` VALUES ('210423', '清原满族自治县', '40', '461');
INSERT INTO `regionarea` VALUES ('210502', '平山区', '41', '462');
INSERT INTO `regionarea` VALUES ('210503', '溪湖区', '41', '463');
INSERT INTO `regionarea` VALUES ('210504', '明山区', '41', '464');
INSERT INTO `regionarea` VALUES ('210505', '南芬区', '41', '465');
INSERT INTO `regionarea` VALUES ('210521', '本溪满族自治县', '41', '466');
INSERT INTO `regionarea` VALUES ('210522', '桓仁满族自治县', '41', '467');
INSERT INTO `regionarea` VALUES ('210602', '元宝区', '42', '468');
INSERT INTO `regionarea` VALUES ('210603', '振兴区', '42', '469');
INSERT INTO `regionarea` VALUES ('210604', '振安区', '42', '470');
INSERT INTO `regionarea` VALUES ('210624', '宽甸满族自治县', '42', '471');
INSERT INTO `regionarea` VALUES ('210681', '东港市', '42', '472');
INSERT INTO `regionarea` VALUES ('210682', '凤城市', '42', '473');
INSERT INTO `regionarea` VALUES ('210702', '古塔区', '43', '474');
INSERT INTO `regionarea` VALUES ('210703', '凌河区', '43', '475');
INSERT INTO `regionarea` VALUES ('210711', '太和区', '43', '476');
INSERT INTO `regionarea` VALUES ('210726', '黑山县', '43', '477');
INSERT INTO `regionarea` VALUES ('210727', '义县', '43', '478');
INSERT INTO `regionarea` VALUES ('210781', '凌海市', '43', '479');
INSERT INTO `regionarea` VALUES ('210782', '北镇市', '43', '480');
INSERT INTO `regionarea` VALUES ('210802', '站前区', '44', '481');
INSERT INTO `regionarea` VALUES ('210803', '西市区', '44', '482');
INSERT INTO `regionarea` VALUES ('210804', '鲅鱼圈区', '44', '483');
INSERT INTO `regionarea` VALUES ('210811', '老边区', '44', '484');
INSERT INTO `regionarea` VALUES ('210881', '盖州市', '44', '485');
INSERT INTO `regionarea` VALUES ('210882', '大石桥市', '44', '486');
INSERT INTO `regionarea` VALUES ('210902', '海州区', '45', '487');
INSERT INTO `regionarea` VALUES ('210903', '新邱区', '45', '488');
INSERT INTO `regionarea` VALUES ('210904', '太平区', '45', '489');
INSERT INTO `regionarea` VALUES ('210905', '清河门区', '45', '490');
INSERT INTO `regionarea` VALUES ('210911', '细河区', '45', '491');
INSERT INTO `regionarea` VALUES ('210921', '阜新蒙古族自治县', '45', '492');
INSERT INTO `regionarea` VALUES ('210922', '彰武县', '45', '493');
INSERT INTO `regionarea` VALUES ('211002', '白塔区', '46', '494');
INSERT INTO `regionarea` VALUES ('211003', '文圣区', '46', '495');
INSERT INTO `regionarea` VALUES ('211004', '宏伟区', '46', '496');
INSERT INTO `regionarea` VALUES ('211005', '弓长岭区', '46', '497');
INSERT INTO `regionarea` VALUES ('211011', '太子河区', '46', '498');
INSERT INTO `regionarea` VALUES ('211021', '辽阳县', '46', '499');
INSERT INTO `regionarea` VALUES ('211081', '灯塔市', '46', '500');
INSERT INTO `regionarea` VALUES ('211102', '双台子区', '47', '501');
INSERT INTO `regionarea` VALUES ('211103', '兴隆台区', '47', '502');
INSERT INTO `regionarea` VALUES ('211121', '大洼县', '47', '503');
INSERT INTO `regionarea` VALUES ('211122', '盘山县', '47', '504');
INSERT INTO `regionarea` VALUES ('211202', '银州区', '48', '505');
INSERT INTO `regionarea` VALUES ('211204', '清河区', '48', '506');
INSERT INTO `regionarea` VALUES ('211221', '铁岭县', '48', '507');
INSERT INTO `regionarea` VALUES ('211223', '西丰县', '48', '508');
INSERT INTO `regionarea` VALUES ('211224', '昌图县', '48', '509');
INSERT INTO `regionarea` VALUES ('211281', '调兵山市', '48', '510');
INSERT INTO `regionarea` VALUES ('211282', '开原市', '48', '511');
INSERT INTO `regionarea` VALUES ('211302', '双塔区', '49', '512');
INSERT INTO `regionarea` VALUES ('211303', '龙城区', '49', '513');
INSERT INTO `regionarea` VALUES ('211321', '朝阳县', '49', '514');
INSERT INTO `regionarea` VALUES ('211322', '建平县', '49', '515');
INSERT INTO `regionarea` VALUES ('211324', '喀喇沁左翼蒙古族自治县', '49', '516');
INSERT INTO `regionarea` VALUES ('211381', '北票市', '49', '517');
INSERT INTO `regionarea` VALUES ('211382', '凌源市', '49', '518');
INSERT INTO `regionarea` VALUES ('211402', '连山区', '50', '519');
INSERT INTO `regionarea` VALUES ('211403', '龙港区', '50', '520');
INSERT INTO `regionarea` VALUES ('211404', '南票区', '50', '521');
INSERT INTO `regionarea` VALUES ('211421', '绥中县', '50', '522');
INSERT INTO `regionarea` VALUES ('211422', '建昌县', '50', '523');
INSERT INTO `regionarea` VALUES ('211481', '兴城市', '50', '524');
INSERT INTO `regionarea` VALUES ('220102', '南关区', '51', '525');
INSERT INTO `regionarea` VALUES ('220103', '宽城区', '51', '526');
INSERT INTO `regionarea` VALUES ('220104', '朝阳区', '51', '527');
INSERT INTO `regionarea` VALUES ('220105', '二道区', '51', '528');
INSERT INTO `regionarea` VALUES ('220106', '绿园区', '51', '529');
INSERT INTO `regionarea` VALUES ('220112', '双阳区', '51', '530');
INSERT INTO `regionarea` VALUES ('220122', '农安县', '51', '531');
INSERT INTO `regionarea` VALUES ('220181', '九台市', '51', '532');
INSERT INTO `regionarea` VALUES ('220182', '榆树市', '51', '533');
INSERT INTO `regionarea` VALUES ('220183', '德惠市', '51', '534');
INSERT INTO `regionarea` VALUES ('220202', '昌邑区', '52', '535');
INSERT INTO `regionarea` VALUES ('220203', '龙潭区', '52', '536');
INSERT INTO `regionarea` VALUES ('220204', '船营区', '52', '537');
INSERT INTO `regionarea` VALUES ('220211', '丰满区', '52', '538');
INSERT INTO `regionarea` VALUES ('220221', '永吉县', '52', '539');
INSERT INTO `regionarea` VALUES ('220281', '蛟河市', '52', '540');
INSERT INTO `regionarea` VALUES ('220282', '桦甸市', '52', '541');
INSERT INTO `regionarea` VALUES ('220283', '舒兰市', '52', '542');
INSERT INTO `regionarea` VALUES ('220284', '磐石市', '52', '543');
INSERT INTO `regionarea` VALUES ('220302', '铁西区', '53', '544');
INSERT INTO `regionarea` VALUES ('220303', '铁东区', '53', '545');
INSERT INTO `regionarea` VALUES ('220322', '梨树县', '53', '546');
INSERT INTO `regionarea` VALUES ('220323', '伊通满族自治县', '53', '547');
INSERT INTO `regionarea` VALUES ('220381', '公主岭市', '53', '548');
INSERT INTO `regionarea` VALUES ('220382', '双辽市', '53', '549');
INSERT INTO `regionarea` VALUES ('220402', '龙山区', '54', '550');
INSERT INTO `regionarea` VALUES ('220403', '西安区', '54', '551');
INSERT INTO `regionarea` VALUES ('220421', '东丰县', '54', '552');
INSERT INTO `regionarea` VALUES ('220422', '东辽县', '54', '553');
INSERT INTO `regionarea` VALUES ('220502', '东昌区', '55', '554');
INSERT INTO `regionarea` VALUES ('220503', '二道江区', '55', '555');
INSERT INTO `regionarea` VALUES ('220521', '通化县', '55', '556');
INSERT INTO `regionarea` VALUES ('220523', '辉南县', '55', '557');
INSERT INTO `regionarea` VALUES ('220524', '柳河县', '55', '558');
INSERT INTO `regionarea` VALUES ('220581', '梅河口市', '55', '559');
INSERT INTO `regionarea` VALUES ('220582', '集安市', '55', '560');
INSERT INTO `regionarea` VALUES ('220602', '浑江区', '56', '561');
INSERT INTO `regionarea` VALUES ('220605', '江源区', '56', '562');
INSERT INTO `regionarea` VALUES ('220621', '抚松县', '56', '563');
INSERT INTO `regionarea` VALUES ('220622', '靖宇县', '56', '564');
INSERT INTO `regionarea` VALUES ('220623', '长白朝鲜族自治县', '56', '565');
INSERT INTO `regionarea` VALUES ('220681', '临江市', '56', '566');
INSERT INTO `regionarea` VALUES ('220702', '宁江区', '57', '567');
INSERT INTO `regionarea` VALUES ('220721', '前郭尔罗斯蒙古族自治县', '57', '568');
INSERT INTO `regionarea` VALUES ('220722', '长岭县', '57', '569');
INSERT INTO `regionarea` VALUES ('220723', '乾安县', '57', '570');
INSERT INTO `regionarea` VALUES ('220724', '扶余县', '57', '571');
INSERT INTO `regionarea` VALUES ('220802', '洮北区', '58', '572');
INSERT INTO `regionarea` VALUES ('220821', '镇赉县', '58', '573');
INSERT INTO `regionarea` VALUES ('220822', '通榆县', '58', '574');
INSERT INTO `regionarea` VALUES ('220881', '洮南市', '58', '575');
INSERT INTO `regionarea` VALUES ('220882', '大安市', '58', '576');
INSERT INTO `regionarea` VALUES ('222401', '延吉市', '59', '577');
INSERT INTO `regionarea` VALUES ('222402', '图们市', '59', '578');
INSERT INTO `regionarea` VALUES ('222403', '敦化市', '59', '579');
INSERT INTO `regionarea` VALUES ('222404', '珲春市', '59', '580');
INSERT INTO `regionarea` VALUES ('222405', '龙井市', '59', '581');
INSERT INTO `regionarea` VALUES ('222406', '和龙市', '59', '582');
INSERT INTO `regionarea` VALUES ('222424', '汪清县', '59', '583');
INSERT INTO `regionarea` VALUES ('222426', '安图县', '59', '584');
INSERT INTO `regionarea` VALUES ('230102', '道里区', '60', '585');
INSERT INTO `regionarea` VALUES ('230103', '南岗区', '60', '586');
INSERT INTO `regionarea` VALUES ('230104', '道外区', '60', '587');
INSERT INTO `regionarea` VALUES ('230108', '平房区', '60', '588');
INSERT INTO `regionarea` VALUES ('230109', '松北区', '60', '589');
INSERT INTO `regionarea` VALUES ('230110', '香坊区', '60', '590');
INSERT INTO `regionarea` VALUES ('230111', '呼兰区', '60', '591');
INSERT INTO `regionarea` VALUES ('230112', '阿城区', '60', '592');
INSERT INTO `regionarea` VALUES ('230123', '依兰县', '60', '593');
INSERT INTO `regionarea` VALUES ('230124', '方正县', '60', '594');
INSERT INTO `regionarea` VALUES ('230125', '宾县', '60', '595');
INSERT INTO `regionarea` VALUES ('230126', '巴彦县', '60', '596');
INSERT INTO `regionarea` VALUES ('230127', '木兰县', '60', '597');
INSERT INTO `regionarea` VALUES ('230128', '通河县', '60', '598');
INSERT INTO `regionarea` VALUES ('230129', '延寿县', '60', '599');
INSERT INTO `regionarea` VALUES ('230182', '双城市', '60', '600');
INSERT INTO `regionarea` VALUES ('230183', '尚志市', '60', '601');
INSERT INTO `regionarea` VALUES ('230184', '五常市', '60', '602');
INSERT INTO `regionarea` VALUES ('230202', '龙沙区', '61', '603');
INSERT INTO `regionarea` VALUES ('230203', '建华区', '61', '604');
INSERT INTO `regionarea` VALUES ('230204', '铁锋区', '61', '605');
INSERT INTO `regionarea` VALUES ('230205', '昂昂溪区', '61', '606');
INSERT INTO `regionarea` VALUES ('230206', '富拉尔基区', '61', '607');
INSERT INTO `regionarea` VALUES ('230207', '碾子山区', '61', '608');
INSERT INTO `regionarea` VALUES ('230208', '梅里斯达斡尔族区', '61', '609');
INSERT INTO `regionarea` VALUES ('230221', '龙江县', '61', '610');
INSERT INTO `regionarea` VALUES ('230223', '依安县', '61', '611');
INSERT INTO `regionarea` VALUES ('230224', '泰来县', '61', '612');
INSERT INTO `regionarea` VALUES ('230225', '甘南县', '61', '613');
INSERT INTO `regionarea` VALUES ('230227', '富裕县', '61', '614');
INSERT INTO `regionarea` VALUES ('230229', '克山县', '61', '615');
INSERT INTO `regionarea` VALUES ('230230', '克东县', '61', '616');
INSERT INTO `regionarea` VALUES ('230231', '拜泉县', '61', '617');
INSERT INTO `regionarea` VALUES ('230281', '讷河市', '61', '618');
INSERT INTO `regionarea` VALUES ('230302', '鸡冠区', '62', '619');
INSERT INTO `regionarea` VALUES ('230303', '恒山区', '62', '620');
INSERT INTO `regionarea` VALUES ('230304', '滴道区', '62', '621');
INSERT INTO `regionarea` VALUES ('230305', '梨树区', '62', '622');
INSERT INTO `regionarea` VALUES ('230306', '城子河区', '62', '623');
INSERT INTO `regionarea` VALUES ('230307', '麻山区', '62', '624');
INSERT INTO `regionarea` VALUES ('230321', '鸡东县', '62', '625');
INSERT INTO `regionarea` VALUES ('230381', '虎林市', '62', '626');
INSERT INTO `regionarea` VALUES ('230382', '密山市', '62', '627');
INSERT INTO `regionarea` VALUES ('230402', '向阳区', '63', '628');
INSERT INTO `regionarea` VALUES ('230403', '工农区', '63', '629');
INSERT INTO `regionarea` VALUES ('230404', '南山区', '63', '630');
INSERT INTO `regionarea` VALUES ('230405', '兴安区', '63', '631');
INSERT INTO `regionarea` VALUES ('230406', '东山区', '63', '632');
INSERT INTO `regionarea` VALUES ('230407', '兴山区', '63', '633');
INSERT INTO `regionarea` VALUES ('230421', '萝北县', '63', '634');
INSERT INTO `regionarea` VALUES ('230422', '绥滨县', '63', '635');
INSERT INTO `regionarea` VALUES ('230502', '尖山区', '64', '636');
INSERT INTO `regionarea` VALUES ('230503', '岭东区', '64', '637');
INSERT INTO `regionarea` VALUES ('230505', '四方台区', '64', '638');
INSERT INTO `regionarea` VALUES ('230506', '宝山区', '64', '639');
INSERT INTO `regionarea` VALUES ('230521', '集贤县', '64', '640');
INSERT INTO `regionarea` VALUES ('230522', '友谊县', '64', '641');
INSERT INTO `regionarea` VALUES ('230523', '宝清县', '64', '642');
INSERT INTO `regionarea` VALUES ('230524', '饶河县', '64', '643');
INSERT INTO `regionarea` VALUES ('230602', '萨尔图区', '65', '644');
INSERT INTO `regionarea` VALUES ('230603', '龙凤区', '65', '645');
INSERT INTO `regionarea` VALUES ('230604', '让胡路区', '65', '646');
INSERT INTO `regionarea` VALUES ('230605', '红岗区', '65', '647');
INSERT INTO `regionarea` VALUES ('230606', '大同区', '65', '648');
INSERT INTO `regionarea` VALUES ('230621', '肇州县', '65', '649');
INSERT INTO `regionarea` VALUES ('230622', '肇源县', '65', '650');
INSERT INTO `regionarea` VALUES ('230623', '林甸县', '65', '651');
INSERT INTO `regionarea` VALUES ('230624', '杜尔伯特蒙古族自治县', '65', '652');
INSERT INTO `regionarea` VALUES ('230702', '伊春区', '66', '653');
INSERT INTO `regionarea` VALUES ('230703', '南岔区', '66', '654');
INSERT INTO `regionarea` VALUES ('230704', '友好区', '66', '655');
INSERT INTO `regionarea` VALUES ('230705', '西林区', '66', '656');
INSERT INTO `regionarea` VALUES ('230706', '翠峦区', '66', '657');
INSERT INTO `regionarea` VALUES ('230707', '新青区', '66', '658');
INSERT INTO `regionarea` VALUES ('230708', '美溪区', '66', '659');
INSERT INTO `regionarea` VALUES ('230709', '金山屯区', '66', '660');
INSERT INTO `regionarea` VALUES ('230710', '五营区', '66', '661');
INSERT INTO `regionarea` VALUES ('230711', '乌马河区', '66', '662');
INSERT INTO `regionarea` VALUES ('230712', '汤旺河区', '66', '663');
INSERT INTO `regionarea` VALUES ('230713', '带岭区', '66', '664');
INSERT INTO `regionarea` VALUES ('230714', '乌伊岭区', '66', '665');
INSERT INTO `regionarea` VALUES ('230715', '红星区', '66', '666');
INSERT INTO `regionarea` VALUES ('230716', '上甘岭区', '66', '667');
INSERT INTO `regionarea` VALUES ('230722', '嘉荫县', '66', '668');
INSERT INTO `regionarea` VALUES ('230781', '铁力市', '66', '669');
INSERT INTO `regionarea` VALUES ('230803', '向阳区', '67', '670');
INSERT INTO `regionarea` VALUES ('230804', '前进区', '67', '671');
INSERT INTO `regionarea` VALUES ('230805', '东风区', '67', '672');
INSERT INTO `regionarea` VALUES ('230811', '郊区', '67', '673');
INSERT INTO `regionarea` VALUES ('230822', '桦南县', '67', '674');
INSERT INTO `regionarea` VALUES ('230826', '桦川县', '67', '675');
INSERT INTO `regionarea` VALUES ('230828', '汤原县', '67', '676');
INSERT INTO `regionarea` VALUES ('230833', '抚远县', '67', '677');
INSERT INTO `regionarea` VALUES ('230881', '同江市', '67', '678');
INSERT INTO `regionarea` VALUES ('230882', '富锦市', '67', '679');
INSERT INTO `regionarea` VALUES ('230902', '新兴区', '68', '680');
INSERT INTO `regionarea` VALUES ('230903', '桃山区', '68', '681');
INSERT INTO `regionarea` VALUES ('230904', '茄子河区', '68', '682');
INSERT INTO `regionarea` VALUES ('230921', '勃利县', '68', '683');
INSERT INTO `regionarea` VALUES ('231002', '东安区', '69', '684');
INSERT INTO `regionarea` VALUES ('231003', '阳明区', '69', '685');
INSERT INTO `regionarea` VALUES ('231004', '爱民区', '69', '686');
INSERT INTO `regionarea` VALUES ('231005', '西安区', '69', '687');
INSERT INTO `regionarea` VALUES ('231024', '东宁县', '69', '688');
INSERT INTO `regionarea` VALUES ('231025', '林口县', '69', '689');
INSERT INTO `regionarea` VALUES ('231081', '绥芬河市', '69', '690');
INSERT INTO `regionarea` VALUES ('231083', '海林市', '69', '691');
INSERT INTO `regionarea` VALUES ('231084', '宁安市', '69', '692');
INSERT INTO `regionarea` VALUES ('231085', '穆棱市', '69', '693');
INSERT INTO `regionarea` VALUES ('231102', '爱辉区', '70', '694');
INSERT INTO `regionarea` VALUES ('231121', '嫩江县', '70', '695');
INSERT INTO `regionarea` VALUES ('231123', '逊克县', '70', '696');
INSERT INTO `regionarea` VALUES ('231124', '孙吴县', '70', '697');
INSERT INTO `regionarea` VALUES ('231181', '北安市', '70', '698');
INSERT INTO `regionarea` VALUES ('231182', '五大连池市', '70', '699');
INSERT INTO `regionarea` VALUES ('231202', '北林区', '71', '700');
INSERT INTO `regionarea` VALUES ('231221', '望奎县', '71', '701');
INSERT INTO `regionarea` VALUES ('231222', '兰西县', '71', '702');
INSERT INTO `regionarea` VALUES ('231223', '青冈县', '71', '703');
INSERT INTO `regionarea` VALUES ('231224', '庆安县', '71', '704');
INSERT INTO `regionarea` VALUES ('231225', '明水县', '71', '705');
INSERT INTO `regionarea` VALUES ('231226', '绥棱县', '71', '706');
INSERT INTO `regionarea` VALUES ('231281', '安达市', '71', '707');
INSERT INTO `regionarea` VALUES ('231282', '肇东市', '71', '708');
INSERT INTO `regionarea` VALUES ('231283', '海伦市', '71', '709');
INSERT INTO `regionarea` VALUES ('232701', '加格达奇区', '72', '710');
INSERT INTO `regionarea` VALUES ('232702', '松岭区', '72', '711');
INSERT INTO `regionarea` VALUES ('232703', '新林区', '72', '712');
INSERT INTO `regionarea` VALUES ('232704', '呼中区', '72', '713');
INSERT INTO `regionarea` VALUES ('232721', '呼玛县', '72', '714');
INSERT INTO `regionarea` VALUES ('232722', '塔河县', '72', '715');
INSERT INTO `regionarea` VALUES ('232723', '漠河县', '72', '716');
INSERT INTO `regionarea` VALUES ('310101', '黄浦区', '73', '717');
INSERT INTO `regionarea` VALUES ('310104', '徐汇区', '73', '718');
INSERT INTO `regionarea` VALUES ('310105', '长宁区', '73', '719');
INSERT INTO `regionarea` VALUES ('310106', '静安区', '73', '720');
INSERT INTO `regionarea` VALUES ('310107', '普陀区', '73', '721');
INSERT INTO `regionarea` VALUES ('310108', '闸北区', '73', '722');
INSERT INTO `regionarea` VALUES ('310109', '虹口区', '73', '723');
INSERT INTO `regionarea` VALUES ('310110', '杨浦区', '73', '724');
INSERT INTO `regionarea` VALUES ('310112', '闵行区', '73', '725');
INSERT INTO `regionarea` VALUES ('310113', '宝山区', '73', '726');
INSERT INTO `regionarea` VALUES ('310114', '嘉定区', '73', '727');
INSERT INTO `regionarea` VALUES ('310115', '浦东新区', '73', '728');
INSERT INTO `regionarea` VALUES ('310116', '金山区', '73', '729');
INSERT INTO `regionarea` VALUES ('310117', '松江区', '73', '730');
INSERT INTO `regionarea` VALUES ('310118', '青浦区', '73', '731');
INSERT INTO `regionarea` VALUES ('310120', '奉贤区', '73', '732');
INSERT INTO `regionarea` VALUES ('310230', '崇明县', '73', '733');
INSERT INTO `regionarea` VALUES ('320102', '玄武区', '74', '734');
INSERT INTO `regionarea` VALUES ('320103', '白下区', '74', '735');
INSERT INTO `regionarea` VALUES ('320104', '秦淮区', '74', '736');
INSERT INTO `regionarea` VALUES ('320105', '建邺区', '74', '737');
INSERT INTO `regionarea` VALUES ('320106', '鼓楼区', '74', '738');
INSERT INTO `regionarea` VALUES ('320107', '下关区', '74', '739');
INSERT INTO `regionarea` VALUES ('320111', '浦口区', '74', '740');
INSERT INTO `regionarea` VALUES ('320113', '栖霞区', '74', '741');
INSERT INTO `regionarea` VALUES ('320114', '雨花台区', '74', '742');
INSERT INTO `regionarea` VALUES ('320115', '江宁区', '74', '743');
INSERT INTO `regionarea` VALUES ('320116', '六合区', '74', '744');
INSERT INTO `regionarea` VALUES ('320124', '溧水县', '74', '745');
INSERT INTO `regionarea` VALUES ('320125', '高淳县', '74', '746');
INSERT INTO `regionarea` VALUES ('320202', '崇安区', '75', '747');
INSERT INTO `regionarea` VALUES ('320203', '南长区', '75', '748');
INSERT INTO `regionarea` VALUES ('320204', '北塘区', '75', '749');
INSERT INTO `regionarea` VALUES ('320205', '锡山区', '75', '750');
INSERT INTO `regionarea` VALUES ('320206', '惠山区', '75', '751');
INSERT INTO `regionarea` VALUES ('320211', '滨湖区', '75', '752');
INSERT INTO `regionarea` VALUES ('320281', '江阴市', '75', '753');
INSERT INTO `regionarea` VALUES ('320282', '宜兴市', '75', '754');
INSERT INTO `regionarea` VALUES ('320302', '鼓楼区', '76', '755');
INSERT INTO `regionarea` VALUES ('320303', '云龙区', '76', '756');
INSERT INTO `regionarea` VALUES ('320305', '贾汪区', '76', '757');
INSERT INTO `regionarea` VALUES ('320311', '泉山区', '76', '758');
INSERT INTO `regionarea` VALUES ('320312', '铜山区', '76', '759');
INSERT INTO `regionarea` VALUES ('320321', '丰县', '76', '760');
INSERT INTO `regionarea` VALUES ('320322', '沛县', '76', '761');
INSERT INTO `regionarea` VALUES ('320324', '睢宁县', '76', '762');
INSERT INTO `regionarea` VALUES ('320381', '新沂市', '76', '763');
INSERT INTO `regionarea` VALUES ('320382', '邳州市', '76', '764');
INSERT INTO `regionarea` VALUES ('320402', '天宁区', '77', '765');
INSERT INTO `regionarea` VALUES ('320404', '钟楼区', '77', '766');
INSERT INTO `regionarea` VALUES ('320405', '戚墅堰区', '77', '767');
INSERT INTO `regionarea` VALUES ('320411', '新北区', '77', '768');
INSERT INTO `regionarea` VALUES ('320412', '武进区', '77', '769');
INSERT INTO `regionarea` VALUES ('320481', '溧阳市', '77', '770');
INSERT INTO `regionarea` VALUES ('320482', '金坛市', '77', '771');
INSERT INTO `regionarea` VALUES ('320503', '姑苏区', '78', '772');
INSERT INTO `regionarea` VALUES ('320505', '虎丘区', '78', '773');
INSERT INTO `regionarea` VALUES ('320506', '吴中区', '78', '774');
INSERT INTO `regionarea` VALUES ('320507', '相城区', '78', '775');
INSERT INTO `regionarea` VALUES ('320584', '吴江区', '78', '776');
INSERT INTO `regionarea` VALUES ('320581', '常熟市', '78', '777');
INSERT INTO `regionarea` VALUES ('320582', '张家港市', '78', '778');
INSERT INTO `regionarea` VALUES ('320583', '昆山市', '78', '779');
INSERT INTO `regionarea` VALUES ('320585', '太仓市', '78', '780');
INSERT INTO `regionarea` VALUES ('320602', '崇川区', '79', '781');
INSERT INTO `regionarea` VALUES ('320611', '港闸区', '79', '782');
INSERT INTO `regionarea` VALUES ('320612', '通州区', '79', '783');
INSERT INTO `regionarea` VALUES ('320621', '海安县', '79', '784');
INSERT INTO `regionarea` VALUES ('320623', '如东县', '79', '785');
INSERT INTO `regionarea` VALUES ('320681', '启东市', '79', '786');
INSERT INTO `regionarea` VALUES ('320682', '如皋市', '79', '787');
INSERT INTO `regionarea` VALUES ('320684', '海门市', '79', '788');
INSERT INTO `regionarea` VALUES ('320703', '连云区', '80', '789');
INSERT INTO `regionarea` VALUES ('320705', '新浦区', '80', '790');
INSERT INTO `regionarea` VALUES ('320706', '海州区', '80', '791');
INSERT INTO `regionarea` VALUES ('320721', '赣榆县', '80', '792');
INSERT INTO `regionarea` VALUES ('320722', '东海县', '80', '793');
INSERT INTO `regionarea` VALUES ('320723', '灌云县', '80', '794');
INSERT INTO `regionarea` VALUES ('320724', '灌南县', '80', '795');
INSERT INTO `regionarea` VALUES ('320802', '清河区', '81', '796');
INSERT INTO `regionarea` VALUES ('320803', '淮安区', '81', '797');
INSERT INTO `regionarea` VALUES ('320804', '淮阴区', '81', '798');
INSERT INTO `regionarea` VALUES ('320811', '青浦区', '81', '799');
INSERT INTO `regionarea` VALUES ('320826', '涟水县', '81', '800');
INSERT INTO `regionarea` VALUES ('320829', '洪泽县', '81', '801');
INSERT INTO `regionarea` VALUES ('320830', '盱眙县', '81', '802');
INSERT INTO `regionarea` VALUES ('320831', '金湖县', '81', '803');
INSERT INTO `regionarea` VALUES ('320902', '亭湖区', '82', '804');
INSERT INTO `regionarea` VALUES ('320903', '盐都区', '82', '805');
INSERT INTO `regionarea` VALUES ('320921', '响水县', '82', '806');
INSERT INTO `regionarea` VALUES ('320922', '滨海县', '82', '807');
INSERT INTO `regionarea` VALUES ('320923', '阜宁县', '82', '808');
INSERT INTO `regionarea` VALUES ('320924', '射阳县', '82', '809');
INSERT INTO `regionarea` VALUES ('320925', '建湖县', '82', '810');
INSERT INTO `regionarea` VALUES ('320981', '东台市', '82', '811');
INSERT INTO `regionarea` VALUES ('320982', '大丰市', '82', '812');
INSERT INTO `regionarea` VALUES ('321002', '广陵区', '83', '813');
INSERT INTO `regionarea` VALUES ('321003', '邗江区', '83', '814');
INSERT INTO `regionarea` VALUES ('321088', '江都市', '83', '815');
INSERT INTO `regionarea` VALUES ('321023', '宝应县', '83', '816');
INSERT INTO `regionarea` VALUES ('321081', '仪征市', '83', '817');
INSERT INTO `regionarea` VALUES ('321084', '高邮市', '83', '818');
INSERT INTO `regionarea` VALUES ('321102', '京口区', '84', '819');
INSERT INTO `regionarea` VALUES ('321111', '润州区', '84', '820');
INSERT INTO `regionarea` VALUES ('321112', '丹徒区', '84', '821');
INSERT INTO `regionarea` VALUES ('321181', '丹阳市', '84', '822');
INSERT INTO `regionarea` VALUES ('321182', '扬中市', '84', '823');
INSERT INTO `regionarea` VALUES ('321183', '句容市', '84', '824');
INSERT INTO `regionarea` VALUES ('321202', '海陵区', '85', '825');
INSERT INTO `regionarea` VALUES ('321203', '高港区', '85', '826');
INSERT INTO `regionarea` VALUES ('321281', '兴化市', '85', '827');
INSERT INTO `regionarea` VALUES ('321282', '靖江市', '85', '828');
INSERT INTO `regionarea` VALUES ('321283', '泰兴市', '85', '829');
INSERT INTO `regionarea` VALUES ('321284', '姜堰市', '85', '830');
INSERT INTO `regionarea` VALUES ('321302', '宿城区', '86', '831');
INSERT INTO `regionarea` VALUES ('321311', '宿豫区', '86', '832');
INSERT INTO `regionarea` VALUES ('321322', '沭阳县', '86', '833');
INSERT INTO `regionarea` VALUES ('321323', '泗阳县', '86', '834');
INSERT INTO `regionarea` VALUES ('321324', '泗洪县', '86', '835');
INSERT INTO `regionarea` VALUES ('330102', '上城区', '87', '836');
INSERT INTO `regionarea` VALUES ('330103', '下城区', '87', '837');
INSERT INTO `regionarea` VALUES ('330104', '江干区', '87', '838');
INSERT INTO `regionarea` VALUES ('330105', '拱墅区', '87', '839');
INSERT INTO `regionarea` VALUES ('330106', '西湖区', '87', '840');
INSERT INTO `regionarea` VALUES ('330108', '滨江区', '87', '841');
INSERT INTO `regionarea` VALUES ('330109', '萧山区', '87', '842');
INSERT INTO `regionarea` VALUES ('330110', '余杭区', '87', '843');
INSERT INTO `regionarea` VALUES ('330122', '桐庐县', '87', '844');
INSERT INTO `regionarea` VALUES ('330127', '淳安县', '87', '845');
INSERT INTO `regionarea` VALUES ('330182', '建德市', '87', '846');
INSERT INTO `regionarea` VALUES ('330183', '富阳市', '87', '847');
INSERT INTO `regionarea` VALUES ('330185', '临安市', '87', '848');
INSERT INTO `regionarea` VALUES ('330203', '海曙区', '88', '849');
INSERT INTO `regionarea` VALUES ('330204', '江东区', '88', '850');
INSERT INTO `regionarea` VALUES ('330205', '江北区', '88', '851');
INSERT INTO `regionarea` VALUES ('330206', '北仑区', '88', '852');
INSERT INTO `regionarea` VALUES ('330211', '镇海区', '88', '853');
INSERT INTO `regionarea` VALUES ('330212', '鄞州区', '88', '854');
INSERT INTO `regionarea` VALUES ('330225', '象山县', '88', '855');
INSERT INTO `regionarea` VALUES ('330226', '宁海县', '88', '856');
INSERT INTO `regionarea` VALUES ('330281', '余姚市', '88', '857');
INSERT INTO `regionarea` VALUES ('330282', '慈溪市', '88', '858');
INSERT INTO `regionarea` VALUES ('330283', '奉化市', '88', '859');
INSERT INTO `regionarea` VALUES ('330302', '鹿城区', '89', '860');
INSERT INTO `regionarea` VALUES ('330303', '龙湾区', '89', '861');
INSERT INTO `regionarea` VALUES ('330304', '瓯海区', '89', '862');
INSERT INTO `regionarea` VALUES ('330322', '洞头县', '89', '863');
INSERT INTO `regionarea` VALUES ('330324', '永嘉县', '89', '864');
INSERT INTO `regionarea` VALUES ('330326', '平阳县', '89', '865');
INSERT INTO `regionarea` VALUES ('330327', '苍南县', '89', '866');
INSERT INTO `regionarea` VALUES ('330328', '文成县', '89', '867');
INSERT INTO `regionarea` VALUES ('330329', '泰顺县', '89', '868');
INSERT INTO `regionarea` VALUES ('330381', '瑞安市', '89', '869');
INSERT INTO `regionarea` VALUES ('330382', '乐清市', '89', '870');
INSERT INTO `regionarea` VALUES ('330402', '南湖区', '90', '871');
INSERT INTO `regionarea` VALUES ('330411', '秀洲区', '90', '872');
INSERT INTO `regionarea` VALUES ('330421', '嘉善县', '90', '873');
INSERT INTO `regionarea` VALUES ('330424', '海盐县', '90', '874');
INSERT INTO `regionarea` VALUES ('330481', '海宁市', '90', '875');
INSERT INTO `regionarea` VALUES ('330482', '平湖市', '90', '876');
INSERT INTO `regionarea` VALUES ('330483', '桐乡市', '90', '877');
INSERT INTO `regionarea` VALUES ('330502', '吴兴区', '91', '878');
INSERT INTO `regionarea` VALUES ('330503', '南浔区', '91', '879');
INSERT INTO `regionarea` VALUES ('330521', '德清县', '91', '880');
INSERT INTO `regionarea` VALUES ('330522', '长兴县', '91', '881');
INSERT INTO `regionarea` VALUES ('330523', '安吉县', '91', '882');
INSERT INTO `regionarea` VALUES ('330602', '越城区', '92', '883');
INSERT INTO `regionarea` VALUES ('330621', '绍兴县', '92', '884');
INSERT INTO `regionarea` VALUES ('330624', '新昌县', '92', '885');
INSERT INTO `regionarea` VALUES ('330681', '诸暨市', '92', '886');
INSERT INTO `regionarea` VALUES ('330682', '上虞市', '92', '887');
INSERT INTO `regionarea` VALUES ('330683', '嵊州市', '92', '888');
INSERT INTO `regionarea` VALUES ('330702', '婺城区', '93', '889');
INSERT INTO `regionarea` VALUES ('330703', '金东区', '93', '890');
INSERT INTO `regionarea` VALUES ('330723', '武义县', '93', '891');
INSERT INTO `regionarea` VALUES ('330726', '浦江县', '93', '892');
INSERT INTO `regionarea` VALUES ('330727', '磐安县', '93', '893');
INSERT INTO `regionarea` VALUES ('330781', '兰溪市', '93', '894');
INSERT INTO `regionarea` VALUES ('330782', '义乌市', '93', '895');
INSERT INTO `regionarea` VALUES ('330783', '东阳市', '93', '896');
INSERT INTO `regionarea` VALUES ('330784', '永康市', '93', '897');
INSERT INTO `regionarea` VALUES ('330802', '柯城区', '94', '898');
INSERT INTO `regionarea` VALUES ('330803', '衢江区', '94', '899');
INSERT INTO `regionarea` VALUES ('330822', '常山县', '94', '900');
INSERT INTO `regionarea` VALUES ('330824', '开化县', '94', '901');
INSERT INTO `regionarea` VALUES ('330825', '龙游县', '94', '902');
INSERT INTO `regionarea` VALUES ('330881', '江山市', '94', '903');
INSERT INTO `regionarea` VALUES ('330902', '定海区', '95', '904');
INSERT INTO `regionarea` VALUES ('330903', '普陀区', '95', '905');
INSERT INTO `regionarea` VALUES ('330921', '岱山县', '95', '906');
INSERT INTO `regionarea` VALUES ('330922', '嵊泗县', '95', '907');
INSERT INTO `regionarea` VALUES ('331002', '椒江区', '96', '908');
INSERT INTO `regionarea` VALUES ('331003', '黄岩区', '96', '909');
INSERT INTO `regionarea` VALUES ('331004', '路桥区', '96', '910');
INSERT INTO `regionarea` VALUES ('331021', '玉环县', '96', '911');
INSERT INTO `regionarea` VALUES ('331022', '三门县', '96', '912');
INSERT INTO `regionarea` VALUES ('331023', '天台县', '96', '913');
INSERT INTO `regionarea` VALUES ('331024', '仙居县', '96', '914');
INSERT INTO `regionarea` VALUES ('331081', '温岭市', '96', '915');
INSERT INTO `regionarea` VALUES ('331082', '临海市', '96', '916');
INSERT INTO `regionarea` VALUES ('331102', '莲都区', '97', '917');
INSERT INTO `regionarea` VALUES ('331121', '青田县', '97', '918');
INSERT INTO `regionarea` VALUES ('331122', '缙云县', '97', '919');
INSERT INTO `regionarea` VALUES ('331123', '遂昌县', '97', '920');
INSERT INTO `regionarea` VALUES ('331124', '松阳县', '97', '921');
INSERT INTO `regionarea` VALUES ('331125', '云和县', '97', '922');
INSERT INTO `regionarea` VALUES ('331126', '庆元县', '97', '923');
INSERT INTO `regionarea` VALUES ('331127', '景宁畲族自治县', '97', '924');
INSERT INTO `regionarea` VALUES ('331181', '龙泉市', '97', '925');
INSERT INTO `regionarea` VALUES ('340102', '瑶海区', '98', '926');
INSERT INTO `regionarea` VALUES ('340103', '庐阳区', '98', '927');
INSERT INTO `regionarea` VALUES ('340104', '蜀山区', '98', '928');
INSERT INTO `regionarea` VALUES ('340111', '包河区', '98', '929');
INSERT INTO `regionarea` VALUES ('340121', '长丰县', '98', '930');
INSERT INTO `regionarea` VALUES ('340122', '肥东县', '98', '931');
INSERT INTO `regionarea` VALUES ('340123', '肥西县', '98', '932');
INSERT INTO `regionarea` VALUES ('340124', '庐江县', '98', '933');
INSERT INTO `regionarea` VALUES ('340181', '巢湖市', '98', '934');
INSERT INTO `regionarea` VALUES ('340202', '镜湖区', '99', '935');
INSERT INTO `regionarea` VALUES ('340203', '弋江区', '99', '936');
INSERT INTO `regionarea` VALUES ('340207', '鸠江区', '99', '937');
INSERT INTO `regionarea` VALUES ('340208', '三山区', '99', '938');
INSERT INTO `regionarea` VALUES ('340221', '芜湖县', '99', '939');
INSERT INTO `regionarea` VALUES ('340222', '繁昌县', '99', '940');
INSERT INTO `regionarea` VALUES ('340223', '南陵县', '99', '941');
INSERT INTO `regionarea` VALUES ('340225', '无为县', '99', '942');
INSERT INTO `regionarea` VALUES ('340302', '龙子湖区', '100', '943');
INSERT INTO `regionarea` VALUES ('340303', '蚌山区', '100', '944');
INSERT INTO `regionarea` VALUES ('340304', '禹会区', '100', '945');
INSERT INTO `regionarea` VALUES ('340311', '淮上区', '100', '946');
INSERT INTO `regionarea` VALUES ('340321', '怀远县', '100', '947');
INSERT INTO `regionarea` VALUES ('340322', '五河县', '100', '948');
INSERT INTO `regionarea` VALUES ('340323', '固镇县', '100', '949');
INSERT INTO `regionarea` VALUES ('340402', '大通区', '101', '950');
INSERT INTO `regionarea` VALUES ('340403', '田家庵区', '101', '951');
INSERT INTO `regionarea` VALUES ('340404', '谢家集区', '101', '952');
INSERT INTO `regionarea` VALUES ('340405', '八公山区', '101', '953');
INSERT INTO `regionarea` VALUES ('340406', '潘集区', '101', '954');
INSERT INTO `regionarea` VALUES ('340421', '凤台县', '101', '955');
INSERT INTO `regionarea` VALUES ('340503', '花山区', '102', '956');
INSERT INTO `regionarea` VALUES ('340504', '雨山区', '102', '957');
INSERT INTO `regionarea` VALUES ('340596', '博望区', '102', '958');
INSERT INTO `regionarea` VALUES ('340521', '当涂县', '102', '959');
INSERT INTO `regionarea` VALUES ('340522', '含山县', '102', '960');
INSERT INTO `regionarea` VALUES ('340523', '和县', '102', '961');
INSERT INTO `regionarea` VALUES ('340602', '杜集区', '103', '962');
INSERT INTO `regionarea` VALUES ('340603', '相山区', '103', '963');
INSERT INTO `regionarea` VALUES ('340604', '烈山区', '103', '964');
INSERT INTO `regionarea` VALUES ('340621', '濉溪县', '103', '965');
INSERT INTO `regionarea` VALUES ('340702', '铜官山区', '104', '966');
INSERT INTO `regionarea` VALUES ('340703', '狮子山区', '104', '967');
INSERT INTO `regionarea` VALUES ('340711', '郊区', '104', '968');
INSERT INTO `regionarea` VALUES ('340721', '铜陵县', '104', '969');
INSERT INTO `regionarea` VALUES ('340802', '迎江区', '105', '970');
INSERT INTO `regionarea` VALUES ('340803', '大观区', '105', '971');
INSERT INTO `regionarea` VALUES ('340811', '宜秀区', '105', '972');
INSERT INTO `regionarea` VALUES ('340822', '怀宁县', '105', '973');
INSERT INTO `regionarea` VALUES ('340823', '枞阳县', '105', '974');
INSERT INTO `regionarea` VALUES ('340824', '潜山县', '105', '975');
INSERT INTO `regionarea` VALUES ('340825', '太湖县', '105', '976');
INSERT INTO `regionarea` VALUES ('340826', '宿松县', '105', '977');
INSERT INTO `regionarea` VALUES ('340827', '望江县', '105', '978');
INSERT INTO `regionarea` VALUES ('340828', '岳西县', '105', '979');
INSERT INTO `regionarea` VALUES ('340881', '桐城市', '105', '980');
INSERT INTO `regionarea` VALUES ('341002', '屯溪区', '106', '981');
INSERT INTO `regionarea` VALUES ('341003', '黄山区', '106', '982');
INSERT INTO `regionarea` VALUES ('341004', '徽州区', '106', '983');
INSERT INTO `regionarea` VALUES ('341021', '歙县', '106', '984');
INSERT INTO `regionarea` VALUES ('341022', '休宁县', '106', '985');
INSERT INTO `regionarea` VALUES ('341023', '黟县', '106', '986');
INSERT INTO `regionarea` VALUES ('341024', '祁门县', '106', '987');
INSERT INTO `regionarea` VALUES ('341102', '琅琊区', '107', '988');
INSERT INTO `regionarea` VALUES ('341103', '南谯区', '107', '989');
INSERT INTO `regionarea` VALUES ('341122', '来安县', '107', '990');
INSERT INTO `regionarea` VALUES ('341124', '全椒县', '107', '991');
INSERT INTO `regionarea` VALUES ('341125', '定远县', '107', '992');
INSERT INTO `regionarea` VALUES ('341126', '凤阳县', '107', '993');
INSERT INTO `regionarea` VALUES ('341181', '天长市', '107', '994');
INSERT INTO `regionarea` VALUES ('341182', '明光市', '107', '995');
INSERT INTO `regionarea` VALUES ('341202', '颍州区', '108', '996');
INSERT INTO `regionarea` VALUES ('341203', '颍东区', '108', '997');
INSERT INTO `regionarea` VALUES ('341204', '颍泉区', '108', '998');
INSERT INTO `regionarea` VALUES ('341221', '临泉县', '108', '999');
INSERT INTO `regionarea` VALUES ('341222', '太和县', '108', '1000');
INSERT INTO `regionarea` VALUES ('341225', '阜南县', '108', '1001');
INSERT INTO `regionarea` VALUES ('341226', '颍上县', '108', '1002');
INSERT INTO `regionarea` VALUES ('341282', '界首市', '108', '1003');
INSERT INTO `regionarea` VALUES ('341302', '埇桥区', '109', '1004');
INSERT INTO `regionarea` VALUES ('341321', '砀山县', '109', '1005');
INSERT INTO `regionarea` VALUES ('341322', '萧县', '109', '1006');
INSERT INTO `regionarea` VALUES ('341323', '灵璧县', '109', '1007');
INSERT INTO `regionarea` VALUES ('341324', '泗县', '109', '1008');
INSERT INTO `regionarea` VALUES ('341502', '金安区', '110', '1009');
INSERT INTO `regionarea` VALUES ('341503', '裕安区', '110', '1010');
INSERT INTO `regionarea` VALUES ('341521', '寿县', '110', '1011');
INSERT INTO `regionarea` VALUES ('341522', '霍邱县', '110', '1012');
INSERT INTO `regionarea` VALUES ('341523', '舒城县', '110', '1013');
INSERT INTO `regionarea` VALUES ('341524', '金寨县', '110', '1014');
INSERT INTO `regionarea` VALUES ('341525', '霍山县', '110', '1015');
INSERT INTO `regionarea` VALUES ('341602', '谯城区', '111', '1016');
INSERT INTO `regionarea` VALUES ('341621', '涡阳县', '111', '1017');
INSERT INTO `regionarea` VALUES ('341622', '蒙城县', '111', '1018');
INSERT INTO `regionarea` VALUES ('341623', '利辛县', '111', '1019');
INSERT INTO `regionarea` VALUES ('341702', '贵池区', '112', '1020');
INSERT INTO `regionarea` VALUES ('341721', '东至县', '112', '1021');
INSERT INTO `regionarea` VALUES ('341722', '石台县', '112', '1022');
INSERT INTO `regionarea` VALUES ('341723', '青阳县', '112', '1023');
INSERT INTO `regionarea` VALUES ('341802', '宣州区', '113', '1024');
INSERT INTO `regionarea` VALUES ('341821', '郎溪县', '113', '1025');
INSERT INTO `regionarea` VALUES ('341822', '广德县', '113', '1026');
INSERT INTO `regionarea` VALUES ('341823', '泾县', '113', '1027');
INSERT INTO `regionarea` VALUES ('341824', '绩溪县', '113', '1028');
INSERT INTO `regionarea` VALUES ('341825', '旌德县', '113', '1029');
INSERT INTO `regionarea` VALUES ('341881', '宁国市', '113', '1030');
INSERT INTO `regionarea` VALUES ('350102', '鼓楼区', '114', '1031');
INSERT INTO `regionarea` VALUES ('350103', '台江区', '114', '1032');
INSERT INTO `regionarea` VALUES ('350104', '仓山区', '114', '1033');
INSERT INTO `regionarea` VALUES ('350105', '马尾区', '114', '1034');
INSERT INTO `regionarea` VALUES ('350111', '晋安区', '114', '1035');
INSERT INTO `regionarea` VALUES ('350121', '闽侯县', '114', '1036');
INSERT INTO `regionarea` VALUES ('350122', '连江县', '114', '1037');
INSERT INTO `regionarea` VALUES ('350123', '罗源县', '114', '1038');
INSERT INTO `regionarea` VALUES ('350124', '闽清县', '114', '1039');
INSERT INTO `regionarea` VALUES ('350125', '永泰县', '114', '1040');
INSERT INTO `regionarea` VALUES ('350128', '平潭县', '114', '1041');
INSERT INTO `regionarea` VALUES ('350181', '福清市', '114', '1042');
INSERT INTO `regionarea` VALUES ('350182', '长乐市', '114', '1043');
INSERT INTO `regionarea` VALUES ('350203', '思明区', '115', '1044');
INSERT INTO `regionarea` VALUES ('350205', '海沧区', '115', '1045');
INSERT INTO `regionarea` VALUES ('350206', '湖里区', '115', '1046');
INSERT INTO `regionarea` VALUES ('350211', '集美区', '115', '1047');
INSERT INTO `regionarea` VALUES ('350212', '同安区', '115', '1048');
INSERT INTO `regionarea` VALUES ('350213', '翔安区', '115', '1049');
INSERT INTO `regionarea` VALUES ('350302', '城厢区', '116', '1050');
INSERT INTO `regionarea` VALUES ('350303', '涵江区', '116', '1051');
INSERT INTO `regionarea` VALUES ('350304', '荔城区', '116', '1052');
INSERT INTO `regionarea` VALUES ('350305', '秀屿区', '116', '1053');
INSERT INTO `regionarea` VALUES ('350322', '仙游县', '116', '1054');
INSERT INTO `regionarea` VALUES ('350402', '梅列区', '117', '1055');
INSERT INTO `regionarea` VALUES ('350403', '三元区', '117', '1056');
INSERT INTO `regionarea` VALUES ('350421', '明溪县', '117', '1057');
INSERT INTO `regionarea` VALUES ('350423', '清流县', '117', '1058');
INSERT INTO `regionarea` VALUES ('350424', '宁化县', '117', '1059');
INSERT INTO `regionarea` VALUES ('350425', '大田县', '117', '1060');
INSERT INTO `regionarea` VALUES ('350426', '尤溪县', '117', '1061');
INSERT INTO `regionarea` VALUES ('350427', '沙县', '117', '1062');
INSERT INTO `regionarea` VALUES ('350428', '将乐县', '117', '1063');
INSERT INTO `regionarea` VALUES ('350429', '泰宁县', '117', '1064');
INSERT INTO `regionarea` VALUES ('350430', '建宁县', '117', '1065');
INSERT INTO `regionarea` VALUES ('350481', '永安市', '117', '1066');
INSERT INTO `regionarea` VALUES ('350502', '鲤城区', '118', '1067');
INSERT INTO `regionarea` VALUES ('350503', '丰泽区', '118', '1068');
INSERT INTO `regionarea` VALUES ('350504', '洛江区', '118', '1069');
INSERT INTO `regionarea` VALUES ('350505', '泉港区', '118', '1070');
INSERT INTO `regionarea` VALUES ('350521', '惠安县', '118', '1071');
INSERT INTO `regionarea` VALUES ('350524', '安溪县', '118', '1072');
INSERT INTO `regionarea` VALUES ('350525', '永春县', '118', '1073');
INSERT INTO `regionarea` VALUES ('350526', '德化县', '118', '1074');
INSERT INTO `regionarea` VALUES ('350527', '金门县', '118', '1075');
INSERT INTO `regionarea` VALUES ('350581', '石狮市', '118', '1076');
INSERT INTO `regionarea` VALUES ('350582', '晋江市', '118', '1077');
INSERT INTO `regionarea` VALUES ('350583', '南安市', '118', '1078');
INSERT INTO `regionarea` VALUES ('350602', '芗城区', '119', '1079');
INSERT INTO `regionarea` VALUES ('350603', '龙文区', '119', '1080');
INSERT INTO `regionarea` VALUES ('350622', '云霄县', '119', '1081');
INSERT INTO `regionarea` VALUES ('350623', '漳浦县', '119', '1082');
INSERT INTO `regionarea` VALUES ('350624', '诏安县', '119', '1083');
INSERT INTO `regionarea` VALUES ('350625', '长泰县', '119', '1084');
INSERT INTO `regionarea` VALUES ('350626', '东山县', '119', '1085');
INSERT INTO `regionarea` VALUES ('350627', '南靖县', '119', '1086');
INSERT INTO `regionarea` VALUES ('350628', '平和县', '119', '1087');
INSERT INTO `regionarea` VALUES ('350629', '华安县', '119', '1088');
INSERT INTO `regionarea` VALUES ('350681', '龙海市', '119', '1089');
INSERT INTO `regionarea` VALUES ('350702', '延平区', '120', '1090');
INSERT INTO `regionarea` VALUES ('350721', '顺昌县', '120', '1091');
INSERT INTO `regionarea` VALUES ('350722', '浦城县', '120', '1092');
INSERT INTO `regionarea` VALUES ('350723', '光泽县', '120', '1093');
INSERT INTO `regionarea` VALUES ('350724', '松溪县', '120', '1094');
INSERT INTO `regionarea` VALUES ('350725', '政和县', '120', '1095');
INSERT INTO `regionarea` VALUES ('350781', '邵武市', '120', '1096');
INSERT INTO `regionarea` VALUES ('350782', '武夷山市', '120', '1097');
INSERT INTO `regionarea` VALUES ('350783', '建瓯市', '120', '1098');
INSERT INTO `regionarea` VALUES ('350784', '建阳市', '120', '1099');
INSERT INTO `regionarea` VALUES ('350802', '新罗区', '121', '1100');
INSERT INTO `regionarea` VALUES ('350821', '长汀县', '121', '1101');
INSERT INTO `regionarea` VALUES ('350822', '永定县', '121', '1102');
INSERT INTO `regionarea` VALUES ('350823', '上杭县', '121', '1103');
INSERT INTO `regionarea` VALUES ('350824', '武平县', '121', '1104');
INSERT INTO `regionarea` VALUES ('350825', '连城县', '121', '1105');
INSERT INTO `regionarea` VALUES ('350881', '漳平市', '121', '1106');
INSERT INTO `regionarea` VALUES ('350902', '蕉城区', '122', '1107');
INSERT INTO `regionarea` VALUES ('350921', '霞浦县', '122', '1108');
INSERT INTO `regionarea` VALUES ('350922', '古田县', '122', '1109');
INSERT INTO `regionarea` VALUES ('350923', '屏南县', '122', '1110');
INSERT INTO `regionarea` VALUES ('350924', '寿宁县', '122', '1111');
INSERT INTO `regionarea` VALUES ('350925', '周宁县', '122', '1112');
INSERT INTO `regionarea` VALUES ('350926', '柘荣县', '122', '1113');
INSERT INTO `regionarea` VALUES ('350981', '福安市', '122', '1114');
INSERT INTO `regionarea` VALUES ('350982', '福鼎市', '122', '1115');
INSERT INTO `regionarea` VALUES ('360102', '东湖区', '123', '1116');
INSERT INTO `regionarea` VALUES ('360103', '西湖区', '123', '1117');
INSERT INTO `regionarea` VALUES ('360104', '青云谱区', '123', '1118');
INSERT INTO `regionarea` VALUES ('360105', '湾里区', '123', '1119');
INSERT INTO `regionarea` VALUES ('360111', '青山湖区', '123', '1120');
INSERT INTO `regionarea` VALUES ('360121', '南昌县', '123', '1121');
INSERT INTO `regionarea` VALUES ('360122', '新建县', '123', '1122');
INSERT INTO `regionarea` VALUES ('360123', '安义县', '123', '1123');
INSERT INTO `regionarea` VALUES ('360124', '进贤县', '123', '1124');
INSERT INTO `regionarea` VALUES ('360202', '昌江区', '124', '1125');
INSERT INTO `regionarea` VALUES ('360203', '珠山区', '124', '1126');
INSERT INTO `regionarea` VALUES ('360222', '浮梁县', '124', '1127');
INSERT INTO `regionarea` VALUES ('360281', '乐平市', '124', '1128');
INSERT INTO `regionarea` VALUES ('360302', '安源区', '125', '1129');
INSERT INTO `regionarea` VALUES ('360313', '湘东区', '125', '1130');
INSERT INTO `regionarea` VALUES ('360321', '莲花县', '125', '1131');
INSERT INTO `regionarea` VALUES ('360322', '上栗县', '125', '1132');
INSERT INTO `regionarea` VALUES ('360323', '芦溪县', '125', '1133');
INSERT INTO `regionarea` VALUES ('360402', '庐山区', '126', '1134');
INSERT INTO `regionarea` VALUES ('360403', '浔阳区', '126', '1135');
INSERT INTO `regionarea` VALUES ('360421', '九江县', '126', '1136');
INSERT INTO `regionarea` VALUES ('360423', '武宁县', '126', '1137');
INSERT INTO `regionarea` VALUES ('360424', '修水县', '126', '1138');
INSERT INTO `regionarea` VALUES ('360425', '永修县', '126', '1139');
INSERT INTO `regionarea` VALUES ('360426', '德安县', '126', '1140');
INSERT INTO `regionarea` VALUES ('360427', '星子县', '126', '1141');
INSERT INTO `regionarea` VALUES ('360428', '都昌县', '126', '1142');
INSERT INTO `regionarea` VALUES ('360429', '湖口县', '126', '1143');
INSERT INTO `regionarea` VALUES ('360430', '彭泽县', '126', '1144');
INSERT INTO `regionarea` VALUES ('360481', '瑞昌市', '126', '1145');
INSERT INTO `regionarea` VALUES ('360482', '共青城市', '126', '1146');
INSERT INTO `regionarea` VALUES ('360502', '渝水区', '127', '1147');
INSERT INTO `regionarea` VALUES ('360521', '分宜县', '127', '1148');
INSERT INTO `regionarea` VALUES ('360602', '月湖区', '128', '1149');
INSERT INTO `regionarea` VALUES ('360622', '余江县', '128', '1150');
INSERT INTO `regionarea` VALUES ('360681', '贵溪市', '128', '1151');
INSERT INTO `regionarea` VALUES ('360702', '章贡区', '129', '1152');
INSERT INTO `regionarea` VALUES ('360721', '赣县', '129', '1153');
INSERT INTO `regionarea` VALUES ('360722', '信丰县', '129', '1154');
INSERT INTO `regionarea` VALUES ('360723', '大余县', '129', '1155');
INSERT INTO `regionarea` VALUES ('360724', '上犹县', '129', '1156');
INSERT INTO `regionarea` VALUES ('360725', '崇义县', '129', '1157');
INSERT INTO `regionarea` VALUES ('360726', '安远县', '129', '1158');
INSERT INTO `regionarea` VALUES ('360727', '龙南县', '129', '1159');
INSERT INTO `regionarea` VALUES ('360728', '定南县', '129', '1160');
INSERT INTO `regionarea` VALUES ('360729', '全南县', '129', '1161');
INSERT INTO `regionarea` VALUES ('360730', '宁都县', '129', '1162');
INSERT INTO `regionarea` VALUES ('360731', '于都县', '129', '1163');
INSERT INTO `regionarea` VALUES ('360732', '兴国县', '129', '1164');
INSERT INTO `regionarea` VALUES ('360733', '会昌县', '129', '1165');
INSERT INTO `regionarea` VALUES ('360734', '寻乌县', '129', '1166');
INSERT INTO `regionarea` VALUES ('360735', '石城县', '129', '1167');
INSERT INTO `regionarea` VALUES ('360781', '瑞金市', '129', '1168');
INSERT INTO `regionarea` VALUES ('360782', '南康市', '129', '1169');
INSERT INTO `regionarea` VALUES ('360802', '吉州区', '130', '1170');
INSERT INTO `regionarea` VALUES ('360803', '青原区', '130', '1171');
INSERT INTO `regionarea` VALUES ('360821', '吉安县', '130', '1172');
INSERT INTO `regionarea` VALUES ('360822', '吉水县', '130', '1173');
INSERT INTO `regionarea` VALUES ('360823', '峡江县', '130', '1174');
INSERT INTO `regionarea` VALUES ('360824', '新干县', '130', '1175');
INSERT INTO `regionarea` VALUES ('360825', '永丰县', '130', '1176');
INSERT INTO `regionarea` VALUES ('360826', '泰和县', '130', '1177');
INSERT INTO `regionarea` VALUES ('360827', '遂川县', '130', '1178');
INSERT INTO `regionarea` VALUES ('360828', '万安县', '130', '1179');
INSERT INTO `regionarea` VALUES ('360829', '安福县', '130', '1180');
INSERT INTO `regionarea` VALUES ('360830', '永新县', '130', '1181');
INSERT INTO `regionarea` VALUES ('360881', '井冈山市', '130', '1182');
INSERT INTO `regionarea` VALUES ('360902', '袁州区', '131', '1183');
INSERT INTO `regionarea` VALUES ('360921', '奉新县', '131', '1184');
INSERT INTO `regionarea` VALUES ('360922', '万载县', '131', '1185');
INSERT INTO `regionarea` VALUES ('360923', '上高县', '131', '1186');
INSERT INTO `regionarea` VALUES ('360924', '宜丰县', '131', '1187');
INSERT INTO `regionarea` VALUES ('360925', '靖安县', '131', '1188');
INSERT INTO `regionarea` VALUES ('360926', '铜鼓县', '131', '1189');
INSERT INTO `regionarea` VALUES ('360981', '丰城市', '131', '1190');
INSERT INTO `regionarea` VALUES ('360982', '樟树市', '131', '1191');
INSERT INTO `regionarea` VALUES ('360983', '高安市', '131', '1192');
INSERT INTO `regionarea` VALUES ('361002', '临川区', '132', '1193');
INSERT INTO `regionarea` VALUES ('361021', '南城县', '132', '1194');
INSERT INTO `regionarea` VALUES ('361022', '黎川县', '132', '1195');
INSERT INTO `regionarea` VALUES ('361023', '南丰县', '132', '1196');
INSERT INTO `regionarea` VALUES ('361024', '崇仁县', '132', '1197');
INSERT INTO `regionarea` VALUES ('361025', '乐安县', '132', '1198');
INSERT INTO `regionarea` VALUES ('361026', '宜黄县', '132', '1199');
INSERT INTO `regionarea` VALUES ('361027', '金溪县', '132', '1200');
INSERT INTO `regionarea` VALUES ('361028', '资溪县', '132', '1201');
INSERT INTO `regionarea` VALUES ('361029', '东乡县', '132', '1202');
INSERT INTO `regionarea` VALUES ('361030', '广昌县', '132', '1203');
INSERT INTO `regionarea` VALUES ('361102', '信州区', '133', '1204');
INSERT INTO `regionarea` VALUES ('361121', '上饶县', '133', '1205');
INSERT INTO `regionarea` VALUES ('361122', '广丰县', '133', '1206');
INSERT INTO `regionarea` VALUES ('361123', '玉山县', '133', '1207');
INSERT INTO `regionarea` VALUES ('361124', '铅山县', '133', '1208');
INSERT INTO `regionarea` VALUES ('361125', '横峰县', '133', '1209');
INSERT INTO `regionarea` VALUES ('361126', '弋阳县', '133', '1210');
INSERT INTO `regionarea` VALUES ('361127', '余干县', '133', '1211');
INSERT INTO `regionarea` VALUES ('361128', '鄱阳县', '133', '1212');
INSERT INTO `regionarea` VALUES ('361129', '万年县', '133', '1213');
INSERT INTO `regionarea` VALUES ('361130', '婺源县', '133', '1214');
INSERT INTO `regionarea` VALUES ('361181', '德兴市', '133', '1215');
INSERT INTO `regionarea` VALUES ('370102', '历下区', '134', '1216');
INSERT INTO `regionarea` VALUES ('370103', '市中区', '134', '1217');
INSERT INTO `regionarea` VALUES ('370104', '槐荫区', '134', '1218');
INSERT INTO `regionarea` VALUES ('370105', '天桥区', '134', '1219');
INSERT INTO `regionarea` VALUES ('370112', '历城区', '134', '1220');
INSERT INTO `regionarea` VALUES ('370113', '长清区', '134', '1221');
INSERT INTO `regionarea` VALUES ('370124', '平阴县', '134', '1222');
INSERT INTO `regionarea` VALUES ('370125', '济阳县', '134', '1223');
INSERT INTO `regionarea` VALUES ('370126', '商河县', '134', '1224');
INSERT INTO `regionarea` VALUES ('370181', '章丘市', '134', '1225');
INSERT INTO `regionarea` VALUES ('370202', '市南区', '135', '1226');
INSERT INTO `regionarea` VALUES ('370203', '市北区', '135', '1227');
INSERT INTO `regionarea` VALUES ('370205', '四方区', '135', '1228');
INSERT INTO `regionarea` VALUES ('370211', '黄岛区', '135', '1229');
INSERT INTO `regionarea` VALUES ('370212', '崂山区', '135', '1230');
INSERT INTO `regionarea` VALUES ('370213', '李沧区', '135', '1231');
INSERT INTO `regionarea` VALUES ('370214', '城阳区', '135', '1232');
INSERT INTO `regionarea` VALUES ('370281', '胶州市', '135', '1233');
INSERT INTO `regionarea` VALUES ('370282', '即墨市', '135', '1234');
INSERT INTO `regionarea` VALUES ('370283', '平度市', '135', '1235');
INSERT INTO `regionarea` VALUES ('370284', '胶南市', '135', '1236');
INSERT INTO `regionarea` VALUES ('370285', '莱西市', '135', '1237');
INSERT INTO `regionarea` VALUES ('370302', '淄川区', '136', '1238');
INSERT INTO `regionarea` VALUES ('370303', '张店区', '136', '1239');
INSERT INTO `regionarea` VALUES ('370304', '博山区', '136', '1240');
INSERT INTO `regionarea` VALUES ('370305', '临淄区', '136', '1241');
INSERT INTO `regionarea` VALUES ('370306', '周村区', '136', '1242');
INSERT INTO `regionarea` VALUES ('370321', '桓台县', '136', '1243');
INSERT INTO `regionarea` VALUES ('370322', '高青县', '136', '1244');
INSERT INTO `regionarea` VALUES ('370323', '沂源县', '136', '1245');
INSERT INTO `regionarea` VALUES ('370402', '市中区', '137', '1246');
INSERT INTO `regionarea` VALUES ('370403', '薛城区', '137', '1247');
INSERT INTO `regionarea` VALUES ('370404', '峄城区', '137', '1248');
INSERT INTO `regionarea` VALUES ('370405', '台儿庄区', '137', '1249');
INSERT INTO `regionarea` VALUES ('370406', '山亭区', '137', '1250');
INSERT INTO `regionarea` VALUES ('370481', '滕州市', '137', '1251');
INSERT INTO `regionarea` VALUES ('370502', '东营区', '138', '1252');
INSERT INTO `regionarea` VALUES ('370503', '河口区', '138', '1253');
INSERT INTO `regionarea` VALUES ('370521', '垦利县', '138', '1254');
INSERT INTO `regionarea` VALUES ('370522', '利津县', '138', '1255');
INSERT INTO `regionarea` VALUES ('370523', '广饶县', '138', '1256');
INSERT INTO `regionarea` VALUES ('370602', '芝罘区', '139', '1257');
INSERT INTO `regionarea` VALUES ('370611', '福山区', '139', '1258');
INSERT INTO `regionarea` VALUES ('370612', '牟平区', '139', '1259');
INSERT INTO `regionarea` VALUES ('370613', '莱山区', '139', '1260');
INSERT INTO `regionarea` VALUES ('370634', '长岛县', '139', '1261');
INSERT INTO `regionarea` VALUES ('370681', '龙口市', '139', '1262');
INSERT INTO `regionarea` VALUES ('370682', '莱阳市', '139', '1263');
INSERT INTO `regionarea` VALUES ('370683', '莱州市', '139', '1264');
INSERT INTO `regionarea` VALUES ('370684', '蓬莱市', '139', '1265');
INSERT INTO `regionarea` VALUES ('370685', '招远市', '139', '1266');
INSERT INTO `regionarea` VALUES ('370686', '栖霞市', '139', '1267');
INSERT INTO `regionarea` VALUES ('370687', '海阳市', '139', '1268');
INSERT INTO `regionarea` VALUES ('370702', '潍城区', '140', '1269');
INSERT INTO `regionarea` VALUES ('370703', '寒亭区', '140', '1270');
INSERT INTO `regionarea` VALUES ('370704', '坊子区', '140', '1271');
INSERT INTO `regionarea` VALUES ('370705', '奎文区', '140', '1272');
INSERT INTO `regionarea` VALUES ('370724', '临朐县', '140', '1273');
INSERT INTO `regionarea` VALUES ('370725', '昌乐县', '140', '1274');
INSERT INTO `regionarea` VALUES ('370781', '青州市', '140', '1275');
INSERT INTO `regionarea` VALUES ('370782', '诸城市', '140', '1276');
INSERT INTO `regionarea` VALUES ('370783', '寿光市', '140', '1277');
INSERT INTO `regionarea` VALUES ('370784', '安丘市', '140', '1278');
INSERT INTO `regionarea` VALUES ('370785', '高密市', '140', '1279');
INSERT INTO `regionarea` VALUES ('370786', '昌邑市', '140', '1280');
INSERT INTO `regionarea` VALUES ('370802', '市中区', '141', '1281');
INSERT INTO `regionarea` VALUES ('370811', '任城区', '141', '1282');
INSERT INTO `regionarea` VALUES ('370826', '微山县', '141', '1283');
INSERT INTO `regionarea` VALUES ('370827', '鱼台县', '141', '1284');
INSERT INTO `regionarea` VALUES ('370828', '金乡县', '141', '1285');
INSERT INTO `regionarea` VALUES ('370829', '嘉祥县', '141', '1286');
INSERT INTO `regionarea` VALUES ('370830', '汶上县', '141', '1287');
INSERT INTO `regionarea` VALUES ('370831', '泗水县', '141', '1288');
INSERT INTO `regionarea` VALUES ('370832', '梁山县', '141', '1289');
INSERT INTO `regionarea` VALUES ('370881', '曲阜市', '141', '1290');
INSERT INTO `regionarea` VALUES ('370882', '兖州市', '141', '1291');
INSERT INTO `regionarea` VALUES ('370883', '邹城市', '141', '1292');
INSERT INTO `regionarea` VALUES ('370902', '泰山区', '142', '1293');
INSERT INTO `regionarea` VALUES ('370911', '岱岳区', '142', '1294');
INSERT INTO `regionarea` VALUES ('370921', '宁阳县', '142', '1295');
INSERT INTO `regionarea` VALUES ('370923', '东平县', '142', '1296');
INSERT INTO `regionarea` VALUES ('370982', '新泰市', '142', '1297');
INSERT INTO `regionarea` VALUES ('370983', '肥城市', '142', '1298');
INSERT INTO `regionarea` VALUES ('371002', '环翠区', '143', '1299');
INSERT INTO `regionarea` VALUES ('371081', '文登市', '143', '1300');
INSERT INTO `regionarea` VALUES ('371082', '荣成市', '143', '1301');
INSERT INTO `regionarea` VALUES ('371083', '乳山市', '143', '1302');
INSERT INTO `regionarea` VALUES ('371102', '东港区', '144', '1303');
INSERT INTO `regionarea` VALUES ('371103', '岚山区', '144', '1304');
INSERT INTO `regionarea` VALUES ('371121', '五莲县', '144', '1305');
INSERT INTO `regionarea` VALUES ('371122', '莒县', '144', '1306');
INSERT INTO `regionarea` VALUES ('371202', '莱城区', '145', '1307');
INSERT INTO `regionarea` VALUES ('371203', '钢城区', '145', '1308');
INSERT INTO `regionarea` VALUES ('371302', '兰山区', '146', '1309');
INSERT INTO `regionarea` VALUES ('371311', '罗庄区', '146', '1310');
INSERT INTO `regionarea` VALUES ('371312', '河东区', '146', '1311');
INSERT INTO `regionarea` VALUES ('371321', '沂南县', '146', '1312');
INSERT INTO `regionarea` VALUES ('371322', '郯城县', '146', '1313');
INSERT INTO `regionarea` VALUES ('371323', '沂水县', '146', '1314');
INSERT INTO `regionarea` VALUES ('371324', '苍山县', '146', '1315');
INSERT INTO `regionarea` VALUES ('371325', '费县', '146', '1316');
INSERT INTO `regionarea` VALUES ('371326', '平邑县', '146', '1317');
INSERT INTO `regionarea` VALUES ('371327', '莒南县', '146', '1318');
INSERT INTO `regionarea` VALUES ('371328', '蒙阴县', '146', '1319');
INSERT INTO `regionarea` VALUES ('371329', '临沭县', '146', '1320');
INSERT INTO `regionarea` VALUES ('371402', '德城区', '147', '1321');
INSERT INTO `regionarea` VALUES ('371421', '陵县', '147', '1322');
INSERT INTO `regionarea` VALUES ('371422', '宁津县', '147', '1323');
INSERT INTO `regionarea` VALUES ('371423', '庆云县', '147', '1324');
INSERT INTO `regionarea` VALUES ('371424', '临邑县', '147', '1325');
INSERT INTO `regionarea` VALUES ('371425', '齐河县', '147', '1326');
INSERT INTO `regionarea` VALUES ('371426', '平原县', '147', '1327');
INSERT INTO `regionarea` VALUES ('371427', '夏津县', '147', '1328');
INSERT INTO `regionarea` VALUES ('371428', '武城县', '147', '1329');
INSERT INTO `regionarea` VALUES ('371481', '乐陵市', '147', '1330');
INSERT INTO `regionarea` VALUES ('371482', '禹城市', '147', '1331');
INSERT INTO `regionarea` VALUES ('371502', '东昌府区', '148', '1332');
INSERT INTO `regionarea` VALUES ('371521', '阳谷县', '148', '1333');
INSERT INTO `regionarea` VALUES ('371522', '莘县', '148', '1334');
INSERT INTO `regionarea` VALUES ('371523', '茌平县', '148', '1335');
INSERT INTO `regionarea` VALUES ('371524', '东阿县', '148', '1336');
INSERT INTO `regionarea` VALUES ('371525', '冠县', '148', '1337');
INSERT INTO `regionarea` VALUES ('371526', '高唐县', '148', '1338');
INSERT INTO `regionarea` VALUES ('371581', '临清市', '148', '1339');
INSERT INTO `regionarea` VALUES ('371602', '滨城区', '149', '1340');
INSERT INTO `regionarea` VALUES ('371621', '惠民县', '149', '1341');
INSERT INTO `regionarea` VALUES ('371622', '阳信县', '149', '1342');
INSERT INTO `regionarea` VALUES ('371623', '无棣县', '149', '1343');
INSERT INTO `regionarea` VALUES ('371624', '沾化县', '149', '1344');
INSERT INTO `regionarea` VALUES ('371625', '博兴县', '149', '1345');
INSERT INTO `regionarea` VALUES ('371626', '邹平县', '149', '1346');
INSERT INTO `regionarea` VALUES ('371702', '牡丹区', '150', '1347');
INSERT INTO `regionarea` VALUES ('371721', '曹县', '150', '1348');
INSERT INTO `regionarea` VALUES ('371722', '单县', '150', '1349');
INSERT INTO `regionarea` VALUES ('371723', '成武县', '150', '1350');
INSERT INTO `regionarea` VALUES ('371724', '巨野县', '150', '1351');
INSERT INTO `regionarea` VALUES ('371725', '郓城县', '150', '1352');
INSERT INTO `regionarea` VALUES ('371726', '鄄城县', '150', '1353');
INSERT INTO `regionarea` VALUES ('371727', '定陶县', '150', '1354');
INSERT INTO `regionarea` VALUES ('371728', '东明县', '150', '1355');
INSERT INTO `regionarea` VALUES ('410102', '中原区', '151', '1356');
INSERT INTO `regionarea` VALUES ('410103', '二七区', '151', '1357');
INSERT INTO `regionarea` VALUES ('410104', '管城回族区', '151', '1358');
INSERT INTO `regionarea` VALUES ('410105', '金水区', '151', '1359');
INSERT INTO `regionarea` VALUES ('410106', '上街区', '151', '1360');
INSERT INTO `regionarea` VALUES ('410108', '惠济区', '151', '1361');
INSERT INTO `regionarea` VALUES ('410122', '中牟县', '151', '1362');
INSERT INTO `regionarea` VALUES ('410181', '巩义市', '151', '1363');
INSERT INTO `regionarea` VALUES ('410182', '荥阳市', '151', '1364');
INSERT INTO `regionarea` VALUES ('410183', '新密市', '151', '1365');
INSERT INTO `regionarea` VALUES ('410184', '新郑市', '151', '1366');
INSERT INTO `regionarea` VALUES ('410185', '登封市', '151', '1367');
INSERT INTO `regionarea` VALUES ('410202', '龙亭区', '152', '1368');
INSERT INTO `regionarea` VALUES ('410203', '顺河回族区', '152', '1369');
INSERT INTO `regionarea` VALUES ('410204', '鼓楼区', '152', '1370');
INSERT INTO `regionarea` VALUES ('410205', '禹王台区', '152', '1371');
INSERT INTO `regionarea` VALUES ('410211', '金明区', '152', '1372');
INSERT INTO `regionarea` VALUES ('410221', '杞县', '152', '1373');
INSERT INTO `regionarea` VALUES ('410222', '通许县', '152', '1374');
INSERT INTO `regionarea` VALUES ('410223', '尉氏县', '152', '1375');
INSERT INTO `regionarea` VALUES ('410224', '开封县', '152', '1376');
INSERT INTO `regionarea` VALUES ('410225', '兰考县', '152', '1377');
INSERT INTO `regionarea` VALUES ('410302', '老城区', '153', '1378');
INSERT INTO `regionarea` VALUES ('410303', '西工区', '153', '1379');
INSERT INTO `regionarea` VALUES ('410304', '瀍河回族区', '153', '1380');
INSERT INTO `regionarea` VALUES ('410305', '涧西区', '153', '1381');
INSERT INTO `regionarea` VALUES ('410306', '吉利区', '153', '1382');
INSERT INTO `regionarea` VALUES ('410311', '洛龙区', '153', '1383');
INSERT INTO `regionarea` VALUES ('410322', '孟津县', '153', '1384');
INSERT INTO `regionarea` VALUES ('410323', '新安县', '153', '1385');
INSERT INTO `regionarea` VALUES ('410324', '栾川县', '153', '1386');
INSERT INTO `regionarea` VALUES ('410325', '嵩县', '153', '1387');
INSERT INTO `regionarea` VALUES ('410326', '汝阳县', '153', '1388');
INSERT INTO `regionarea` VALUES ('410327', '宜阳县', '153', '1389');
INSERT INTO `regionarea` VALUES ('410328', '洛宁县', '153', '1390');
INSERT INTO `regionarea` VALUES ('410329', '伊川县', '153', '1391');
INSERT INTO `regionarea` VALUES ('410381', '偃师市', '153', '1392');
INSERT INTO `regionarea` VALUES ('410402', '新华区', '154', '1393');
INSERT INTO `regionarea` VALUES ('410403', '卫东区', '154', '1394');
INSERT INTO `regionarea` VALUES ('410404', '石龙区', '154', '1395');
INSERT INTO `regionarea` VALUES ('410411', '湛河区', '154', '1396');
INSERT INTO `regionarea` VALUES ('410421', '宝丰县', '154', '1397');
INSERT INTO `regionarea` VALUES ('410422', '叶县', '154', '1398');
INSERT INTO `regionarea` VALUES ('410423', '鲁山县', '154', '1399');
INSERT INTO `regionarea` VALUES ('410425', '郏县', '154', '1400');
INSERT INTO `regionarea` VALUES ('410481', '舞钢市', '154', '1401');
INSERT INTO `regionarea` VALUES ('410482', '汝州市', '154', '1402');
INSERT INTO `regionarea` VALUES ('410502', '文峰区', '155', '1403');
INSERT INTO `regionarea` VALUES ('410503', '北关区', '155', '1404');
INSERT INTO `regionarea` VALUES ('410505', '殷都区', '155', '1405');
INSERT INTO `regionarea` VALUES ('410506', '龙安区', '155', '1406');
INSERT INTO `regionarea` VALUES ('410522', '安阳县', '155', '1407');
INSERT INTO `regionarea` VALUES ('410523', '汤阴县', '155', '1408');
INSERT INTO `regionarea` VALUES ('410526', '滑县', '155', '1409');
INSERT INTO `regionarea` VALUES ('410527', '内黄县', '155', '1410');
INSERT INTO `regionarea` VALUES ('410581', '林州市', '155', '1411');
INSERT INTO `regionarea` VALUES ('410602', '鹤山区', '156', '1412');
INSERT INTO `regionarea` VALUES ('410603', '山城区', '156', '1413');
INSERT INTO `regionarea` VALUES ('410611', '淇滨区', '156', '1414');
INSERT INTO `regionarea` VALUES ('410621', '浚县', '156', '1415');
INSERT INTO `regionarea` VALUES ('410622', '淇县', '156', '1416');
INSERT INTO `regionarea` VALUES ('410702', '红旗区', '157', '1417');
INSERT INTO `regionarea` VALUES ('410703', '卫滨区', '157', '1418');
INSERT INTO `regionarea` VALUES ('410704', '凤泉区', '157', '1419');
INSERT INTO `regionarea` VALUES ('410711', '牧野区', '157', '1420');
INSERT INTO `regionarea` VALUES ('410721', '新乡县', '157', '1421');
INSERT INTO `regionarea` VALUES ('410724', '获嘉县', '157', '1422');
INSERT INTO `regionarea` VALUES ('410725', '原阳县', '157', '1423');
INSERT INTO `regionarea` VALUES ('410726', '延津县', '157', '1424');
INSERT INTO `regionarea` VALUES ('410727', '封丘县', '157', '1425');
INSERT INTO `regionarea` VALUES ('410728', '长垣县', '157', '1426');
INSERT INTO `regionarea` VALUES ('410781', '卫辉市', '157', '1427');
INSERT INTO `regionarea` VALUES ('410782', '辉县市', '157', '1428');
INSERT INTO `regionarea` VALUES ('410802', '解放区', '158', '1429');
INSERT INTO `regionarea` VALUES ('410803', '中站区', '158', '1430');
INSERT INTO `regionarea` VALUES ('410804', '马村区', '158', '1431');
INSERT INTO `regionarea` VALUES ('410811', '山阳区', '158', '1432');
INSERT INTO `regionarea` VALUES ('410821', '修武县', '158', '1433');
INSERT INTO `regionarea` VALUES ('410822', '博爱县', '158', '1434');
INSERT INTO `regionarea` VALUES ('410823', '武陟县', '158', '1435');
INSERT INTO `regionarea` VALUES ('410825', '温县', '158', '1436');
INSERT INTO `regionarea` VALUES ('410882', '沁阳市', '158', '1437');
INSERT INTO `regionarea` VALUES ('410883', '孟州市', '158', '1438');
INSERT INTO `regionarea` VALUES ('410902', '华龙区', '159', '1439');
INSERT INTO `regionarea` VALUES ('410922', '清丰县', '159', '1440');
INSERT INTO `regionarea` VALUES ('410923', '南乐县', '159', '1441');
INSERT INTO `regionarea` VALUES ('410926', '范县', '159', '1442');
INSERT INTO `regionarea` VALUES ('410927', '台前县', '159', '1443');
INSERT INTO `regionarea` VALUES ('410928', '濮阳县', '159', '1444');
INSERT INTO `regionarea` VALUES ('411002', '魏都区', '160', '1445');
INSERT INTO `regionarea` VALUES ('411023', '许昌县', '160', '1446');
INSERT INTO `regionarea` VALUES ('411024', '鄢陵县', '160', '1447');
INSERT INTO `regionarea` VALUES ('411025', '襄城县', '160', '1448');
INSERT INTO `regionarea` VALUES ('411081', '禹州市', '160', '1449');
INSERT INTO `regionarea` VALUES ('411082', '长葛市', '160', '1450');
INSERT INTO `regionarea` VALUES ('411102', '源汇区', '161', '1451');
INSERT INTO `regionarea` VALUES ('411103', '郾城区', '161', '1452');
INSERT INTO `regionarea` VALUES ('411104', '召陵区', '161', '1453');
INSERT INTO `regionarea` VALUES ('411121', '舞阳县', '161', '1454');
INSERT INTO `regionarea` VALUES ('411122', '临颍县', '161', '1455');
INSERT INTO `regionarea` VALUES ('411202', '湖滨区', '162', '1456');
INSERT INTO `regionarea` VALUES ('411221', '渑池县', '162', '1457');
INSERT INTO `regionarea` VALUES ('411222', '陕县', '162', '1458');
INSERT INTO `regionarea` VALUES ('411224', '卢氏县', '162', '1459');
INSERT INTO `regionarea` VALUES ('411281', '义马市', '162', '1460');
INSERT INTO `regionarea` VALUES ('411282', '灵宝市', '162', '1461');
INSERT INTO `regionarea` VALUES ('411302', '宛城区', '163', '1462');
INSERT INTO `regionarea` VALUES ('411303', '卧龙区', '163', '1463');
INSERT INTO `regionarea` VALUES ('411321', '南召县', '163', '1464');
INSERT INTO `regionarea` VALUES ('411322', '方城县', '163', '1465');
INSERT INTO `regionarea` VALUES ('411323', '西峡县', '163', '1466');
INSERT INTO `regionarea` VALUES ('411324', '镇平县', '163', '1467');
INSERT INTO `regionarea` VALUES ('411325', '内乡县', '163', '1468');
INSERT INTO `regionarea` VALUES ('411326', '淅川县', '163', '1469');
INSERT INTO `regionarea` VALUES ('411327', '社旗县', '163', '1470');
INSERT INTO `regionarea` VALUES ('411328', '唐河县', '163', '1471');
INSERT INTO `regionarea` VALUES ('411329', '新野县', '163', '1472');
INSERT INTO `regionarea` VALUES ('411330', '桐柏县', '163', '1473');
INSERT INTO `regionarea` VALUES ('411381', '邓州市', '163', '1474');
INSERT INTO `regionarea` VALUES ('411402', '梁园区', '164', '1475');
INSERT INTO `regionarea` VALUES ('411403', '睢阳区', '164', '1476');
INSERT INTO `regionarea` VALUES ('411421', '民权县', '164', '1477');
INSERT INTO `regionarea` VALUES ('411422', '睢县', '164', '1478');
INSERT INTO `regionarea` VALUES ('411423', '宁陵县', '164', '1479');
INSERT INTO `regionarea` VALUES ('411424', '柘城县', '164', '1480');
INSERT INTO `regionarea` VALUES ('411425', '虞城县', '164', '1481');
INSERT INTO `regionarea` VALUES ('411426', '夏邑县', '164', '1482');
INSERT INTO `regionarea` VALUES ('411481', '永城市', '164', '1483');
INSERT INTO `regionarea` VALUES ('411502', '浉河区', '165', '1484');
INSERT INTO `regionarea` VALUES ('411503', '平桥区', '165', '1485');
INSERT INTO `regionarea` VALUES ('411521', '罗山县', '165', '1486');
INSERT INTO `regionarea` VALUES ('411522', '光山县', '165', '1487');
INSERT INTO `regionarea` VALUES ('411523', '新县', '165', '1488');
INSERT INTO `regionarea` VALUES ('411524', '商城县', '165', '1489');
INSERT INTO `regionarea` VALUES ('411525', '固始县', '165', '1490');
INSERT INTO `regionarea` VALUES ('411526', '潢川县', '165', '1491');
INSERT INTO `regionarea` VALUES ('411527', '淮滨县', '165', '1492');
INSERT INTO `regionarea` VALUES ('411528', '息县', '165', '1493');
INSERT INTO `regionarea` VALUES ('411602', '川汇区', '166', '1494');
INSERT INTO `regionarea` VALUES ('411621', '扶沟县', '166', '1495');
INSERT INTO `regionarea` VALUES ('411622', '西华县', '166', '1496');
INSERT INTO `regionarea` VALUES ('411623', '商水县', '166', '1497');
INSERT INTO `regionarea` VALUES ('411624', '沈丘县', '166', '1498');
INSERT INTO `regionarea` VALUES ('411625', '郸城县', '166', '1499');
INSERT INTO `regionarea` VALUES ('411626', '淮阳县', '166', '1500');
INSERT INTO `regionarea` VALUES ('411627', '太康县', '166', '1501');
INSERT INTO `regionarea` VALUES ('411628', '鹿邑县', '166', '1502');
INSERT INTO `regionarea` VALUES ('411681', '项城市', '166', '1503');
INSERT INTO `regionarea` VALUES ('411702', '驿城区', '167', '1504');
INSERT INTO `regionarea` VALUES ('411721', '西平县', '167', '1505');
INSERT INTO `regionarea` VALUES ('411722', '上蔡县', '167', '1506');
INSERT INTO `regionarea` VALUES ('411723', '平舆县', '167', '1507');
INSERT INTO `regionarea` VALUES ('411724', '正阳县', '167', '1508');
INSERT INTO `regionarea` VALUES ('411725', '确山县', '167', '1509');
INSERT INTO `regionarea` VALUES ('411726', '泌阳县', '167', '1510');
INSERT INTO `regionarea` VALUES ('411727', '汝南县', '167', '1511');
INSERT INTO `regionarea` VALUES ('411728', '遂平县', '167', '1512');
INSERT INTO `regionarea` VALUES ('411729', '新蔡县', '167', '1513');
INSERT INTO `regionarea` VALUES ('419001', '济源市', '168', '1514');
INSERT INTO `regionarea` VALUES ('420102', '江岸区', '169', '1515');
INSERT INTO `regionarea` VALUES ('420103', '江汉区', '169', '1516');
INSERT INTO `regionarea` VALUES ('420104', '硚口区', '169', '1517');
INSERT INTO `regionarea` VALUES ('420105', '汉阳区', '169', '1518');
INSERT INTO `regionarea` VALUES ('420106', '武昌区', '169', '1519');
INSERT INTO `regionarea` VALUES ('420107', '青山区', '169', '1520');
INSERT INTO `regionarea` VALUES ('420111', '洪山区', '169', '1521');
INSERT INTO `regionarea` VALUES ('420112', '东西湖区', '169', '1522');
INSERT INTO `regionarea` VALUES ('420113', '汉南区', '169', '1523');
INSERT INTO `regionarea` VALUES ('420114', '蔡甸区', '169', '1524');
INSERT INTO `regionarea` VALUES ('420115', '江夏区', '169', '1525');
INSERT INTO `regionarea` VALUES ('420116', '黄陂区', '169', '1526');
INSERT INTO `regionarea` VALUES ('420117', '新洲区', '169', '1527');
INSERT INTO `regionarea` VALUES ('420202', '黄石港区', '170', '1528');
INSERT INTO `regionarea` VALUES ('420203', '西塞山区', '170', '1529');
INSERT INTO `regionarea` VALUES ('420204', '下陆区', '170', '1530');
INSERT INTO `regionarea` VALUES ('420205', '铁山区', '170', '1531');
INSERT INTO `regionarea` VALUES ('420222', '阳新县', '170', '1532');
INSERT INTO `regionarea` VALUES ('420281', '大冶市', '170', '1533');
INSERT INTO `regionarea` VALUES ('420302', '茅箭区', '171', '1534');
INSERT INTO `regionarea` VALUES ('420303', '张湾区', '171', '1535');
INSERT INTO `regionarea` VALUES ('420321', '郧县', '171', '1536');
INSERT INTO `regionarea` VALUES ('420322', '郧西县', '171', '1537');
INSERT INTO `regionarea` VALUES ('420323', '竹山县', '171', '1538');
INSERT INTO `regionarea` VALUES ('420324', '竹溪县', '171', '1539');
INSERT INTO `regionarea` VALUES ('420325', '房县', '171', '1540');
INSERT INTO `regionarea` VALUES ('420381', '丹江口市', '171', '1541');
INSERT INTO `regionarea` VALUES ('420502', '西陵区', '172', '1542');
INSERT INTO `regionarea` VALUES ('420503', '伍家岗区', '172', '1543');
INSERT INTO `regionarea` VALUES ('420504', '点军区', '172', '1544');
INSERT INTO `regionarea` VALUES ('420505', '猇亭区', '172', '1545');
INSERT INTO `regionarea` VALUES ('420506', '夷陵区', '172', '1546');
INSERT INTO `regionarea` VALUES ('420525', '远安县', '172', '1547');
INSERT INTO `regionarea` VALUES ('420526', '兴山县', '172', '1548');
INSERT INTO `regionarea` VALUES ('420527', '秭归县', '172', '1549');
INSERT INTO `regionarea` VALUES ('420528', '长阳土家族自治县', '172', '1550');
INSERT INTO `regionarea` VALUES ('420529', '五峰土家族自治县', '172', '1551');
INSERT INTO `regionarea` VALUES ('420581', '宜都市', '172', '1552');
INSERT INTO `regionarea` VALUES ('420582', '当阳市', '172', '1553');
INSERT INTO `regionarea` VALUES ('420583', '枝江市', '172', '1554');
INSERT INTO `regionarea` VALUES ('420602', '襄城区', '173', '1555');
INSERT INTO `regionarea` VALUES ('420606', '樊城区', '173', '1556');
INSERT INTO `regionarea` VALUES ('420607', '襄州区', '173', '1557');
INSERT INTO `regionarea` VALUES ('420624', '南漳县', '173', '1558');
INSERT INTO `regionarea` VALUES ('420625', '谷城县', '173', '1559');
INSERT INTO `regionarea` VALUES ('420626', '保康县', '173', '1560');
INSERT INTO `regionarea` VALUES ('420682', '老河口市', '173', '1561');
INSERT INTO `regionarea` VALUES ('420683', '枣阳市', '173', '1562');
INSERT INTO `regionarea` VALUES ('420684', '宜城市', '173', '1563');
INSERT INTO `regionarea` VALUES ('420702', '梁子湖区', '174', '1564');
INSERT INTO `regionarea` VALUES ('420703', '华容区', '174', '1565');
INSERT INTO `regionarea` VALUES ('420704', '鄂城区', '174', '1566');
INSERT INTO `regionarea` VALUES ('420802', '东宝区', '175', '1567');
INSERT INTO `regionarea` VALUES ('420804', '掇刀区', '175', '1568');
INSERT INTO `regionarea` VALUES ('420821', '京山县', '175', '1569');
INSERT INTO `regionarea` VALUES ('420822', '沙洋县', '175', '1570');
INSERT INTO `regionarea` VALUES ('420881', '钟祥市', '175', '1571');
INSERT INTO `regionarea` VALUES ('420902', '孝南区', '176', '1572');
INSERT INTO `regionarea` VALUES ('420921', '孝昌县', '176', '1573');
INSERT INTO `regionarea` VALUES ('420922', '大悟县', '176', '1574');
INSERT INTO `regionarea` VALUES ('420923', '云梦县', '176', '1575');
INSERT INTO `regionarea` VALUES ('420981', '应城市', '176', '1576');
INSERT INTO `regionarea` VALUES ('420982', '安陆市', '176', '1577');
INSERT INTO `regionarea` VALUES ('420984', '汉川市', '176', '1578');
INSERT INTO `regionarea` VALUES ('421002', '沙市区', '177', '1579');
INSERT INTO `regionarea` VALUES ('421003', '荆州区', '177', '1580');
INSERT INTO `regionarea` VALUES ('421022', '公安县', '177', '1581');
INSERT INTO `regionarea` VALUES ('421023', '监利县', '177', '1582');
INSERT INTO `regionarea` VALUES ('421024', '江陵县', '177', '1583');
INSERT INTO `regionarea` VALUES ('421081', '石首市', '177', '1584');
INSERT INTO `regionarea` VALUES ('421083', '洪湖市', '177', '1585');
INSERT INTO `regionarea` VALUES ('421087', '松滋市', '177', '1586');
INSERT INTO `regionarea` VALUES ('421102', '黄州区', '178', '1587');
INSERT INTO `regionarea` VALUES ('421121', '团风县', '178', '1588');
INSERT INTO `regionarea` VALUES ('421122', '红安县', '178', '1589');
INSERT INTO `regionarea` VALUES ('421123', '罗田县', '178', '1590');
INSERT INTO `regionarea` VALUES ('421124', '英山县', '178', '1591');
INSERT INTO `regionarea` VALUES ('421125', '浠水县', '178', '1592');
INSERT INTO `regionarea` VALUES ('421126', '蕲春县', '178', '1593');
INSERT INTO `regionarea` VALUES ('421127', '黄梅县', '178', '1594');
INSERT INTO `regionarea` VALUES ('421181', '麻城市', '178', '1595');
INSERT INTO `regionarea` VALUES ('421182', '武穴市', '178', '1596');
INSERT INTO `regionarea` VALUES ('421202', '咸安区', '179', '1597');
INSERT INTO `regionarea` VALUES ('421221', '嘉鱼县', '179', '1598');
INSERT INTO `regionarea` VALUES ('421222', '通城县', '179', '1599');
INSERT INTO `regionarea` VALUES ('421223', '崇阳县', '179', '1600');
INSERT INTO `regionarea` VALUES ('421224', '通山县', '179', '1601');
INSERT INTO `regionarea` VALUES ('421281', '赤壁市', '179', '1602');
INSERT INTO `regionarea` VALUES ('421303', '曾都区', '180', '1603');
INSERT INTO `regionarea` VALUES ('421321', '随县', '180', '1604');
INSERT INTO `regionarea` VALUES ('421381', '广水市', '180', '1605');
INSERT INTO `regionarea` VALUES ('422801', '恩施市', '181', '1606');
INSERT INTO `regionarea` VALUES ('422802', '利川市', '181', '1607');
INSERT INTO `regionarea` VALUES ('422822', '建始县', '181', '1608');
INSERT INTO `regionarea` VALUES ('422823', '巴东县', '181', '1609');
INSERT INTO `regionarea` VALUES ('422825', '宣恩县', '181', '1610');
INSERT INTO `regionarea` VALUES ('422826', '咸丰县', '181', '1611');
INSERT INTO `regionarea` VALUES ('422827', '来凤县', '181', '1612');
INSERT INTO `regionarea` VALUES ('422828', '鹤峰县', '181', '1613');
INSERT INTO `regionarea` VALUES ('429004', '仙桃市', '182', '1614');
INSERT INTO `regionarea` VALUES ('429005', '潜江市', '182', '1615');
INSERT INTO `regionarea` VALUES ('429006', '天门市', '182', '1616');
INSERT INTO `regionarea` VALUES ('429021', '神农架林区', '182', '1617');
INSERT INTO `regionarea` VALUES ('430102', '芙蓉区', '183', '1618');
INSERT INTO `regionarea` VALUES ('430103', '天心区', '183', '1619');
INSERT INTO `regionarea` VALUES ('430104', '岳麓区', '183', '1620');
INSERT INTO `regionarea` VALUES ('430105', '开福区', '183', '1621');
INSERT INTO `regionarea` VALUES ('430111', '雨花区', '183', '1622');
INSERT INTO `regionarea` VALUES ('430112', '望城区', '183', '1623');
INSERT INTO `regionarea` VALUES ('430121', '长沙县', '183', '1624');
INSERT INTO `regionarea` VALUES ('430124', '宁乡县', '183', '1625');
INSERT INTO `regionarea` VALUES ('430181', '浏阳市', '183', '1626');
INSERT INTO `regionarea` VALUES ('430202', '荷塘区', '184', '1627');
INSERT INTO `regionarea` VALUES ('430203', '芦淞区', '184', '1628');
INSERT INTO `regionarea` VALUES ('430204', '石峰区', '184', '1629');
INSERT INTO `regionarea` VALUES ('430211', '天元区', '184', '1630');
INSERT INTO `regionarea` VALUES ('430221', '株洲县', '184', '1631');
INSERT INTO `regionarea` VALUES ('430223', '攸县', '184', '1632');
INSERT INTO `regionarea` VALUES ('430224', '茶陵县', '184', '1633');
INSERT INTO `regionarea` VALUES ('430225', '炎陵县', '184', '1634');
INSERT INTO `regionarea` VALUES ('430281', '醴陵市', '184', '1635');
INSERT INTO `regionarea` VALUES ('430302', '雨湖区', '185', '1636');
INSERT INTO `regionarea` VALUES ('430304', '岳塘区', '185', '1637');
INSERT INTO `regionarea` VALUES ('430321', '湘潭县', '185', '1638');
INSERT INTO `regionarea` VALUES ('430381', '湘乡市', '185', '1639');
INSERT INTO `regionarea` VALUES ('430382', '韶山市', '185', '1640');
INSERT INTO `regionarea` VALUES ('430405', '珠晖区', '186', '1641');
INSERT INTO `regionarea` VALUES ('430406', '雁峰区', '186', '1642');
INSERT INTO `regionarea` VALUES ('430407', '石鼓区', '186', '1643');
INSERT INTO `regionarea` VALUES ('430408', '蒸湘区', '186', '1644');
INSERT INTO `regionarea` VALUES ('430412', '南岳区', '186', '1645');
INSERT INTO `regionarea` VALUES ('430421', '衡阳县', '186', '1646');
INSERT INTO `regionarea` VALUES ('430422', '衡南县', '186', '1647');
INSERT INTO `regionarea` VALUES ('430423', '衡山县', '186', '1648');
INSERT INTO `regionarea` VALUES ('430424', '衡东县', '186', '1649');
INSERT INTO `regionarea` VALUES ('430426', '祁东县', '186', '1650');
INSERT INTO `regionarea` VALUES ('430481', '耒阳市', '186', '1651');
INSERT INTO `regionarea` VALUES ('430482', '常宁市', '186', '1652');
INSERT INTO `regionarea` VALUES ('430502', '双清区', '187', '1653');
INSERT INTO `regionarea` VALUES ('430503', '大祥区', '187', '1654');
INSERT INTO `regionarea` VALUES ('430511', '北塔区', '187', '1655');
INSERT INTO `regionarea` VALUES ('430521', '邵东县', '187', '1656');
INSERT INTO `regionarea` VALUES ('430522', '新邵县', '187', '1657');
INSERT INTO `regionarea` VALUES ('430523', '邵阳县', '187', '1658');
INSERT INTO `regionarea` VALUES ('430524', '隆回县', '187', '1659');
INSERT INTO `regionarea` VALUES ('430525', '洞口县', '187', '1660');
INSERT INTO `regionarea` VALUES ('430527', '绥宁县', '187', '1661');
INSERT INTO `regionarea` VALUES ('430528', '新宁县', '187', '1662');
INSERT INTO `regionarea` VALUES ('430529', '城步苗族自治县', '187', '1663');
INSERT INTO `regionarea` VALUES ('430581', '武冈市', '187', '1664');
INSERT INTO `regionarea` VALUES ('430602', '岳阳楼区', '188', '1665');
INSERT INTO `regionarea` VALUES ('430603', '云溪区', '188', '1666');
INSERT INTO `regionarea` VALUES ('430611', '君山区', '188', '1667');
INSERT INTO `regionarea` VALUES ('430621', '岳阳县', '188', '1668');
INSERT INTO `regionarea` VALUES ('430623', '华容县', '188', '1669');
INSERT INTO `regionarea` VALUES ('430624', '湘阴县', '188', '1670');
INSERT INTO `regionarea` VALUES ('430626', '平江县', '188', '1671');
INSERT INTO `regionarea` VALUES ('430681', '汨罗市', '188', '1672');
INSERT INTO `regionarea` VALUES ('430682', '临湘市', '188', '1673');
INSERT INTO `regionarea` VALUES ('430702', '武陵区', '189', '1674');
INSERT INTO `regionarea` VALUES ('430703', '鼎城区', '189', '1675');
INSERT INTO `regionarea` VALUES ('430721', '安乡县', '189', '1676');
INSERT INTO `regionarea` VALUES ('430722', '汉寿县', '189', '1677');
INSERT INTO `regionarea` VALUES ('430723', '澧县', '189', '1678');
INSERT INTO `regionarea` VALUES ('430724', '临澧县', '189', '1679');
INSERT INTO `regionarea` VALUES ('430725', '桃源县', '189', '1680');
INSERT INTO `regionarea` VALUES ('430726', '石门县', '189', '1681');
INSERT INTO `regionarea` VALUES ('430781', '津市市', '189', '1682');
INSERT INTO `regionarea` VALUES ('430802', '永定区', '190', '1683');
INSERT INTO `regionarea` VALUES ('430811', '武陵源区', '190', '1684');
INSERT INTO `regionarea` VALUES ('430821', '慈利县', '190', '1685');
INSERT INTO `regionarea` VALUES ('430822', '桑植县', '190', '1686');
INSERT INTO `regionarea` VALUES ('430902', '资阳区', '191', '1687');
INSERT INTO `regionarea` VALUES ('430903', '赫山区', '191', '1688');
INSERT INTO `regionarea` VALUES ('430921', '南县', '191', '1689');
INSERT INTO `regionarea` VALUES ('430922', '桃江县', '191', '1690');
INSERT INTO `regionarea` VALUES ('430923', '安化县', '191', '1691');
INSERT INTO `regionarea` VALUES ('430981', '沅江市', '191', '1692');
INSERT INTO `regionarea` VALUES ('431002', '北湖区', '192', '1693');
INSERT INTO `regionarea` VALUES ('431003', '苏仙区', '192', '1694');
INSERT INTO `regionarea` VALUES ('431021', '桂阳县', '192', '1695');
INSERT INTO `regionarea` VALUES ('431022', '宜章县', '192', '1696');
INSERT INTO `regionarea` VALUES ('431023', '永兴县', '192', '1697');
INSERT INTO `regionarea` VALUES ('431024', '嘉禾县', '192', '1698');
INSERT INTO `regionarea` VALUES ('431025', '临武县', '192', '1699');
INSERT INTO `regionarea` VALUES ('431026', '汝城县', '192', '1700');
INSERT INTO `regionarea` VALUES ('431027', '桂东县', '192', '1701');
INSERT INTO `regionarea` VALUES ('431028', '安仁县', '192', '1702');
INSERT INTO `regionarea` VALUES ('431081', '资兴市', '192', '1703');
INSERT INTO `regionarea` VALUES ('431102', '零陵区', '193', '1704');
INSERT INTO `regionarea` VALUES ('431103', '冷水滩区', '193', '1705');
INSERT INTO `regionarea` VALUES ('431121', '祁阳县', '193', '1706');
INSERT INTO `regionarea` VALUES ('431122', '东安县', '193', '1707');
INSERT INTO `regionarea` VALUES ('431123', '双牌县', '193', '1708');
INSERT INTO `regionarea` VALUES ('431124', '道县', '193', '1709');
INSERT INTO `regionarea` VALUES ('431125', '江永县', '193', '1710');
INSERT INTO `regionarea` VALUES ('431126', '宁远县', '193', '1711');
INSERT INTO `regionarea` VALUES ('431127', '蓝山县', '193', '1712');
INSERT INTO `regionarea` VALUES ('431128', '新田县', '193', '1713');
INSERT INTO `regionarea` VALUES ('431129', '江华瑶族自治县', '193', '1714');
INSERT INTO `regionarea` VALUES ('431202', '鹤城区', '194', '1715');
INSERT INTO `regionarea` VALUES ('431221', '中方县', '194', '1716');
INSERT INTO `regionarea` VALUES ('431222', '沅陵县', '194', '1717');
INSERT INTO `regionarea` VALUES ('431223', '辰溪县', '194', '1718');
INSERT INTO `regionarea` VALUES ('431224', '溆浦县', '194', '1719');
INSERT INTO `regionarea` VALUES ('431225', '会同县', '194', '1720');
INSERT INTO `regionarea` VALUES ('431226', '麻阳苗族自治县', '194', '1721');
INSERT INTO `regionarea` VALUES ('431227', '新晃侗族自治县', '194', '1722');
INSERT INTO `regionarea` VALUES ('431228', '芷江侗族自治县', '194', '1723');
INSERT INTO `regionarea` VALUES ('431229', '靖州苗族侗族自治县', '194', '1724');
INSERT INTO `regionarea` VALUES ('431230', '通道侗族自治县', '194', '1725');
INSERT INTO `regionarea` VALUES ('431281', '洪江市', '194', '1726');
INSERT INTO `regionarea` VALUES ('431302', '娄星区', '195', '1727');
INSERT INTO `regionarea` VALUES ('431321', '双峰县', '195', '1728');
INSERT INTO `regionarea` VALUES ('431322', '新化县', '195', '1729');
INSERT INTO `regionarea` VALUES ('431381', '冷水江市', '195', '1730');
INSERT INTO `regionarea` VALUES ('431382', '涟源市', '195', '1731');
INSERT INTO `regionarea` VALUES ('433101', '吉首市', '196', '1732');
INSERT INTO `regionarea` VALUES ('433122', '泸溪县', '196', '1733');
INSERT INTO `regionarea` VALUES ('433123', '凤凰县', '196', '1734');
INSERT INTO `regionarea` VALUES ('433124', '花垣县', '196', '1735');
INSERT INTO `regionarea` VALUES ('433125', '保靖县', '196', '1736');
INSERT INTO `regionarea` VALUES ('433126', '古丈县', '196', '1737');
INSERT INTO `regionarea` VALUES ('433127', '永顺县', '196', '1738');
INSERT INTO `regionarea` VALUES ('433130', '龙山县', '196', '1739');
INSERT INTO `regionarea` VALUES ('440103', '荔湾区', '197', '1740');
INSERT INTO `regionarea` VALUES ('440104', '越秀区', '197', '1741');
INSERT INTO `regionarea` VALUES ('440105', '海珠区', '197', '1742');
INSERT INTO `regionarea` VALUES ('440106', '天河区', '197', '1743');
INSERT INTO `regionarea` VALUES ('440111', '白云区', '197', '1744');
INSERT INTO `regionarea` VALUES ('440112', '黄埔区', '197', '1745');
INSERT INTO `regionarea` VALUES ('440113', '番禺区', '197', '1746');
INSERT INTO `regionarea` VALUES ('440114', '花都区', '197', '1747');
INSERT INTO `regionarea` VALUES ('440115', '南沙区', '197', '1748');
INSERT INTO `regionarea` VALUES ('440116', '萝岗区', '197', '1749');
INSERT INTO `regionarea` VALUES ('440183', '增城市', '197', '1750');
INSERT INTO `regionarea` VALUES ('440184', '从化市', '197', '1751');
INSERT INTO `regionarea` VALUES ('440203', '武江区', '198', '1752');
INSERT INTO `regionarea` VALUES ('440204', '浈江区', '198', '1753');
INSERT INTO `regionarea` VALUES ('440205', '曲江区', '198', '1754');
INSERT INTO `regionarea` VALUES ('440222', '始兴县', '198', '1755');
INSERT INTO `regionarea` VALUES ('440224', '仁化县', '198', '1756');
INSERT INTO `regionarea` VALUES ('440229', '翁源县', '198', '1757');
INSERT INTO `regionarea` VALUES ('440232', '乳源瑶族自治县', '198', '1758');
INSERT INTO `regionarea` VALUES ('440233', '新丰县', '198', '1759');
INSERT INTO `regionarea` VALUES ('440281', '乐昌市', '198', '1760');
INSERT INTO `regionarea` VALUES ('440282', '南雄市', '198', '1761');
INSERT INTO `regionarea` VALUES ('440303', '罗湖区', '199', '1762');
INSERT INTO `regionarea` VALUES ('440304', '福田区', '199', '1763');
INSERT INTO `regionarea` VALUES ('440305', '南山区', '199', '1764');
INSERT INTO `regionarea` VALUES ('440306', '宝安区', '199', '1765');
INSERT INTO `regionarea` VALUES ('440307', '龙岗区', '199', '1766');
INSERT INTO `regionarea` VALUES ('440308', '盐田区', '199', '1767');
INSERT INTO `regionarea` VALUES ('440402', '香洲区', '200', '1768');
INSERT INTO `regionarea` VALUES ('440403', '斗门区', '200', '1769');
INSERT INTO `regionarea` VALUES ('440404', '金湾区', '200', '1770');
INSERT INTO `regionarea` VALUES ('440507', '龙湖区', '201', '1771');
INSERT INTO `regionarea` VALUES ('440511', '金平区', '201', '1772');
INSERT INTO `regionarea` VALUES ('440512', '濠江区', '201', '1773');
INSERT INTO `regionarea` VALUES ('440513', '潮阳区', '201', '1774');
INSERT INTO `regionarea` VALUES ('440514', '潮南区', '201', '1775');
INSERT INTO `regionarea` VALUES ('440515', '澄海区', '201', '1776');
INSERT INTO `regionarea` VALUES ('440523', '南澳县', '201', '1777');
INSERT INTO `regionarea` VALUES ('440604', '禅城区', '202', '1778');
INSERT INTO `regionarea` VALUES ('440605', '南海区', '202', '1779');
INSERT INTO `regionarea` VALUES ('440606', '顺德区', '202', '1780');
INSERT INTO `regionarea` VALUES ('440607', '三水区', '202', '1781');
INSERT INTO `regionarea` VALUES ('440608', '高明区', '202', '1782');
INSERT INTO `regionarea` VALUES ('440703', '蓬江区', '203', '1783');
INSERT INTO `regionarea` VALUES ('440704', '江海区', '203', '1784');
INSERT INTO `regionarea` VALUES ('440705', '新会区', '203', '1785');
INSERT INTO `regionarea` VALUES ('440781', '台山市', '203', '1786');
INSERT INTO `regionarea` VALUES ('440783', '开平市', '203', '1787');
INSERT INTO `regionarea` VALUES ('440784', '鹤山市', '203', '1788');
INSERT INTO `regionarea` VALUES ('440785', '恩平市', '203', '1789');
INSERT INTO `regionarea` VALUES ('440802', '赤坎区', '204', '1790');
INSERT INTO `regionarea` VALUES ('440803', '霞山区', '204', '1791');
INSERT INTO `regionarea` VALUES ('440804', '坡头区', '204', '1792');
INSERT INTO `regionarea` VALUES ('440811', '麻章区', '204', '1793');
INSERT INTO `regionarea` VALUES ('440823', '遂溪县', '204', '1794');
INSERT INTO `regionarea` VALUES ('440825', '徐闻县', '204', '1795');
INSERT INTO `regionarea` VALUES ('440881', '廉江市', '204', '1796');
INSERT INTO `regionarea` VALUES ('440882', '雷州市', '204', '1797');
INSERT INTO `regionarea` VALUES ('440883', '吴川市', '204', '1798');
INSERT INTO `regionarea` VALUES ('440902', '茂南区', '205', '1799');
INSERT INTO `regionarea` VALUES ('440903', '茂港区', '205', '1800');
INSERT INTO `regionarea` VALUES ('440923', '电白县', '205', '1801');
INSERT INTO `regionarea` VALUES ('440981', '高州市', '205', '1802');
INSERT INTO `regionarea` VALUES ('440982', '化州市', '205', '1803');
INSERT INTO `regionarea` VALUES ('440983', '信宜市', '205', '1804');
INSERT INTO `regionarea` VALUES ('441202', '端州区', '206', '1805');
INSERT INTO `regionarea` VALUES ('441203', '鼎湖区', '206', '1806');
INSERT INTO `regionarea` VALUES ('441223', '广宁县', '206', '1807');
INSERT INTO `regionarea` VALUES ('441224', '怀集县', '206', '1808');
INSERT INTO `regionarea` VALUES ('441225', '封开县', '206', '1809');
INSERT INTO `regionarea` VALUES ('441226', '德庆县', '206', '1810');
INSERT INTO `regionarea` VALUES ('441283', '高要市', '206', '1811');
INSERT INTO `regionarea` VALUES ('441284', '四会市', '206', '1812');
INSERT INTO `regionarea` VALUES ('441302', '惠城区', '207', '1813');
INSERT INTO `regionarea` VALUES ('441303', '惠阳区', '207', '1814');
INSERT INTO `regionarea` VALUES ('441322', '博罗县', '207', '1815');
INSERT INTO `regionarea` VALUES ('441323', '惠东县', '207', '1816');
INSERT INTO `regionarea` VALUES ('441324', '龙门县', '207', '1817');
INSERT INTO `regionarea` VALUES ('441402', '梅江区', '208', '1818');
INSERT INTO `regionarea` VALUES ('441421', '梅县', '208', '1819');
INSERT INTO `regionarea` VALUES ('441422', '大埔县', '208', '1820');
INSERT INTO `regionarea` VALUES ('441423', '丰顺县', '208', '1821');
INSERT INTO `regionarea` VALUES ('441424', '五华县', '208', '1822');
INSERT INTO `regionarea` VALUES ('441426', '平远县', '208', '1823');
INSERT INTO `regionarea` VALUES ('441427', '蕉岭县', '208', '1824');
INSERT INTO `regionarea` VALUES ('441481', '兴宁市', '208', '1825');
INSERT INTO `regionarea` VALUES ('441502', '城区', '209', '1826');
INSERT INTO `regionarea` VALUES ('441521', '海丰县', '209', '1827');
INSERT INTO `regionarea` VALUES ('441523', '陆河县', '209', '1828');
INSERT INTO `regionarea` VALUES ('441581', '陆丰市', '209', '1829');
INSERT INTO `regionarea` VALUES ('441602', '源城区', '210', '1830');
INSERT INTO `regionarea` VALUES ('441621', '紫金县', '210', '1831');
INSERT INTO `regionarea` VALUES ('441622', '龙川县', '210', '1832');
INSERT INTO `regionarea` VALUES ('441623', '连平县', '210', '1833');
INSERT INTO `regionarea` VALUES ('441624', '和平县', '210', '1834');
INSERT INTO `regionarea` VALUES ('441625', '东源县', '210', '1835');
INSERT INTO `regionarea` VALUES ('441702', '江城区', '211', '1836');
INSERT INTO `regionarea` VALUES ('441721', '阳西县', '211', '1837');
INSERT INTO `regionarea` VALUES ('441723', '阳东县', '211', '1838');
INSERT INTO `regionarea` VALUES ('441781', '阳春市', '211', '1839');
INSERT INTO `regionarea` VALUES ('441802', '清城区', '212', '1840');
INSERT INTO `regionarea` VALUES ('441821', '佛冈县', '212', '1841');
INSERT INTO `regionarea` VALUES ('441823', '阳山县', '212', '1842');
INSERT INTO `regionarea` VALUES ('441825', '连山壮族瑶族自治县', '212', '1843');
INSERT INTO `regionarea` VALUES ('441826', '连南瑶族自治县', '212', '1844');
INSERT INTO `regionarea` VALUES ('441827', '清新县', '212', '1845');
INSERT INTO `regionarea` VALUES ('441881', '英德市', '212', '1846');
INSERT INTO `regionarea` VALUES ('441882', '连州市', '212', '1847');
INSERT INTO `regionarea` VALUES ('441901', '东莞市', '213', '1848');
INSERT INTO `regionarea` VALUES ('442001', '中山市', '214', '1849');
INSERT INTO `regionarea` VALUES ('445102', '湘桥区', '215', '1850');
INSERT INTO `regionarea` VALUES ('445121', '潮安县', '215', '1851');
INSERT INTO `regionarea` VALUES ('445122', '饶平县', '215', '1852');
INSERT INTO `regionarea` VALUES ('445202', '榕城区', '216', '1853');
INSERT INTO `regionarea` VALUES ('445221', '揭东县', '216', '1854');
INSERT INTO `regionarea` VALUES ('445222', '揭西县', '216', '1855');
INSERT INTO `regionarea` VALUES ('445224', '惠来县', '216', '1856');
INSERT INTO `regionarea` VALUES ('445281', '普宁市', '216', '1857');
INSERT INTO `regionarea` VALUES ('445302', '云城区', '217', '1858');
INSERT INTO `regionarea` VALUES ('445321', '新兴县', '217', '1859');
INSERT INTO `regionarea` VALUES ('445322', '郁南县', '217', '1860');
INSERT INTO `regionarea` VALUES ('445323', '云安县', '217', '1861');
INSERT INTO `regionarea` VALUES ('445381', '罗定市', '217', '1862');
INSERT INTO `regionarea` VALUES ('450102', '兴宁区', '218', '1863');
INSERT INTO `regionarea` VALUES ('450103', '青秀区', '218', '1864');
INSERT INTO `regionarea` VALUES ('450105', '江南区', '218', '1865');
INSERT INTO `regionarea` VALUES ('450107', '西乡塘区', '218', '1866');
INSERT INTO `regionarea` VALUES ('450108', '良庆区', '218', '1867');
INSERT INTO `regionarea` VALUES ('450109', '邕宁区', '218', '1868');
INSERT INTO `regionarea` VALUES ('450122', '武鸣县', '218', '1869');
INSERT INTO `regionarea` VALUES ('450123', '隆安县', '218', '1870');
INSERT INTO `regionarea` VALUES ('450124', '马山县', '218', '1871');
INSERT INTO `regionarea` VALUES ('450125', '上林县', '218', '1872');
INSERT INTO `regionarea` VALUES ('450126', '宾阳县', '218', '1873');
INSERT INTO `regionarea` VALUES ('450127', '横县', '218', '1874');
INSERT INTO `regionarea` VALUES ('450202', '城中区', '219', '1875');
INSERT INTO `regionarea` VALUES ('450203', '鱼峰区', '219', '1876');
INSERT INTO `regionarea` VALUES ('450204', '柳南区', '219', '1877');
INSERT INTO `regionarea` VALUES ('450205', '柳北区', '219', '1878');
INSERT INTO `regionarea` VALUES ('450221', '柳江县', '219', '1879');
INSERT INTO `regionarea` VALUES ('450222', '柳城县', '219', '1880');
INSERT INTO `regionarea` VALUES ('450223', '鹿寨县', '219', '1881');
INSERT INTO `regionarea` VALUES ('450224', '融安县', '219', '1882');
INSERT INTO `regionarea` VALUES ('450225', '融水苗族自治县', '219', '1883');
INSERT INTO `regionarea` VALUES ('450226', '三江侗族自治县', '219', '1884');
INSERT INTO `regionarea` VALUES ('450302', '秀峰区', '220', '1885');
INSERT INTO `regionarea` VALUES ('450303', '叠彩区', '220', '1886');
INSERT INTO `regionarea` VALUES ('450304', '象山区', '220', '1887');
INSERT INTO `regionarea` VALUES ('450305', '七星区', '220', '1888');
INSERT INTO `regionarea` VALUES ('450311', '雁山区', '220', '1889');
INSERT INTO `regionarea` VALUES ('450321', '阳朔县', '220', '1890');
INSERT INTO `regionarea` VALUES ('450322', '临桂县', '220', '1891');
INSERT INTO `regionarea` VALUES ('450323', '灵川县', '220', '1892');
INSERT INTO `regionarea` VALUES ('450324', '全州县', '220', '1893');
INSERT INTO `regionarea` VALUES ('450325', '兴安县', '220', '1894');
INSERT INTO `regionarea` VALUES ('450326', '永福县', '220', '1895');
INSERT INTO `regionarea` VALUES ('450327', '灌阳县', '220', '1896');
INSERT INTO `regionarea` VALUES ('450328', '龙胜各族自治县', '220', '1897');
INSERT INTO `regionarea` VALUES ('450329', '资源县', '220', '1898');
INSERT INTO `regionarea` VALUES ('450330', '平乐县', '220', '1899');
INSERT INTO `regionarea` VALUES ('450331', '荔蒲县', '220', '1900');
INSERT INTO `regionarea` VALUES ('450332', '恭城瑶族自治县', '220', '1901');
INSERT INTO `regionarea` VALUES ('450403', '万秀区', '221', '1902');
INSERT INTO `regionarea` VALUES ('450404', '蝶山区', '221', '1903');
INSERT INTO `regionarea` VALUES ('450405', '长洲区', '221', '1904');
INSERT INTO `regionarea` VALUES ('450421', '苍梧县', '221', '1905');
INSERT INTO `regionarea` VALUES ('450422', '藤县', '221', '1906');
INSERT INTO `regionarea` VALUES ('450423', '蒙山县', '221', '1907');
INSERT INTO `regionarea` VALUES ('450481', '岑溪市', '221', '1908');
INSERT INTO `regionarea` VALUES ('450502', '海城区', '222', '1909');
INSERT INTO `regionarea` VALUES ('450503', '银海区', '222', '1910');
INSERT INTO `regionarea` VALUES ('450512', '铁山港区', '222', '1911');
INSERT INTO `regionarea` VALUES ('450521', '合浦县', '222', '1912');
INSERT INTO `regionarea` VALUES ('450602', '港口区', '223', '1913');
INSERT INTO `regionarea` VALUES ('450603', '防城区', '223', '1914');
INSERT INTO `regionarea` VALUES ('450621', '上思县', '223', '1915');
INSERT INTO `regionarea` VALUES ('450681', '东兴市', '223', '1916');
INSERT INTO `regionarea` VALUES ('450702', '钦南区', '224', '1917');
INSERT INTO `regionarea` VALUES ('450703', '钦北区', '224', '1918');
INSERT INTO `regionarea` VALUES ('450721', '灵山县', '224', '1919');
INSERT INTO `regionarea` VALUES ('450722', '浦北县', '224', '1920');
INSERT INTO `regionarea` VALUES ('450802', '港北区', '225', '1921');
INSERT INTO `regionarea` VALUES ('450803', '港南区', '225', '1922');
INSERT INTO `regionarea` VALUES ('450804', '覃塘区', '225', '1923');
INSERT INTO `regionarea` VALUES ('450821', '平南县', '225', '1924');
INSERT INTO `regionarea` VALUES ('450881', '桂平市', '225', '1925');
INSERT INTO `regionarea` VALUES ('450902', '玉州区', '226', '1926');
INSERT INTO `regionarea` VALUES ('450921', '容县', '226', '1927');
INSERT INTO `regionarea` VALUES ('450922', '陆川县', '226', '1928');
INSERT INTO `regionarea` VALUES ('450923', '博白县', '226', '1929');
INSERT INTO `regionarea` VALUES ('450924', '兴业县', '226', '1930');
INSERT INTO `regionarea` VALUES ('450981', '北流市', '226', '1931');
INSERT INTO `regionarea` VALUES ('451002', '右江区', '227', '1932');
INSERT INTO `regionarea` VALUES ('451021', '田阳县', '227', '1933');
INSERT INTO `regionarea` VALUES ('451022', '田东县', '227', '1934');
INSERT INTO `regionarea` VALUES ('451023', '平果县', '227', '1935');
INSERT INTO `regionarea` VALUES ('451024', '德保县', '227', '1936');
INSERT INTO `regionarea` VALUES ('451025', '靖西县', '227', '1937');
INSERT INTO `regionarea` VALUES ('451026', '那坡县', '227', '1938');
INSERT INTO `regionarea` VALUES ('451027', '凌云县', '227', '1939');
INSERT INTO `regionarea` VALUES ('451028', '乐业县', '227', '1940');
INSERT INTO `regionarea` VALUES ('451029', '田林县', '227', '1941');
INSERT INTO `regionarea` VALUES ('451030', '西林县', '227', '1942');
INSERT INTO `regionarea` VALUES ('451031', '隆林各族自治县', '227', '1943');
INSERT INTO `regionarea` VALUES ('451102', '八步区', '228', '1944');
INSERT INTO `regionarea` VALUES ('451119', '平桂管理区', '228', '1945');
INSERT INTO `regionarea` VALUES ('451121', '昭平县', '228', '1946');
INSERT INTO `regionarea` VALUES ('451122', '钟山县', '228', '1947');
INSERT INTO `regionarea` VALUES ('451123', '富川瑶族自治县', '228', '1948');
INSERT INTO `regionarea` VALUES ('451202', '金城江区', '229', '1949');
INSERT INTO `regionarea` VALUES ('451221', '南丹县', '229', '1950');
INSERT INTO `regionarea` VALUES ('451222', '天峨县', '229', '1951');
INSERT INTO `regionarea` VALUES ('451223', '凤山县', '229', '1952');
INSERT INTO `regionarea` VALUES ('451224', '东兰县', '229', '1953');
INSERT INTO `regionarea` VALUES ('451225', '罗城仫佬族自治县', '229', '1954');
INSERT INTO `regionarea` VALUES ('451226', '环江毛南族自治县', '229', '1955');
INSERT INTO `regionarea` VALUES ('451227', '巴马瑶族自治县', '229', '1956');
INSERT INTO `regionarea` VALUES ('451228', '都安瑶族自治县', '229', '1957');
INSERT INTO `regionarea` VALUES ('451229', '大化瑶族自治县', '229', '1958');
INSERT INTO `regionarea` VALUES ('451281', '宜州市', '229', '1959');
INSERT INTO `regionarea` VALUES ('451302', '兴宾区', '230', '1960');
INSERT INTO `regionarea` VALUES ('451321', '忻城县', '230', '1961');
INSERT INTO `regionarea` VALUES ('451322', '象州县', '230', '1962');
INSERT INTO `regionarea` VALUES ('451323', '武宣县', '230', '1963');
INSERT INTO `regionarea` VALUES ('451324', '金秀瑶族自治县', '230', '1964');
INSERT INTO `regionarea` VALUES ('451381', '合山市', '230', '1965');
INSERT INTO `regionarea` VALUES ('451402', '江洲区', '231', '1966');
INSERT INTO `regionarea` VALUES ('451421', '扶绥县', '231', '1967');
INSERT INTO `regionarea` VALUES ('451422', '宁明县', '231', '1968');
INSERT INTO `regionarea` VALUES ('451423', '龙州县', '231', '1969');
INSERT INTO `regionarea` VALUES ('451424', '大新县', '231', '1970');
INSERT INTO `regionarea` VALUES ('451425', '天等县', '231', '1971');
INSERT INTO `regionarea` VALUES ('451481', '凭祥市', '231', '1972');
INSERT INTO `regionarea` VALUES ('460105', '秀英区', '232', '1973');
INSERT INTO `regionarea` VALUES ('460106', '龙华区', '232', '1974');
INSERT INTO `regionarea` VALUES ('460107', '琼山区', '232', '1975');
INSERT INTO `regionarea` VALUES ('460108', '美兰区', '232', '1976');
INSERT INTO `regionarea` VALUES ('460201', '三亚市', '233', '1977');
INSERT INTO `regionarea` VALUES ('460901', '西沙群岛', '234', '1978');
INSERT INTO `regionarea` VALUES ('460902', '南沙群岛', '234', '1979');
INSERT INTO `regionarea` VALUES ('460903', '中沙群岛', '234', '1980');
INSERT INTO `regionarea` VALUES ('469001', '五指山市', '235', '1981');
INSERT INTO `regionarea` VALUES ('469002', '琼海市', '235', '1982');
INSERT INTO `regionarea` VALUES ('469003', '儋州市', '235', '1983');
INSERT INTO `regionarea` VALUES ('469005', '文昌市', '235', '1984');
INSERT INTO `regionarea` VALUES ('469006', '万宁市', '235', '1985');
INSERT INTO `regionarea` VALUES ('469007', '东方市', '235', '1986');
INSERT INTO `regionarea` VALUES ('469021', '定安县', '235', '1987');
INSERT INTO `regionarea` VALUES ('469022', '屯昌县', '235', '1988');
INSERT INTO `regionarea` VALUES ('469023', '澄迈县', '235', '1989');
INSERT INTO `regionarea` VALUES ('469024', '临高县', '235', '1990');
INSERT INTO `regionarea` VALUES ('469025', '白沙黎族自治县', '235', '1991');
INSERT INTO `regionarea` VALUES ('469026', '昌江黎族自治县', '235', '1992');
INSERT INTO `regionarea` VALUES ('469027', '乐东黎族自治县', '235', '1993');
INSERT INTO `regionarea` VALUES ('469028', '陵水黎族自治县', '235', '1994');
INSERT INTO `regionarea` VALUES ('469029', '保亭黎族苗族自治县', '235', '1995');
INSERT INTO `regionarea` VALUES ('469030', '琼中黎族苗族自治县', '235', '1996');
INSERT INTO `regionarea` VALUES ('500101', '万州区', '236', '1997');
INSERT INTO `regionarea` VALUES ('500102', '涪陵区', '236', '1998');
INSERT INTO `regionarea` VALUES ('500103', '渝中区', '236', '1999');
INSERT INTO `regionarea` VALUES ('500104', '大渡口区', '236', '2000');
INSERT INTO `regionarea` VALUES ('500105', '江北区', '236', '2001');
INSERT INTO `regionarea` VALUES ('500106', '沙坪坝区', '236', '2002');
INSERT INTO `regionarea` VALUES ('500107', '九龙坡区', '236', '2003');
INSERT INTO `regionarea` VALUES ('500108', '南岸区', '236', '2004');
INSERT INTO `regionarea` VALUES ('500109', '北碚区', '236', '2005');
INSERT INTO `regionarea` VALUES ('500110', '綦江区', '236', '2006');
INSERT INTO `regionarea` VALUES ('500111', '大足区', '236', '2007');
INSERT INTO `regionarea` VALUES ('500112', '渝北区', '236', '2008');
INSERT INTO `regionarea` VALUES ('500113', '巴南区', '236', '2009');
INSERT INTO `regionarea` VALUES ('500114', '黔江区', '236', '2010');
INSERT INTO `regionarea` VALUES ('500115', '长寿区', '236', '2011');
INSERT INTO `regionarea` VALUES ('500116', '江津区', '236', '2012');
INSERT INTO `regionarea` VALUES ('500117', '合川区', '236', '2013');
INSERT INTO `regionarea` VALUES ('500118', '永川区', '236', '2014');
INSERT INTO `regionarea` VALUES ('500119', '南川区', '236', '2015');
INSERT INTO `regionarea` VALUES ('500223', '潼南县', '236', '2016');
INSERT INTO `regionarea` VALUES ('500224', '铜梁县', '236', '2017');
INSERT INTO `regionarea` VALUES ('500226', '荣昌县', '236', '2018');
INSERT INTO `regionarea` VALUES ('500227', '璧山县', '236', '2019');
INSERT INTO `regionarea` VALUES ('500228', '梁平县', '236', '2020');
INSERT INTO `regionarea` VALUES ('500229', '城口县', '236', '2021');
INSERT INTO `regionarea` VALUES ('500230', '丰都县', '236', '2022');
INSERT INTO `regionarea` VALUES ('500231', '垫江县', '236', '2023');
INSERT INTO `regionarea` VALUES ('500232', '武隆县', '236', '2024');
INSERT INTO `regionarea` VALUES ('500233', '忠县', '236', '2025');
INSERT INTO `regionarea` VALUES ('500234', '开县', '236', '2026');
INSERT INTO `regionarea` VALUES ('500235', '云阳县', '236', '2027');
INSERT INTO `regionarea` VALUES ('500236', '奉节县', '236', '2028');
INSERT INTO `regionarea` VALUES ('500237', '巫山县', '236', '2029');
INSERT INTO `regionarea` VALUES ('500238', '巫溪县', '236', '2030');
INSERT INTO `regionarea` VALUES ('500240', '石柱土家族自治县', '236', '2031');
INSERT INTO `regionarea` VALUES ('500241', '秀山土家族苗族自治县', '236', '2032');
INSERT INTO `regionarea` VALUES ('500242', '酉阳土家族苗族自治县', '236', '2033');
INSERT INTO `regionarea` VALUES ('500243', '彭水苗族土家族自治县', '236', '2034');
INSERT INTO `regionarea` VALUES ('510104', '锦江区', '237', '2035');
INSERT INTO `regionarea` VALUES ('510105', '青羊区', '237', '2036');
INSERT INTO `regionarea` VALUES ('510106', '金牛区', '237', '2037');
INSERT INTO `regionarea` VALUES ('510107', '武侯区', '237', '2038');
INSERT INTO `regionarea` VALUES ('510108', '成华区', '237', '2039');
INSERT INTO `regionarea` VALUES ('510112', '龙泉驿区', '237', '2040');
INSERT INTO `regionarea` VALUES ('510113', '青白江区', '237', '2041');
INSERT INTO `regionarea` VALUES ('510114', '新都区', '237', '2042');
INSERT INTO `regionarea` VALUES ('510115', '温江区', '237', '2043');
INSERT INTO `regionarea` VALUES ('510121', '金堂县', '237', '2044');
INSERT INTO `regionarea` VALUES ('510122', '双流县', '237', '2045');
INSERT INTO `regionarea` VALUES ('510124', '郫县', '237', '2046');
INSERT INTO `regionarea` VALUES ('510129', '大邑县', '237', '2047');
INSERT INTO `regionarea` VALUES ('510131', '蒲江县', '237', '2048');
INSERT INTO `regionarea` VALUES ('510132', '新津县', '237', '2049');
INSERT INTO `regionarea` VALUES ('510181', '都江堰市', '237', '2050');
INSERT INTO `regionarea` VALUES ('510182', '彭州市', '237', '2051');
INSERT INTO `regionarea` VALUES ('510183', '邛崃市', '237', '2052');
INSERT INTO `regionarea` VALUES ('510184', '崇州市', '237', '2053');
INSERT INTO `regionarea` VALUES ('510302', '自流井区', '238', '2054');
INSERT INTO `regionarea` VALUES ('510303', '贡井区', '238', '2055');
INSERT INTO `regionarea` VALUES ('510304', '大安区', '238', '2056');
INSERT INTO `regionarea` VALUES ('510311', '沿滩区', '238', '2057');
INSERT INTO `regionarea` VALUES ('510321', '荣县', '238', '2058');
INSERT INTO `regionarea` VALUES ('510322', '富顺县', '238', '2059');
INSERT INTO `regionarea` VALUES ('510402', '东区', '239', '2060');
INSERT INTO `regionarea` VALUES ('510403', '西区', '239', '2061');
INSERT INTO `regionarea` VALUES ('510411', '仁和区', '239', '2062');
INSERT INTO `regionarea` VALUES ('510421', '米易县', '239', '2063');
INSERT INTO `regionarea` VALUES ('510422', '盐边县', '239', '2064');
INSERT INTO `regionarea` VALUES ('510502', '江阳区', '240', '2065');
INSERT INTO `regionarea` VALUES ('510503', '纳溪区', '240', '2066');
INSERT INTO `regionarea` VALUES ('510504', '龙马潭区', '240', '2067');
INSERT INTO `regionarea` VALUES ('510521', '泸县', '240', '2068');
INSERT INTO `regionarea` VALUES ('510522', '合江县', '240', '2069');
INSERT INTO `regionarea` VALUES ('510524', '叙永县', '240', '2070');
INSERT INTO `regionarea` VALUES ('510525', '古蔺县', '240', '2071');
INSERT INTO `regionarea` VALUES ('510603', '旌阳区', '241', '2072');
INSERT INTO `regionarea` VALUES ('510623', '中江县', '241', '2073');
INSERT INTO `regionarea` VALUES ('510626', '罗江县', '241', '2074');
INSERT INTO `regionarea` VALUES ('510681', '广汉市', '241', '2075');
INSERT INTO `regionarea` VALUES ('510682', '什邡市', '241', '2076');
INSERT INTO `regionarea` VALUES ('510683', '绵竹市', '241', '2077');
INSERT INTO `regionarea` VALUES ('510703', '涪城区', '242', '2078');
INSERT INTO `regionarea` VALUES ('510704', '游仙区', '242', '2079');
INSERT INTO `regionarea` VALUES ('510722', '三台县', '242', '2080');
INSERT INTO `regionarea` VALUES ('510723', '盐亭县', '242', '2081');
INSERT INTO `regionarea` VALUES ('510724', '安县', '242', '2082');
INSERT INTO `regionarea` VALUES ('510725', '梓潼县', '242', '2083');
INSERT INTO `regionarea` VALUES ('510726', '北川羌族自治县', '242', '2084');
INSERT INTO `regionarea` VALUES ('510727', '平武县', '242', '2085');
INSERT INTO `regionarea` VALUES ('510781', '江油市', '242', '2086');
INSERT INTO `regionarea` VALUES ('510802', '利州区', '243', '2087');
INSERT INTO `regionarea` VALUES ('510811', '元坝区', '243', '2088');
INSERT INTO `regionarea` VALUES ('510812', '朝天区', '243', '2089');
INSERT INTO `regionarea` VALUES ('510821', '旺苍县', '243', '2090');
INSERT INTO `regionarea` VALUES ('510822', '青川县', '243', '2091');
INSERT INTO `regionarea` VALUES ('510823', '剑阁县', '243', '2092');
INSERT INTO `regionarea` VALUES ('510824', '苍溪县', '243', '2093');
INSERT INTO `regionarea` VALUES ('510903', '船山区', '244', '2094');
INSERT INTO `regionarea` VALUES ('510904', '安居区', '244', '2095');
INSERT INTO `regionarea` VALUES ('510921', '蓬溪县', '244', '2096');
INSERT INTO `regionarea` VALUES ('510922', '射洪县', '244', '2097');
INSERT INTO `regionarea` VALUES ('510923', '大英县', '244', '2098');
INSERT INTO `regionarea` VALUES ('511002', '市中区', '245', '2099');
INSERT INTO `regionarea` VALUES ('511011', '东兴区', '245', '2100');
INSERT INTO `regionarea` VALUES ('511024', '威远县', '245', '2101');
INSERT INTO `regionarea` VALUES ('511025', '资中县', '245', '2102');
INSERT INTO `regionarea` VALUES ('511028', '隆昌县', '245', '2103');
INSERT INTO `regionarea` VALUES ('511102', '市中区', '246', '2104');
INSERT INTO `regionarea` VALUES ('511111', '沙湾区', '246', '2105');
INSERT INTO `regionarea` VALUES ('511112', '五通桥区', '246', '2106');
INSERT INTO `regionarea` VALUES ('511113', '金口河区', '246', '2107');
INSERT INTO `regionarea` VALUES ('511123', '犍为县', '246', '2108');
INSERT INTO `regionarea` VALUES ('511124', '井研县', '246', '2109');
INSERT INTO `regionarea` VALUES ('511126', '夹江县', '246', '2110');
INSERT INTO `regionarea` VALUES ('511129', '沐川县', '246', '2111');
INSERT INTO `regionarea` VALUES ('511132', '峨边彝族自治县', '246', '2112');
INSERT INTO `regionarea` VALUES ('511133', '马边彝族自治县', '246', '2113');
INSERT INTO `regionarea` VALUES ('511181', '峨眉山市', '246', '2114');
INSERT INTO `regionarea` VALUES ('511302', '顺庆区', '247', '2115');
INSERT INTO `regionarea` VALUES ('511303', '高坪区', '247', '2116');
INSERT INTO `regionarea` VALUES ('511304', '嘉陵区', '247', '2117');
INSERT INTO `regionarea` VALUES ('511321', '南部县', '247', '2118');
INSERT INTO `regionarea` VALUES ('511322', '营山县', '247', '2119');
INSERT INTO `regionarea` VALUES ('511323', '蓬安县', '247', '2120');
INSERT INTO `regionarea` VALUES ('511324', '仪陇县', '247', '2121');
INSERT INTO `regionarea` VALUES ('511325', '西充县', '247', '2122');
INSERT INTO `regionarea` VALUES ('511381', '阆中市', '247', '2123');
INSERT INTO `regionarea` VALUES ('511402', '东坡区', '248', '2124');
INSERT INTO `regionarea` VALUES ('511421', '仁寿县', '248', '2125');
INSERT INTO `regionarea` VALUES ('511422', '彭山县', '248', '2126');
INSERT INTO `regionarea` VALUES ('511423', '洪雅县', '248', '2127');
INSERT INTO `regionarea` VALUES ('511424', '丹棱县', '248', '2128');
INSERT INTO `regionarea` VALUES ('511425', '青神县', '248', '2129');
INSERT INTO `regionarea` VALUES ('511502', '翠屏区', '249', '2130');
INSERT INTO `regionarea` VALUES ('511522', '南溪区', '249', '2131');
INSERT INTO `regionarea` VALUES ('511521', '宜宾县', '249', '2132');
INSERT INTO `regionarea` VALUES ('511523', '江安县', '249', '2133');
INSERT INTO `regionarea` VALUES ('511524', '长宁县', '249', '2134');
INSERT INTO `regionarea` VALUES ('511525', '高县', '249', '2135');
INSERT INTO `regionarea` VALUES ('511526', '珙县', '249', '2136');
INSERT INTO `regionarea` VALUES ('511527', '筠连县', '249', '2137');
INSERT INTO `regionarea` VALUES ('511528', '兴文县', '249', '2138');
INSERT INTO `regionarea` VALUES ('511529', '屏山县', '249', '2139');
INSERT INTO `regionarea` VALUES ('511602', '广安区', '250', '2140');
INSERT INTO `regionarea` VALUES ('511621', '岳池县', '250', '2141');
INSERT INTO `regionarea` VALUES ('511622', '武胜县', '250', '2142');
INSERT INTO `regionarea` VALUES ('511623', '邻水县', '250', '2143');
INSERT INTO `regionarea` VALUES ('511681', '华蓥市', '250', '2144');
INSERT INTO `regionarea` VALUES ('511702', '通川区', '251', '2145');
INSERT INTO `regionarea` VALUES ('511721', '达县', '251', '2146');
INSERT INTO `regionarea` VALUES ('511722', '宣汉县', '251', '2147');
INSERT INTO `regionarea` VALUES ('511723', '开江县', '251', '2148');
INSERT INTO `regionarea` VALUES ('511724', '大竹县', '251', '2149');
INSERT INTO `regionarea` VALUES ('511725', '渠县', '251', '2150');
INSERT INTO `regionarea` VALUES ('511781', '万源市', '251', '2151');
INSERT INTO `regionarea` VALUES ('511802', '雨城区', '252', '2152');
INSERT INTO `regionarea` VALUES ('511821', '名山县', '252', '2153');
INSERT INTO `regionarea` VALUES ('511822', '荥经县', '252', '2154');
INSERT INTO `regionarea` VALUES ('511823', '汉源县', '252', '2155');
INSERT INTO `regionarea` VALUES ('511824', '石棉县', '252', '2156');
INSERT INTO `regionarea` VALUES ('511825', '天全县', '252', '2157');
INSERT INTO `regionarea` VALUES ('511826', '芦山县', '252', '2158');
INSERT INTO `regionarea` VALUES ('511827', '宝兴县', '252', '2159');
INSERT INTO `regionarea` VALUES ('511902', '巴州区', '253', '2160');
INSERT INTO `regionarea` VALUES ('511921', '通江县', '253', '2161');
INSERT INTO `regionarea` VALUES ('511922', '南江县', '253', '2162');
INSERT INTO `regionarea` VALUES ('511923', '平昌县', '253', '2163');
INSERT INTO `regionarea` VALUES ('512002', '雁江区', '254', '2164');
INSERT INTO `regionarea` VALUES ('512021', '安岳县', '254', '2165');
INSERT INTO `regionarea` VALUES ('512022', '乐至县', '254', '2166');
INSERT INTO `regionarea` VALUES ('512081', '简阳市', '254', '2167');
INSERT INTO `regionarea` VALUES ('513221', '汶川县', '255', '2168');
INSERT INTO `regionarea` VALUES ('513222', '理县', '255', '2169');
INSERT INTO `regionarea` VALUES ('513223', '茂县', '255', '2170');
INSERT INTO `regionarea` VALUES ('513224', '松潘县', '255', '2171');
INSERT INTO `regionarea` VALUES ('513225', '九寨沟县', '255', '2172');
INSERT INTO `regionarea` VALUES ('513226', '金川县', '255', '2173');
INSERT INTO `regionarea` VALUES ('513227', '小金县', '255', '2174');
INSERT INTO `regionarea` VALUES ('513228', '黑水县', '255', '2175');
INSERT INTO `regionarea` VALUES ('513229', '马尔康县', '255', '2176');
INSERT INTO `regionarea` VALUES ('513230', '壤塘县', '255', '2177');
INSERT INTO `regionarea` VALUES ('513231', '阿坝县', '255', '2178');
INSERT INTO `regionarea` VALUES ('513232', '若尔盖县', '255', '2179');
INSERT INTO `regionarea` VALUES ('513233', '红原县', '255', '2180');
INSERT INTO `regionarea` VALUES ('513321', '康定县', '256', '2181');
INSERT INTO `regionarea` VALUES ('513322', '泸定县', '256', '2182');
INSERT INTO `regionarea` VALUES ('513323', '丹巴县', '256', '2183');
INSERT INTO `regionarea` VALUES ('513324', '九龙县', '256', '2184');
INSERT INTO `regionarea` VALUES ('513325', '雅江县', '256', '2185');
INSERT INTO `regionarea` VALUES ('513326', '道孚县', '256', '2186');
INSERT INTO `regionarea` VALUES ('513327', '炉霍县', '256', '2187');
INSERT INTO `regionarea` VALUES ('513328', '甘孜县', '256', '2188');
INSERT INTO `regionarea` VALUES ('513329', '新龙县', '256', '2189');
INSERT INTO `regionarea` VALUES ('513330', '德格县', '256', '2190');
INSERT INTO `regionarea` VALUES ('513331', '白玉县', '256', '2191');
INSERT INTO `regionarea` VALUES ('513332', '石渠县', '256', '2192');
INSERT INTO `regionarea` VALUES ('513333', '色达县', '256', '2193');
INSERT INTO `regionarea` VALUES ('513334', '理塘县', '256', '2194');
INSERT INTO `regionarea` VALUES ('513335', '巴塘县', '256', '2195');
INSERT INTO `regionarea` VALUES ('513336', '乡城县', '256', '2196');
INSERT INTO `regionarea` VALUES ('513337', '稻城县', '256', '2197');
INSERT INTO `regionarea` VALUES ('513338', '得荣县', '256', '2198');
INSERT INTO `regionarea` VALUES ('513401', '西昌市', '257', '2199');
INSERT INTO `regionarea` VALUES ('513422', '木里藏族自治县', '257', '2200');
INSERT INTO `regionarea` VALUES ('513423', '盐源县', '257', '2201');
INSERT INTO `regionarea` VALUES ('513424', '德昌县', '257', '2202');
INSERT INTO `regionarea` VALUES ('513425', '会理县', '257', '2203');
INSERT INTO `regionarea` VALUES ('513426', '会东县', '257', '2204');
INSERT INTO `regionarea` VALUES ('513427', '宁南县', '257', '2205');
INSERT INTO `regionarea` VALUES ('513428', '普格县', '257', '2206');
INSERT INTO `regionarea` VALUES ('513429', '布拖县', '257', '2207');
INSERT INTO `regionarea` VALUES ('513430', '金阳县', '257', '2208');
INSERT INTO `regionarea` VALUES ('513431', '昭觉县', '257', '2209');
INSERT INTO `regionarea` VALUES ('513432', '喜德县', '257', '2210');
INSERT INTO `regionarea` VALUES ('513433', '冕宁县', '257', '2211');
INSERT INTO `regionarea` VALUES ('513434', '越西县', '257', '2212');
INSERT INTO `regionarea` VALUES ('513435', '甘洛县', '257', '2213');
INSERT INTO `regionarea` VALUES ('513436', '美姑县', '257', '2214');
INSERT INTO `regionarea` VALUES ('513437', '雷波县', '257', '2215');
INSERT INTO `regionarea` VALUES ('520102', '南明区', '258', '2216');
INSERT INTO `regionarea` VALUES ('520103', '云岩区', '258', '2217');
INSERT INTO `regionarea` VALUES ('520111', '花溪区', '258', '2218');
INSERT INTO `regionarea` VALUES ('520112', '乌当区', '258', '2219');
INSERT INTO `regionarea` VALUES ('520113', '白云区', '258', '2220');
INSERT INTO `regionarea` VALUES ('520114', '小河区', '258', '2221');
INSERT INTO `regionarea` VALUES ('520121', '开阳县', '258', '2222');
INSERT INTO `regionarea` VALUES ('520122', '息烽县', '258', '2223');
INSERT INTO `regionarea` VALUES ('520123', '修文县', '258', '2224');
INSERT INTO `regionarea` VALUES ('520181', '清镇市', '258', '2225');
INSERT INTO `regionarea` VALUES ('520201', '钟山区', '259', '2226');
INSERT INTO `regionarea` VALUES ('520203', '六枝特区', '259', '2227');
INSERT INTO `regionarea` VALUES ('520221', '水城县', '259', '2228');
INSERT INTO `regionarea` VALUES ('520222', '盘县', '259', '2229');
INSERT INTO `regionarea` VALUES ('520302', '红花岗区', '260', '2230');
INSERT INTO `regionarea` VALUES ('520303', '汇川区', '260', '2231');
INSERT INTO `regionarea` VALUES ('520321', '遵义县', '260', '2232');
INSERT INTO `regionarea` VALUES ('520322', '桐梓县', '260', '2233');
INSERT INTO `regionarea` VALUES ('520323', '绥阳县', '260', '2234');
INSERT INTO `regionarea` VALUES ('520324', '正安县', '260', '2235');
INSERT INTO `regionarea` VALUES ('520325', '道真仡佬族苗族自治县', '260', '2236');
INSERT INTO `regionarea` VALUES ('520326', '务川仡佬族苗族自治县', '260', '2237');
INSERT INTO `regionarea` VALUES ('520327', '凤冈县', '260', '2238');
INSERT INTO `regionarea` VALUES ('520328', '湄潭县', '260', '2239');
INSERT INTO `regionarea` VALUES ('520329', '余庆县', '260', '2240');
INSERT INTO `regionarea` VALUES ('520330', '习水县', '260', '2241');
INSERT INTO `regionarea` VALUES ('520381', '赤水市', '260', '2242');
INSERT INTO `regionarea` VALUES ('520382', '仁怀市', '260', '2243');
INSERT INTO `regionarea` VALUES ('520402', '西秀区', '261', '2244');
INSERT INTO `regionarea` VALUES ('520421', '平坝县', '261', '2245');
INSERT INTO `regionarea` VALUES ('520422', '普定县', '261', '2246');
INSERT INTO `regionarea` VALUES ('520423', '镇宁布依族苗族自治县', '261', '2247');
INSERT INTO `regionarea` VALUES ('520424', '关岭布依族苗族自治县', '261', '2248');
INSERT INTO `regionarea` VALUES ('520425', '紫云苗族布依族自治县', '261', '2249');
INSERT INTO `regionarea` VALUES ('520601', '碧江区', '262', '2250');
INSERT INTO `regionarea` VALUES ('520602', '万山区', '262', '2251');
INSERT INTO `regionarea` VALUES ('520622', '江口县', '262', '2252');
INSERT INTO `regionarea` VALUES ('520623', '玉屏侗族自治县', '262', '2253');
INSERT INTO `regionarea` VALUES ('520624', '石阡县', '262', '2254');
INSERT INTO `regionarea` VALUES ('520625', '思南县', '262', '2255');
INSERT INTO `regionarea` VALUES ('520626', '印江土家族苗族自治县', '262', '2256');
INSERT INTO `regionarea` VALUES ('520627', '德江县', '262', '2257');
INSERT INTO `regionarea` VALUES ('520628', '沿河土家族自治县', '262', '2258');
INSERT INTO `regionarea` VALUES ('520629', '松桃苗族自治县', '262', '2259');
INSERT INTO `regionarea` VALUES ('522301', '兴义市', '263', '2260');
INSERT INTO `regionarea` VALUES ('522322', '兴仁县', '263', '2261');
INSERT INTO `regionarea` VALUES ('522323', '普安县', '263', '2262');
INSERT INTO `regionarea` VALUES ('522324', '晴隆县', '263', '2263');
INSERT INTO `regionarea` VALUES ('522325', '贞丰县', '263', '2264');
INSERT INTO `regionarea` VALUES ('522326', '望谟县', '263', '2265');
INSERT INTO `regionarea` VALUES ('522327', '册亨县', '263', '2266');
INSERT INTO `regionarea` VALUES ('522328', '安龙县', '263', '2267');
INSERT INTO `regionarea` VALUES ('520501', '七星关区', '264', '2268');
INSERT INTO `regionarea` VALUES ('520522', '大方县', '264', '2269');
INSERT INTO `regionarea` VALUES ('520523', '黔西县', '264', '2270');
INSERT INTO `regionarea` VALUES ('520524', '金沙县', '264', '2271');
INSERT INTO `regionarea` VALUES ('520525', '织金县', '264', '2272');
INSERT INTO `regionarea` VALUES ('520526', '纳雍县', '264', '2273');
INSERT INTO `regionarea` VALUES ('520527', '威宁彝族回族苗族自治县', '264', '2274');
INSERT INTO `regionarea` VALUES ('520528', '赫章县', '264', '2275');
INSERT INTO `regionarea` VALUES ('522601', '凯里市', '265', '2276');
INSERT INTO `regionarea` VALUES ('522622', '黄平县', '265', '2277');
INSERT INTO `regionarea` VALUES ('522623', '施秉县', '265', '2278');
INSERT INTO `regionarea` VALUES ('522624', '三穗县', '265', '2279');
INSERT INTO `regionarea` VALUES ('522625', '镇远县', '265', '2280');
INSERT INTO `regionarea` VALUES ('522626', '岑巩县', '265', '2281');
INSERT INTO `regionarea` VALUES ('522627', '天柱县', '265', '2282');
INSERT INTO `regionarea` VALUES ('522628', '锦屏县', '265', '2283');
INSERT INTO `regionarea` VALUES ('522629', '剑河县', '265', '2284');
INSERT INTO `regionarea` VALUES ('522630', '台江县', '265', '2285');
INSERT INTO `regionarea` VALUES ('522631', '黎平县', '265', '2286');
INSERT INTO `regionarea` VALUES ('522632', '榕江县', '265', '2287');
INSERT INTO `regionarea` VALUES ('522633', '从江县', '265', '2288');
INSERT INTO `regionarea` VALUES ('522634', '雷山县', '265', '2289');
INSERT INTO `regionarea` VALUES ('522635', '麻江县', '265', '2290');
INSERT INTO `regionarea` VALUES ('522636', '丹寨县', '265', '2291');
INSERT INTO `regionarea` VALUES ('522701', '都匀市', '266', '2292');
INSERT INTO `regionarea` VALUES ('522702', '福泉市', '266', '2293');
INSERT INTO `regionarea` VALUES ('522722', '荔波县', '266', '2294');
INSERT INTO `regionarea` VALUES ('522723', '贵定县', '266', '2295');
INSERT INTO `regionarea` VALUES ('522725', '瓮安县', '266', '2296');
INSERT INTO `regionarea` VALUES ('522726', '独山县', '266', '2297');
INSERT INTO `regionarea` VALUES ('522727', '平塘县', '266', '2298');
INSERT INTO `regionarea` VALUES ('522728', '罗甸县', '266', '2299');
INSERT INTO `regionarea` VALUES ('522729', '长顺县', '266', '2300');
INSERT INTO `regionarea` VALUES ('522730', '龙里县', '266', '2301');
INSERT INTO `regionarea` VALUES ('522731', '惠水县', '266', '2302');
INSERT INTO `regionarea` VALUES ('522732', '三都水族自治县', '266', '2303');
INSERT INTO `regionarea` VALUES ('530102', '五华区', '267', '2304');
INSERT INTO `regionarea` VALUES ('530103', '盘龙区', '267', '2305');
INSERT INTO `regionarea` VALUES ('530111', '官渡区', '267', '2306');
INSERT INTO `regionarea` VALUES ('530112', '西山区', '267', '2307');
INSERT INTO `regionarea` VALUES ('530113', '东川区', '267', '2308');
INSERT INTO `regionarea` VALUES ('530121', '呈贡区', '267', '2309');
INSERT INTO `regionarea` VALUES ('530122', '晋宁县', '267', '2310');
INSERT INTO `regionarea` VALUES ('530124', '富民县', '267', '2311');
INSERT INTO `regionarea` VALUES ('530125', '宜良县', '267', '2312');
INSERT INTO `regionarea` VALUES ('530126', '石林彝族自治县', '267', '2313');
INSERT INTO `regionarea` VALUES ('530127', '嵩明县', '267', '2314');
INSERT INTO `regionarea` VALUES ('530128', '禄劝彝族苗族自治县', '267', '2315');
INSERT INTO `regionarea` VALUES ('530129', '寻甸回族彝族自治县', '267', '2316');
INSERT INTO `regionarea` VALUES ('530181', '安宁市', '267', '2317');
INSERT INTO `regionarea` VALUES ('530302', '麒麟区', '268', '2318');
INSERT INTO `regionarea` VALUES ('530321', '马龙县', '268', '2319');
INSERT INTO `regionarea` VALUES ('530322', '陆良县', '268', '2320');
INSERT INTO `regionarea` VALUES ('530323', '师宗县', '268', '2321');
INSERT INTO `regionarea` VALUES ('530324', '罗平县', '268', '2322');
INSERT INTO `regionarea` VALUES ('530325', '富源县', '268', '2323');
INSERT INTO `regionarea` VALUES ('530326', '会泽县', '268', '2324');
INSERT INTO `regionarea` VALUES ('530328', '沾益县', '268', '2325');
INSERT INTO `regionarea` VALUES ('530381', '宣威市', '268', '2326');
INSERT INTO `regionarea` VALUES ('530402', '红塔区', '269', '2327');
INSERT INTO `regionarea` VALUES ('530421', '江川县', '269', '2328');
INSERT INTO `regionarea` VALUES ('530422', '澄江县', '269', '2329');
INSERT INTO `regionarea` VALUES ('530423', '通海县', '269', '2330');
INSERT INTO `regionarea` VALUES ('530424', '华宁县', '269', '2331');
INSERT INTO `regionarea` VALUES ('530425', '易门县', '269', '2332');
INSERT INTO `regionarea` VALUES ('530426', '峨山彝族自治县', '269', '2333');
INSERT INTO `regionarea` VALUES ('530427', '新平彝族傣族自治县', '269', '2334');
INSERT INTO `regionarea` VALUES ('530428', '元江哈尼族彝族傣族自治县', '269', '2335');
INSERT INTO `regionarea` VALUES ('530502', '隆阳区', '270', '2336');
INSERT INTO `regionarea` VALUES ('530521', '施甸县', '270', '2337');
INSERT INTO `regionarea` VALUES ('530522', '腾冲县', '270', '2338');
INSERT INTO `regionarea` VALUES ('530523', '龙陵县', '270', '2339');
INSERT INTO `regionarea` VALUES ('530524', '昌宁县', '270', '2340');
INSERT INTO `regionarea` VALUES ('530602', '昭阳区', '271', '2341');
INSERT INTO `regionarea` VALUES ('530621', '鲁甸县', '271', '2342');
INSERT INTO `regionarea` VALUES ('530622', '巧家县', '271', '2343');
INSERT INTO `regionarea` VALUES ('530623', '盐津县', '271', '2344');
INSERT INTO `regionarea` VALUES ('530624', '大关县', '271', '2345');
INSERT INTO `regionarea` VALUES ('530625', '永善县', '271', '2346');
INSERT INTO `regionarea` VALUES ('530626', '绥江县', '271', '2347');
INSERT INTO `regionarea` VALUES ('530627', '镇雄县', '271', '2348');
INSERT INTO `regionarea` VALUES ('530628', '彝良县', '271', '2349');
INSERT INTO `regionarea` VALUES ('530629', '威信县', '271', '2350');
INSERT INTO `regionarea` VALUES ('530630', '水富县', '271', '2351');
INSERT INTO `regionarea` VALUES ('530702', '古城区', '272', '2352');
INSERT INTO `regionarea` VALUES ('530721', '玉龙纳西族自治县', '272', '2353');
INSERT INTO `regionarea` VALUES ('530722', '永胜县', '272', '2354');
INSERT INTO `regionarea` VALUES ('530723', '华坪县', '272', '2355');
INSERT INTO `regionarea` VALUES ('530724', '宁蒗彝族自治县', '272', '2356');
INSERT INTO `regionarea` VALUES ('530802', '思茅区', '273', '2357');
INSERT INTO `regionarea` VALUES ('530821', '宁洱哈尼族彝族自治县', '273', '2358');
INSERT INTO `regionarea` VALUES ('530822', '墨江哈尼族自治县', '273', '2359');
INSERT INTO `regionarea` VALUES ('530823', '景东彝族自治县', '273', '2360');
INSERT INTO `regionarea` VALUES ('530824', '景谷傣族彝族自治县', '273', '2361');
INSERT INTO `regionarea` VALUES ('530825', '镇沅彝族哈尼族拉祜族自治县', '273', '2362');
INSERT INTO `regionarea` VALUES ('530826', '江城哈尼族彝族自治县', '273', '2363');
INSERT INTO `regionarea` VALUES ('530827', '孟连傣族拉祜族佤族自治县', '273', '2364');
INSERT INTO `regionarea` VALUES ('530828', '澜沧拉祜族自治县', '273', '2365');
INSERT INTO `regionarea` VALUES ('530829', '西盟佤族自治县', '273', '2366');
INSERT INTO `regionarea` VALUES ('530902', '临翔区', '274', '2367');
INSERT INTO `regionarea` VALUES ('530921', '凤庆县', '274', '2368');
INSERT INTO `regionarea` VALUES ('530922', '云县', '274', '2369');
INSERT INTO `regionarea` VALUES ('530923', '永德县', '274', '2370');
INSERT INTO `regionarea` VALUES ('530924', '镇康县', '274', '2371');
INSERT INTO `regionarea` VALUES ('530925', '双江拉祜族佤族布朗族傣族自治县', '274', '2372');
INSERT INTO `regionarea` VALUES ('530926', '耿马傣族佤族自治县', '274', '2373');
INSERT INTO `regionarea` VALUES ('530927', '沧源佤族自治县', '274', '2374');
INSERT INTO `regionarea` VALUES ('532301', '楚雄市', '275', '2375');
INSERT INTO `regionarea` VALUES ('532322', '双柏县', '275', '2376');
INSERT INTO `regionarea` VALUES ('532323', '牟定县', '275', '2377');
INSERT INTO `regionarea` VALUES ('532324', '南华县', '275', '2378');
INSERT INTO `regionarea` VALUES ('532325', '姚安县', '275', '2379');
INSERT INTO `regionarea` VALUES ('532326', '大姚县', '275', '2380');
INSERT INTO `regionarea` VALUES ('532327', '永仁县', '275', '2381');
INSERT INTO `regionarea` VALUES ('532328', '元谋县', '275', '2382');
INSERT INTO `regionarea` VALUES ('532329', '武定县', '275', '2383');
INSERT INTO `regionarea` VALUES ('532331', '禄丰县', '275', '2384');
INSERT INTO `regionarea` VALUES ('532501', '个旧市', '276', '2385');
INSERT INTO `regionarea` VALUES ('532502', '开远市', '276', '2386');
INSERT INTO `regionarea` VALUES ('532503', '蒙自市', '276', '2387');
INSERT INTO `regionarea` VALUES ('532523', '屏边苗族自治县', '276', '2388');
INSERT INTO `regionarea` VALUES ('532524', '建水县', '276', '2389');
INSERT INTO `regionarea` VALUES ('532525', '石屏县', '276', '2390');
INSERT INTO `regionarea` VALUES ('532526', '弥勒县', '276', '2391');
INSERT INTO `regionarea` VALUES ('532527', '泸西县', '276', '2392');
INSERT INTO `regionarea` VALUES ('532528', '元阳县', '276', '2393');
INSERT INTO `regionarea` VALUES ('532529', '红河县', '276', '2394');
INSERT INTO `regionarea` VALUES ('532530', '金平苗族瑶族傣族自治县', '276', '2395');
INSERT INTO `regionarea` VALUES ('532531', '绿春县', '276', '2396');
INSERT INTO `regionarea` VALUES ('532532', '河口瑶族自治县', '276', '2397');
INSERT INTO `regionarea` VALUES ('532621', '文山市', '277', '2398');
INSERT INTO `regionarea` VALUES ('532622', '砚山县', '277', '2399');
INSERT INTO `regionarea` VALUES ('532623', '西畴县', '277', '2400');
INSERT INTO `regionarea` VALUES ('532624', '麻栗坡县', '277', '2401');
INSERT INTO `regionarea` VALUES ('532625', '马关县', '277', '2402');
INSERT INTO `regionarea` VALUES ('532626', '丘北县', '277', '2403');
INSERT INTO `regionarea` VALUES ('532627', '广南县', '277', '2404');
INSERT INTO `regionarea` VALUES ('532628', '富宁县', '277', '2405');
INSERT INTO `regionarea` VALUES ('532801', '景洪市', '278', '2406');
INSERT INTO `regionarea` VALUES ('532822', '勐海县', '278', '2407');
INSERT INTO `regionarea` VALUES ('532823', '勐腊县', '278', '2408');
INSERT INTO `regionarea` VALUES ('532901', '大理市', '279', '2409');
INSERT INTO `regionarea` VALUES ('532922', '漾濞彝族自治县', '279', '2410');
INSERT INTO `regionarea` VALUES ('532923', '祥云县', '279', '2411');
INSERT INTO `regionarea` VALUES ('532924', '宾川县', '279', '2412');
INSERT INTO `regionarea` VALUES ('532925', '弥渡县', '279', '2413');
INSERT INTO `regionarea` VALUES ('532926', '南涧彝族自治县', '279', '2414');
INSERT INTO `regionarea` VALUES ('532927', '巍山彝族回族自治县', '279', '2415');
INSERT INTO `regionarea` VALUES ('532928', '永平县', '279', '2416');
INSERT INTO `regionarea` VALUES ('532929', '云龙县', '279', '2417');
INSERT INTO `regionarea` VALUES ('532930', '洱源县', '279', '2418');
INSERT INTO `regionarea` VALUES ('532931', '剑川县', '279', '2419');
INSERT INTO `regionarea` VALUES ('532932', '鹤庆县', '279', '2420');
INSERT INTO `regionarea` VALUES ('533102', '瑞丽市', '280', '2421');
INSERT INTO `regionarea` VALUES ('533103', '芒市', '280', '2422');
INSERT INTO `regionarea` VALUES ('533122', '梁河县', '280', '2423');
INSERT INTO `regionarea` VALUES ('533123', '盈江县', '280', '2424');
INSERT INTO `regionarea` VALUES ('533124', '陇川县', '280', '2425');
INSERT INTO `regionarea` VALUES ('533321', '泸水县', '281', '2426');
INSERT INTO `regionarea` VALUES ('533323', '福贡县', '281', '2427');
INSERT INTO `regionarea` VALUES ('533324', '贡山独龙族怒族自治县', '281', '2428');
INSERT INTO `regionarea` VALUES ('533325', '兰坪白族普米族自治县', '281', '2429');
INSERT INTO `regionarea` VALUES ('533421', '香格里拉县', '282', '2430');
INSERT INTO `regionarea` VALUES ('533422', '德钦县', '282', '2431');
INSERT INTO `regionarea` VALUES ('533423', '维西傈僳族自治县', '282', '2432');
INSERT INTO `regionarea` VALUES ('540102', '城关区', '283', '2433');
INSERT INTO `regionarea` VALUES ('540121', '林周县', '283', '2434');
INSERT INTO `regionarea` VALUES ('540122', '当雄县', '283', '2435');
INSERT INTO `regionarea` VALUES ('540123', '尼木县', '283', '2436');
INSERT INTO `regionarea` VALUES ('540124', '曲水县', '283', '2437');
INSERT INTO `regionarea` VALUES ('540125', '堆龙德庆县', '283', '2438');
INSERT INTO `regionarea` VALUES ('540126', '达孜县', '283', '2439');
INSERT INTO `regionarea` VALUES ('540127', '墨竹工卡县', '283', '2440');
INSERT INTO `regionarea` VALUES ('542121', '昌都县', '284', '2441');
INSERT INTO `regionarea` VALUES ('542122', '江达县', '284', '2442');
INSERT INTO `regionarea` VALUES ('542123', '贡觉县', '284', '2443');
INSERT INTO `regionarea` VALUES ('542124', '类乌齐县', '284', '2444');
INSERT INTO `regionarea` VALUES ('542125', '丁青县', '284', '2445');
INSERT INTO `regionarea` VALUES ('542126', '察雅县', '284', '2446');
INSERT INTO `regionarea` VALUES ('542127', '八宿县', '284', '2447');
INSERT INTO `regionarea` VALUES ('542128', '左贡县', '284', '2448');
INSERT INTO `regionarea` VALUES ('542129', '芒康县', '284', '2449');
INSERT INTO `regionarea` VALUES ('542132', '洛隆县', '284', '2450');
INSERT INTO `regionarea` VALUES ('542133', '边坝县', '284', '2451');
INSERT INTO `regionarea` VALUES ('542221', '乃东县', '285', '2452');
INSERT INTO `regionarea` VALUES ('542222', '扎囊县', '285', '2453');
INSERT INTO `regionarea` VALUES ('542223', '贡嘎县', '285', '2454');
INSERT INTO `regionarea` VALUES ('542224', '桑日县', '285', '2455');
INSERT INTO `regionarea` VALUES ('542225', '琼结县', '285', '2456');
INSERT INTO `regionarea` VALUES ('542226', '曲松县', '285', '2457');
INSERT INTO `regionarea` VALUES ('542227', '措美县', '285', '2458');
INSERT INTO `regionarea` VALUES ('542228', '洛扎县', '285', '2459');
INSERT INTO `regionarea` VALUES ('542229', '加查县', '285', '2460');
INSERT INTO `regionarea` VALUES ('542231', '隆子县', '285', '2461');
INSERT INTO `regionarea` VALUES ('542232', '错那县', '285', '2462');
INSERT INTO `regionarea` VALUES ('542233', '浪卡子县', '285', '2463');
INSERT INTO `regionarea` VALUES ('542301', '日喀则市', '286', '2464');
INSERT INTO `regionarea` VALUES ('542322', '南木林县', '286', '2465');
INSERT INTO `regionarea` VALUES ('542323', '江孜县', '286', '2466');
INSERT INTO `regionarea` VALUES ('542324', '定日县', '286', '2467');
INSERT INTO `regionarea` VALUES ('542325', '萨迦县', '286', '2468');
INSERT INTO `regionarea` VALUES ('542326', '拉孜县', '286', '2469');
INSERT INTO `regionarea` VALUES ('542327', '昂仁县', '286', '2470');
INSERT INTO `regionarea` VALUES ('542328', '谢通门县', '286', '2471');
INSERT INTO `regionarea` VALUES ('542329', '白朗县', '286', '2472');
INSERT INTO `regionarea` VALUES ('542330', '仁布县', '286', '2473');
INSERT INTO `regionarea` VALUES ('542331', '康马县', '286', '2474');
INSERT INTO `regionarea` VALUES ('542332', '定结县', '286', '2475');
INSERT INTO `regionarea` VALUES ('542333', '仲巴县', '286', '2476');
INSERT INTO `regionarea` VALUES ('542334', '亚东县', '286', '2477');
INSERT INTO `regionarea` VALUES ('542335', '吉隆县', '286', '2478');
INSERT INTO `regionarea` VALUES ('542336', '聂拉木县', '286', '2479');
INSERT INTO `regionarea` VALUES ('542337', '萨嘎县', '286', '2480');
INSERT INTO `regionarea` VALUES ('542338', '岗巴县', '286', '2481');
INSERT INTO `regionarea` VALUES ('542421', '那曲县', '287', '2482');
INSERT INTO `regionarea` VALUES ('542422', '嘉黎县', '287', '2483');
INSERT INTO `regionarea` VALUES ('542423', '比如县', '287', '2484');
INSERT INTO `regionarea` VALUES ('542424', '聂荣县', '287', '2485');
INSERT INTO `regionarea` VALUES ('542425', '安多县', '287', '2486');
INSERT INTO `regionarea` VALUES ('542426', '申扎县', '287', '2487');
INSERT INTO `regionarea` VALUES ('542427', '索县', '287', '2488');
INSERT INTO `regionarea` VALUES ('542428', '班戈县', '287', '2489');
INSERT INTO `regionarea` VALUES ('542429', '巴青县', '287', '2490');
INSERT INTO `regionarea` VALUES ('542430', '尼玛县', '287', '2491');
INSERT INTO `regionarea` VALUES ('542521', '普兰县', '288', '2492');
INSERT INTO `regionarea` VALUES ('542522', '札达县', '288', '2493');
INSERT INTO `regionarea` VALUES ('542523', '噶尔县', '288', '2494');
INSERT INTO `regionarea` VALUES ('542524', '日土县', '288', '2495');
INSERT INTO `regionarea` VALUES ('542525', '革吉县', '288', '2496');
INSERT INTO `regionarea` VALUES ('542526', '改则县', '288', '2497');
INSERT INTO `regionarea` VALUES ('542527', '措勤县', '288', '2498');
INSERT INTO `regionarea` VALUES ('542621', '林芝县', '289', '2499');
INSERT INTO `regionarea` VALUES ('542622', '工布江达县', '289', '2500');
INSERT INTO `regionarea` VALUES ('542623', '米林县', '289', '2501');
INSERT INTO `regionarea` VALUES ('542624', '墨脱县', '289', '2502');
INSERT INTO `regionarea` VALUES ('542625', '波密县', '289', '2503');
INSERT INTO `regionarea` VALUES ('542626', '察隅县', '289', '2504');
INSERT INTO `regionarea` VALUES ('542627', '朗县', '289', '2505');
INSERT INTO `regionarea` VALUES ('610102', '新城区', '290', '2506');
INSERT INTO `regionarea` VALUES ('610103', '碑林区', '290', '2507');
INSERT INTO `regionarea` VALUES ('610104', '莲湖区', '290', '2508');
INSERT INTO `regionarea` VALUES ('610111', '灞桥区', '290', '2509');
INSERT INTO `regionarea` VALUES ('610112', '未央区', '290', '2510');
INSERT INTO `regionarea` VALUES ('610113', '雁塔区', '290', '2511');
INSERT INTO `regionarea` VALUES ('610114', '阎良区', '290', '2512');
INSERT INTO `regionarea` VALUES ('610115', '临潼区', '290', '2513');
INSERT INTO `regionarea` VALUES ('610116', '长安区', '290', '2514');
INSERT INTO `regionarea` VALUES ('610122', '蓝田县', '290', '2515');
INSERT INTO `regionarea` VALUES ('610124', '周至县', '290', '2516');
INSERT INTO `regionarea` VALUES ('610125', '户县', '290', '2517');
INSERT INTO `regionarea` VALUES ('610126', '高陵县', '290', '2518');
INSERT INTO `regionarea` VALUES ('610202', '王益区', '291', '2519');
INSERT INTO `regionarea` VALUES ('610203', '印台区', '291', '2520');
INSERT INTO `regionarea` VALUES ('610204', '耀州区', '291', '2521');
INSERT INTO `regionarea` VALUES ('610222', '宜君县', '291', '2522');
INSERT INTO `regionarea` VALUES ('610302', '渭滨区', '292', '2523');
INSERT INTO `regionarea` VALUES ('610303', '金台区', '292', '2524');
INSERT INTO `regionarea` VALUES ('610304', '陈仓区', '292', '2525');
INSERT INTO `regionarea` VALUES ('610322', '凤翔县', '292', '2526');
INSERT INTO `regionarea` VALUES ('610323', '岐山县', '292', '2527');
INSERT INTO `regionarea` VALUES ('610324', '扶风县', '292', '2528');
INSERT INTO `regionarea` VALUES ('610326', '眉县', '292', '2529');
INSERT INTO `regionarea` VALUES ('610327', '陇县', '292', '2530');
INSERT INTO `regionarea` VALUES ('610328', '千阳县', '292', '2531');
INSERT INTO `regionarea` VALUES ('610329', '麟游县', '292', '2532');
INSERT INTO `regionarea` VALUES ('610330', '凤县', '292', '2533');
INSERT INTO `regionarea` VALUES ('610331', '太白县', '292', '2534');
INSERT INTO `regionarea` VALUES ('610402', '秦都区', '293', '2535');
INSERT INTO `regionarea` VALUES ('610403', '杨陵区', '293', '2536');
INSERT INTO `regionarea` VALUES ('610404', '渭城区', '293', '2537');
INSERT INTO `regionarea` VALUES ('610422', '三原县', '293', '2538');
INSERT INTO `regionarea` VALUES ('610423', '泾阳县', '293', '2539');
INSERT INTO `regionarea` VALUES ('610424', '乾县', '293', '2540');
INSERT INTO `regionarea` VALUES ('610425', '礼泉县', '293', '2541');
INSERT INTO `regionarea` VALUES ('610426', '永寿县', '293', '2542');
INSERT INTO `regionarea` VALUES ('610427', '彬县', '293', '2543');
INSERT INTO `regionarea` VALUES ('610428', '长武县', '293', '2544');
INSERT INTO `regionarea` VALUES ('610429', '旬邑县', '293', '2545');
INSERT INTO `regionarea` VALUES ('610430', '淳化县', '293', '2546');
INSERT INTO `regionarea` VALUES ('610431', '武功县', '293', '2547');
INSERT INTO `regionarea` VALUES ('610481', '兴平市', '293', '2548');
INSERT INTO `regionarea` VALUES ('610502', '临渭区', '294', '2549');
INSERT INTO `regionarea` VALUES ('610521', '华县', '294', '2550');
INSERT INTO `regionarea` VALUES ('610522', '潼关县', '294', '2551');
INSERT INTO `regionarea` VALUES ('610523', '大荔县', '294', '2552');
INSERT INTO `regionarea` VALUES ('610524', '合阳县', '294', '2553');
INSERT INTO `regionarea` VALUES ('610525', '澄城县', '294', '2554');
INSERT INTO `regionarea` VALUES ('610526', '蒲城县', '294', '2555');
INSERT INTO `regionarea` VALUES ('610527', '白水县', '294', '2556');
INSERT INTO `regionarea` VALUES ('610528', '富平县', '294', '2557');
INSERT INTO `regionarea` VALUES ('610581', '韩城市', '294', '2558');
INSERT INTO `regionarea` VALUES ('610582', '华阴市', '294', '2559');
INSERT INTO `regionarea` VALUES ('610602', '宝塔区', '295', '2560');
INSERT INTO `regionarea` VALUES ('610621', '延长县', '295', '2561');
INSERT INTO `regionarea` VALUES ('610622', '延川县', '295', '2562');
INSERT INTO `regionarea` VALUES ('610623', '子长县', '295', '2563');
INSERT INTO `regionarea` VALUES ('610624', '安塞县', '295', '2564');
INSERT INTO `regionarea` VALUES ('610625', '志丹县', '295', '2565');
INSERT INTO `regionarea` VALUES ('610626', '吴起县', '295', '2566');
INSERT INTO `regionarea` VALUES ('610627', '甘泉县', '295', '2567');
INSERT INTO `regionarea` VALUES ('610628', '富县', '295', '2568');
INSERT INTO `regionarea` VALUES ('610629', '洛川县', '295', '2569');
INSERT INTO `regionarea` VALUES ('610630', '宜川县', '295', '2570');
INSERT INTO `regionarea` VALUES ('610631', '黄龙县', '295', '2571');
INSERT INTO `regionarea` VALUES ('610632', '黄陵县', '295', '2572');
INSERT INTO `regionarea` VALUES ('610702', '汉台区', '296', '2573');
INSERT INTO `regionarea` VALUES ('610721', '南郑县', '296', '2574');
INSERT INTO `regionarea` VALUES ('610722', '城固县', '296', '2575');
INSERT INTO `regionarea` VALUES ('610723', '洋县', '296', '2576');
INSERT INTO `regionarea` VALUES ('610724', '西乡县', '296', '2577');
INSERT INTO `regionarea` VALUES ('610725', '勉县', '296', '2578');
INSERT INTO `regionarea` VALUES ('610726', '宁强县', '296', '2579');
INSERT INTO `regionarea` VALUES ('610727', '略阳县', '296', '2580');
INSERT INTO `regionarea` VALUES ('610728', '镇巴县', '296', '2581');
INSERT INTO `regionarea` VALUES ('610729', '留坝县', '296', '2582');
INSERT INTO `regionarea` VALUES ('610730', '佛坪县', '296', '2583');
INSERT INTO `regionarea` VALUES ('610802', '榆阳区', '297', '2584');
INSERT INTO `regionarea` VALUES ('610821', '神木县', '297', '2585');
INSERT INTO `regionarea` VALUES ('610822', '府谷县', '297', '2586');
INSERT INTO `regionarea` VALUES ('610823', '横山县', '297', '2587');
INSERT INTO `regionarea` VALUES ('610824', '靖边县', '297', '2588');
INSERT INTO `regionarea` VALUES ('610825', '定边县', '297', '2589');
INSERT INTO `regionarea` VALUES ('610826', '绥德县', '297', '2590');
INSERT INTO `regionarea` VALUES ('610827', '米脂县', '297', '2591');
INSERT INTO `regionarea` VALUES ('610828', '佳县', '297', '2592');
INSERT INTO `regionarea` VALUES ('610829', '吴堡县', '297', '2593');
INSERT INTO `regionarea` VALUES ('610830', '清涧县', '297', '2594');
INSERT INTO `regionarea` VALUES ('610831', '子洲县', '297', '2595');
INSERT INTO `regionarea` VALUES ('610902', '汉滨区', '298', '2596');
INSERT INTO `regionarea` VALUES ('610921', '汉阴县', '298', '2597');
INSERT INTO `regionarea` VALUES ('610922', '石泉县', '298', '2598');
INSERT INTO `regionarea` VALUES ('610923', '宁陕县', '298', '2599');
INSERT INTO `regionarea` VALUES ('610924', '紫阳县', '298', '2600');
INSERT INTO `regionarea` VALUES ('610925', '岚皋县', '298', '2601');
INSERT INTO `regionarea` VALUES ('610926', '平利县', '298', '2602');
INSERT INTO `regionarea` VALUES ('610927', '镇坪县', '298', '2603');
INSERT INTO `regionarea` VALUES ('610928', '旬阳县', '298', '2604');
INSERT INTO `regionarea` VALUES ('610929', '白河县', '298', '2605');
INSERT INTO `regionarea` VALUES ('611002', '商州区', '299', '2606');
INSERT INTO `regionarea` VALUES ('611021', '洛南县', '299', '2607');
INSERT INTO `regionarea` VALUES ('611022', '丹凤县', '299', '2608');
INSERT INTO `regionarea` VALUES ('611023', '商南县', '299', '2609');
INSERT INTO `regionarea` VALUES ('611024', '山阳县', '299', '2610');
INSERT INTO `regionarea` VALUES ('611025', '镇安县', '299', '2611');
INSERT INTO `regionarea` VALUES ('611026', '柞水县', '299', '2612');
INSERT INTO `regionarea` VALUES ('620102', '城关区', '300', '2613');
INSERT INTO `regionarea` VALUES ('620103', '七里河区', '300', '2614');
INSERT INTO `regionarea` VALUES ('620104', '西固区', '300', '2615');
INSERT INTO `regionarea` VALUES ('620105', '安宁区', '300', '2616');
INSERT INTO `regionarea` VALUES ('620111', '红古区', '300', '2617');
INSERT INTO `regionarea` VALUES ('620121', '永登县', '300', '2618');
INSERT INTO `regionarea` VALUES ('620122', '皋兰县', '300', '2619');
INSERT INTO `regionarea` VALUES ('620123', '榆中县', '300', '2620');
INSERT INTO `regionarea` VALUES ('620201', '嘉峪关市', '301', '2621');
INSERT INTO `regionarea` VALUES ('620302', '金川区', '302', '2622');
INSERT INTO `regionarea` VALUES ('620321', '永昌县', '302', '2623');
INSERT INTO `regionarea` VALUES ('620402', '白银区', '303', '2624');
INSERT INTO `regionarea` VALUES ('620403', '平川区', '303', '2625');
INSERT INTO `regionarea` VALUES ('620421', '靖远县', '303', '2626');
INSERT INTO `regionarea` VALUES ('620422', '会宁县', '303', '2627');
INSERT INTO `regionarea` VALUES ('620423', '景泰县', '303', '2628');
INSERT INTO `regionarea` VALUES ('620502', '秦州区', '304', '2629');
INSERT INTO `regionarea` VALUES ('620503', '麦积区', '304', '2630');
INSERT INTO `regionarea` VALUES ('620521', '清水县', '304', '2631');
INSERT INTO `regionarea` VALUES ('620522', '秦安县', '304', '2632');
INSERT INTO `regionarea` VALUES ('620523', '甘谷县', '304', '2633');
INSERT INTO `regionarea` VALUES ('620524', '武山县', '304', '2634');
INSERT INTO `regionarea` VALUES ('620525', '张家川回族自治县', '304', '2635');
INSERT INTO `regionarea` VALUES ('620602', '凉州区', '305', '2636');
INSERT INTO `regionarea` VALUES ('620621', '民勤县', '305', '2637');
INSERT INTO `regionarea` VALUES ('620622', '古浪县', '305', '2638');
INSERT INTO `regionarea` VALUES ('620623', '天祝藏族自治县', '305', '2639');
INSERT INTO `regionarea` VALUES ('620702', '甘州区', '306', '2640');
INSERT INTO `regionarea` VALUES ('620721', '肃南裕固族自治县', '306', '2641');
INSERT INTO `regionarea` VALUES ('620722', '民乐县', '306', '2642');
INSERT INTO `regionarea` VALUES ('620723', '临泽县', '306', '2643');
INSERT INTO `regionarea` VALUES ('620724', '高台县', '306', '2644');
INSERT INTO `regionarea` VALUES ('620725', '山丹县', '306', '2645');
INSERT INTO `regionarea` VALUES ('620802', '崆峒区', '307', '2646');
INSERT INTO `regionarea` VALUES ('620821', '泾川县', '307', '2647');
INSERT INTO `regionarea` VALUES ('620822', '灵台县', '307', '2648');
INSERT INTO `regionarea` VALUES ('620823', '崇信县', '307', '2649');
INSERT INTO `regionarea` VALUES ('620824', '华亭县', '307', '2650');
INSERT INTO `regionarea` VALUES ('620825', '庄浪县', '307', '2651');
INSERT INTO `regionarea` VALUES ('620826', '静宁县', '307', '2652');
INSERT INTO `regionarea` VALUES ('620902', '肃州区', '308', '2653');
INSERT INTO `regionarea` VALUES ('620921', '金塔县', '308', '2654');
INSERT INTO `regionarea` VALUES ('620922', '瓜州县', '308', '2655');
INSERT INTO `regionarea` VALUES ('620923', '肃北蒙古族自治县', '308', '2656');
INSERT INTO `regionarea` VALUES ('620924', '阿克塞哈萨克族自治县', '308', '2657');
INSERT INTO `regionarea` VALUES ('620981', '玉门市', '308', '2658');
INSERT INTO `regionarea` VALUES ('620982', '敦煌市', '308', '2659');
INSERT INTO `regionarea` VALUES ('621002', '西峰区', '309', '2660');
INSERT INTO `regionarea` VALUES ('621021', '庆城县', '309', '2661');
INSERT INTO `regionarea` VALUES ('621022', '环县', '309', '2662');
INSERT INTO `regionarea` VALUES ('621023', '华池县', '309', '2663');
INSERT INTO `regionarea` VALUES ('621024', '合水县', '309', '2664');
INSERT INTO `regionarea` VALUES ('621025', '正宁县', '309', '2665');
INSERT INTO `regionarea` VALUES ('621026', '宁县', '309', '2666');
INSERT INTO `regionarea` VALUES ('621027', '镇原县', '309', '2667');
INSERT INTO `regionarea` VALUES ('621102', '安定区', '310', '2668');
INSERT INTO `regionarea` VALUES ('621121', '通渭县', '310', '2669');
INSERT INTO `regionarea` VALUES ('621122', '陇西县', '310', '2670');
INSERT INTO `regionarea` VALUES ('621123', '渭源县', '310', '2671');
INSERT INTO `regionarea` VALUES ('621124', '临洮县', '310', '2672');
INSERT INTO `regionarea` VALUES ('621125', '漳县', '310', '2673');
INSERT INTO `regionarea` VALUES ('621126', '岷县', '310', '2674');
INSERT INTO `regionarea` VALUES ('621202', '武都区', '311', '2675');
INSERT INTO `regionarea` VALUES ('621221', '成县', '311', '2676');
INSERT INTO `regionarea` VALUES ('621222', '文县', '311', '2677');
INSERT INTO `regionarea` VALUES ('621223', '宕昌县', '311', '2678');
INSERT INTO `regionarea` VALUES ('621224', '康县', '311', '2679');
INSERT INTO `regionarea` VALUES ('621225', '西和县', '311', '2680');
INSERT INTO `regionarea` VALUES ('621226', '礼县', '311', '2681');
INSERT INTO `regionarea` VALUES ('621227', '徽县', '311', '2682');
INSERT INTO `regionarea` VALUES ('621228', '两当县', '311', '2683');
INSERT INTO `regionarea` VALUES ('622901', '临夏市', '312', '2684');
INSERT INTO `regionarea` VALUES ('622921', '临夏县', '312', '2685');
INSERT INTO `regionarea` VALUES ('622922', '康乐县', '312', '2686');
INSERT INTO `regionarea` VALUES ('622923', '永靖县', '312', '2687');
INSERT INTO `regionarea` VALUES ('622924', '广河县', '312', '2688');
INSERT INTO `regionarea` VALUES ('622925', '和政县', '312', '2689');
INSERT INTO `regionarea` VALUES ('622926', '东乡族自治县', '312', '2690');
INSERT INTO `regionarea` VALUES ('622927', '积石山保安族东乡族撒拉族自治县', '312', '2691');
INSERT INTO `regionarea` VALUES ('623001', '合作市', '313', '2692');
INSERT INTO `regionarea` VALUES ('623021', '临潭县', '313', '2693');
INSERT INTO `regionarea` VALUES ('623022', '卓尼县', '313', '2694');
INSERT INTO `regionarea` VALUES ('623023', '舟曲县', '313', '2695');
INSERT INTO `regionarea` VALUES ('623024', '迭部县', '313', '2696');
INSERT INTO `regionarea` VALUES ('623025', '玛曲县', '313', '2697');
INSERT INTO `regionarea` VALUES ('623026', '碌曲县', '313', '2698');
INSERT INTO `regionarea` VALUES ('623027', '夏河县', '313', '2699');
INSERT INTO `regionarea` VALUES ('630102', '城东区', '314', '2700');
INSERT INTO `regionarea` VALUES ('630103', '城中区', '314', '2701');
INSERT INTO `regionarea` VALUES ('630104', '城西区', '314', '2702');
INSERT INTO `regionarea` VALUES ('630105', '城北区', '314', '2703');
INSERT INTO `regionarea` VALUES ('630121', '大通回族土族自治县', '314', '2704');
INSERT INTO `regionarea` VALUES ('630122', '湟中县', '314', '2705');
INSERT INTO `regionarea` VALUES ('630123', '湟源县', '314', '2706');
INSERT INTO `regionarea` VALUES ('632121', '平安县', '315', '2707');
INSERT INTO `regionarea` VALUES ('632122', '民和回族土族自治县', '315', '2708');
INSERT INTO `regionarea` VALUES ('632123', '乐都县', '315', '2709');
INSERT INTO `regionarea` VALUES ('632126', '互助土族自治县', '315', '2710');
INSERT INTO `regionarea` VALUES ('632127', '化隆回族自治县', '315', '2711');
INSERT INTO `regionarea` VALUES ('632128', '循化撒拉族自治县', '315', '2712');
INSERT INTO `regionarea` VALUES ('632221', '门源回族自治县', '316', '2713');
INSERT INTO `regionarea` VALUES ('632222', '祁连县', '316', '2714');
INSERT INTO `regionarea` VALUES ('632223', '海晏县', '316', '2715');
INSERT INTO `regionarea` VALUES ('632224', '刚察县', '316', '2716');
INSERT INTO `regionarea` VALUES ('632321', '同仁县', '317', '2717');
INSERT INTO `regionarea` VALUES ('632322', '尖扎县', '317', '2718');
INSERT INTO `regionarea` VALUES ('632323', '泽库县', '317', '2719');
INSERT INTO `regionarea` VALUES ('632324', '河南蒙古族自治县', '317', '2720');
INSERT INTO `regionarea` VALUES ('632521', '共和县', '318', '2721');
INSERT INTO `regionarea` VALUES ('632522', '同德县', '318', '2722');
INSERT INTO `regionarea` VALUES ('632523', '贵德县', '318', '2723');
INSERT INTO `regionarea` VALUES ('632524', '兴海县', '318', '2724');
INSERT INTO `regionarea` VALUES ('632525', '贵南县', '318', '2725');
INSERT INTO `regionarea` VALUES ('632621', '玛沁县', '319', '2726');
INSERT INTO `regionarea` VALUES ('632622', '班玛县', '319', '2727');
INSERT INTO `regionarea` VALUES ('632623', '甘德县', '319', '2728');
INSERT INTO `regionarea` VALUES ('632624', '达日县', '319', '2729');
INSERT INTO `regionarea` VALUES ('632625', '久治县', '319', '2730');
INSERT INTO `regionarea` VALUES ('632626', '玛多县', '319', '2731');
INSERT INTO `regionarea` VALUES ('632721', '玉树县', '320', '2732');
INSERT INTO `regionarea` VALUES ('632722', '杂多县', '320', '2733');
INSERT INTO `regionarea` VALUES ('632723', '称多县', '320', '2734');
INSERT INTO `regionarea` VALUES ('632724', '治多县', '320', '2735');
INSERT INTO `regionarea` VALUES ('632725', '囊谦县', '320', '2736');
INSERT INTO `regionarea` VALUES ('632726', '曲麻莱县', '320', '2737');
INSERT INTO `regionarea` VALUES ('632801', '格尔木市', '321', '2738');
INSERT INTO `regionarea` VALUES ('632802', '德令哈市', '321', '2739');
INSERT INTO `regionarea` VALUES ('632821', '乌兰县', '321', '2740');
INSERT INTO `regionarea` VALUES ('632822', '都兰县', '321', '2741');
INSERT INTO `regionarea` VALUES ('632823', '天峻县', '321', '2742');
INSERT INTO `regionarea` VALUES ('640104', '兴庆区', '322', '2743');
INSERT INTO `regionarea` VALUES ('640105', '西夏区', '322', '2744');
INSERT INTO `regionarea` VALUES ('640106', '金凤区', '322', '2745');
INSERT INTO `regionarea` VALUES ('640121', '永宁县', '322', '2746');
INSERT INTO `regionarea` VALUES ('640122', '贺兰县', '322', '2747');
INSERT INTO `regionarea` VALUES ('640181', '灵武市', '322', '2748');
INSERT INTO `regionarea` VALUES ('640202', '大武口区', '323', '2749');
INSERT INTO `regionarea` VALUES ('640205', '惠农区', '323', '2750');
INSERT INTO `regionarea` VALUES ('640221', '平罗县', '323', '2751');
INSERT INTO `regionarea` VALUES ('640302', '利通区', '324', '2752');
INSERT INTO `regionarea` VALUES ('640303', '红寺堡区', '324', '2753');
INSERT INTO `regionarea` VALUES ('640323', '盐池县', '324', '2754');
INSERT INTO `regionarea` VALUES ('640324', '同心县', '324', '2755');
INSERT INTO `regionarea` VALUES ('640381', '青铜峡市', '324', '2756');
INSERT INTO `regionarea` VALUES ('640402', '原州区', '325', '2757');
INSERT INTO `regionarea` VALUES ('640422', '西吉县', '325', '2758');
INSERT INTO `regionarea` VALUES ('640423', '隆德县', '325', '2759');
INSERT INTO `regionarea` VALUES ('640424', '泾源县', '325', '2760');
INSERT INTO `regionarea` VALUES ('640425', '彭阳县', '325', '2761');
INSERT INTO `regionarea` VALUES ('640502', '沙坡头区', '326', '2762');
INSERT INTO `regionarea` VALUES ('640521', '中宁县', '326', '2763');
INSERT INTO `regionarea` VALUES ('640522', '海原县', '326', '2764');
INSERT INTO `regionarea` VALUES ('650102', '天山区', '327', '2765');
INSERT INTO `regionarea` VALUES ('650103', '沙依巴克区', '327', '2766');
INSERT INTO `regionarea` VALUES ('650104', '新市区', '327', '2767');
INSERT INTO `regionarea` VALUES ('650105', '水磨沟区', '327', '2768');
INSERT INTO `regionarea` VALUES ('650106', '头屯河区', '327', '2769');
INSERT INTO `regionarea` VALUES ('650107', '达坂城区', '327', '2770');
INSERT INTO `regionarea` VALUES ('650109', '米东区', '327', '2771');
INSERT INTO `regionarea` VALUES ('650121', '乌鲁木齐县', '327', '2772');
INSERT INTO `regionarea` VALUES ('650202', '独山子区', '328', '2773');
INSERT INTO `regionarea` VALUES ('650203', '克拉玛依区', '328', '2774');
INSERT INTO `regionarea` VALUES ('650204', '白碱滩区', '328', '2775');
INSERT INTO `regionarea` VALUES ('650205', '乌尔禾区', '328', '2776');
INSERT INTO `regionarea` VALUES ('652101', '吐鲁番市', '329', '2777');
INSERT INTO `regionarea` VALUES ('652122', '鄯善县', '329', '2778');
INSERT INTO `regionarea` VALUES ('652123', '托克逊县', '329', '2779');
INSERT INTO `regionarea` VALUES ('652201', '哈密市', '330', '2780');
INSERT INTO `regionarea` VALUES ('652222', '巴里坤哈萨克自治县', '330', '2781');
INSERT INTO `regionarea` VALUES ('652223', '伊吾县', '330', '2782');
INSERT INTO `regionarea` VALUES ('652301', '昌吉市', '331', '2783');
INSERT INTO `regionarea` VALUES ('652302', '阜康市', '331', '2784');
INSERT INTO `regionarea` VALUES ('652323', '呼图壁县', '331', '2785');
INSERT INTO `regionarea` VALUES ('652324', '玛纳斯县', '331', '2786');
INSERT INTO `regionarea` VALUES ('652325', '奇台县', '331', '2787');
INSERT INTO `regionarea` VALUES ('652327', '吉木萨尔县', '331', '2788');
INSERT INTO `regionarea` VALUES ('652328', '木垒哈萨克自治县', '331', '2789');
INSERT INTO `regionarea` VALUES ('652701', '博乐市', '332', '2790');
INSERT INTO `regionarea` VALUES ('652722', '精河县', '332', '2791');
INSERT INTO `regionarea` VALUES ('652723', '温泉县', '332', '2792');
INSERT INTO `regionarea` VALUES ('652801', '库尔勒市', '333', '2793');
INSERT INTO `regionarea` VALUES ('652822', '轮台县', '333', '2794');
INSERT INTO `regionarea` VALUES ('652823', '尉犁县', '333', '2795');
INSERT INTO `regionarea` VALUES ('652824', '若羌县', '333', '2796');
INSERT INTO `regionarea` VALUES ('652825', '且末县', '333', '2797');
INSERT INTO `regionarea` VALUES ('652826', '焉耆回族自治县', '333', '2798');
INSERT INTO `regionarea` VALUES ('652827', '和静县', '333', '2799');
INSERT INTO `regionarea` VALUES ('652828', '和硕县', '333', '2800');
INSERT INTO `regionarea` VALUES ('652829', '博湖县', '333', '2801');
INSERT INTO `regionarea` VALUES ('652901', '阿克苏市', '334', '2802');
INSERT INTO `regionarea` VALUES ('652922', '温宿县', '334', '2803');
INSERT INTO `regionarea` VALUES ('652923', '库车县', '334', '2804');
INSERT INTO `regionarea` VALUES ('652924', '沙雅县', '334', '2805');
INSERT INTO `regionarea` VALUES ('652925', '新和县', '334', '2806');
INSERT INTO `regionarea` VALUES ('652926', '拜城县', '334', '2807');
INSERT INTO `regionarea` VALUES ('652927', '乌什县', '334', '2808');
INSERT INTO `regionarea` VALUES ('652928', '阿瓦提县', '334', '2809');
INSERT INTO `regionarea` VALUES ('652929', '柯坪县', '334', '2810');
INSERT INTO `regionarea` VALUES ('653001', '阿图什市', '335', '2811');
INSERT INTO `regionarea` VALUES ('653022', '阿克陶县', '335', '2812');
INSERT INTO `regionarea` VALUES ('653023', '阿合奇县', '335', '2813');
INSERT INTO `regionarea` VALUES ('653024', '乌恰县', '335', '2814');
INSERT INTO `regionarea` VALUES ('653101', '喀什市', '336', '2815');
INSERT INTO `regionarea` VALUES ('653121', '疏附县', '336', '2816');
INSERT INTO `regionarea` VALUES ('653122', '疏勒县', '336', '2817');
INSERT INTO `regionarea` VALUES ('653123', '英吉沙县', '336', '2818');
INSERT INTO `regionarea` VALUES ('653124', '泽普县', '336', '2819');
INSERT INTO `regionarea` VALUES ('653125', '莎车县', '336', '2820');
INSERT INTO `regionarea` VALUES ('653126', '叶城县', '336', '2821');
INSERT INTO `regionarea` VALUES ('653127', '麦盖提县', '336', '2822');
INSERT INTO `regionarea` VALUES ('653128', '岳普湖县', '336', '2823');
INSERT INTO `regionarea` VALUES ('653129', '伽师县', '336', '2824');
INSERT INTO `regionarea` VALUES ('653130', '巴楚县', '336', '2825');
INSERT INTO `regionarea` VALUES ('653131', '塔什库尔干塔吉克自治县', '336', '2826');
INSERT INTO `regionarea` VALUES ('653201', '和田市', '337', '2827');
INSERT INTO `regionarea` VALUES ('653221', '和田县', '337', '2828');
INSERT INTO `regionarea` VALUES ('653222', '墨玉县', '337', '2829');
INSERT INTO `regionarea` VALUES ('653223', '皮山县', '337', '2830');
INSERT INTO `regionarea` VALUES ('653224', '洛浦县', '337', '2831');
INSERT INTO `regionarea` VALUES ('653225', '策勒县', '337', '2832');
INSERT INTO `regionarea` VALUES ('653226', '于田县', '337', '2833');
INSERT INTO `regionarea` VALUES ('653227', '民丰县', '337', '2834');
INSERT INTO `regionarea` VALUES ('654002', '伊宁市', '338', '2835');
INSERT INTO `regionarea` VALUES ('654003', '奎屯市', '338', '2836');
INSERT INTO `regionarea` VALUES ('654021', '伊宁县', '338', '2837');
INSERT INTO `regionarea` VALUES ('654022', '察布查尔锡伯自治县', '338', '2838');
INSERT INTO `regionarea` VALUES ('654023', '霍城县', '338', '2839');
INSERT INTO `regionarea` VALUES ('654024', '巩留县', '338', '2840');
INSERT INTO `regionarea` VALUES ('654025', '新源县', '338', '2841');
INSERT INTO `regionarea` VALUES ('654026', '昭苏县', '338', '2842');
INSERT INTO `regionarea` VALUES ('654027', '特克斯县', '338', '2843');
INSERT INTO `regionarea` VALUES ('654028', '尼勒克县', '338', '2844');
INSERT INTO `regionarea` VALUES ('654201', '塔城市', '339', '2845');
INSERT INTO `regionarea` VALUES ('654202', '乌苏市', '339', '2846');
INSERT INTO `regionarea` VALUES ('654221', '额敏县', '339', '2847');
INSERT INTO `regionarea` VALUES ('654223', '沙湾县', '339', '2848');
INSERT INTO `regionarea` VALUES ('654224', '托里县', '339', '2849');
INSERT INTO `regionarea` VALUES ('654225', '裕民县', '339', '2850');
INSERT INTO `regionarea` VALUES ('654226', '和布克赛尔蒙古自治县', '339', '2851');
INSERT INTO `regionarea` VALUES ('654301', '阿勒泰市', '340', '2852');
INSERT INTO `regionarea` VALUES ('654321', '布尔津县', '340', '2853');
INSERT INTO `regionarea` VALUES ('654322', '富蕴县', '340', '2854');
INSERT INTO `regionarea` VALUES ('654323', '福海县', '340', '2855');
INSERT INTO `regionarea` VALUES ('654324', '哈巴河县', '340', '2856');
INSERT INTO `regionarea` VALUES ('654325', '青河县', '340', '2857');
INSERT INTO `regionarea` VALUES ('654326', '吉木乃县', '340', '2858');
INSERT INTO `regionarea` VALUES ('659001', '石河子市', '341', '2859');
INSERT INTO `regionarea` VALUES ('659002', '阿拉尔市', '341', '2860');
INSERT INTO `regionarea` VALUES ('659003', '图木舒克市', '341', '2861');
INSERT INTO `regionarea` VALUES ('659004', '五家渠市', '341', '2862');
INSERT INTO `regionarea` VALUES ('710101', '中正区', '342', '2863');
INSERT INTO `regionarea` VALUES ('710102', '大同区', '342', '2864');
INSERT INTO `regionarea` VALUES ('710103', '中山区', '342', '2865');
INSERT INTO `regionarea` VALUES ('710104', '松山区', '342', '2866');
INSERT INTO `regionarea` VALUES ('710105', '大安区', '342', '2867');
INSERT INTO `regionarea` VALUES ('710106', '万华区', '342', '2868');
INSERT INTO `regionarea` VALUES ('710107', '信义区', '342', '2869');
INSERT INTO `regionarea` VALUES ('710108', '士林区', '342', '2870');
INSERT INTO `regionarea` VALUES ('710109', '北投区', '342', '2871');
INSERT INTO `regionarea` VALUES ('710110', '内湖区', '342', '2872');
INSERT INTO `regionarea` VALUES ('710111', '南港区', '342', '2873');
INSERT INTO `regionarea` VALUES ('710112', '文山区', '342', '2874');
INSERT INTO `regionarea` VALUES ('710201', '新兴区', '343', '2875');
INSERT INTO `regionarea` VALUES ('710202', '前金区', '343', '2876');
INSERT INTO `regionarea` VALUES ('710203', '芩雅区', '343', '2877');
INSERT INTO `regionarea` VALUES ('710204', '盐埕区', '343', '2878');
INSERT INTO `regionarea` VALUES ('710205', '鼓山区', '343', '2879');
INSERT INTO `regionarea` VALUES ('710206', '旗津区', '343', '2880');
INSERT INTO `regionarea` VALUES ('710207', '前镇区', '343', '2881');
INSERT INTO `regionarea` VALUES ('710208', '三民区', '343', '2882');
INSERT INTO `regionarea` VALUES ('710209', '左营区', '343', '2883');
INSERT INTO `regionarea` VALUES ('710210', '楠梓区', '343', '2884');
INSERT INTO `regionarea` VALUES ('710211', '小港区', '343', '2885');
INSERT INTO `regionarea` VALUES ('710301', '仁爱区', '344', '2886');
INSERT INTO `regionarea` VALUES ('710302', '信义区', '344', '2887');
INSERT INTO `regionarea` VALUES ('710303', '中正区', '344', '2888');
INSERT INTO `regionarea` VALUES ('710304', '中山区', '344', '2889');
INSERT INTO `regionarea` VALUES ('710305', '安乐区', '344', '2890');
INSERT INTO `regionarea` VALUES ('710306', '暖暖区', '344', '2891');
INSERT INTO `regionarea` VALUES ('710307', '七堵区', '344', '2892');
INSERT INTO `regionarea` VALUES ('710401', '中区', '345', '2893');
INSERT INTO `regionarea` VALUES ('710402', '东区', '345', '2894');
INSERT INTO `regionarea` VALUES ('710403', '南区', '345', '2895');
INSERT INTO `regionarea` VALUES ('710404', '西区', '345', '2896');
INSERT INTO `regionarea` VALUES ('710405', '北区', '345', '2897');
INSERT INTO `regionarea` VALUES ('710406', '北屯区', '345', '2898');
INSERT INTO `regionarea` VALUES ('710407', '西屯区', '345', '2899');
INSERT INTO `regionarea` VALUES ('710408', '南屯区', '345', '2900');
INSERT INTO `regionarea` VALUES ('710501', '中西区', '346', '2901');
INSERT INTO `regionarea` VALUES ('710502', '东区', '346', '2902');
INSERT INTO `regionarea` VALUES ('710503', '南区', '346', '2903');
INSERT INTO `regionarea` VALUES ('710504', '北区', '346', '2904');
INSERT INTO `regionarea` VALUES ('710505', '安平区', '346', '2905');
INSERT INTO `regionarea` VALUES ('710506', '安南区', '346', '2906');
INSERT INTO `regionarea` VALUES ('710601', '东区', '347', '2907');
INSERT INTO `regionarea` VALUES ('710602', '北区', '347', '2908');
INSERT INTO `regionarea` VALUES ('710603', '香山区', '347', '2909');
INSERT INTO `regionarea` VALUES ('710701', '东区', '348', '2910');
INSERT INTO `regionarea` VALUES ('710702', '西区', '348', '2911');
INSERT INTO `regionarea` VALUES ('719001', '台北县', '349', '2912');
INSERT INTO `regionarea` VALUES ('719002', '宜兰县', '349', '2913');
INSERT INTO `regionarea` VALUES ('719003', '新竹县', '349', '2914');
INSERT INTO `regionarea` VALUES ('719004', '桃园县', '349', '2915');
INSERT INTO `regionarea` VALUES ('719005', '苗栗县', '349', '2916');
INSERT INTO `regionarea` VALUES ('719006', '台中县', '349', '2917');
INSERT INTO `regionarea` VALUES ('719007', '彰化县', '349', '2918');
INSERT INTO `regionarea` VALUES ('719008', '南投县', '349', '2919');
INSERT INTO `regionarea` VALUES ('719009', '嘉义县', '349', '2920');
INSERT INTO `regionarea` VALUES ('719010', '云林县', '349', '2921');
INSERT INTO `regionarea` VALUES ('719011', '台南县', '349', '2922');
INSERT INTO `regionarea` VALUES ('719012', '高雄县', '349', '2923');
INSERT INTO `regionarea` VALUES ('719013', '屏东县', '349', '2924');
INSERT INTO `regionarea` VALUES ('719014', '台东县', '349', '2925');
INSERT INTO `regionarea` VALUES ('719015', '花莲县', '349', '2926');
INSERT INTO `regionarea` VALUES ('719016', '澎湖县', '349', '2927');
INSERT INTO `regionarea` VALUES ('810101', '中西区', '350', '2928');
INSERT INTO `regionarea` VALUES ('810102', '湾仔区', '350', '2929');
INSERT INTO `regionarea` VALUES ('810103', '东区', '350', '2930');
INSERT INTO `regionarea` VALUES ('810104', '南区', '350', '2931');
INSERT INTO `regionarea` VALUES ('810201', '油尖旺区', '351', '2932');
INSERT INTO `regionarea` VALUES ('810202', '深水埗区', '351', '2933');
INSERT INTO `regionarea` VALUES ('810203', '九龙城区', '351', '2934');
INSERT INTO `regionarea` VALUES ('810204', '黄大仙区', '351', '2935');
INSERT INTO `regionarea` VALUES ('810205', '观塘区', '351', '2936');
INSERT INTO `regionarea` VALUES ('810301', '北区', '352', '2937');
INSERT INTO `regionarea` VALUES ('810302', '大埔区', '352', '2938');
INSERT INTO `regionarea` VALUES ('810303', '沙田区', '352', '2939');
INSERT INTO `regionarea` VALUES ('810304', '西贡区', '352', '2940');
INSERT INTO `regionarea` VALUES ('810305', '荃湾区', '352', '2941');
INSERT INTO `regionarea` VALUES ('810306', '屯门区', '352', '2942');
INSERT INTO `regionarea` VALUES ('810307', '元朗区', '352', '2943');
INSERT INTO `regionarea` VALUES ('810308', '葵青区', '352', '2944');
INSERT INTO `regionarea` VALUES ('810309', '离岛区', '352', '2945');
INSERT INTO `regionarea` VALUES ('820101', '花地玛堂区', '353', '2946');
INSERT INTO `regionarea` VALUES ('820102', '圣安多尼堂区', '353', '2947');
INSERT INTO `regionarea` VALUES ('820103', '大堂区', '353', '2948');
INSERT INTO `regionarea` VALUES ('820104', '望德堂区', '353', '2949');
INSERT INTO `regionarea` VALUES ('820105', '风顺堂区', '353', '2950');
INSERT INTO `regionarea` VALUES ('820201', '嘉模堂区', '354', '2951');
INSERT INTO `regionarea` VALUES ('820202', '圣方济各堂区', '354', '2952');
INSERT INTO `regionarea` VALUES ('820301', '路氹城', '355', '2953');

-- ----------------------------
-- Table structure for regioncity
-- ----------------------------
DROP TABLE IF EXISTS `regioncity`;
CREATE TABLE `regioncity` (
  `city_code` int(11) DEFAULT NULL,
  `city_name` varchar(50) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_regioncity_city_code` (`city_code`),
  KEY `ix_regioncity_city_name` (`city_name`)
) ENGINE=InnoDB AUTO_INCREMENT=356 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of regioncity
-- ----------------------------
INSERT INTO `regioncity` VALUES ('110000', '北京市', '1', '1');
INSERT INTO `regioncity` VALUES ('120000', '天津市', '2', '2');
INSERT INTO `regioncity` VALUES ('130100', '石家庄市', '3', '3');
INSERT INTO `regioncity` VALUES ('130200', '唐山市', '3', '4');
INSERT INTO `regioncity` VALUES ('130300', '秦皇岛市', '3', '5');
INSERT INTO `regioncity` VALUES ('130400', '邯郸市', '3', '6');
INSERT INTO `regioncity` VALUES ('130500', '邢台市', '3', '7');
INSERT INTO `regioncity` VALUES ('130600', '保定市', '3', '8');
INSERT INTO `regioncity` VALUES ('130700', '张家口市', '3', '9');
INSERT INTO `regioncity` VALUES ('130800', '承德市', '3', '10');
INSERT INTO `regioncity` VALUES ('130900', '沧州市', '3', '11');
INSERT INTO `regioncity` VALUES ('131000', '廊坊市', '3', '12');
INSERT INTO `regioncity` VALUES ('131100', '衡水市', '3', '13');
INSERT INTO `regioncity` VALUES ('140100', '太原市', '4', '14');
INSERT INTO `regioncity` VALUES ('140200', '大同市', '4', '15');
INSERT INTO `regioncity` VALUES ('140300', '阳泉市', '4', '16');
INSERT INTO `regioncity` VALUES ('140400', '长治市', '4', '17');
INSERT INTO `regioncity` VALUES ('140500', '晋城市', '4', '18');
INSERT INTO `regioncity` VALUES ('140600', '朔州市', '4', '19');
INSERT INTO `regioncity` VALUES ('140700', '晋中市', '4', '20');
INSERT INTO `regioncity` VALUES ('140800', '运城市', '4', '21');
INSERT INTO `regioncity` VALUES ('140900', '忻州市', '4', '22');
INSERT INTO `regioncity` VALUES ('141000', '临汾市', '4', '23');
INSERT INTO `regioncity` VALUES ('141100', '吕梁市', '4', '24');
INSERT INTO `regioncity` VALUES ('150100', '呼和浩特市', '5', '25');
INSERT INTO `regioncity` VALUES ('150200', '包头市', '5', '26');
INSERT INTO `regioncity` VALUES ('150300', '乌海市', '5', '27');
INSERT INTO `regioncity` VALUES ('150400', '赤峰市', '5', '28');
INSERT INTO `regioncity` VALUES ('150500', '通辽市', '5', '29');
INSERT INTO `regioncity` VALUES ('150600', '鄂尔多斯市', '5', '30');
INSERT INTO `regioncity` VALUES ('150700', '呼伦贝尔市', '5', '31');
INSERT INTO `regioncity` VALUES ('150800', '巴彦淖尔市', '5', '32');
INSERT INTO `regioncity` VALUES ('150900', '乌兰察布市', '5', '33');
INSERT INTO `regioncity` VALUES ('152200', '兴安盟', '5', '34');
INSERT INTO `regioncity` VALUES ('152500', '锡林郭勒盟', '5', '35');
INSERT INTO `regioncity` VALUES ('152900', '阿拉善盟', '5', '36');
INSERT INTO `regioncity` VALUES ('210100', '沈阳市', '6', '37');
INSERT INTO `regioncity` VALUES ('210200', '大连市', '6', '38');
INSERT INTO `regioncity` VALUES ('210300', '鞍山市', '6', '39');
INSERT INTO `regioncity` VALUES ('210400', '抚顺市', '6', '40');
INSERT INTO `regioncity` VALUES ('210500', '本溪市', '6', '41');
INSERT INTO `regioncity` VALUES ('210600', '丹东市', '6', '42');
INSERT INTO `regioncity` VALUES ('210700', '锦州市', '6', '43');
INSERT INTO `regioncity` VALUES ('210800', '营口市', '6', '44');
INSERT INTO `regioncity` VALUES ('210900', '阜新市', '6', '45');
INSERT INTO `regioncity` VALUES ('211000', '辽阳市', '6', '46');
INSERT INTO `regioncity` VALUES ('211100', '盘锦市', '6', '47');
INSERT INTO `regioncity` VALUES ('211200', '铁岭市', '6', '48');
INSERT INTO `regioncity` VALUES ('211300', '朝阳市', '6', '49');
INSERT INTO `regioncity` VALUES ('211400', '葫芦岛市', '6', '50');
INSERT INTO `regioncity` VALUES ('220100', '长春市', '7', '51');
INSERT INTO `regioncity` VALUES ('220200', '吉林市', '7', '52');
INSERT INTO `regioncity` VALUES ('220300', '四平市', '7', '53');
INSERT INTO `regioncity` VALUES ('220400', '辽源市', '7', '54');
INSERT INTO `regioncity` VALUES ('220500', '通化市', '7', '55');
INSERT INTO `regioncity` VALUES ('220600', '白山市', '7', '56');
INSERT INTO `regioncity` VALUES ('220700', '松原市', '7', '57');
INSERT INTO `regioncity` VALUES ('220800', '白城市', '7', '58');
INSERT INTO `regioncity` VALUES ('222400', '延边朝鲜族自治州', '7', '59');
INSERT INTO `regioncity` VALUES ('230100', '哈尔滨市', '8', '60');
INSERT INTO `regioncity` VALUES ('230200', '齐齐哈尔市', '8', '61');
INSERT INTO `regioncity` VALUES ('230300', '鸡西市', '8', '62');
INSERT INTO `regioncity` VALUES ('230400', '鹤岗市', '8', '63');
INSERT INTO `regioncity` VALUES ('230500', '双鸭山市', '8', '64');
INSERT INTO `regioncity` VALUES ('230600', '大庆市', '8', '65');
INSERT INTO `regioncity` VALUES ('230700', '伊春市', '8', '66');
INSERT INTO `regioncity` VALUES ('230800', '佳木斯市', '8', '67');
INSERT INTO `regioncity` VALUES ('230900', '七台河市', '8', '68');
INSERT INTO `regioncity` VALUES ('231000', '牡丹江市', '8', '69');
INSERT INTO `regioncity` VALUES ('231100', '黑河市', '8', '70');
INSERT INTO `regioncity` VALUES ('231200', '绥化市', '8', '71');
INSERT INTO `regioncity` VALUES ('232700', '大兴安岭地区', '8', '72');
INSERT INTO `regioncity` VALUES ('310000', '上海市', '9', '73');
INSERT INTO `regioncity` VALUES ('320100', '南京市', '10', '74');
INSERT INTO `regioncity` VALUES ('320200', '无锡市', '10', '75');
INSERT INTO `regioncity` VALUES ('320300', '徐州市', '10', '76');
INSERT INTO `regioncity` VALUES ('320400', '常州市', '10', '77');
INSERT INTO `regioncity` VALUES ('320500', '苏州市', '10', '78');
INSERT INTO `regioncity` VALUES ('320600', '南通市', '10', '79');
INSERT INTO `regioncity` VALUES ('320700', '连云港市', '10', '80');
INSERT INTO `regioncity` VALUES ('320800', '淮安市', '10', '81');
INSERT INTO `regioncity` VALUES ('320900', '盐城市', '10', '82');
INSERT INTO `regioncity` VALUES ('321000', '扬州市', '10', '83');
INSERT INTO `regioncity` VALUES ('321100', '镇江市', '10', '84');
INSERT INTO `regioncity` VALUES ('321200', '泰州市', '10', '85');
INSERT INTO `regioncity` VALUES ('321300', '宿迁市', '10', '86');
INSERT INTO `regioncity` VALUES ('330100', '杭州市', '11', '87');
INSERT INTO `regioncity` VALUES ('330200', '宁波市', '11', '88');
INSERT INTO `regioncity` VALUES ('330300', '温州市', '11', '89');
INSERT INTO `regioncity` VALUES ('330400', '嘉兴市', '11', '90');
INSERT INTO `regioncity` VALUES ('330500', '湖州市', '11', '91');
INSERT INTO `regioncity` VALUES ('330600', '绍兴市', '11', '92');
INSERT INTO `regioncity` VALUES ('330700', '金华市', '11', '93');
INSERT INTO `regioncity` VALUES ('330800', '衢州市', '11', '94');
INSERT INTO `regioncity` VALUES ('330900', '舟山市', '11', '95');
INSERT INTO `regioncity` VALUES ('331000', '台州市', '11', '96');
INSERT INTO `regioncity` VALUES ('331100', '丽水市', '11', '97');
INSERT INTO `regioncity` VALUES ('340100', '合肥市', '12', '98');
INSERT INTO `regioncity` VALUES ('340200', '芜湖市', '12', '99');
INSERT INTO `regioncity` VALUES ('340300', '蚌埠市', '12', '100');
INSERT INTO `regioncity` VALUES ('340400', '淮南市', '12', '101');
INSERT INTO `regioncity` VALUES ('340500', '马鞍山市', '12', '102');
INSERT INTO `regioncity` VALUES ('340600', '淮北市', '12', '103');
INSERT INTO `regioncity` VALUES ('340700', '铜陵市', '12', '104');
INSERT INTO `regioncity` VALUES ('340800', '安庆市', '12', '105');
INSERT INTO `regioncity` VALUES ('341000', '黄山市', '12', '106');
INSERT INTO `regioncity` VALUES ('341100', '滁州市', '12', '107');
INSERT INTO `regioncity` VALUES ('341200', '阜阳市', '12', '108');
INSERT INTO `regioncity` VALUES ('341300', '宿州市', '12', '109');
INSERT INTO `regioncity` VALUES ('341500', '六安市', '12', '110');
INSERT INTO `regioncity` VALUES ('341600', '亳州市', '12', '111');
INSERT INTO `regioncity` VALUES ('341700', '池州市', '12', '112');
INSERT INTO `regioncity` VALUES ('341800', '宣城市', '12', '113');
INSERT INTO `regioncity` VALUES ('350100', '福州市', '13', '114');
INSERT INTO `regioncity` VALUES ('350200', '厦门市', '13', '115');
INSERT INTO `regioncity` VALUES ('350300', '莆田市', '13', '116');
INSERT INTO `regioncity` VALUES ('350400', '三明市', '13', '117');
INSERT INTO `regioncity` VALUES ('350500', '泉州市', '13', '118');
INSERT INTO `regioncity` VALUES ('350600', '漳州市', '13', '119');
INSERT INTO `regioncity` VALUES ('350700', '南平市', '13', '120');
INSERT INTO `regioncity` VALUES ('350800', '龙岩市', '13', '121');
INSERT INTO `regioncity` VALUES ('350900', '宁德市', '13', '122');
INSERT INTO `regioncity` VALUES ('360100', '南昌市', '14', '123');
INSERT INTO `regioncity` VALUES ('360200', '景德镇市', '14', '124');
INSERT INTO `regioncity` VALUES ('360300', '萍乡市', '14', '125');
INSERT INTO `regioncity` VALUES ('360400', '九江市', '14', '126');
INSERT INTO `regioncity` VALUES ('360500', '新余市', '14', '127');
INSERT INTO `regioncity` VALUES ('360600', '鹰潭市', '14', '128');
INSERT INTO `regioncity` VALUES ('360700', '赣州市', '14', '129');
INSERT INTO `regioncity` VALUES ('360800', '吉安市', '14', '130');
INSERT INTO `regioncity` VALUES ('360900', '宜春市', '14', '131');
INSERT INTO `regioncity` VALUES ('361000', '抚州市', '14', '132');
INSERT INTO `regioncity` VALUES ('361100', '上饶市', '14', '133');
INSERT INTO `regioncity` VALUES ('370100', '济南市', '15', '134');
INSERT INTO `regioncity` VALUES ('370200', '青岛市', '15', '135');
INSERT INTO `regioncity` VALUES ('370300', '淄博市', '15', '136');
INSERT INTO `regioncity` VALUES ('370400', '枣庄市', '15', '137');
INSERT INTO `regioncity` VALUES ('370500', '东营市', '15', '138');
INSERT INTO `regioncity` VALUES ('370600', '烟台市', '15', '139');
INSERT INTO `regioncity` VALUES ('370700', '潍坊市', '15', '140');
INSERT INTO `regioncity` VALUES ('370800', '济宁市', '15', '141');
INSERT INTO `regioncity` VALUES ('370900', '泰安市', '15', '142');
INSERT INTO `regioncity` VALUES ('371000', '威海市', '15', '143');
INSERT INTO `regioncity` VALUES ('371100', '日照市', '15', '144');
INSERT INTO `regioncity` VALUES ('371200', '莱芜市', '15', '145');
INSERT INTO `regioncity` VALUES ('371300', '临沂市', '15', '146');
INSERT INTO `regioncity` VALUES ('371400', '德州市', '15', '147');
INSERT INTO `regioncity` VALUES ('371500', '聊城市', '15', '148');
INSERT INTO `regioncity` VALUES ('371600', '滨州市', '15', '149');
INSERT INTO `regioncity` VALUES ('371700', '菏泽市', '15', '150');
INSERT INTO `regioncity` VALUES ('410100', '郑州市', '16', '151');
INSERT INTO `regioncity` VALUES ('410200', '开封市', '16', '152');
INSERT INTO `regioncity` VALUES ('410300', '洛阳市', '16', '153');
INSERT INTO `regioncity` VALUES ('410400', '平顶山市', '16', '154');
INSERT INTO `regioncity` VALUES ('410500', '安阳市', '16', '155');
INSERT INTO `regioncity` VALUES ('410600', '鹤壁市', '16', '156');
INSERT INTO `regioncity` VALUES ('410700', '新乡市', '16', '157');
INSERT INTO `regioncity` VALUES ('410800', '焦作市', '16', '158');
INSERT INTO `regioncity` VALUES ('410900', '濮阳市', '16', '159');
INSERT INTO `regioncity` VALUES ('411000', '许昌市', '16', '160');
INSERT INTO `regioncity` VALUES ('411100', '漯河市', '16', '161');
INSERT INTO `regioncity` VALUES ('411200', '三门峡市', '16', '162');
INSERT INTO `regioncity` VALUES ('411300', '南阳市', '16', '163');
INSERT INTO `regioncity` VALUES ('411400', '商丘市', '16', '164');
INSERT INTO `regioncity` VALUES ('411500', '信阳市', '16', '165');
INSERT INTO `regioncity` VALUES ('411600', '周口市', '16', '166');
INSERT INTO `regioncity` VALUES ('411700', '驻马店市', '16', '167');
INSERT INTO `regioncity` VALUES ('419000', '省直辖', '16', '168');
INSERT INTO `regioncity` VALUES ('420100', '武汉市', '17', '169');
INSERT INTO `regioncity` VALUES ('420200', '黄石市', '17', '170');
INSERT INTO `regioncity` VALUES ('420300', '十堰市', '17', '171');
INSERT INTO `regioncity` VALUES ('420500', '宜昌市', '17', '172');
INSERT INTO `regioncity` VALUES ('420600', '襄阳市', '17', '173');
INSERT INTO `regioncity` VALUES ('420700', '鄂州市', '17', '174');
INSERT INTO `regioncity` VALUES ('420800', '荆门市', '17', '175');
INSERT INTO `regioncity` VALUES ('420900', '孝感市', '17', '176');
INSERT INTO `regioncity` VALUES ('421000', '荆州市', '17', '177');
INSERT INTO `regioncity` VALUES ('421100', '黄冈市', '17', '178');
INSERT INTO `regioncity` VALUES ('421200', '咸宁市', '17', '179');
INSERT INTO `regioncity` VALUES ('421300', '随州市', '17', '180');
INSERT INTO `regioncity` VALUES ('422800', '恩施土家族苗族自治州', '17', '181');
INSERT INTO `regioncity` VALUES ('429000', '省直辖', '17', '182');
INSERT INTO `regioncity` VALUES ('430100', '长沙市', '18', '183');
INSERT INTO `regioncity` VALUES ('430200', '株洲市', '18', '184');
INSERT INTO `regioncity` VALUES ('430300', '湘潭市', '18', '185');
INSERT INTO `regioncity` VALUES ('430400', '衡阳市', '18', '186');
INSERT INTO `regioncity` VALUES ('430500', '邵阳市', '18', '187');
INSERT INTO `regioncity` VALUES ('430600', '岳阳市', '18', '188');
INSERT INTO `regioncity` VALUES ('430700', '常德市', '18', '189');
INSERT INTO `regioncity` VALUES ('430800', '张家界市', '18', '190');
INSERT INTO `regioncity` VALUES ('430900', '益阳市', '18', '191');
INSERT INTO `regioncity` VALUES ('431000', '郴州市', '18', '192');
INSERT INTO `regioncity` VALUES ('431100', '永州市', '18', '193');
INSERT INTO `regioncity` VALUES ('431200', '怀化市', '18', '194');
INSERT INTO `regioncity` VALUES ('431300', '娄底市', '18', '195');
INSERT INTO `regioncity` VALUES ('433100', '湘西土家族苗族自治州', '18', '196');
INSERT INTO `regioncity` VALUES ('440100', '广州市', '19', '197');
INSERT INTO `regioncity` VALUES ('440200', '韶关市', '19', '198');
INSERT INTO `regioncity` VALUES ('440300', '深圳市', '19', '199');
INSERT INTO `regioncity` VALUES ('440400', '珠海市', '19', '200');
INSERT INTO `regioncity` VALUES ('440500', '汕头市', '19', '201');
INSERT INTO `regioncity` VALUES ('440600', '佛山市', '19', '202');
INSERT INTO `regioncity` VALUES ('440700', '江门市', '19', '203');
INSERT INTO `regioncity` VALUES ('440800', '湛江市', '19', '204');
INSERT INTO `regioncity` VALUES ('440900', '茂名市', '19', '205');
INSERT INTO `regioncity` VALUES ('441200', '肇庆市', '19', '206');
INSERT INTO `regioncity` VALUES ('441300', '惠州市', '19', '207');
INSERT INTO `regioncity` VALUES ('441400', '梅州市', '19', '208');
INSERT INTO `regioncity` VALUES ('441500', '汕尾市', '19', '209');
INSERT INTO `regioncity` VALUES ('441600', '河源市', '19', '210');
INSERT INTO `regioncity` VALUES ('441700', '阳江市', '19', '211');
INSERT INTO `regioncity` VALUES ('441800', '清远市', '19', '212');
INSERT INTO `regioncity` VALUES ('441900', '东莞市', '19', '213');
INSERT INTO `regioncity` VALUES ('442000', '中山市', '19', '214');
INSERT INTO `regioncity` VALUES ('445100', '潮州市', '19', '215');
INSERT INTO `regioncity` VALUES ('445200', '揭阳市', '19', '216');
INSERT INTO `regioncity` VALUES ('445300', '云浮市', '19', '217');
INSERT INTO `regioncity` VALUES ('450100', '南宁市', '20', '218');
INSERT INTO `regioncity` VALUES ('450200', '柳州市', '20', '219');
INSERT INTO `regioncity` VALUES ('450300', '桂林市', '20', '220');
INSERT INTO `regioncity` VALUES ('450400', '梧州市', '20', '221');
INSERT INTO `regioncity` VALUES ('450500', '北海市', '20', '222');
INSERT INTO `regioncity` VALUES ('450600', '防城港市', '20', '223');
INSERT INTO `regioncity` VALUES ('450700', '钦州市', '20', '224');
INSERT INTO `regioncity` VALUES ('450800', '贵港市', '20', '225');
INSERT INTO `regioncity` VALUES ('450900', '玉林市', '20', '226');
INSERT INTO `regioncity` VALUES ('451000', '百色市', '20', '227');
INSERT INTO `regioncity` VALUES ('451100', '贺州市', '20', '228');
INSERT INTO `regioncity` VALUES ('451200', '河池市', '20', '229');
INSERT INTO `regioncity` VALUES ('451300', '来宾市', '20', '230');
INSERT INTO `regioncity` VALUES ('451400', '崇左市', '20', '231');
INSERT INTO `regioncity` VALUES ('460100', '海口市', '21', '232');
INSERT INTO `regioncity` VALUES ('460200', '三亚市', '21', '233');
INSERT INTO `regioncity` VALUES ('460300', '三沙市', '21', '234');
INSERT INTO `regioncity` VALUES ('469000', '省直辖', '21', '235');
INSERT INTO `regioncity` VALUES ('500001', '重庆市', '22', '236');
INSERT INTO `regioncity` VALUES ('510100', '成都市', '23', '237');
INSERT INTO `regioncity` VALUES ('510300', '自贡市', '23', '238');
INSERT INTO `regioncity` VALUES ('510400', '攀枝花市', '23', '239');
INSERT INTO `regioncity` VALUES ('510500', '泸州市', '23', '240');
INSERT INTO `regioncity` VALUES ('510600', '德阳市', '23', '241');
INSERT INTO `regioncity` VALUES ('510700', '绵阳市', '23', '242');
INSERT INTO `regioncity` VALUES ('510800', '广元市', '23', '243');
INSERT INTO `regioncity` VALUES ('510900', '遂宁市', '23', '244');
INSERT INTO `regioncity` VALUES ('511000', '内江市', '23', '245');
INSERT INTO `regioncity` VALUES ('511100', '乐山市', '23', '246');
INSERT INTO `regioncity` VALUES ('511300', '南充市', '23', '247');
INSERT INTO `regioncity` VALUES ('511400', '眉山市', '23', '248');
INSERT INTO `regioncity` VALUES ('511500', '宜宾市', '23', '249');
INSERT INTO `regioncity` VALUES ('511600', '广安市', '23', '250');
INSERT INTO `regioncity` VALUES ('511700', '达州市', '23', '251');
INSERT INTO `regioncity` VALUES ('511800', '雅安市', '23', '252');
INSERT INTO `regioncity` VALUES ('511900', '巴中市', '23', '253');
INSERT INTO `regioncity` VALUES ('512000', '资阳市', '23', '254');
INSERT INTO `regioncity` VALUES ('513200', '阿坝藏族羌族自治州', '23', '255');
INSERT INTO `regioncity` VALUES ('513300', '甘孜藏族自治州', '23', '256');
INSERT INTO `regioncity` VALUES ('513400', '凉山彝族自治州', '23', '257');
INSERT INTO `regioncity` VALUES ('520100', '贵阳市', '24', '258');
INSERT INTO `regioncity` VALUES ('520200', '六盘水市', '24', '259');
INSERT INTO `regioncity` VALUES ('520300', '遵义市', '24', '260');
INSERT INTO `regioncity` VALUES ('520400', '安顺市', '24', '261');
INSERT INTO `regioncity` VALUES ('522200', '铜仁市', '24', '262');
INSERT INTO `regioncity` VALUES ('522300', '黔西南布依族苗族自治州', '24', '263');
INSERT INTO `regioncity` VALUES ('522400', '毕节市', '24', '264');
INSERT INTO `regioncity` VALUES ('522600', '黔东南苗族侗族自治州', '24', '265');
INSERT INTO `regioncity` VALUES ('522700', '黔南布依族苗族自治州', '24', '266');
INSERT INTO `regioncity` VALUES ('530100', '昆明市', '25', '267');
INSERT INTO `regioncity` VALUES ('530300', '曲靖市', '25', '268');
INSERT INTO `regioncity` VALUES ('530400', '玉溪市', '25', '269');
INSERT INTO `regioncity` VALUES ('530500', '保山市', '25', '270');
INSERT INTO `regioncity` VALUES ('530600', '昭通市', '25', '271');
INSERT INTO `regioncity` VALUES ('530700', '丽江市', '25', '272');
INSERT INTO `regioncity` VALUES ('530800', '普洱市', '25', '273');
INSERT INTO `regioncity` VALUES ('530900', '临沧市', '25', '274');
INSERT INTO `regioncity` VALUES ('532300', '楚雄彝族自治州', '25', '275');
INSERT INTO `regioncity` VALUES ('532500', '红河哈尼族彝族自治州', '25', '276');
INSERT INTO `regioncity` VALUES ('532600', '文山壮族苗族自治州', '25', '277');
INSERT INTO `regioncity` VALUES ('532800', '西双版纳傣族自治州', '25', '278');
INSERT INTO `regioncity` VALUES ('532900', '大理白族自治州', '25', '279');
INSERT INTO `regioncity` VALUES ('533100', '德宏傣族景颇族自治州', '25', '280');
INSERT INTO `regioncity` VALUES ('533300', '怒江傈僳族自治州', '25', '281');
INSERT INTO `regioncity` VALUES ('533400', '迪庆藏族自治州', '25', '282');
INSERT INTO `regioncity` VALUES ('540100', '拉萨市', '26', '283');
INSERT INTO `regioncity` VALUES ('542100', '昌都地区', '26', '284');
INSERT INTO `regioncity` VALUES ('542200', '山南地区', '26', '285');
INSERT INTO `regioncity` VALUES ('542300', '日喀则地区', '26', '286');
INSERT INTO `regioncity` VALUES ('542400', '那曲地区', '26', '287');
INSERT INTO `regioncity` VALUES ('542500', '阿里地区', '26', '288');
INSERT INTO `regioncity` VALUES ('542600', '林芝地区', '26', '289');
INSERT INTO `regioncity` VALUES ('610100', '西安市', '27', '290');
INSERT INTO `regioncity` VALUES ('610200', '铜川市', '27', '291');
INSERT INTO `regioncity` VALUES ('610300', '宝鸡市', '27', '292');
INSERT INTO `regioncity` VALUES ('610400', '咸阳市', '27', '293');
INSERT INTO `regioncity` VALUES ('610500', '渭南市', '27', '294');
INSERT INTO `regioncity` VALUES ('610600', '延安市', '27', '295');
INSERT INTO `regioncity` VALUES ('610700', '汉中市', '27', '296');
INSERT INTO `regioncity` VALUES ('610800', '榆林市', '27', '297');
INSERT INTO `regioncity` VALUES ('610900', '安康市', '27', '298');
INSERT INTO `regioncity` VALUES ('611000', '商洛市', '27', '299');
INSERT INTO `regioncity` VALUES ('620100', '兰州市', '28', '300');
INSERT INTO `regioncity` VALUES ('620200', '嘉峪关市', '28', '301');
INSERT INTO `regioncity` VALUES ('620300', '金昌市', '28', '302');
INSERT INTO `regioncity` VALUES ('620400', '白银市', '28', '303');
INSERT INTO `regioncity` VALUES ('620500', '天水市', '28', '304');
INSERT INTO `regioncity` VALUES ('620600', '武威市', '28', '305');
INSERT INTO `regioncity` VALUES ('620700', '张掖市', '28', '306');
INSERT INTO `regioncity` VALUES ('620800', '平凉市', '28', '307');
INSERT INTO `regioncity` VALUES ('620900', '酒泉市', '28', '308');
INSERT INTO `regioncity` VALUES ('621000', '庆阳市', '28', '309');
INSERT INTO `regioncity` VALUES ('621100', '定西市', '28', '310');
INSERT INTO `regioncity` VALUES ('621200', '陇南市', '28', '311');
INSERT INTO `regioncity` VALUES ('622900', '临夏回族自治州', '28', '312');
INSERT INTO `regioncity` VALUES ('623000', '甘南藏族自治州', '28', '313');
INSERT INTO `regioncity` VALUES ('630100', '西宁市', '29', '314');
INSERT INTO `regioncity` VALUES ('632100', '海东地区', '29', '315');
INSERT INTO `regioncity` VALUES ('632200', '海北藏族自治州', '29', '316');
INSERT INTO `regioncity` VALUES ('632300', '黄南藏族自治州', '29', '317');
INSERT INTO `regioncity` VALUES ('632500', '海南藏族自治州', '29', '318');
INSERT INTO `regioncity` VALUES ('632600', '果洛藏族自治州', '29', '319');
INSERT INTO `regioncity` VALUES ('632700', '玉树藏族自治州', '29', '320');
INSERT INTO `regioncity` VALUES ('632800', '海西蒙古族藏族自治州', '29', '321');
INSERT INTO `regioncity` VALUES ('640100', '银川市', '30', '322');
INSERT INTO `regioncity` VALUES ('640200', '石嘴山市', '30', '323');
INSERT INTO `regioncity` VALUES ('640300', '吴忠市', '30', '324');
INSERT INTO `regioncity` VALUES ('640400', '固原市', '30', '325');
INSERT INTO `regioncity` VALUES ('640500', '中卫市', '30', '326');
INSERT INTO `regioncity` VALUES ('650100', '乌鲁木齐市', '31', '327');
INSERT INTO `regioncity` VALUES ('650200', '克拉玛依市', '31', '328');
INSERT INTO `regioncity` VALUES ('652100', '吐鲁番地区', '31', '329');
INSERT INTO `regioncity` VALUES ('652200', '哈密地区', '31', '330');
INSERT INTO `regioncity` VALUES ('652300', '昌吉回族自治州', '31', '331');
INSERT INTO `regioncity` VALUES ('652700', '博尔塔拉蒙古自治州', '31', '332');
INSERT INTO `regioncity` VALUES ('652800', '巴音郭楞蒙古自治州', '31', '333');
INSERT INTO `regioncity` VALUES ('652900', '阿克苏地区', '31', '334');
INSERT INTO `regioncity` VALUES ('653000', '克孜勒苏柯尔克孜自治州', '31', '335');
INSERT INTO `regioncity` VALUES ('653100', '喀什地区', '31', '336');
INSERT INTO `regioncity` VALUES ('653200', '和田地区', '31', '337');
INSERT INTO `regioncity` VALUES ('654000', '伊犁哈萨克自治州', '31', '338');
INSERT INTO `regioncity` VALUES ('654200', '塔城地区', '31', '339');
INSERT INTO `regioncity` VALUES ('654300', '阿勒泰地区', '31', '340');
INSERT INTO `regioncity` VALUES ('659000', '自治区直辖', '31', '341');
INSERT INTO `regioncity` VALUES ('710100', '台北市', '32', '342');
INSERT INTO `regioncity` VALUES ('710200', '高雄市', '32', '343');
INSERT INTO `regioncity` VALUES ('710300', '基隆市', '32', '344');
INSERT INTO `regioncity` VALUES ('710400', '台中市', '32', '345');
INSERT INTO `regioncity` VALUES ('710500', '台南市', '32', '346');
INSERT INTO `regioncity` VALUES ('710600', '新竹市', '32', '347');
INSERT INTO `regioncity` VALUES ('710700', '嘉义市', '32', '348');
INSERT INTO `regioncity` VALUES ('719000', '省直辖', '32', '349');
INSERT INTO `regioncity` VALUES ('810100', '香港岛', '33', '350');
INSERT INTO `regioncity` VALUES ('810200', '九龙', '33', '351');
INSERT INTO `regioncity` VALUES ('810300', '新界', '33', '352');
INSERT INTO `regioncity` VALUES ('820100', '澳门半岛', '34', '353');
INSERT INTO `regioncity` VALUES ('820200', '澳门离岛', '34', '354');
INSERT INTO `regioncity` VALUES ('820300', '无堂区划分区域', '34', '355');

-- ----------------------------
-- Table structure for regionprovince
-- ----------------------------
DROP TABLE IF EXISTS `regionprovince`;
CREATE TABLE `regionprovince` (
  `province_code` int(11) DEFAULT NULL,
  `province_name` varchar(50) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_regionprovince_province_name` (`province_name`),
  UNIQUE KEY `ix_regionprovince_province_code` (`province_code`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of regionprovince
-- ----------------------------
INSERT INTO `regionprovince` VALUES ('110000', '北京', null, '1');
INSERT INTO `regionprovince` VALUES ('120000', '天津', null, '2');
INSERT INTO `regionprovince` VALUES ('130000', '河北省', null, '3');
INSERT INTO `regionprovince` VALUES ('140000', '山西省', null, '4');
INSERT INTO `regionprovince` VALUES ('150000', '内蒙古自治区', null, '5');
INSERT INTO `regionprovince` VALUES ('210000', '辽宁省', null, '6');
INSERT INTO `regionprovince` VALUES ('220000', '吉林省', null, '7');
INSERT INTO `regionprovince` VALUES ('230000', '黑龙江省', null, '8');
INSERT INTO `regionprovince` VALUES ('310000', '上海', null, '9');
INSERT INTO `regionprovince` VALUES ('320000', '江苏省', null, '10');
INSERT INTO `regionprovince` VALUES ('330000', '浙江省', null, '11');
INSERT INTO `regionprovince` VALUES ('340000', '安徽省', null, '12');
INSERT INTO `regionprovince` VALUES ('350000', '福建省', null, '13');
INSERT INTO `regionprovince` VALUES ('360000', '江西省', null, '14');
INSERT INTO `regionprovince` VALUES ('370000', '山东省', null, '15');
INSERT INTO `regionprovince` VALUES ('410000', '河南省', null, '16');
INSERT INTO `regionprovince` VALUES ('420000', '湖北省', null, '17');
INSERT INTO `regionprovince` VALUES ('430000', '湖南省', null, '18');
INSERT INTO `regionprovince` VALUES ('440000', '广东省', null, '19');
INSERT INTO `regionprovince` VALUES ('450000', '广西壮族自治区', null, '20');
INSERT INTO `regionprovince` VALUES ('460000', '海南省', null, '21');
INSERT INTO `regionprovince` VALUES ('500000', '重庆', null, '22');
INSERT INTO `regionprovince` VALUES ('510000', '四川省', null, '23');
INSERT INTO `regionprovince` VALUES ('520000', '贵州省', null, '24');
INSERT INTO `regionprovince` VALUES ('530000', '云南省', null, '25');
INSERT INTO `regionprovince` VALUES ('540000', '西藏自治区', null, '26');
INSERT INTO `regionprovince` VALUES ('610000', '陕西省', null, '27');
INSERT INTO `regionprovince` VALUES ('620000', '甘肃省', null, '28');
INSERT INTO `regionprovince` VALUES ('630000', '青海省', null, '29');
INSERT INTO `regionprovince` VALUES ('640000', '宁夏回族自治区', null, '30');
INSERT INTO `regionprovince` VALUES ('650000', '新疆维吾尔自治区', null, '31');
INSERT INTO `regionprovince` VALUES ('710000', '台湾省', null, '32');
INSERT INTO `regionprovince` VALUES ('810000', '香港特别行政区', null, '33');
INSERT INTO `regionprovince` VALUES ('820000', '澳门特别行政区', null, '34');

-- ----------------------------
-- Table structure for resourcetype
-- ----------------------------
DROP TABLE IF EXISTS `resourcetype`;
CREATE TABLE `resourcetype` (
  `resourcetype_name` varchar(10) DEFAULT NULL,
  `resourcetype_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resourcetype
-- ----------------------------
INSERT INTO `resourcetype` VALUES ('菜单', '1', '1');
INSERT INTO `resourcetype` VALUES ('功能', '2', '2');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `name` varchar(80) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `reserve` tinyint(1) DEFAULT NULL,
  `relative_usergroup` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------

-- ----------------------------
-- Table structure for rolecategory
-- ----------------------------
DROP TABLE IF EXISTS `rolecategory`;
CREATE TABLE `rolecategory` (
  `name` varchar(30) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `number_of_roles` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rolecategory_idx` (`parent`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rolecategory
-- ----------------------------

-- ----------------------------
-- Table structure for role_perm_rel
-- ----------------------------
DROP TABLE IF EXISTS `role_perm_rel`;
CREATE TABLE `role_perm_rel` (
  `role` int(11) DEFAULT NULL,
  `permission` int(11) DEFAULT NULL,
  `props` blob,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_perm_rel
-- ----------------------------

-- ----------------------------
-- Table structure for role_usergroup_usergroups
-- ----------------------------
DROP TABLE IF EXISTS `role_usergroup_usergroups`;
CREATE TABLE `role_usergroup_usergroups` (
  `role_id` int(11) DEFAULT NULL,
  `usergroup_id` int(11) DEFAULT NULL,
  UNIQUE KEY `role_usergroup_usergroups_mindx` (`role_id`,`usergroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_usergroup_usergroups
-- ----------------------------

-- ----------------------------
-- Table structure for role_user_users
-- ----------------------------
DROP TABLE IF EXISTS `role_user_users`;
CREATE TABLE `role_user_users` (
  `role_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  UNIQUE KEY `role_user_users_mindx` (`role_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_user_users
-- ----------------------------

-- ----------------------------
-- Table structure for server
-- ----------------------------
DROP TABLE IF EXISTS `server`;
CREATE TABLE `server` (
  `rackspace_id` int(11) DEFAULT NULL,
  `hardware_id` int(11) DEFAULT NULL,
  `server_sn` varchar(100) DEFAULT NULL,
  `server_height` int(11) DEFAULT NULL,
  `server_asset_tag` varchar(200) DEFAULT NULL,
  `server_cdrom` int(11) DEFAULT NULL,
  `server_raidtype` varchar(200) DEFAULT NULL,
  `server_buydate` datetime DEFAULT NULL,
  `hardwarewarranty_id` int(11) DEFAULT NULL,
  `server_warranty_datetime` datetime DEFAULT NULL,
  `server_price` varchar(200) DEFAULT NULL,
  `server_purchasing_director` varchar(200) DEFAULT NULL,
  `server_buy_datetime` datetime DEFAULT NULL,
  `hardwarestatus_id` int(11) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_server_server_asset_tag` (`server_asset_tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of server
-- ----------------------------

-- ----------------------------
-- Table structure for servercpu
-- ----------------------------
DROP TABLE IF EXISTS `servercpu`;
CREATE TABLE `servercpu` (
  `server_id` int(11) DEFAULT NULL,
  `servercpu_type` varchar(100) DEFAULT NULL,
  `servercpu_hz` int(11) DEFAULT NULL,
  `servercpu_num` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of servercpu
-- ----------------------------

-- ----------------------------
-- Table structure for serverdisk
-- ----------------------------
DROP TABLE IF EXISTS `serverdisk`;
CREATE TABLE `serverdisk` (
  `server_id` int(11) DEFAULT NULL,
  `serverdisk_type` varchar(100) DEFAULT NULL,
  `serverdisk_speed` int(11) DEFAULT NULL,
  `serverdisk_size` int(11) DEFAULT NULL,
  `serverdisk_num` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of serverdisk
-- ----------------------------

-- ----------------------------
-- Table structure for serverethernet
-- ----------------------------
DROP TABLE IF EXISTS `serverethernet`;
CREATE TABLE `serverethernet` (
  `server_id` int(11) DEFAULT NULL,
  `servereth_type` varchar(100) DEFAULT NULL,
  `servereth_num` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of serverethernet
-- ----------------------------

-- ----------------------------
-- Table structure for serverethernetip
-- ----------------------------
DROP TABLE IF EXISTS `serverethernetip`;
CREATE TABLE `serverethernetip` (
  `server_id` int(11) DEFAULT NULL,
  `ethernet_type` int(11) DEFAULT NULL,
  `ethernet_physical_interface` int(11) DEFAULT NULL,
  `ethernet_dev_name` varchar(20) DEFAULT NULL,
  `ethernet_mac` varchar(128) DEFAULT NULL,
  `ethernet_ip` varchar(128) DEFAULT NULL,
  `ethernet_mip` varchar(128) DEFAULT NULL,
  `switch_id` int(11) DEFAULT NULL,
  `switchport_id_port` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of serverethernetip
-- ----------------------------

-- ----------------------------
-- Table structure for servermem
-- ----------------------------
DROP TABLE IF EXISTS `servermem`;
CREATE TABLE `servermem` (
  `server_id` int(11) DEFAULT NULL,
  `servermem_type` varchar(100) DEFAULT NULL,
  `servermem_hz` int(11) DEFAULT NULL,
  `servermem_size` int(11) DEFAULT NULL,
  `servermem_num` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of servermem
-- ----------------------------

-- ----------------------------
-- Table structure for serverpower
-- ----------------------------
DROP TABLE IF EXISTS `serverpower`;
CREATE TABLE `serverpower` (
  `server_id` int(11) DEFAULT NULL,
  `serverpower_type` varchar(100) DEFAULT NULL,
  `serverpower_kw` int(11) DEFAULT NULL,
  `serverpower_num` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of serverpower
-- ----------------------------

-- ----------------------------
-- Table structure for serversystem
-- ----------------------------
DROP TABLE IF EXISTS `serversystem`;
CREATE TABLE `serversystem` (
  `server_id` int(11) DEFAULT NULL,
  `os_id` int(11) DEFAULT NULL,
  `system_hostname` varchar(200) DEFAULT NULL,
  `system_raid` varchar(50) DEFAULT NULL,
  `system_partition` text,
  `department_id` int(11) DEFAULT NULL,
  `opersgroup_id` int(11) DEFAULT NULL,
  `opersgroupproject_id` int(11) DEFAULT NULL,
  `applicationtype_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `ix_serversystem_server_id` (`server_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of serversystem
-- ----------------------------

-- ----------------------------
-- Table structure for switch
-- ----------------------------
DROP TABLE IF EXISTS `switch`;
CREATE TABLE `switch` (
  `switch_aliasname` varchar(50) DEFAULT NULL,
  `hardware_id` int(11) DEFAULT NULL,
  `computerroom_id` int(11) DEFAULT NULL,
  `switch_sn` varchar(100) DEFAULT NULL,
  `switch_asset_tag` varchar(100) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hardwarestatus_id` int(11) DEFAULT NULL,
  `rackspace_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_switch_switch_aliasname` (`switch_aliasname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of switch
-- ----------------------------

-- ----------------------------
-- Table structure for switchip
-- ----------------------------
DROP TABLE IF EXISTS `switchip`;
CREATE TABLE `switchip` (
  `switch_ip_vlan` int(11) DEFAULT NULL,
  `switch_id` int(11) DEFAULT NULL,
  `switch_ip_addr` varchar(128) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of switchip
-- ----------------------------

-- ----------------------------
-- Table structure for switchport
-- ----------------------------
DROP TABLE IF EXISTS `switchport`;
CREATE TABLE `switchport` (
  `switch_port_num` int(11) DEFAULT NULL,
  `switch_id` int(11) DEFAULT NULL,
  `server_id` int(11) DEFAULT NULL,
  `switch_port_status` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of switchport
-- ----------------------------

-- ----------------------------
-- Table structure for tables
-- ----------------------------
DROP TABLE IF EXISTS `tables`;
CREATE TABLE `tables` (
  `table_name` varchar(40) DEFAULT NULL,
  `verbose_name` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tables
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `username` varchar(30) NOT NULL,
  `nickname` varchar(30) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `is_superuser` tinyint(1) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `date_join` datetime DEFAULT NULL,
  `image` varchar(256) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `locked` tinyint(1) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_user_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('admin', '管理员', 'admin@admin.com', 'sha1$d15c4$d5d28c421038e0d0811ea4af663fd6b73f277a0a', '1', '2014-04-03 14:10:18', '2014-03-03 12:00:33', '', '1', '0', '1', '', '1');
INSERT INTO `user` VALUES ('sftang', 'sadasd', '', 'sha1$65838$9d073e53b3c36bf937246372c0b32d4146c3e7b7', '0', '2014-03-28 10:37:22', '2014-03-03 12:50:11', '', '0', '0', '1', '1222', '2');
INSERT INTO `user` VALUES ('t1', 't1', '', 'sha1$c7269$c86995631042bf1739fd83e18b328d0bf3050e6c', '0', '2014-03-21 10:35:53', '2014-03-17 14:03:10', '', '0', '0', '1', '', '5');
INSERT INTO `user` VALUES ('t2', 't2', '', 'sha1$c43d4$d0cce9279c4a9c9ac98de09cc8f109ecffb18b3e', '0', '2014-03-18 10:20:53', '2014-03-17 14:03:21', '', '0', '0', '1', '', '6');
INSERT INTO `user` VALUES ('t3', 't3', '', 'sha1$db3dc$70be5a2075f66769b6cd62575563b790a059902c', '0', '2014-03-31 11:56:19', '2014-03-17 14:03:32', '', '0', '0', '1', '', '7');
INSERT INTO `user` VALUES ('t4', 't4', '', 'sha1$ab23d$2c7d525796e8ec52196c8f62991223fff943638c', '0', '2014-03-18 10:20:32', '2014-03-17 14:03:43', '', '0', '0', '1', '', '8');
INSERT INTO `user` VALUES ('test', '测试用户', '', 'sha1$75725$cf475ecf46268c8d2f5f940fbe33fdb456f78934', '0', null, '2014-03-31 16:12:29', '', '0', '0', '1', '', '9');

-- ----------------------------
-- Table structure for usergroup
-- ----------------------------
DROP TABLE IF EXISTS `usergroup`;
CREATE TABLE `usergroup` (
  `name` varchar(30) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `number_of_children` int(11) DEFAULT NULL,
  `number_of_people` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usergroup_idx` (`parent`,`name`),
  KEY `ix_usergroup_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usergroup
-- ----------------------------

-- ----------------------------
-- Table structure for usergroup_user_users
-- ----------------------------
DROP TABLE IF EXISTS `usergroup_user_users`;
CREATE TABLE `usergroup_user_users` (
  `usergroup_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  UNIQUE KEY `usergroup_user_users_mindx` (`usergroup_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usergroup_user_users
-- ----------------------------

-- ----------------------------
-- View structure for view_region
-- ----------------------------
DROP VIEW IF EXISTS `view_region`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_region` AS select `regionprovince`.`province_name` AS `province_name`,`regioncity`.`city_name` AS `city_name`,`regionarea`.`area_name` AS `area_name`,`regionprovince`.`province_code` AS `province_code`,`regioncity`.`city_code` AS `city_code`,`regionarea`.`area_code` AS `area_code`,`regionarea`.`id` AS `id` from ((`regionprovince` join `regioncity`) join `regionarea`) where ((`regioncity`.`province_id` = `regionprovince`.`id`) and (`regionarea`.`city_id` = `regioncity`.`id`)) ; ;

-- ----------------------------
-- View structure for view_switch
-- ----------------------------
DROP VIEW IF EXISTS `view_switch`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_switch` AS select `switch`.`switch_aliasname` AS `switch_aliasname`,`computerroom`.`cr_name` AS `cr_name`,`hardware`.`hardware_manufacturer` AS `hardware_manufacturer`,`hardware`.`hardware_name` AS `hardware_name`,`switch`.`switch_sn` AS `switch_sn`,`switch`.`switch_asset_tag` AS `switch_asset_tag`,`switch`.`id` AS `id` from ((`hardware` join `switch`) join `computerroom`) where ((`computerroom`.`id` = `switch`.`computerroom_id`) and (`hardware`.`id` = `switch`.`hardware_id`)) ;
