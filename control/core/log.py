#coding=utf-8

import logging
import logging.config
import os

#
#if __name__ == '__main__':
#    import time
#
#    logging.config.fileConfig("./../logger.conf")
#    logger = logging.getLogger()
#
#    while True:
#        import logging.handlers
#        logger.debug("debug msg")
#        logger.error("error msg")
#        logger.critical("critical msg")
#        logger.warning('warn msg')
#        logger.info('info msg')
#        time.sleep(5)

def getLogger(name = "root"):
    """
    获得Logger对象
    """
    pwd = os.path.abspath(os.path.dirname(__file__))
    
    logging.config.fileConfig(os.path.join(pwd,"logger.conf"))

    #logging.config.fileConfig("logger.conf")
    
    return logging.getLogger(name)

#logger = getLogger()    #logger变量


