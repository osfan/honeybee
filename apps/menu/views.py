#coding=utf-8
from uliweb import expose

#可以省略直接引用
from uliweb import request, error, redirect
from sqlalchemy.sql import or_

def __begin__():
    """
    用户验证 权限验证
    """
    from uliweb import functions
    functions.require_login()
    return functions.has_role()
 
@expose('/menu')
class Menu(object):

    
    def __init__(self):
        from uliweb.orm import get_model
        self.menumodel = get_model('menu')

    def __begin__(self):
        if not request.user.is_superuser:
            error('你不是超级用户不能进行这项操作！')        
        

    def index(self):
        return {}

    def _get_menu(self, id):
        menu = self.menumodel.get(int(id))
        if not menu:
            error('没找到这条记录')
        return menu

    def _get_menugraph(self, id):
        menu = self.menugraphmodel.get(int(id))
        if not menu:
            error('没找到这条记录')
        return menu    
    
    def menu(self):
        """
        菜单管理首页
        """
        return {}
    
    def menuedit(self):
        """
        菜单编辑
        """
        return {}    
    
    def menuadd(self):
        """
        菜单添加
        """
        return {} 
    
    #def menusave(self):
        #"""
        #保存编辑菜单
        #"""
        #menuid = request.POST.get("menuid")
        #menuname = request.POST.get("menuname")
        #menuurl = request.POST.get("menuurl")
        #pid = request.POST.get("pid")
        
        #try:
            ##如果查询不到id，那么执行异常的保存
            ##menu = self.menumodel.get(int(list(self.menumodel.filter(self.menumodel.c.id == menuid).limit(1).values(self.menumodel.c.id))[0][0]))
            #menu = self.menumodel.get(int(menuid))
            ##update操作
            #menu.menuname=menuname
            #menu.menuurl=menuurl
        #except:
            ##insert操作
            #menu = self.menumodel(menuname=menuname, \
                          #menuurl=menuurl, \
                          #pid=pid
                          #)
        #menu.save()
        #return redirect(url_for(Menu.index))
        
    def menusave(self):
        """
        保存编辑菜单
        """
        menuid = request.POST.get("id")
        menuname = request.POST.get("menuname")
        menuurl = request.POST.get("menuurl")
        pid = request.POST.get("pid")
        menuicon = request.POST.get("menuicon")
        menuseq = request.POST.get("menuseq")
        
        try:
            menupath = list(self.menumodel.filter(self.menumodel.c.id == pid).limit(1).values(self.menumodel.c.menupath))[0][0]
            menupath=menupath + '-' + str(pid)
        except:
            #说明菜单是顶级菜单
            menupath = "0"

        if len(pid) == 0:
            pid = 0
            
        if menuid == None:
            menu = self.menumodel(menuname=menuname,
                          menuurl=menuurl,
                          pid=pid,
                          menuicon=menuicon,
                          menuseq=menuseq,
                          menupath=menupath
                          )
            
        else:
            menu = self.menumodel.get(int(menuid))
            #update操作
            menu.menuname=menuname
            menu.menuurl=menuurl
            menu.pid=pid
            menu.menuicon=menuicon
            menu.menuseq=menuseq
            menu.menupath=menupath

        
        try:
            menu.save()
            return json({"success":True,"msg":"添加成功"})
            
        except:
            return json({"error":500,"msg":"已经有同名存在"})
    
    #def menudel(self):
        #"""
        #删除菜单
        #"""
        ##一次删除所有数据
        #self.menumodel.filter(or_(self.menumodel.c.pid == id, self.menumodel.c.id == id)).remove()

        #return redirect(url_for(Menu.index))
        
    def menudel(self):
        """
        删除菜单
        """
        
        _id = request.POST.get('id')
        
        #一次删除所有数据
        #self.menumodel.filter(or_(self.menumodel.c.pid == id, self.menumodel.c.id == id)).remove()

        self.menumodel.filter(or_(self.menumodel.c.pid == _id,self.menumodel.c.id == _id)).remove()
        
        return json({'success':True,'msg':'删除成功','obj':_id})
        