#coding=utf-8
from uliweb import expose, functions, json

#import datetime

def __begin__():
    """
    用户验证 权限验证
    """
    from uliweb import functions
    functions.require_login()
    return functions.has_role()

@expose('/operation')
class Operation(object):
    
    def __init__(self):
        from uliweb.orm import get_model
        self.departmentmodel = get_model('department')
        self.opersgroupmodel = get_model('opersgroup')
        self.opersgroupprojectmodel = get_model('opersgroupproject')
        self.applicationtypemodel = get_model('applicationtype')

    def _get_department(self, id):
        data = self.departmentmodel.get(int(id))
        if not data:
            return False
        return data 

    def _get_opersgroup(self, id):
        data = self.opersgroupmodel.get(int(id))
        if not data:
            return False
        return data 

    def _get_opersgroupproject(self, id):
        data = self.opersgroupprojectmodel.get(int(id))
        if not data:
            return False
        return data     
    
    def _get_applicationtype(self, id):
        _data = self.applicationtypemodel.get(int(id))
        if not _data:
            error('没找到这条记录')
        return _data

    def department(self):
        """
        部门信息
        """
        return {}    

    def departmentadd(self):
        """
        添加部门信息
        """
        return {}


    def departmentedit(self):
        """
        编辑部门信息
        """
        _id = request.GET.get('id')

        return {}        


    def departmentsave(self):
        """
        保存部门信息
        """
        _id = request.POST.get('id')
        department_name= request.POST.get('department_name')
        comment = request.POST.get('comment')

        if _id == None:
            ormdata = self.departmentmodel(department_name=department_name,
                                           comment=comment
                                           )
        else:
            ormdata = self._get_department(_id)

            ormdata.department_name=department_name
            ormdata.comment=comment

        try:
            ormdata.save()

            return json({"success":200,"msg":"添加成功"})
        except:
            return json({"error":500,"msg":"机房已经存在"})

    def departmentdel(self):
        """
        删除部门信息
        """
        _id = request.POST.get('id')

        mid = []
        for i in _id.split(','):
            mid.append(int(i))

        #一次删除多个
        self.departmentmodel.filter(self.departmentmodel.c.id.in_(mid)).remove()

        d = {"status":"200","msg":"删除成功"}
        return json(d)

    def opersgroup(self):
        """
        运营组信息
        """
        return {}    

    def opersgroupadd(self):
        """
        添加运营组信息
        """
        return {}


    def opersgroupedit(self):
        """
        编辑运营组信息
        """
        _id = request.GET.get('id')
        
        ormdata = self._get_opersgroup(_id)

        return {'department_name':ormdata.department_id}        


    def opersgroupsave(self):
        """
        保存运营组信息
        """
        _id = request.POST.get('id')
        opersgroup_name= request.POST.get('opersgroup_name')
        department_id= request.POST.get('department_id')
        comment = request.POST.get('comment')

        if _id == None:
            ormdata = self.opersgroupmodel(opersgroup_name=opersgroup_name,
                                                  department_id=department_id,
                                                  comment=comment
                                                  )
        else:
            ormdata = self._get_opersgroup(_id)

            ormdata.opersgroup_name=opersgroup_name
            ormdata.department_id=department_id
            ormdata.comment=comment

        try:
            ormdata.save()

            return json({"success":200,"msg":"添加成功"})
        except:
            return json({"error":500,"msg":"运营组已经存在"})

    def opersgroupdel(self):
        """
        删除运营组信息
        """
        _id = request.POST.get('id')

        mid = []
        for i in _id.split(','):
            mid.append(int(i))

        #一次删除多个
        self.opersgroupmodel.filter(self.opersgroupmodel.c.id.in_(mid)).remove()

        d = {"status":"200","msg":"删除成功"}
        return json(d)

    def opersgroupproject(self):
        """
        运营组项目信息
        """
        return {}    

    def opersgroupprojectadd(self):
        """
        添加运营组项目信息
        """
        return {}


    def opersgroupprojectedit(self):
        """
        编辑运营组项目信息
        """
        _id = request.GET.get('id')
        
        ormdata = self._get_opersgroupproject(_id)

        return {'department_id':ormdata.opersgroup_id.department_id,
                'opersgroup_id':ormdata.opersgroup_id,
                'ogproject_user':ormdata.ogproject_user,
                'ogproject_opuser':ormdata.ogproject_opuser
                }        


    def opersgroupprojectsave(self):
        """
        保存运营组项目信息
        """
        _id = request.POST.get('id')
        opersgroup_id= request.POST.get('opersgroup_id')
        ogproject_name= request.POST.get('ogproject_name')
        ogproject_user= request.POST.get('ogproject_user')
        ogproject_opuser= request.POST.get('ogproject_opuser')
        comment = request.POST.get('comment')

        #修正修改运维负责人和项目负责人数据为None
        if ogproject_user == 'None' or ogproject_opuser == 'None':
            return json({"error":500,"msg":"负责人项数据错误，请修正"})

        if _id == None:
            ormdata = self.opersgroupprojectmodel(ogproject_name=ogproject_name,
                                                  opersgroup_id=opersgroup_id,
                                                  ogproject_user=ogproject_user,
                                                  ogproject_opuser=ogproject_opuser,
                                                  comment=comment
                                                  )
        else:
            
            ormdata = self._get_opersgroupproject(_id)

            ormdata.opersgroup_id=opersgroup_id
            ormdata.ogproject_name=ogproject_name
            ormdata.ogproject_user=ogproject_user
            ormdata.ogproject_opuser=ogproject_opuser
            ormdata.comment=comment

        try:
            ormdata.save()

            return json({"success":200,"msg":"添加成功"})
        except:
            return json({"error":500,"msg":"项目已经存在"})

    def opersgroupprojectdel(self):
        """
        删除运营组项目信息
        """
        _id = request.POST.get('id')

        mid = []
        for i in _id.split(','):
            mid.append(int(i))

        #一次删除多个
        self.opersgroupprojectmodel.filter(self.opersgroupprojectmodel.c.id.in_(mid)).remove()

        d = {"status":"200","msg":"删除成功"}
        return json(d)
    
    def applicationtype(self):
        """
        应用主题
        """
        return {}    

    def applicationtypeadd(self):
        """
        添加应用主题
        """
        return {}

    def applicationtypeedit(self):
        """
        编辑应用主题
        """
        _id = request.GET.get('id')
        
        #ormdata = self._get_applicationtype(id)
        ormdata = functions.get_object(self.applicationtypemodel, _id, cache=True, use_local=True)
        
        return {'opersgroupproject_id':ormdata.opersgroupproject_id,
                'opersgroup_id':ormdata.opersgroupproject_id.opersgroup_id,
                'department_id':ormdata.opersgroupproject_id.opersgroup_id.department_id
                }

    def applicationtypesave(self):
        """
        保存应用主题
        """
        _id = request.POST.get('id')
        applicationtype_name= request.POST.get('applicationtype_name')
        opersgroupproject_id= request.POST.get('opersgroupproject_id')
        applicationtype_dep_env= request.POST.get('applicationtype_dep_env')
        comment = request.POST.get('comment')

        if _id == None:
            ormdata = self.applicationtypemodel(applicationtype_name=applicationtype_name,
                                                opersgroupproject_id=opersgroupproject_id,
                                                applicationtype_dep_env=applicationtype_dep_env,
                                               comment=comment
                                               )
        else:
            #ormdata = self._get_applicationtype(_id)
            ormdata = functions.get_object(self.applicationtypemodel, _id, cache=True, use_local=True)
            
            ormdata.applicationtype_name=applicationtype_name
            ormdata.opersgroupproject_id=opersgroupproject_id
            ormdata.applicationtype_dep_env=applicationtype_dep_env
            ormdata.comment=comment

        try:
            ormdata.save()

            return json({"success":200,"msg":"添加成功"})
        except:
            return json({"error":500,"msg":"应用主题已经存在"})

    def applicationtypedel(self):
        """
        删除应用主题
        """
        _id = request.POST.get('id')

        mid = []
        for i in _id.split(','):
            mid.append(int(i))

        #一次删除多个
        self.applicationtypemodel.filter(self.applicationtypemodel.c.id.in_(mid)).remove()

        d = {"status":"200","msg":"删除成功"}
        return json(d)
    
        