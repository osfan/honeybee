#coding=utf-8
from uliweb.orm import *
import datetime


class Region(Model):
    """
    区域分布信息
    """
    #region_code = Field(int, verbose_name='区域代码,自定义', required=True, unique=True, index=True)
    region_name = Field(str, verbose_name='地区名字', max_length=50, required=True, unique=True, index=True)

    
class RegionProvince(Model):
    """
    区域省份信息
    """ 

    province_code = Field(int, verbose_name='行政代码', required=True, unique=True, index=True)
    province_name = Field(str, verbose_name='省份名字',max_length=50, required=True, unique=True, index=True)
    region_id = Reference('region')

class RegionCity(Model):
    """
    地区市
    """ 

    city_code = Field(int, verbose_name='行政代码', required=True, index=True)
    city_name = Field(str, verbose_name='市名字，存在同名不同省的地区',max_length=50, required=True, index=True)
    province_id = Reference('regionprovince')
    @classmethod
    def OnInit(cls):
        #建立联合索引
        Index('ix_code_name', cls.c.city_code, cls.c.city_name, unique=True)     

class RegionArea(Model):
    """
    地区
    """ 

    area_code = Field(int, verbose_name='行政代码', required=True, index=True)
    area_name = Field(str, verbose_name='地区名字，存在同名不同省的地区',max_length=50, required=True, index=True)
    city_id = Reference('regioncity')
    @classmethod
    def OnInit(cls):
        #建立联合索引
        Index('ix_code_name', cls.c.area_code, cls.c.area_name, unique=True)      
    
    
class View_Region(Model):
    """
    试图 建立区域信息试图
    """ 
    #__without_id__ = True

    province_code = Field(int, verbose_name='行政代码')
    province_name = Field(str, verbose_name='省份名字',max_length=50)
    city_code = Field(int, verbose_name='行政代码')
    city_name = Field(str, verbose_name='市名字',max_length=50)
    area_code = Field(int, verbose_name='行政代码')
    area_name = Field(str, verbose_name='地区名字',max_length=50)
        
